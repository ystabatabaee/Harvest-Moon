import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class VillageUI implements Mover {
    Group root = new Group();
    static double tileSize = 25;

    private static Rectangle market;
    private static Rectangle clinic;
    private static Rectangle lab;
    private static Rectangle workshop;
    private static Rectangle cafe;
    private static Rectangle gym;
    private static Rectangle ranch;

    public VillageUI(Group root) {
        this.root = root;
    }

    private Rectangle rectangleBuilder(double width, double height, Paint fill, double setX, double setY) {
        Rectangle rectangle = new Rectangle(width, height, fill);
        rectangle.setX(setX);
        rectangle.setY(setY);
        return rectangle;
    }


    public Scene sceneBuilder(Stage primaryStage) {
        Image villagePic = new Image("pics/maps/village-winter.png");
        Scene villageScene = new Scene(root, 1000, 700, new ImagePattern(villagePic));
        Image ranchPic = new Image("pics/buildings/ranch.png");
        Image cafePic = new Image("pics/buildings/cafe.png");
        Image clinicPic = new Image("pics/buildings/clinic.png");
        Image marketPic = new Image("pics/buildings/market.png");
        Image labPic = new Image("pics/buildings/lab.png");
        Image workshopPic = new Image("pics/buildings/workshop.png");
        Image gymPic = new Image("pics/buildings/gym.png");

        clinic = rectangleBuilder(tileSize * 5, tileSize * 4, new ImagePattern(clinicPic), tileSize, tileSize);
        market = rectangleBuilder(tileSize * 8, tileSize * 8, new ImagePattern(marketPic), tileSize * 3, tileSize * 15);
        lab = rectangleBuilder(tileSize * 4, tileSize * 3, new ImagePattern(labPic), tileSize * 14, tileSize * 18);
        workshop = rectangleBuilder(tileSize * 3, tileSize * 4, new ImagePattern(workshopPic), tileSize * 21, tileSize * 21);
        cafe = rectangleBuilder(tileSize * 4, tileSize * 5, new ImagePattern(cafePic), tileSize * 28, tileSize * 17);
        gym = rectangleBuilder(tileSize * 7, tileSize * 7, new ImagePattern(gymPic), tileSize * 10, 0);
        ranch = rectangleBuilder(tileSize * 4, tileSize * 4, new ImagePattern(ranchPic), tileSize * 19, tileSize);
        root.getChildren().addAll(clinic, market, lab, workshop, cafe, gym, ranch);


        // TODO: 6/23/2017 - age az 1000 - 4 * tileSize rad shod X-esh bere JungleUI;
        // TODO: 6/23/2017 - kenare har sakhtamun bood (jolosh ba fasele nahayatan tileSize) S zad bere too sakhtemoon;
        // TODO: 7/3/2017 to winter dar nadaraim:D


        return villageScene;
    }

    public boolean moveUp(Rectangle player) {
        if (player.getY() + player.getHeight() >= tileSize * 3 / 2d + 2 &&
                player.getX() + player.getWidth() < 1000 - tileSize * 3.4 &&
                !(player.getX() + player.getWidth() - 5 >= clinic.getX() &&
                        player.getX() + tileSize * 0.8 + 1 <= clinic.getX() + clinic.getWidth() &&
                        player.getY() + player.getHeight() >= clinic.getY() + clinic.getHeight() &&
                        player.getY() + player.getHeight() <= clinic.getY() + clinic.getHeight() + 10) &&
                !(player.getX() + player.getWidth() - tileSize - 5 >= gym.getX() &&
                        player.getX() + tileSize * 0.6 <= gym.getX() + gym.getWidth() &&
                        player.getY() + player.getHeight() >= gym.getY() + gym.getHeight() &&
                        player.getY() + player.getHeight() <= gym.getY() + gym.getHeight() + 10) &&
                !(player.getX() + player.getWidth() - tileSize - 5 >= ranch.getX() &&
                        player.getX() + tileSize * 0.6 <= ranch.getX() + ranch.getWidth() &&
                        player.getY() + player.getHeight() >= ranch.getY() + ranch.getHeight() &&
                        player.getY() + player.getHeight() <= ranch.getY() + ranch.getHeight() + 10) &&
                !(player.getX() + player.getWidth() - tileSize * 0.4 >= tileSize * 6 &&
                        player.getX() + tileSize / 2 <= tileSize * 10 &&
                        player.getY() + player.getHeight() >= 7 * tileSize &&
                        player.getY() + player.getHeight() <= 7 * tileSize + 10) &&
                !(player.getX() + player.getWidth() - 10 >= tileSize * 26 &&
                        player.getX() + tileSize / 2 <= tileSize * 36 &&
                        player.getY() + player.getHeight() >= 5 * tileSize &&
                        player.getY() + player.getHeight() <= 5 * tileSize + 10) &&
                !(player.getX() + player.getWidth() + tileSize * 0.6 >= market.getX() &&
                        player.getX() - tileSize * 0.4 <= market.getX() + market.getWidth() &&
                        player.getY() + player.getHeight() >= market.getY() + market.getHeight() + tileSize &&
                        player.getY() + player.getHeight() <= market.getY() + market.getHeight() + tileSize + 10) &&
                !(player.getX() + player.getWidth() - tileSize >= lab.getX() &&
                        player.getX() + tileSize * 0.6 <= lab.getX() + lab.getWidth() &&
                        player.getY() + player.getHeight() >= lab.getY() + lab.getHeight() &&
                        player.getY() + player.getHeight() <= lab.getY() + lab.getHeight() + 10) &&
                !(player.getX() + player.getWidth() - tileSize >= workshop.getX() &&
                        player.getX() + tileSize * 0.6 <= workshop.getX() + workshop.getWidth() &&
                        player.getY() + player.getHeight() >= workshop.getY() + workshop.getHeight() &&
                        player.getY() + player.getHeight() <= workshop.getY() + workshop.getHeight() + 10) &&
                !(player.getX() + player.getWidth() - tileSize * 0.6 >= cafe.getX() &&
                        player.getX() + tileSize <= cafe.getX() + cafe.getWidth() &&
                        player.getY() + player.getHeight() >= cafe.getY() + cafe.getHeight() &&
                        player.getY() + player.getHeight() <= cafe.getY() + cafe.getHeight() + 10) &&
                !(player.getX() >= tileSize * 12 &&
                        player.getX() - 10 <= tileSize * 13 &&
                        player.getY() + player.getHeight() >= 22 * tileSize &&
                        player.getY() + player.getHeight() <= 22 * tileSize + 10) &&
                !(player.getX() >= tileSize * 17 &&
                        player.getX() - 10 <= tileSize * 18 &&
                        player.getY() + player.getHeight() >= 22 * tileSize &&
                        player.getY() + player.getHeight() <= 22 * tileSize + 10) &&
                !(player.getX() >= tileSize * 24 &&
                        player.getX() - 10 <= tileSize * 27 &&
                        player.getY() + player.getHeight() >= 18 * tileSize &&
                        player.getY() + player.getHeight() <= 18 * tileSize + 10) &&
                !(player.getX() >= tileSize * 31 &&
                        player.getX() - 10 <= tileSize * 34 &&
                        player.getY() + player.getHeight() >= 18 * tileSize &&
                        player.getY() + player.getHeight() <= 18 * tileSize + 10) &&
                !(player.getX() >= tileSize * 1 &&
                        player.getX() - 10 <= tileSize * 4 &&
                        player.getY() + player.getHeight() >= 27.2 * tileSize &&
                        player.getY() + player.getHeight() <= 27.2 * tileSize + 10) &&
                !(player.getX() >= tileSize * 8 &&
                        player.getX() - 10 <= tileSize * 11 &&
                        player.getY() + player.getHeight() >= 27.2 * tileSize &&
                        player.getY() + player.getHeight() <= 27.2 * tileSize + 10) &&
                !(player.getX() >= tileSize * 16 &&
                        player.getX() - 10 <= tileSize * 19 &&
                        player.getY() + player.getHeight() >= 27.2 * tileSize &&
                        player.getY() + player.getHeight() <= 27.2 * tileSize + 10) &&
                !(player.getX() >= tileSize * 25 &&
                        player.getX() - 10 <= tileSize * 28 &&
                        player.getY() + player.getHeight() >= 27.2 * tileSize &&
                        player.getY() + player.getHeight() <= 27.2 * tileSize + 10) &&
                !(player.getX() >= tileSize * 32 &&
                        player.getX() - 10 <= tileSize * 35 &&
                        player.getY() + player.getHeight() >= 27.2 * tileSize &&
                        player.getY() + player.getHeight() <= 27.2 * tileSize + 10)) {
            return true;
        }
        return false;
    }

    public boolean moveDown(Rectangle player) {
        if (player.getY() <= 700 - tileSize * 3 / 2 &&
                player.getX() + player.getWidth() < 1000 - tileSize * 3.4 &&
                !(player.getX() + player.getWidth() - 5 >= clinic.getX() &&
                        player.getX() + tileSize * 0.8 + 1 <= clinic.getX() + clinic.getWidth() &&
                        player.getY() + player.getHeight() <= clinic.getY() + tileSize / 2 &&
                        player.getY() + player.getHeight() >= clinic.getY() - 10) &&
                !(player.getX() + player.getWidth() - tileSize - 5 >= gym.getX() &&
                        player.getX() + 15 <= gym.getX() + gym.getWidth() &&
                        player.getY() + player.getHeight() <= gym.getY() + tileSize * 3 / 2 &&
                        player.getY() + player.getHeight() >= gym.getY() - 10) &&
                !(player.getX() + player.getWidth() - tileSize - 5 >= ranch.getX() &&
                        player.getX() + 15 <= ranch.getX() + ranch.getWidth() &&
                        player.getY() + player.getHeight() <= ranch.getY() + tileSize * 3 / 4 &&
                        player.getY() + player.getHeight() >= ranch.getY() + tileSize / 2) &&
                !(player.getX() + player.getWidth() - 10 >= tileSize * 6 &&
                        player.getX() + tileSize / 2 <= tileSize * 10 &&
                        player.getY() + player.getHeight() <= 5 * tileSize + 10 &&
                        player.getY() + player.getHeight() >= 5 * tileSize) &&
                !(player.getX() + player.getWidth() - 10 >= tileSize * 26 &&
                        player.getX() + tileSize / 2 <= tileSize * 36 &&
                        player.getY() + player.getHeight() >= 3 / 2 * tileSize &&
                        player.getY() + player.getHeight() <= 3 / 2 * tileSize + 10) &&
                !(player.getX() + player.getWidth() + tileSize * 0.6 >= market.getX() &&
                        player.getX() - tileSize * 0.4 <= market.getX() + market.getWidth() &&
                        player.getY() + player.getHeight() <= market.getY() + tileSize - 10 &&
                        player.getY() + player.getHeight() >= market.getY() + tileSize - 20) &&
                !(player.getX() + player.getWidth() - tileSize >= lab.getX() &&
                        player.getX() + 15 <= lab.getX() + lab.getWidth() &&
                        player.getY() + player.getHeight() <= lab.getY() + 5 &&
                        player.getY() + player.getHeight() >= lab.getY() - 5) &&
                !(player.getX() + player.getWidth() - tileSize >= workshop.getX() &&
                        player.getX() + 15 <= workshop.getX() + workshop.getWidth() &&
                        player.getY() + player.getHeight() <= workshop.getY() + 10 &&
                        player.getY() + player.getHeight() >= workshop.getY()) &&
                !(player.getX() + player.getWidth() - tileSize * 0.6 >= cafe.getX() &&
                        player.getX() + tileSize <= cafe.getX() + cafe.getWidth() &&
                        player.getY() + player.getHeight() <= cafe.getY() + 10 &&
                        player.getY() + player.getHeight() >= cafe.getY()) &&
                !(player.getX() >= tileSize * 12 &&
                        player.getX() - 10 <= tileSize * 13 &&
                        player.getY() + player.getHeight() <= 20 * tileSize + 15 &&
                        player.getY() + player.getHeight() >= 20 * tileSize + 5) &&
                !(player.getX() >= tileSize * 17 &&
                        player.getX() - 10 <= tileSize * 18 &&
                        player.getY() + player.getHeight() <= 20 * tileSize + 15 &&
                        player.getY() + player.getHeight() >= 20 * tileSize + 5) &&
                !(player.getX() >= tileSize * 24 &&
                        player.getX() - 10 <= tileSize * 27 &&
                        player.getY() + player.getHeight() <= 17.2 * tileSize &&
                        player.getY() + player.getHeight() >= 16.8 * tileSize) &&
                !(player.getX() >= tileSize * 31 &&
                        player.getX() - 10 <= tileSize * 34 &&
                        player.getY() + player.getHeight() <= 17.2 * tileSize &&
                        player.getY() + player.getHeight() >= 16.8 * tileSize) &&
                !(player.getX() >= tileSize * 1 &&
                        player.getX() - 10 <= tileSize * 4 &&
                        player.getY() + player.getHeight() <= 25.6 * tileSize &&
                        player.getY() + player.getHeight() >= 25.2 * tileSize) &&
                !(player.getX() >= tileSize * 8 &&
                        player.getX() - 10 <= tileSize * 11 &&
                        player.getY() + player.getHeight() <= 25.6 * tileSize &&
                        player.getY() + player.getHeight() >= 25.2 * tileSize) &&
                !(player.getX() >= tileSize * 16 &&
                        player.getX() - 10 <= tileSize * 19 &&
                        player.getY() + player.getHeight() <= 25.6 * tileSize &&
                        player.getY() + player.getHeight() >= 25.2 * tileSize) &&
                !(player.getX() >= tileSize * 25 &&
                        player.getX() - 10 <= tileSize * 28 &&
                        player.getY() + player.getHeight() <= 25.6 * tileSize &&
                        player.getY() + player.getHeight() >= 25.2 * tileSize) &&
                !(player.getX() >= tileSize * 32 &&
                        player.getX() - 10 <= tileSize * 35 &&
                        player.getY() + player.getHeight() <= 25.6 * tileSize &&
                        player.getY() + player.getHeight() >= 25.2 * tileSize)) {
            return true;
        }
        return false;
    }

    public boolean moveRight(Rectangle player) {
        if (((player.getX() + player.getWidth() <= 1000 - tileSize * 4) ||
                (player.getX() + player.getWidth() >= 1000 - tileSize * 4 &&
                        player.getY() + player.getHeight() >= tileSize * 6 &&
                        player.getY() + player.getHeight() <= tileSize * 7) ||
                (player.getX() + player.getWidth() >= 1000 - tileSize * 4 &&
                        player.getY() + player.getHeight() >= tileSize * 14 &&
                        player.getY() + player.getHeight() <= tileSize * 15)) &&
                !(player.getY() + player.getHeight() - 5 >= clinic.getY() + tileSize / 4 + 1 &&
                        player.getY() + tileSize * 2 <= clinic.getY() + clinic.getHeight() &&
                        player.getX() + player.getWidth() <= clinic.getX() + tileSize * 3 / 4 &&
                        player.getX() + player.getWidth() >= clinic.getX() - 5) &&
                !(player.getY() + player.getHeight() >= gym.getY() + tileSize * 3 / 2d + 1 &&
                        player.getY() + player.getHeight() <= gym.getY() + gym.getHeight() &&
                        player.getX() + player.getWidth() <= gym.getX() + 30 &&
                        player.getX() + player.getWidth() >= gym.getX() + 20) &&
                !(player.getY() + player.getHeight() >= ranch.getY() + tileSize * 3 / 4 &&
                        player.getY() <= ranch.getY() + ranch.getHeight() - tileSize * 2 &&
                        player.getX() + player.getWidth() <= ranch.getX() + tileSize &&
                        player.getX() + player.getWidth() >= ranch.getX() + tileSize * 0.6) &&
                !(player.getY() + player.getHeight() >= tileSize * 5 + tileSize * 0.6 &&
                        player.getY() + player.getHeight() <= tileSize * 7 &&
                        player.getX() + player.getWidth() <= 6 * tileSize + 8 &&
                        player.getX() + player.getWidth() >= 6 * tileSize - 2) &&
                !(player.getY() + player.getHeight() >= tileSize * 3 / 2 + 1 &&
                        player.getY() + player.getHeight() <= tileSize * 9 / 2d + 10 &&
                        player.getX() + player.getWidth() >= 26 * tileSize &&
                        player.getX() + player.getWidth() <= 26 * tileSize + 10) &&
                !(player.getY() + player.getHeight() >= market.getY() + tileSize * 0.6 &&
                        player.getY() - player.getHeight() <= market.getY() + market.getHeight() - tileSize * 2.8 &&
                        player.getX() + player.getWidth() >= market.getX() - 20 &&
                        player.getX() + player.getWidth() <= market.getX() - 10) &&
                !(player.getY() + player.getHeight() >= lab.getY() + tileSize * 0.4 &&
                        player.getY() + player.getHeight() <= lab.getY() + lab.getHeight() &&
                        player.getX() + player.getWidth() <= lab.getX() + 20 &&
                        player.getX() + player.getWidth() >= lab.getX() + 10) &&
                !(player.getY() + player.getHeight() >= workshop.getY() + tileSize * 0.4 &&
                        player.getY() + player.getHeight() <= workshop.getY() + workshop.getHeight() &&
                        player.getX() + player.getWidth() <= workshop.getX() + 30 &&
                        player.getX() + player.getWidth() >= workshop.getX() + 20) &&
                !(player.getY() + player.getHeight() >= cafe.getY() &&
                        player.getY() + player.getHeight() <= cafe.getY() + cafe.getHeight() &&
                        player.getX() + player.getWidth() <= cafe.getX() + 10 &&
                        player.getX() + player.getWidth() >= cafe.getX()) &&
                !(player.getY() + player.getHeight() >= tileSize * 20.5 &&
                        player.getY() + player.getHeight() <= tileSize * 22 &&
                        player.getX() + player.getWidth() <= 13 * tileSize + 5 &&
                        player.getX() + player.getWidth() >= 13 * tileSize - 5) &&
                !(player.getY() + player.getHeight() >= tileSize * 20.5 &&
                        player.getY() + player.getHeight() <= tileSize * 22 &&
                        player.getX() + player.getWidth() <= 18 * tileSize + 5 &&
                        player.getX() + player.getWidth() >= 18 * tileSize - 5) &&
                !(player.getY() + player.getHeight() >= tileSize * 17 &&
                        player.getY() + player.getHeight() <= tileSize * 18 &&
                        player.getX() + player.getWidth() <= 25 * tileSize + 5 &&
                        player.getX() + player.getWidth() >= 25 * tileSize - 5) &&
                !(player.getY() + player.getHeight() >= tileSize * 17 &&
                        player.getY() + player.getHeight() <= tileSize * 18 &&
                        player.getX() + player.getWidth() <= 32 * tileSize + 5 &&
                        player.getX() + player.getWidth() >= 32 * tileSize - 5) &&
                !(player.getY() + player.getHeight() >= tileSize * 25.4 &&
                        player.getY() + player.getHeight() <= tileSize * 27.4 &&
                        player.getX() + player.getWidth() <= 2 * tileSize + 10 &&
                        player.getX() + player.getWidth() >= 2 * tileSize) &&
                !(player.getY() + player.getHeight() >= tileSize * 25.4 &&
                        player.getY() + player.getHeight() <= tileSize * 27.4 &&
                        player.getX() + player.getWidth() <= 9 * tileSize + 10 &&
                        player.getX() + player.getWidth() >= 9 * tileSize) &&
                !(player.getY() + player.getHeight() >= tileSize * 25.4 &&
                        player.getY() + player.getHeight() <= tileSize * 27.4 &&
                        player.getX() + player.getWidth() <= 17 * tileSize + 10 &&
                        player.getX() + player.getWidth() >= 17 * tileSize) &&
                !(player.getY() + player.getHeight() >= tileSize * 25.4 &&
                        player.getY() + player.getHeight() <= tileSize * 27.4 &&
                        player.getX() + player.getWidth() <= 26 * tileSize + 10 &&
                        player.getX() + player.getWidth() >= 26 * tileSize) &&
                !(player.getY() + player.getHeight() >= tileSize * 25.4 &&
                        player.getY() + player.getHeight() <= tileSize * 27.4 &&
                        player.getX() + player.getWidth() <= 33 * tileSize + 10 &&
                        player.getX() + player.getWidth() >= 33 * tileSize)) {
            return true;
        }
        return false;
    }

    public boolean moveLeft(Rectangle player) {
        if (player.getX() >= 0 &&
                !(player.getY() + player.getHeight() >= clinic.getY() + tileSize / 2 + 1 &&
                        player.getY() + tileSize * 2 <= clinic.getY() + clinic.getHeight() &&
                        player.getX() >= clinic.getX() + clinic.getWidth() - tileSize * 1.4 &&
                        player.getX() <= clinic.getX() + clinic.getWidth() - tileSize * 0.8) &&
                !(player.getY() + player.getHeight() >= gym.getY() + tileSize * 3 / 2d + 1 &&
                        player.getY() + player.getHeight() <= gym.getY() + gym.getHeight() &&
                        player.getX() >= gym.getX() + gym.getWidth() - 10 &&
                        player.getX() <= gym.getX() + gym.getWidth()) &&
                !(player.getY() + player.getHeight() >= ranch.getY() + tileSize * 3 / 4 &&
                        player.getY() <= ranch.getY() + ranch.getHeight() - tileSize * 2 &&
                        player.getX() >= ranch.getX() + ranch.getWidth() - 15 &&
                        player.getX() <= ranch.getX() + ranch.getWidth() - 5) &&
                !(player.getY() + player.getHeight() >= tileSize * 5 + tileSize * 0.6 &&
                        player.getY() + player.getHeight() <= tileSize * 7 &&
                        player.getX() >= 10 * tileSize - 10 &&
                        player.getX() <= 10 * tileSize) &&
                !(player.getY() + player.getHeight() >= market.getY() + tileSize * 0.6 &&
                        player.getY() - player.getHeight() <= market.getY() + market.getHeight() - tileSize * 2.8 &&
                        player.getX() <= market.getX() + market.getWidth() + 20 &&
                        player.getX() >= market.getX() + market.getWidth() + 10) &&
                !(player.getY() + player.getHeight() >= lab.getY() + tileSize * 0.4 &&
                        player.getY() + player.getHeight() <= lab.getY() + lab.getHeight() &&
                        player.getX() >= lab.getX() + lab.getWidth() - 20 &&
                        player.getX() <= lab.getX() + lab.getWidth() - 10) &&
                !(player.getY() + player.getHeight() >= workshop.getY() + tileSize * 0.4 &&
                        player.getY() + player.getHeight() <= workshop.getY() + workshop.getHeight() &&
                        player.getX() >= workshop.getX() + workshop.getWidth() - 20 &&
                        player.getX() <= workshop.getX() + workshop.getWidth() - 10) &&
                !(player.getY() + player.getHeight() >= cafe.getY() &&
                        player.getY() + player.getHeight() <= cafe.getY() + cafe.getHeight() &&
                        player.getX() >= cafe.getX() + cafe.getWidth() - 30 &&
                        player.getX() <= cafe.getX() + cafe.getWidth() - 20) &&
                !(player.getY() + player.getHeight() >= tileSize * 20.5 &&
                        player.getY() + player.getHeight() <= tileSize * 22 &&
                        player.getX() <= 14 * tileSize - 5 &&
                        player.getX() >= 14 * tileSize - 15) &&
                !(player.getY() + player.getHeight() >= tileSize * 20.5 &&
                        player.getY() + player.getHeight() <= tileSize * 22 &&
                        player.getX() <= 19 * tileSize - 5 &&
                        player.getX() >= 19 * tileSize - 15) &&
                !(player.getY() + player.getHeight() >= tileSize * 17 &&
                        player.getY() + player.getHeight() <= tileSize * 18 &&
                        player.getX() <= 28 * tileSize + 5 &&
                        player.getX() >= 28 * tileSize - 5) &&
                !(player.getY() + player.getHeight() >= tileSize * 17 &&
                        player.getY() + player.getHeight() <= tileSize * 18 &&
                        player.getX() <= 35 * tileSize + 5 &&
                        player.getX() >= 35 * tileSize - 5) &&
                !(player.getY() + player.getHeight() >= tileSize * 25.4 &&
                        player.getY() + player.getHeight() <= tileSize * 27.4 &&
                        player.getX() >= 5 * tileSize - 10 &&
                        player.getX() <= 5 * tileSize) &&
                !(player.getY() + player.getHeight() >= tileSize * 25.4 &&
                        player.getY() + player.getHeight() <= tileSize * 27.4 &&
                        player.getX() >= 12 * tileSize - 15 &&
                        player.getX() <= 12 * tileSize - 5) &&
                !(player.getY() + player.getHeight() >= tileSize * 25.4 &&
                        player.getY() + player.getHeight() <= tileSize * 27.4 &&
                        player.getX() >= 20 * tileSize - 15 &&
                        player.getX() <= 20 * tileSize - 5) &&
                !(player.getY() + player.getHeight() >= tileSize * 25.4 &&
                        player.getY() + player.getHeight() <= tileSize * 27.4 &&
                        player.getX() >= 29 * tileSize - 15 &&
                        player.getX() <= 29 * tileSize - 5)) {
            return true;
        }
        return false;
    }

}
