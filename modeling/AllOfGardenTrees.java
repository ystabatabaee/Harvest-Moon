import java.util.ArrayList;

public class AllOfGardenTrees {

    AllOFWeathers allOFWeathers = new AllOFWeathers();
    AllOfFruits allOfFruits = new AllOfFruits();

    public GardenTree peach() {
        return new GardenTree("Peach", allOFWeathers.spring(), new ArrayList<Integer>(){{add(2);add(3);}}, allOfFruits.peach());
    }

    public GardenTree pear() {
        return new GardenTree("Pear", allOFWeathers.spring(), new ArrayList<Integer>(){{add(2);add(3);}}, allOfFruits.pear());
    }

    public GardenTree lemon() {
        return new GardenTree("Lemon", allOFWeathers.summer(), new ArrayList<Integer>(){{add(2);add(3);}}, allOfFruits.lemon());
    }

    public GardenTree pomegranate() {
        return new GardenTree("Pomegranate", allOFWeathers.summer(), new ArrayList<Integer>(){{add(2);add(3);}}, allOfFruits.pomegranate());
    }

    public GardenTree apple() {
        return new GardenTree("Apple", allOFWeathers.fall(), new ArrayList<Integer>(){{add(2);add(3);}}, allOfFruits.apple());
    }

    public GardenTree orange() {
        return new GardenTree("Orange", allOFWeathers.fall(), new ArrayList<Integer>(){{add(2);add(3);}}, allOfFruits.orange());
    }




}
