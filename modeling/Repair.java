import java.util.HashMap;

public class Repair extends Task {

    public Repair() {
        super.name = "Replace";
        super.featureChangeRate = new HashMap<String, Double>();
        super.executionTime = new Date();
    }

    public boolean run(Greenhouse greenhouse) {
        System.out.println("Greenhouse is repaired!\n");
        greenhouse.setIsRuin(false);
        // TODO: 5/20/2017 complete
        return true;
    }

    public String run(Tool tool, Player player) {
        if (player.getMoney() < tool.getRepairPrice()) {
            String[] string = {"You don't have enough money!", "Vous n'avez pas assez d'argent!", "¡No tienes suficiente dinero!"};
            return string[UI.language];
        }
        for (Item item : tool.getRepairItems().elementSet()) {
            player.getBackpack().getItem().remove(item, tool.getRepairItems().count(item));
        }
        tool.setValid(true);
        player.setMoney(player.getMoney() - tool.getRepairPrice());
        String[] string = {" is repaired for ", "Est réparé pour", "Se repara para"};
        return (tool.getName() + string[UI.language] + tool.getRepairPrice() + " Gils!");

    }
}
