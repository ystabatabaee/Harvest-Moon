
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class Server implements Runnable {
    private int port;
    private ServerSocket serverSocket;
    private ArrayList<Socket> clients = new ArrayList<>();
    private String name;
    private ArrayList<ObjectOutputStream> objectOutputStream = new ArrayList<>();
    private ArrayList<ObjectInputStream> objectInputStream = new ArrayList<>();
    private ArrayList<OutputStream> outputStream = new ArrayList<>();
    private ArrayList<InputStream> inputStream = new ArrayList<>();
    private ObservableList serverLog;
    private ArrayList<Thread> clientListenerThread = new ArrayList<>();
    private Group root;
    private Group villageRoot;
    private Rectangle player;
    private Rectangle[] players;
    private boolean[] flag;
    private String[] names;
    private Text[] texts;
    private int chat = -1;
    private int chatReqFrom = -1;
    private boolean acceptChatReq = false;
    private Game game;
    private boolean receivedStatusReq = false;
    private PlayerStatus requestedPlayerStatus;
    private boolean[] friendReq;
    private boolean[] friend;
    private boolean[] lockInTo;
    private boolean[] lockInFrom;
    private int trade = -1;
    private int tradeReqFrom = -1;
    private boolean lockIn;
    private boolean acceptTradeReq = false;
    private ItemPack itemPack;


    public boolean isLockIn() {
        return lockIn;
    }

    public void setLockIn(boolean lockIn) {
        this.lockIn = lockIn;
    }

    public boolean[] getLockInTo() {
        return lockInTo;
    }

    public void setLockInTo(boolean[] lockInTo) {
        this.lockInTo = lockInTo;
    }

    public boolean[] getLockInFrom() {
        return lockInFrom;
    }

    public void setLockInFrom(boolean[] lockInFrom) {
        this.lockInFrom = lockInFrom;
    }

    public int getTrade() {
        return trade;
    }

    public int getTradeReqFrom() {
        return tradeReqFrom;
    }

    public boolean isAcceptTradeReq() {
        return acceptTradeReq;
    }

    public void setTrade(int trade) {
        this.trade = trade;
    }

    public void setAcceptTradeReq(boolean acceptTradeReq) {
        this.acceptTradeReq = acceptTradeReq;
    }

    public boolean[] getFriendReq() {
        return friendReq;
    }

    public void setFriendReq(boolean[] friendReq) {
        this.friendReq = friendReq;
    }

    public boolean[] getFriend() {
        return friend;
    }

    public void setFriend(boolean[] friend) {
        this.friend = friend;
    }

    public boolean isReceivedStatusReq() {
        return receivedStatusReq;
    }

    public PlayerStatus getRequestedPlayerStatus() {
        return requestedPlayerStatus;
    }

    public void setReceivedStatusReq(boolean receivedStatusReq) {
        this.receivedStatusReq = receivedStatusReq;
    }

    public boolean isAcceptChatReq() {
        return acceptChatReq;
    }

    public void setAcceptChatReq(boolean acceptChatReq) {
        this.acceptChatReq = acceptChatReq;
    }

    public int getChatReqFrom() {
        return chatReqFrom;
    }

    public int getChat() {
        return chat;
    }

    public void setChat(int chat) {
        this.chat = chat;
    }

    public String[] getNames() {
        return names;
    }

    public ObservableList getServerLog() {
        return serverLog;
    }

    public Server(int port, String name, Group root, boolean[] flag, Group villageRoot, Rectangle player, Game game) {
        this.port = port;
        this.name = name;
        this.root = root;
        this.flag = flag;
        this.villageRoot = villageRoot;
        game.getPlayer().setName(name);
        this.player = player;//new Rectangle(player.getX(), player.getY(), player.getWidth(), player.getHeight());
        serverLog = FXCollections.observableArrayList();
        this.game = game;
    }

    public String getName() {
        return name;
    }

    @Override
    public void run() {
        setUpServer();
        waitForClient();
        getIOStreams();
        process();
        try {
            for (int i = 0; i < clientListenerThread.size(); i++) {
                clientListenerThread.get(i).join();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        closeConnection();
    }

    private void setUpServer() {
        try {
            serverSocket = new ServerSocket(port);
            Text setUp = UI.buttonDesigner("Server set up successfully", 160, 200, 20, Color.SADDLEBROWN);
            Platform.runLater(() -> root.getChildren().add(setUp));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    private void waitForClient() {
        try {
            serverSocket.setSoTimeout(20000);

        } catch (SocketException e) {
            e.printStackTrace();
        }

        try {
            long stop = System.nanoTime() + TimeUnit.SECONDS.toNanos(20);
            Text wait = UI.buttonDesigner("Waiting 20s for a client", 160, 230, 20, Color.SADDLEBROWN);
            Platform.runLater(() -> root.getChildren().add(wait));
            while (stop > System.nanoTime()) {
                final Socket server = serverSocket.accept();
                clients.add(server);
                Text connection = UI.buttonDesigner("Connection established with the client", 160, 260, 20, Color.SADDLEBROWN);
                Platform.runLater(() -> root.getChildren().add(connection));
            }
        } catch (IOException e) {
            Text finish = UI.buttonDesigner("Time finished", 160, 290, 20, Color.SADDLEBROWN);
            Platform.runLater(() -> root.getChildren().add(finish));
        }
    }


    private void getIOStreams() {
        try {
            for (int i = 0; i < clients.size(); i++) {
                objectInputStream.add(new ObjectInputStream(clients.get(i).getInputStream()));
                objectOutputStream.add(new ObjectOutputStream(clients.get(i).getOutputStream()));
                outputStream.add(clients.get(i).getOutputStream());
                inputStream.add(clients.get(i).getInputStream());
            }
            Text establish = UI.buttonDesigner("Server established I/O streams", 160, 320, 20, Color.SADDLEBROWN);
            Platform.runLater(() -> root.getChildren().add(establish));
            ios = true;
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    private byte[] ByteTobyte(Byte[] bytes) {
        byte[] buffer = new byte[bytes.length];
        for (int i = 0; i < bytes.length; i++)
            buffer[i] = bytes[i].byteValue();
        return buffer;
    }

    boolean[] clientsAdd;
    boolean[] setName;
    boolean allNamesSet;
    boolean namesAdded;
    boolean ios = false;

    private void process() {
        flag[0] = true;
        players = new Rectangle[clients.size()];
        clientsAdd = new boolean[clients.size()];
        setName = new boolean[clients.size() + 1];
        names = new String[clients.size() + 1];
        texts = new Text[clients.size() + 1];
        friend = new boolean[clients.size() + 1];
        friendReq = new boolean[clients.size() + 1];
        lockInTo = new boolean[clients.size() + 1];
        lockInFrom = new boolean[clients.size() + 1];
        names[clients.size()] = name;
        setName[clients.size()] = true;
        texts[clients.size()] = UI.buttonDesigner(name, player.getX(), player.getY() - 10, 20, Color.SADDLEBROWN);
        //this.villageRoot.getChildren().add(this.player);
        Text process = UI.buttonDesigner("Start processing", 160, 350, 20, Color.SADDLEBROWN);
        Platform.runLater(() -> root.getChildren().add(process));
        for (int i = 0; i < clients.size(); i++) {
            int finalI = i;
            clientListenerThread.add(new Thread(() -> {
                if (!ios) {
                    getIOStreams();
                }
                DataPack dataPack;
                NamePack namePack;
                Request request;
                Message message;
                PlayerStatus playerStatus;
                ItemPack itemPack;
                Item item = null;
                int size = 0;
                String fileName = "";
                String sender = "";
                Object object;
                do {
                    if (allNamesSet()) {
                        if (!namesAdded && nonNull()) {
                            for (int j = 0; j < clients.size() + 1; j++) {
                                Text text;
                                if (j != clients.size())
                                    text = UI.buttonDesigner(names[j], players[j].getX(),
                                            players[j].getY() - 10, 20, Color.SADDLEBROWN);
                                else
                                    text = UI.buttonDesigner(names[j], player.getX(), player.getY() - 10, 20, Color.SADDLEBROWN);
                                texts[j] = text;
                                int finalJ = j;
                                Platform.runLater(() -> villageRoot.getChildren().add(texts[finalJ]));
                            }
                            namesAdded = true;
                        }
                        allNamesSet = true;
                        sendName(finalI);
                    }
                    try {
                        object = objectInputStream.get(finalI).readObject();
                        if (object instanceof DataPack) {
                            //sendDataPack(new DataPack(player.getX(), player.getY(), player.getWidth(), player.getHeight()), -1);
                            dataPack = (DataPack) object;
                            DataPack newDataPack = new DataPack(dataPack.getPlayerX(), dataPack.getPlayerY(), dataPack.getPlayerWidth(), dataPack.getPlayerHeight(), dataPack.getPlayerImageURL());
                            newDataPack.setIndex(finalI);
                            if (!clientsAdd[finalI]) {
                                //Platform.runLater(() -> villageRoot.getChildren().remove(player));
                                players[finalI] = new Rectangle(dataPack.getPlayerX(), dataPack.getPlayerY(), dataPack.getPlayerWidth(), dataPack.getPlayerHeight());
                                Platform.runLater(() -> villageRoot.getChildren().add(players[finalI]));
                                clientsAdd[finalI] = true;
                                sendDataPack(newDataPack, -1);
                                sendFirstData(new FirstData(clients.size() + 1));
                                //System.out.println("Sent");
                                //Platform.runLater(() -> villageRoot.getChildren().remove(player));
                                //Platform.runLater(() -> villageRoot.getChildren().add(player));
                            } else {
                                players[finalI].setX(dataPack.getPlayerX());
                                players[finalI].setY(dataPack.getPlayerY());
                                players[finalI].setWidth(dataPack.getPlayerWidth());
                                players[finalI].setHeight(dataPack.getPlayerHeight());
                                players[finalI].setFill(new ImagePattern(new Image(dataPack.getPlayerImageURL())));
                                if (names != null && names[finalI] != null && players[finalI] != null) {
                                    texts[finalI].setX(players[finalI].getX());
                                    texts[finalI].setY(players[finalI].getY() - 10);
                                    texts[clients.size()].setX(player.getX());
                                    texts[clients.size()].setY(player.getY() - 10);
                                }
                                sendDataPack(newDataPack, finalI);
                                //sendDataPack(newDataPack, -1);
                                sendFirstData(new FirstData(clients.size() + 1));
                                //System.out.println(clients.size() + 1);
                                //System.out.println("Sent");
                            }
                        } else if (object instanceof NamePack && !setName[finalI]) {
                            setName[finalI] = true;
                            namePack = (NamePack) object;
                            names[finalI] = namePack.getPlayerName();
                        } else if (object instanceof Request) {
                            request = (Request) object;
                            if (request.to == clients.size()) {
                                if (request.getReq().equals("Chat")) {
                                    if (request.getAccept() != null && request.getAccept()) {
                                        chat = request.getFrom();
                                        chatReqFrom = request.getFrom();
                                        acceptChatReq = true;
                                    } else {
                                        chat = request.getTo();
                                        chatReqFrom = request.getFrom();
                                    }
                                } else if (request.getReq().equals("Status")) {
                                    sendPlayerStatus(request.from);
                                } else if (request.getReq().equals("Friend")) {
                                    if (request.getAccept() != null && request.getAccept()) {
                                        friendReq[request.getFrom()] = false;
                                        friend[request.getFrom()] = true;
                                    } else if (!friend[request.getFrom()]) {
                                        friendReq[request.getFrom()] = true;
                                    }
                                } else if (request.getReq().equals("Remove")) {
                                    friendReq[request.getFrom()] = false;
                                } else if (request.getReq().equals("Trade")) {
                                    if (request.getAccept() != null && request.getAccept()) {
                                        trade = request.getFrom();
                                        tradeReqFrom = request.getFrom();
                                        acceptTradeReq = true;
                                    } else {
                                        trade = request.getTo();
                                        tradeReqFrom = request.getFrom();
                                    }
                                } else if (request.getReq().equals("LockIn")) {
                                    lockInFrom[request.getFrom()] = true;
                                }
                            } else sendRequest(request);
                        } else if (object instanceof Message) {
                            message = (Message) object;
                            Message finalMessage = message;
                            if (message.getAll().equals("All") || message.getNumOfReceiver() == clients.size()) {
                                Media mediaa = new Media(new File("sounds/jingle-bells-sms.mp3").toURI().toString());
                                MediaPlayer mediaPlayerr = new MediaPlayer(mediaa);
                                mediaPlayerr.play();
                                if (message.getAll().equals("All")) {
                                    sendMessagePublic(message);
                                }
                                if (message.getMessage().contains("@@@@@@@@@@@@@@@@@@@@@@@##FILESIZE:")) {
                                    String[] mess = message.getMessage().split("/");
                                    size = Integer.parseInt(mess[1]);
                                    fileName = mess[2];
                                    sender = message.getSender();
                                    File file = new File("sendFiles\\" + fileName);
                                    if (file.exists())
                                        file.delete();
                                    FileOutputStream out = new FileOutputStream("sendFiles\\" + fileName);
                                    int remainingSize = size;
                                    int count;
                                    byte[] buffer = new byte[32768];
                                    int maxRead = Math.min(32768, remainingSize);
                                    while ((count = inputStream.get(finalI).read(buffer, 0, maxRead)) > 0) {
                                        out.write(buffer, 0, count);
                                        remainingSize -= count;
                                        maxRead = Math.min(32768, remainingSize);
                                    }
                                    if (fileName.contains(".jpg") || fileName.contains(".png") || fileName.contains(".gif")) {
                                        Image image = new Image("file:\\\\\\" + file.getAbsolutePath());
                                        ImageView imageView = new ImageView(image);
                                        Platform.runLater(() -> serverLog.add(imageView));
                                    } else if (fileName.contains(".mp3")) {
                                        Media media = new Media(file.toURI().toString());
                                        MediaPlayer mediaPlayer = new MediaPlayer(media);
                                        String finalSender = sender;
                                        String finalFileName = fileName;
                                        Platform.runLater(() -> serverLog.add(finalSender + ": " + finalFileName));
                                        mediaPlayer.play();
                                    }
                                    out.close();
                                } else
                                    Platform.runLater(() -> serverLog.add(finalMessage.getSender() + ": " + finalMessage.getMessage()));
                            } else sendMessagePV(message);
                        } else if (object instanceof PlayerStatus) {
                            playerStatus = (PlayerStatus) object;
                            if (playerStatus.getNumOfReceiver() == clients.size()) {
                                requestedPlayerStatus = playerStatus;
                                receivedStatusReq = true;
                            } else sendPlayerStatus(playerStatus);
                        } else if (object instanceof ItemPack) {
                            itemPack = (ItemPack) object;
                            if (itemPack.getNumOfReceiver() == clients.size()) {
                                for (int j = 0; j < UI.items.size(); j++) {
                                    if (itemPack.getName().equals(UI.items.get(j).getName())) {
                                        item = UI.items.get(j).clone();
                                        item.setUseCount(itemPack.getUseCount());
                                        item.setValid(itemPack.getValid());
                                        break;
                                    }
                                }
                                if (item != null)
                                    game.getPlayer().getBackpack().getItem().add(item);
                            } else sendItemPack(itemPack);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } while (true);
            }));
            clientListenerThread.get(i).setDaemon(true);
            clientListenerThread.get(i).start();
        }
    }


    public boolean nonNull() {
        for (int i = 0; i < players.length; i++)
            if (players[i] == null)
                return false;
        return true;
    }

    public boolean allNamesSet() {
        for (int i = 0; i < clients.size(); i++)
            if (!setName[i])
                return false;
        return true;
    }

    public void sendDataPack(DataPack dataPack, int j) {
        if (dataPack.getIndex() == -1)
            dataPack.setIndex(clients.size());
        dataPack.setDontAdd(j);
        for (int i = 0; i < clients.size(); i++) {
            writeObject(i, dataPack);
        }
    }

    public void sendFirstData(FirstData firstData) {
        for (int i = 0; i < clients.size(); i++) {
            firstData.setClientIndex(i);
            writeObject(i, firstData);
        }
    }

    public void sendName(int i) {
        for (int j = 0; j < clients.size() + 1; j++) {
            NamePack namePack = new NamePack(names[j], j);
            //System.out.println("NamePack packed : " + j);
            writeObject(i, namePack);
        }
    }


    public int nearAnyPlayer() {
        for (int i = 0; i < players.length; i++)
            if (Math.sqrt((player.getX() - players[i].getX()) * (player.getX() - players[i].getX()) +
                    (player.getY() - players[i].getY()) * (player.getY() - players[i].getY())) <= 60)
                return i;
        return -1;
    }

    public void sendRequest(Request request) {
        writeObject(request.getTo(), request);
    }


    public void sendPlayerStatus(PlayerStatus playerStatus) {
        writeObject(playerStatus.getNumOfReceiver(), playerStatus);
    }

    public void sendPlayerStatus(int numOfPlayer) {
        Double healthCurrent = 0.0;
        Double healthMaxCurrent = 0.0;
        Double healthRefill = 0.0;
        Double healthMaxRefill = 0.0;
        Double healthConsumption = 0.0;
        Double healthMinConsumption = 0.0;
        Double energyCurrent = 0.0;
        Double energyMaxCurrent = 0.0;
        Double energyRefill = 0.0;
        Double energyMaxRefill = 0.0;
        Double energyConsumption = 0.0;
        Double energyMinConsumption = 0.0;
        Double staminaCurrent = 0.0;
        Double staminaMaxCurrent = 0.0;
        Double staminaRefill = 0.0;
        Double staminaMaxRefill = 0.0;
        Double staminaConsumption = 0.0;
        Double staminaMinConsumption = 0.0;
        Double satietyCurrent = 0.0;
        Double satietyMaxCurrent = 0.0;
        Double satietyRefill = 0.0;
        Double satietyMaxRefill = 0.0;
        Double satietyConsumption = 0.0;
        Double satietyMinConsumption = 0.0;
        Double money = 0.0;
        Integer animalCount = 0;
        for (int i = 0; i < game.getPlayer().getFeature().size(); i++) {
            if (game.getPlayer().getFeature().get(i).getName().equals("Health")) {
                healthCurrent = game.getPlayer().getFeature().get(i).getCurrent();
                healthMaxCurrent = game.getPlayer().getFeature().get(i).getMaxCurrent();
                healthRefill = game.getPlayer().getFeature().get(i).getRefillRate();
                healthMaxRefill = game.getPlayer().getFeature().get(i).getMaxRefillRate();
                healthConsumption = game.getPlayer().getFeature().get(i).getConsumptionRate();
                healthMinConsumption = game.getPlayer().getFeature().get(i).getMinConsumptionRate();
            } else if (game.getPlayer().getFeature().get(i).getName().equals("Energy")) {
                energyCurrent = game.getPlayer().getFeature().get(i).getCurrent();
                energyMaxCurrent = game.getPlayer().getFeature().get(i).getMaxCurrent();
                energyRefill = game.getPlayer().getFeature().get(i).getRefillRate();
                energyMaxRefill = game.getPlayer().getFeature().get(i).getMaxRefillRate();
                energyConsumption = game.getPlayer().getFeature().get(i).getConsumptionRate();
                energyMinConsumption = game.getPlayer().getFeature().get(i).getMinConsumptionRate();
            } else if (game.getPlayer().getFeature().get(i).getName().equals("Stamina")) {
                staminaCurrent = game.getPlayer().getFeature().get(i).getCurrent();
                staminaMaxCurrent = game.getPlayer().getFeature().get(i).getMaxCurrent();
                staminaRefill = game.getPlayer().getFeature().get(i).getRefillRate();
                staminaMaxRefill = game.getPlayer().getFeature().get(i).getMaxRefillRate();
                staminaConsumption = game.getPlayer().getFeature().get(i).getConsumptionRate();
                staminaMinConsumption = game.getPlayer().getFeature().get(i).getMinConsumptionRate();
            } else if (game.getPlayer().getFeature().get(i).getName().equals("Satiety")) {
                satietyCurrent = game.getPlayer().getFeature().get(i).getCurrent();
                satietyMaxCurrent = game.getPlayer().getFeature().get(i).getMaxCurrent();
                satietyRefill = game.getPlayer().getFeature().get(i).getRefillRate();
                satietyMaxRefill = game.getPlayer().getFeature().get(i).getMaxRefillRate();
                satietyConsumption = game.getPlayer().getFeature().get(i).getConsumptionRate();
                satietyMinConsumption = game.getPlayer().getFeature().get(i).getMinConsumptionRate();
            }
        }
        money = game.getPlayer().getMoney();
        for (int i = 0; i < game.getFarm().getBarn().getBarnPart().size(); i++) {
            animalCount += game.getFarm().getBarn().getBarnPart().get(i).getAnimal().size();
        }
        PlayerStatus playerStatus = new PlayerStatus(healthCurrent, healthMaxCurrent, healthRefill, healthMaxRefill,
                healthConsumption, healthMinConsumption, energyCurrent, energyMaxCurrent, energyRefill, energyMaxRefill,
                energyConsumption, energyMinConsumption, staminaCurrent, staminaMaxCurrent, staminaRefill, staminaMaxRefill,
                staminaConsumption, staminaMinConsumption, satietyCurrent, satietyMaxCurrent, satietyRefill, satietyMaxRefill,
                satietyConsumption, satietyMinConsumption, money, animalCount, name);
        writeObject(numOfPlayer, playerStatus);
    }

    public void sendItem(Item item, int count, int numOfPlayer) {
        for (int i = 0; i < count; i++)
            writeObject(numOfPlayer, new ItemPack(item.isValid(), item.getName(), item.getUseCount(), numOfPlayer));
    }


    public void sendItemPack(ItemPack itemPack) {
        writeObject(itemPack.getNumOfReceiver(), itemPack);
    }


    public void sendTextPV(String string, int numOfPlayer) {
        Message message = new Message(string, name, "Client");
        writeObject(numOfPlayer, message);
        if (!string.contains(":"))
            Platform.runLater(() -> serverLog.add(name + ": " + string));
    }


    public void sendTextPublic(String string) {
        Message message;
        if (!string.contains(":"))
            message = new Message(name + ": " + string, "Client");
        else message = new Message(string, "Client");
        for (int i = 0; i < clients.size(); i++) {
            writeObject(i, message);
        }
        if (!string.contains(":"))
            Platform.runLater(() -> serverLog.add(name + ": " + string));
    }


    public void sendMessagePublic(Message message) {
        for (int i = 0; i < clients.size(); i++) {
            if (i != message.getFrom())
                writeObject(i, message);
        }
    }

    public void sendMessagePV(Message message) {
        if (message.getNumOfReceiver() != -1)
            writeObject(message.getNumOfReceiver(), message);
    }


    public void sendImagePublic(String path, String fileName) {
        try {
            for (int i = 0; i < clients.size(); i++) {
                File file = new File(path);
                int count;
                int size = (int) file.length();
                Message message = new Message("@@@@@@@@@@@@@@@@@@@@@@@##FILESIZE:/" + size + "/" + fileName, name, "Client");
                writeObject(i, message);
                byte[] buffer = new byte[32768];
                FileInputStream in = new FileInputStream(file);
                while ((count = in.read(buffer)) > 0)
                    outputStream.get(i).write(buffer, 0, count);
                in.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        Image image = new Image("file:\\\\\\" + path);
        ImageView imageView = new ImageView(image);
        Platform.runLater(() -> serverLog.add(imageView));
    }

    public void sendImagePV(String path, String fileName, int numOfPlayer) {
        try {

            File file = new File(path);
            int count;
            int size = (int) file.length();
            Message message = new Message("@@@@@@@@@@@@@@@@@@@@@@@##FILESIZE:/" + size + "/" + fileName, name, "Client");
            writeObject(numOfPlayer, message);
            byte[] buffer = new byte[32768];
            FileInputStream in = new FileInputStream(file);
            while ((count = in.read(buffer)) > 0)
                outputStream.get(numOfPlayer).write(buffer, 0, count);
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Image image = new Image("file:\\\\\\" + path);
        ImageView imageView = new ImageView(image);
        Platform.runLater(() -> serverLog.add(imageView));
    }

    public void sendAudioPublic(String path, String fileName) {
        try {
            for (int i = 0; i < clients.size(); i++) {
                File file = new File(path);
                int count;
                int size = (int) file.length();
                Message message = new Message("@@@@@@@@@@@@@@@@@@@@@@@##FILESIZE:/" + size + "/" + fileName, name, "Client");
                writeObject(i, message);
                byte[] buffer = new byte[32768];
                FileInputStream in = new FileInputStream(file);
                while ((count = in.read(buffer)) > 0)
                    outputStream.get(i).write(buffer, 0, count);
                in.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        Platform.runLater(() -> serverLog.add(name + ": " + fileName));
    }

    public void sendAudioPV(String path, String fileName, int numOfPlayer) {
        try {
            File file = new File(path);
            int count;
            int size = (int) file.length();
            Message message = new Message("@@@@@@@@@@@@@@@@@@@@@@@##FILESIZE:/" + size + "/" + fileName, name, "Client");
            writeObject(numOfPlayer, message);
            byte[] buffer = new byte[32768];
            FileInputStream in = new FileInputStream(file);
            while ((count = in.read(buffer)) > 0)
                outputStream.get(numOfPlayer).write(buffer, 0, count);
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Platform.runLater(() -> serverLog.add(name + ": " + fileName));
    }

    public void closeConnection() {
        try {
            for (int i = 0; i < clients.size(); i++) {
                objectInputStream.get(i).close();
                objectOutputStream.get(i).close();
                clients.get(i).close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeObject(int i, Object object) {
        synchronized (objectOutputStream.get(i)) {
            try {
                objectOutputStream.get(i).writeObject(object);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
