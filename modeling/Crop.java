import javafx.scene.image.Image;

import java.util.ArrayList;

public class Crop {

	private String type;
	private Weather weather;
	private int lifeCycle;
	private int spoilageTime;
	private int harvestLeft;
	private int age;
	private ArrayList<Task> task;
	private Image picture;
	private Fruit fruit;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Weather getWeather() {
		return weather;
	}

	public void setWeather(Weather weather) {
		this.weather = weather;
	}

	public int getLifeCycle() {
		return lifeCycle;
	}

	public void setLifeCycle(int lifeCycle) {
		this.lifeCycle = lifeCycle;
	}

	public int getSpoilageTime() {
		return spoilageTime;
	}

	public void setSpoilageTime(int spoilageTime) {
		this.spoilageTime = spoilageTime;
	}

	public int getHarvestLeft() {
		return harvestLeft;
	}

	public void setHarvestLeft(int harvestLeft) {
		this.harvestLeft = harvestLeft;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public ArrayList<Task> getTask() {
		return task;
	}

	public void setTask(ArrayList<Task> task) {
		this.task = task;
	}

	public Fruit getFruit() {
		return fruit;
	}

	public void setFruit(Fruit fruit) {
		this.fruit = fruit;
	}

	public Image getPicture() {
		return picture;
	}

	public void setPicture(Image picture) {
		this.picture = picture;
	}


	public Crop(String type, Weather weather, int lifeCycle, int spoilageTime, int harvestLeft, ArrayList<Task> task, Fruit fruit) {
		this.type = type;
		this.weather = weather;
		this.lifeCycle = lifeCycle;
		this.spoilageTime = spoilageTime;
		this.harvestLeft = harvestLeft;
		age = 0;
		this.task = task;
		this.fruit = fruit;
		picture = fruit.getPicture();
	}


}