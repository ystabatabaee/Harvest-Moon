import com.google.common.collect.HashMultiset;

import java.util.ArrayList;

public class Market extends Building {

    private ArrayList<Shop> shop;

    public Market(boolean isRuin, Probability ruin, HashMultiset repairObjects, double repairPrice, ArrayList<Shop> shop) {
        super(isRuin, ruin, repairObjects, repairPrice);
        this.shop = shop;
    }

    public Market() {
        super();
        shop = new ArrayList<Shop>();
        AllOfShops allOfShops = new AllOfShops();
        try {
            shop.add(allOfShops.groceriesStore());
            shop.add(allOfShops.butchery());
            shop.add(allOfShops.generalStore());
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Shop> getShop() {
        return shop;
    }

    public void setShop(ArrayList<Shop> shop) {
        this.shop = shop;
    }

}