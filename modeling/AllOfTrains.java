import java.util.ArrayList;

public class AllOfTrains {

    public Train bodyBuilding() {
        ArrayList<Feature> feature = new ArrayList<>();
        feature.add(new Feature("Energy", 500, 1000, 0, 5));
        Train train = new Train("Body Building", feature, 120, 4);
        return train;
    }

    public Train tredmil() {
        ArrayList<Feature> feature = new ArrayList<>();
        feature.add(new Feature("Health", 500, 1000, 0, 5));
        Train train = new Train("Tredmil", feature, 150, 10);
        return train;
    }

    public Train bicycle() {
        ArrayList<Feature> feature = new ArrayList<>();
        feature.add(new Feature("Energy", 400, 500, 2, 0));
        Train train = new Train("Bicycle", feature, 200, 25);
        return train;
    }

    public Train football() {
        ArrayList<Feature> feature = new ArrayList<>();
        feature.add(new Feature("Health", 350, 100, 3, 0));
        Train train = new Train("Football", feature, 100, 20);
        return train;
    }

    public Train soccer() {
        ArrayList<Feature> feature = new ArrayList<>();
        feature.add(new Feature("Health", 450, 100, 3, 0));
        Train train = new Train("Soccer", feature, 130, 20);
        return train;
    }

    public Train volleyball() {
        ArrayList<Feature> feature = new ArrayList<>();
        feature.add(new Feature("Health", 450, 100, 3, 0));
        Train train = new Train("volleyball", feature, 130, 20);
        return train;
    }

    public Train swimming() {
        ArrayList<Feature> feature = new ArrayList<>();
        feature.add(new Feature("Stamina", 40, 100, 3, 5));
        Train train = new Train("swimming", feature, 130, 20);
        return train;
    }

}
