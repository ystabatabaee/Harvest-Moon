import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class GeneralStoreUI implements Mover {
    Group root = new Group();
    static double tileSize = 50;
    private static Rectangle chair;
    private static Rectangle table;
    private static Rectangle firewood;
    private static Rectangle breadTable;
    private static Rectangle seedBox;
    private static Rectangle kiln;

    private Rectangle rectangleBuilder(double width, double height, Paint fill, double setX, double setY) {
        Rectangle rectangle = new Rectangle(width, height, fill);
        rectangle.setX(setX);
        rectangle.setY(setY);
        return rectangle;
    }

    public GeneralStoreUI(Group root) {
        this.root = root;
    }

    public Scene sceneBuilder(Stage primaryStage) {
        Scene generalStoreScene = new Scene(root, 1000, 700, Color.BLACK);

        Image generalStorePic = new Image("pics/maps/generalStoreMap.png");
        Image tablePic = new Image("pics/generalStoreTable.png");
        Image chairPic = new Image("pics/generalStoreChair.png");
        Image breadTablePic = new Image("pics/breadTable.png");
        Image firewoodPic = new Image("pics/firewood.png");
        Image seedBoxPic = new Image("pics/seedBox.png");
        Image kilnPic = new Image("pics/kiln.png");
        Image doorPic = new Image("pics/doors/generalStoreDoor.png");


        Rectangle rectangle = new Rectangle(1000, 700, new ImagePattern(generalStorePic));
        root.getChildren().add(rectangle);

        chair = rectangleBuilder(tileSize, tileSize * 2, new ImagePattern(chairPic), 1000 - tileSize * 6, tileSize * 2);
        table = rectangleBuilder(tileSize * 3, tileSize * 2, new ImagePattern(tablePic), 1000 - tileSize * 7, tileSize * 3);
        breadTable = rectangleBuilder(tileSize * 7, tileSize * 4, new ImagePattern(breadTablePic), tileSize * 3, tileSize * 6);
        firewood = rectangleBuilder(tileSize * 2, tileSize * 2, new ImagePattern(firewoodPic), tileSize, tileSize * 3);
        kiln = rectangleBuilder(tileSize * 4, tileSize * 5, new ImagePattern(kilnPic), tileSize * 3, 0);
        Rectangle door = rectangleBuilder(tileSize * 1, tileSize * 2, new ImagePattern(doorPic), 1000 - tileSize * 3, tileSize);
        root.getChildren().addAll(chair, table, breadTable, kiln, firewood, door);

        for (int i = 0; i < 4; i++) {
            seedBox = rectangleBuilder(tileSize * 2, tileSize * 2, new ImagePattern(seedBoxPic), tileSize * (2 * i + 3), 700 - tileSize * 3);
            root.getChildren().addAll(seedBox);
        }

        return generalStoreScene;
    }

    public boolean moveUp(Rectangle player) {
        if (player.getY() + player.getHeight() >= 3.4 * tileSize &&
                !(player.getX() + player.getWidth() >= chair.getX() + tileSize * 0.6 &&
                        player.getX() <= chair.getX() + chair.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= chair.getY() + chair.getHeight() &&
                        player.getY() + player.getHeight() <= chair.getY() + chair.getHeight() + tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= table.getX() + tileSize * 0.4 &&
                        player.getX() <= table.getX() + table.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= table.getY() + table.getHeight() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= table.getY() + table.getHeight() + tileSize * 0.4) &&
                !(player.getX() + player.getWidth() >= firewood.getX() + tileSize * 0.6 &&
                        player.getX() <= firewood.getX() + firewood.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= firewood.getY() + firewood.getHeight() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= firewood.getY() + firewood.getHeight() + tileSize * 0.4) &&
                !(player.getX() + player.getWidth() >= kiln.getX() + tileSize * 1 &&
                        player.getX() <= kiln.getX() + kiln.getWidth() - tileSize * 1.4 &&
                        player.getY() + player.getHeight() >= kiln.getY() + kiln.getHeight() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= kiln.getY() + kiln.getHeight() + tileSize * 0.4) &&
                !(player.getX() + player.getWidth() >= breadTable.getX() + tileSize * 0.6 &&
                        player.getX() <= breadTable.getX() + breadTable.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= breadTable.getY() + breadTable.getHeight() - tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= breadTable.getY() + breadTable.getHeight()) &&
                !(player.getX() + player.getWidth() >= tileSize * 3.6 &&
                        player.getX() <= tileSize * 10 &&
                        player.getY() + player.getHeight() >= tileSize * 13.2 &&
                        player.getY() + player.getHeight() <= tileSize * 13.4)) {
            return true;
        }
        return false;
    }

    public boolean moveDown(Rectangle player) {
        if (player.getY() + player.getHeight() <= 14 * tileSize &&
                !(player.getX() + player.getWidth() >= table.getX() + tileSize * 0.4 &&
                        player.getX() <= table.getX() + table.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= table.getY() + tileSize * 0.4 &&
                        player.getY() + player.getHeight() <= table.getY() + tileSize * 0.6) &&
                !(player.getX() + player.getWidth() >= breadTable.getX() + tileSize * 0.6 &&
                        player.getX() <= breadTable.getX() + breadTable.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= breadTable.getY() - tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= breadTable.getY()) &&
                !(player.getX() + player.getWidth() >= tileSize * 3.6 &&
                        player.getX() <= tileSize * 10 &&
                        player.getY() + player.getHeight() >= tileSize * 10.8 &&
                        player.getY() + player.getHeight() <= tileSize * 11)) {
            return true;
        }
        return false;
    }

    public boolean moveRight(Rectangle player) {
        if (player.getX() + player.getWidth() <= 19.2 * tileSize &&
                !(player.getX() + player.getWidth() >= chair.getX() + tileSize * 0.4 &&
                        player.getX() + player.getWidth() <= chair.getX() + tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= chair.getY() &&
                        player.getY() + player.getHeight() <= chair.getY() + chair.getHeight() + tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= table.getX() + tileSize * 0.2 &&
                        player.getX() + player.getWidth() <= table.getX() + tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= table.getY() + tileSize * 0.6 &&
                        player.getY() + player.getHeight() <= table.getY() + table.getHeight() + tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= firewood.getX() + tileSize * 0.4 &&
                        player.getX() + player.getWidth() <= firewood.getX() + tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= firewood.getY() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= firewood.getY() + firewood.getHeight() + tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= kiln.getX() + tileSize * 1.2 &&
                        player.getX() + player.getWidth() <= kiln.getX() + tileSize * 1.4 &&
                        player.getY() + player.getHeight() >= kiln.getY() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= kiln.getY() + kiln.getHeight() + tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= breadTable.getX() + tileSize * 0.4 &&
                        player.getX() + player.getWidth()<= breadTable.getX() + tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= breadTable.getY() &&
                        player.getY() + player.getHeight() <= breadTable.getY() + breadTable.getHeight() - tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= tileSize * 3.4 &&
                        player.getX() <= tileSize * 3.6 &&
                        player.getY() + player.getHeight() >= tileSize * 11 &&
                        player.getY() + player.getHeight() <= tileSize * 13.2)) {
            return true;
        }
        return false;
    }

    public boolean moveLeft(Rectangle player) {
        if (player.getX() >= -tileSize * 0.2 &&
                !(player.getX() >= chair.getX() + chair.getWidth() - tileSize * 0.4 &&
                        player.getX() <= chair.getX() + chair.getWidth() - tileSize * 0.2 &&
                        player.getY() + player.getHeight() >= chair.getY() &&
                        player.getY() + player.getHeight() <= chair.getY() + chair.getHeight() + tileSize * 0.2) &&
                !(player.getX() >= table.getX() + table.getWidth() - tileSize * 0.6 &&
                        player.getX() <= table.getX() + table.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= table.getY() + tileSize * 0.6 &&
                        player.getY() + player.getHeight() <= table.getY() + table.getHeight() + tileSize * 0.2) &&
                !(player.getX() >= firewood.getX() + firewood.getWidth() - tileSize * 0.6 &&
                        player.getX() <= firewood.getX() + firewood.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= firewood.getY() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= firewood.getY() + firewood.getHeight() + tileSize * 0.2) &&
                !(player.getX() >= kiln.getX() + kiln.getWidth() - tileSize * 1.4 &&
                        player.getX() <= kiln.getX() + kiln.getWidth() - tileSize * 1 &&
                        player.getY() + player.getHeight() >= kiln.getY() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= kiln.getY() + kiln.getHeight() + tileSize * 0.2) &&
                !(player.getX() >= breadTable.getX() + breadTable.getWidth() - tileSize * 0.6 &&
                        player.getX() <= breadTable.getX() + breadTable.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= breadTable.getY() &&
                        player.getY() + player.getHeight() <= breadTable.getY() + breadTable.getHeight() - tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= tileSize * 10 &&
                        player.getX() <= tileSize * 10.2 &&
                        player.getY() + player.getHeight() >= tileSize * 11 &&
                        player.getY() + player.getHeight() <= tileSize * 13.2)) {
            return true;
        }
        return false;
    }
}
