import java.util.HashMap;

public class Replace extends Task{

    public Replace() {
        super.name = "Replace";
        super.featureChangeRate = new HashMap<String, Double>();
        super.executionTime = new Date();
    }

    public String  run(Item newItem, Item oldItem, Player player, ToolShelf toolShelf) {
        if(newItem.getName().equals(oldItem.getName())){
            toolShelf.getExistingTools().add((Tool)newItem);
            toolShelf.getExistingTools().remove((Tool)oldItem);
            player.getBackpack().getItem().add(newItem);
            player.getBackpack().getItem().remove(oldItem);
            return newItem.getName() + "was replaced successfully!";
        }
        return "These tools can't be replaced because they are not of the same kind!";
    }
}
