import java.util.HashMap;

public class FeatureChanger extends Task{

    public FeatureChanger() {
        super.name = "Feature Changer";
        super.featureChangeRate = new HashMap<String, Double>();
        super.executionTime = new Date();
    }

    public boolean changeCurrent(Feature feature, int value) {
        if(feature.getMaxCurrent() >=  feature.getCurrent() + value) {
            feature.setCurrent(feature.getCurrent() + value);
            return true;
        }
        else if(feature.getCurrent() + value < 0){
            System.out.println();
            return false;
        }
        System.out.println(feature.getName() + "can't " );
        return false;
    }

    
}
