import javafx.scene.Group;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;

import java.util.HashMap;

public class Buy extends Task{

    public Buy() {
        super.name = "Buy";
        super.featureChangeRate = new HashMap<String, Double>();
        super.executionTime = new Date();
    }

    public String run(Shop shop, Item item, Player player, int num) {
        if (player.getBackpack().getCapacity() < player.getBackpack().getCurrentFullness() + item.getCapacity() * num) {
            return "Backpack is full";
        }
        if(player.getMoney() > item.getPrice() * num) {
            for(int  i = 0; i < num; i++)
                shop.getGoods().remove(item);
            player.setMoney(player.getMoney() - item.getPrice() * num);
            player.getBackpack().getItem().add(item, num);
            player.getBackpack().setCurrentFullness(player.getBackpack().getCurrentFullness() + item.getCapacity() * num);
            UI.missionHandler(name, item.getName());
            return ("Item " + item.getName() + " x" + num + " was baught form " + shop.getName());
        }
        return "You don't have enough money";
    }
    public String run(Ranch ranch, Animal animal, Player player, Barn barn, int num, Rectangle[][] cellG, int x, int y,
                      Group group, BarnUI barnUI) {
        for(BarnPart barnPart : barn.getBarnPart()){
            if(barnPart.getAnimalType().equals(animal.getType())){
                if(player.getMoney() > animal.getPrice() * num) {
                    if(animal.getType().equals("Cow")){
                        int i = (barnPart.getAnimal().size()) % 3;
                        int j = (barnPart.getAnimal().size()) / 3;
                        barnUI.getCowG()[i][j] = barnUI.rectangleBuilder(barnUI.tileSize * 2, BarnUI.tileSize * 3,
                                new ImagePattern(animal.getPicture()), barnUI. tileSize * (3 * i + 1), barnUI.tileSize * 3 * (j + 1));
                        barnUI.getRoot().getChildren().addAll(barnUI.getCowG()[i][j]);
                    }
                    else if(animal.getType().equals("Sheep")){
                        int i = (barnPart.getAnimal().size()) % 3;
                        int j = (barnPart.getAnimal().size()) / 3;
                        barnUI.getSheepG()[i][j] = barnUI.rectangleBuilder(barnUI.tileSize * 2, BarnUI.tileSize * 2,
                                new ImagePattern(animal.getPicture()), barnUI. tileSize * (3 * i + 12), barnUI.tileSize * 3 * (j + 1));
                        barnUI.getRoot().getChildren().addAll(barnUI.getSheepG()[i][j]);
                    }
                    else if(animal.getType().equals("Chicken")){
                        int i = (barnPart.getAnimal().size()) % 4;
                        int j = (barnPart.getAnimal().size()) / 4;
                        barnUI.getChickenG()[i][j] = barnUI.rectangleBuilder(barnUI.tileSize * 1, barnUI.tileSize * 1,
                                new ImagePattern(animal.getPicture()), barnUI.tileSize * (2 * i + 1), barnUI.tileSize * (10 + 2 * j) - 10);
                        barnUI.getRoot().getChildren().addAll(barnUI.getChickenG()[i][j]);
                    }
                    for(int  i = 0; i < num; i++)
                        barnPart.getAnimal().add(animal.clone());
                    player.setMoney(player.getMoney() - animal.getPrice() * num);
                    group.getChildren().remove(cellG[x][y]);
                    ranch.getAnimals().remove(animal, num);
                    UI.missionHandler(name, animal.getType());
                    return ("Animal " + animal.getType() + " x" + num + " was baught form ranch");
                }
                return ("You don't have enough money");
            }
        }
        return "wrong action";
    }

    public boolean run(GardenTree gardenTree, Player player) {
        if(player.getMoney() < gardenTree.getPrice()){
            System.out.println("You don't have enough money");
            return false;
        }
        player.setMoney(player.getMoney() - gardenTree.getPrice());
        gardenTree.setFree(true);
        return true;
    }
}
