import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

public class GreenhouseUI implements Mover {
    Group root = new Group();
    static double tileSize = 50;
    private static Ellipse[][] ellipse;
    private static int cellX;
    private static int cellY;
    private Greenhouse greenhouse;
    private ArrayList<Rectangle[][]> field;
    private ArrayList<Rectangle[][]> fieldCrop;

    private Rectangle rectangleBuilder(double width, double height, Paint fill, double setX, double setY) {
        Rectangle rectangle = new Rectangle(width, height, fill);
        rectangle.setX(setX);
        rectangle.setY(setY);
        return rectangle;
    }

    public GreenhouseUI(Group root, Farm farm) {
        this.root = root;
        this.greenhouse = farm.getGreenhouse();
    }

    public Scene sceneBuilder(Stage primaryStage) throws IllegalAccessException, InstantiationException, InvocationTargetException {
        Scene greenhouseScene = new Scene(root, 1000, 700, Color.BLACK);
        Image greenhousePic = new Image("pics/maps/greenhouseMap.png");
        Image doorPic = new Image("pics/doors/greenhouseDoor.png");
        Image weatherMachinePic = new Image("pics/weatheringMachine.png");
        Image flowerPic = new Image("pics/flowerPot4.png");
        Image farmingFieldPic = new Image("pics/farmingField.png");

        int fieldNum = greenhouse.getFarmingField().size();
        field = new ArrayList<>();
        fieldCrop = new ArrayList<>();
        for (int i = 0; i < fieldNum; i++) {
            field.add(new Rectangle[3][3]);
            fieldCrop.add(new Rectangle[3][3]);
        }
        Rectangle rectangle = new Rectangle(1000, 700, new ImagePattern(greenhousePic));
        root.getChildren().add(rectangle);

        Rectangle door = rectangleBuilder(tileSize, tileSize * 2, new ImagePattern(doorPic), 1000 - tileSize * 4, tileSize);
        Rectangle weatheringMachine = rectangleBuilder(tileSize * 2, tileSize * 3, new ImagePattern(weatherMachinePic),
                1000 - tileSize * 3, 700 - tileSize * 5);
        root.getChildren().addAll(door, weatheringMachine);
        for (int i = 0; i < fieldNum / 2; i++) {
            for (int j = 0; j < 3; j++) {
                for (int k = 0; k < 3; k++) {
                    field.get(2 * i)[j][k] = rectangleBuilder(tileSize, tileSize, new ImagePattern(farmingFieldPic),
                            (4 * i + 1 + j) * tileSize, (6 + k) * tileSize);
                    fieldCrop.get(2 * i)[j][k] = rectangleBuilder(tileSize, tileSize, null,
                            (4 * i + 1 + j) * tileSize, (6 + k) * tileSize);
                    root.getChildren().addAll(field.get(2 * i)[j][k], fieldCrop.get(2 * i)[j][k]);
                }
            }
            for (int j = 0; j < 3; j++) {
                for (int k = 0; k < 3; k++) {
                    field.get(2 * i + 1)[j][k] = rectangleBuilder(tileSize, tileSize, new ImagePattern(farmingFieldPic),
                            (4 * i + 1 + j) * tileSize, (10 + k) * tileSize);
                    fieldCrop.get(2 * i + 1)[j][k] = rectangleBuilder(tileSize, tileSize, null,
                            (4 * i + 1 + j) * tileSize, (10 + k) * tileSize);
                    root.getChildren().addAll(field.get(2 * i + 1)[j][k], fieldCrop.get(2 * i + 1)[j][k]);
                }
            }
        }

        for (int i = 0; i < 3; i++) {
            Rectangle flower = rectangleBuilder(tileSize, tileSize * 2, new ImagePattern(flowerPic), tileSize * (4 * i + 3), tileSize * 2);
            root.getChildren().add(flower);
        }

        return greenhouseScene;
    }

    public boolean moveUp(Rectangle player) {
        if (player.getY() + player.getHeight() >= tileSize * 3.4 &&
                !(player.getX() + player.getWidth() >= tileSize * 11.6 &&
                        player.getX() <= tileSize * 11.4 &&
                        player.getY() + player.getHeight() >= tileSize * 4 &&
                        player.getY() + player.getHeight() <= tileSize * 4.2) &&
                !(player.getX() + player.getWidth() >= tileSize * 7.6 &&
                        player.getX() <= tileSize * 7.4 &&
                        player.getY() + player.getHeight() >= tileSize * 4 &&
                        player.getY() + player.getHeight() <= tileSize * 4.2) &&
                !(player.getX() + player.getWidth() >= tileSize * 3.6 &&
                        player.getX() <= tileSize * 3.4 &&
                        player.getY() + player.getHeight() >= tileSize * 4 &&
                        player.getY() + player.getHeight() <= tileSize * 4.2) &&
                !(player.getX() + player.getWidth() >= tileSize * 17.4 &&
                        player.getX() <= tileSize * 18.4 &&
                        player.getY() + player.getHeight() >= tileSize * 12 &&
                        player.getY() + player.getHeight() <= tileSize * 12.2)) {
            return true;
        }
        return false;
    }

    public boolean moveDown(Rectangle player) {
        if (player.getY() + player.getHeight() <= tileSize * 14 &&
                !(player.getX() + player.getWidth() >= tileSize * 17.4 &&
                        player.getX() <= tileSize * 18.4 &&
                        player.getY() + player.getHeight() >= tileSize * 8.8 &&
                        player.getY() + player.getHeight() <= tileSize * 9)) {
            return true;
        }
        return false;
    }

    public boolean moveRight(Rectangle player) {
        if (player.getX() + player.getWidth() <= tileSize * 20.2 &&
                !(player.getX() + player.getWidth() >= tileSize * 11.4 &&
                        player.getX() + player.getWidth() <= tileSize * 11.6 &&
                        player.getY() + player.getHeight() >= tileSize * 3 &&
                        player.getY() + player.getHeight() <= tileSize * 4) &&
                !(player.getX() + player.getWidth() >= tileSize * 7.4 &&
                        player.getX() + player.getWidth() <= tileSize * 7.6 &&
                        player.getY() + player.getHeight() >= tileSize * 3 &&
                        player.getY() + player.getHeight() <= tileSize * 4) &&
                !(player.getX() + player.getWidth() >= tileSize * 3.4 &&
                        player.getX() + player.getWidth() <= tileSize * 3.6 &&
                        player.getY() + player.getHeight() >= tileSize * 3 &&
                        player.getY() + player.getHeight() <= tileSize * 4) &&
                !(player.getX() + player.getWidth() >= tileSize * 17.2 &&
                        player.getX() + player.getWidth() <= tileSize * 17.4 &&
                        player.getY() + player.getHeight() >= tileSize * 9.2 &&
                        player.getY() + player.getHeight() <= tileSize * 12)) {
            return true;
        }
        return false;
    }

    public boolean moveLeft(Rectangle player) {
        if (player.getX() >= -tileSize * 0.2 &&
                !(player.getX() >= tileSize * 11.4 &&
                        player.getX() <= tileSize * 11.6 &&
                        player.getY() + player.getHeight() >= tileSize * 3 &&
                        player.getY() + player.getHeight() <= tileSize * 4) &&
                !(player.getX() >= tileSize * 7.4 &&
                        player.getX() <= tileSize * 7.6 &&
                        player.getY() + player.getHeight() >= tileSize * 3 &&
                        player.getY() + player.getHeight() <= tileSize * 4) &&
                !(player.getX() >= tileSize * 3.4 &&
                        player.getX() <= tileSize * 3.6 &&
                        player.getY() + player.getHeight() >= tileSize * 3 &&
                        player.getY() + player.getHeight() <= tileSize * 4) &&
                !(player.getX() >= tileSize * 18.4 &&
                        player.getX() <= tileSize * 18.6 &&
                        player.getY() + player.getHeight() >= tileSize * 9.2 &&
                        player.getY() + player.getHeight() <= tileSize * 12)) {
            return true;
        }
        return false;
    }

    private boolean firstPPressed = true;

    public Cell[][] chooseCell(Item item, KeyEvent event, Rectangle player, boolean[] flags, int num) {
        Rectangle[][] cellG = field.get(num);
        if (firstPPressed) {
            ellipse = new Ellipse[3][3];
            for (int i = 0; i < 3; i++) {
                ellipse[i] = new Ellipse[3];
                for (int j = 0; j < 3; j++) {
                    ellipse[i][j] = new Ellipse(tileSize / 2, tileSize / 4);
                    ellipse[i][j].setFill(new ImagePattern(new Image("pics/icons/arrow.png")));
                    ellipse[i][j].setCenterX(cellG[0][0].getX() + cellG[0][0].getWidth() / 2 + j * cellG[0][0].getWidth());
                    ellipse[i][j].setCenterY(cellG[0][0].getY() + cellG[0][0].getHeight() / 4 + i * cellG[0][0].getHeight());
                }
            }
        }
        int k = 0, l = 0;
        if (item.getLevel() == 1) {
            k = 1;
            l = 1;
        } else if (item.getLevel() == 2) {
            k = 1;
            l = 2;
        } else if (item.getLevel() == 3) {
            k = 1;
            l = 3;
        } else if (item.getLevel() == 4) {
            k = 3;
            l = 3;
        }

        if (firstPPressed)
            for (int i = 0; i < k; i++)
                for (int j = 0; j < l; j++)
                    root.getChildren().add(ellipse[i][j]);
        firstPPressed = false;
        if (event.getCode() == KeyCode.RIGHT && ellipse[0][0].getCenterX() < cellG[0][0].getX() + cellG[0][0].getWidth() * (3.5 - l)) {
            for (int i = 0; i < k; i++)
                for (int j = 0; j < l; j++)
                    ellipse[i][j].setCenterX(ellipse[i][j].getCenterX() + cellG[0][0].getWidth());
            return null;
        } else if (event.getCode() == KeyCode.LEFT && ellipse[0][0].getCenterX() > cellG[0][0].getX() + cellG[0][0].getWidth() * 0.5) {
            for (int i = 0; i < k; i++)
                for (int j = 0; j < l; j++)
                    ellipse[i][j].setCenterX(ellipse[i][j].getCenterX() - cellG[0][0].getWidth());
            return null;
        } else if (event.getCode() == KeyCode.UP && ellipse[0][0].getCenterY() > cellG[0][0].getY() + cellG[0][0].getHeight() * 0.25) {
            for (int i = 0; i < k; i++)
                for (int j = 0; j < l; j++)
                    ellipse[i][j].setCenterY(ellipse[i][j].getCenterY() - cellG[0][0].getHeight());
            return null;
        } else if (event.getCode() == KeyCode.DOWN && ellipse[0][0].getCenterY() < cellG[0][0].getY() + cellG[0][0].getHeight() * (3.25 - k)) {
            for (int i = 0; i < k; i++)
                for (int j = 0; j < l; j++)
                    ellipse[i][j].setCenterY(ellipse[i][j].getCenterY() + cellG[0][0].getHeight());
            return null;
        } else if (event.getCode() == KeyCode.ENTER) {
            firstPPressed = true;
            for (int i = 0; i < k; i++)
                for (int j = 0; j < l; j++)
                    root.getChildren().remove(ellipse[i][j]);
            Cell[][] cell = new Cell[3][3];
            for (int i = 0; i < k; i++) {
                for (int j = 0; j < l; j++) {
                    cell[i][j] = new Cell();
                }
            }
            cellX = (int) ((ellipse[0][0].getCenterX() - cellG[0][0].getWidth() / 2 - cellG[0][0].getX()) / cellG[0][0].getWidth());
            cellY = (int) ((ellipse[0][0].getCenterY() - cellG[0][0].getHeight() / 4 - cellG[0][0].getY()) / cellG[0][0].getHeight() + 0.1);
            for (int i = 0; i < k; i++)
                for (int j = 0; j < l; j++)
                    cell[i][j] = greenhouse.getFarmingField().get(num).getCell()
                            [(int) ((ellipse[i][j].getCenterX() - cellG[0][0].getWidth() / 2 - cellG[0][0].getX()) / cellG[0][0].getWidth())]
                            [(int) ((ellipse[i][j].getCenterY() - cellG[0][0].getHeight() / 4 - cellG[0][0].getY()) / cellG[0][0].getHeight() + 0.1)];
            flags[1] = false;
            flags[2] = true;
            return cell;
        }
        return null;
    }

    public void plow(Shovel shovel, Cell[][] cell, Player player, boolean[] flags, int num) {
        Rectangle[][] cellG = field.get(num);
        Plow plow = new Plow(shovel);
        UI.showPopup(plow.run(shovel, cell, player, cellX, cellY, cellG), root);
        flags[3] = true;
    }

    public void watering(WateringCan wateringCan, Cell[][] cell, Player player, boolean[] flags, int num) {
        Rectangle[][] cellG = field.get(num);
        Watering watering = new Watering(wateringCan);
        UI.showPopup(watering.run(wateringCan, cell, player, cellX, cellY, cellG), root);
        flags[3] = true;
    }

    public void destroyCrops(Cell[][] cell, Player player, boolean[] flags, int num) {
        Rectangle[][] cellG = field.get(num);
        Rectangle[][] cropCellG = fieldCrop.get(num);
        DestroyCrops destroyCrops = new DestroyCrops();
        UI.showPopup(destroyCrops.run(cell, player, cellX, cellY, cellG, cropCellG), root);
        flags[3] = true;
    }

    public void harvestCrops(Cell[][] cell, Player player, boolean[] flags, int num) {
        Rectangle[][] cellG = field.get(num);
        Rectangle[][] cropCellG = fieldCrop.get(num);
        Harvest harvest = new Harvest();
        UI.showPopup(harvest.run(cell, player, cellX, cellY, cellG, cropCellG), root);
        flags[3] = true;
    }

    public void plantSeeds(Seed seed, Cell[][] cell, Player player, boolean[] flags, Weather weather, int num) {
        Rectangle[][] cellG = field.get(num);
        PlantSeeds plantSeeds = new PlantSeeds();
        UI.showPopup(plantSeeds.run(seed, cell, player, weather, cellX, cellY, cellG), root);
        flags[3] = true;
    }

    public int chosenFieldNum(Rectangle player) {
        for (int i = 0; i < field.size(); i++) {
            if (player.getX() + player.getWidth() / 2 <= field.get(i)[0][0].getX() + field.get(i)[0][0].getWidth() + 30 &&
                    player.getX() + player.getWidth() / 2 >= field.get(i)[0][0].getX() - 30 &&
                    player.getY() + player.getHeight() <= field.get(i)[0][0].getY() + field.get(i)[0][0].getHeight() + 30 &&
                    player.getY() + player.getHeight() >= field.get(i)[0][0].getY() - 30) {
                return i;
            }
        }
        return -1;
    }

    public ArrayList<Rectangle[][]> getField() {
        return field;
    }

    public void setField(ArrayList<Rectangle[][]> field) {
        this.field = field;
    }

    public ArrayList<Rectangle[][]> getFieldCrop() {
        return fieldCrop;
    }

    public void setFieldCrop(ArrayList<Rectangle[][]> fieldCrop) {
        this.fieldCrop = fieldCrop;
    }


    public boolean closeToMachine(Rectangle player) {
        if (player.getX() + player.getWidth() / 2 >= tileSize * 16 &&
                player.getX() + player.getWidth() / 2 <= tileSize * 20 &&
                player.getY() + player.getHeight() <= tileSize * 13 &&
                player.getY() + player.getHeight() >= tileSize * 9) {
            return true;
        }
        return false;
    }

    public void setWeather(String choosedButton, boolean[] flags) {
        if(choosedButton.toLowerCase().contains("spring"))
            greenhouse.setWeather(new AllOFWeathers().spring());
        else if(choosedButton.toLowerCase().contains("summer"))
            greenhouse.setWeather(new AllOFWeathers().summer());
        else if(choosedButton.toLowerCase().contains("autumn"))
            greenhouse.setWeather(new AllOFWeathers().fall());
        else if(choosedButton.toLowerCase().contains("tropical"))
            greenhouse.setWeather(new AllOFWeathers().tropical());
        UI.showPopup("Greenhouse weather was set to " + choosedButton, root);
        flags[2] = true;
    }

    public void extend(Player player, boolean[] flags) {
        Image farmingFieldPic = new Image("pics/farmingField.png");
        if(greenhouse.getFarmingField().size() >= 8){
            UI.showPopup("Greenhouse can't be extended more!", root);
            return;
        }
        Update update = new Update();
        update.run(greenhouse, player);
        field.add(new Rectangle[3][3]);
        fieldCrop.add(new Rectangle[3][3]);
        int i = greenhouse.getFarmingField().size() / 2;
        if(greenhouse.getFarmingField().size() % 2 == 0){
            for (int j = 0; j < 3; j++) {
                for (int k = 0; k < 3; k++) {
                    field.get(2 * i)[j][k] = rectangleBuilder(tileSize, tileSize, new ImagePattern(farmingFieldPic),
                            (4 * i + 1 + j) * tileSize, (6 + k) * tileSize);
                    fieldCrop.get(2 * i)[j][k] = rectangleBuilder(tileSize, tileSize, null,
                            (4 * i + 1 + j) * tileSize, (6 + k) * tileSize);
                    root.getChildren().addAll(field.get(2 * i)[j][k], fieldCrop.get(2 * i)[j][k]);
                }
            }
        }
        else {
            for (int j = 0; j < 3; j++) {
                for (int k = 0; k < 3; k++) {
                    field.get(2 * i + 1)[j][k] = rectangleBuilder(tileSize, tileSize, new ImagePattern(farmingFieldPic),
                            (4 * i + 1 + j) * tileSize, (10 + k) * tileSize);
                    fieldCrop.get(2 * i + 1)[j][k] = rectangleBuilder(tileSize, tileSize, null,
                            (4 * i + 1 + j) * tileSize, (10 + k) * tileSize);
                    root.getChildren().addAll(field.get(2 * i + 1)[j][k], fieldCrop.get(2 * i + 1)[j][k]);
                }
            }
        }
        greenhouse.getFarmingField().add(new FarmingField());
        UI.showPopup("Greenhouse's fields extended and one field was added to it!", root);
        flags[3] = true;
    }

    public boolean closeToFields(Rectangle player) {
        if (player.getY() + player.getHeight() >= tileSize * 6) {
            return true;
        }
        return false;
    }
}
