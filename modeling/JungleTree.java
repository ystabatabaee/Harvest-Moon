import javafx.scene.image.Image;

import java.util.ArrayList;

public class JungleTree {

	private String type;
	private ArrayList<Task> task;
	private Image picture;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public ArrayList<Task> getTask() {
		return task;
	}

	public void setTask(ArrayList<Task> task) {
		this.task = task;
	}

	public Image getPicture() {
		return picture;
	}

	public void setPicture(Image picture) {
		this.picture = picture;
	}

	public JungleTree(String type, ArrayList<Task> task) {
		this.type = type;
		this.task = task;
		picture = new Image("pics/tree/" + type.toLowerCase() + ".png");
	}

}