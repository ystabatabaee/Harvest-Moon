import com.google.common.collect.HashMultiset;
import javafx.scene.image.Image;

import java.util.ArrayList;
import java.util.HashMap;

public class Tool extends Item {

	private double buildingPrice;
	private double repairPrice;
	private HashMultiset<Item> repairItems;

	public double getBuildingPrice() {
		return buildingPrice;
	}

	public void setBuildingPrice(double buildingPrice) {
		this.buildingPrice = buildingPrice;
	}

	public double getRepairPrice() {
		return repairPrice;
	}

	public void setRepairPrice(double repairPrice) {
		this.repairPrice = repairPrice;
	}

	public HashMultiset<Item> getRepairItems() {
		return repairItems;
	}

	public void setRepairItems(HashMultiset<Item> repairItems) {
		this.repairItems = repairItems;
	}

	public Tool(String type, HashMap<String, Double> featureChangeRate, double capacity, String name, Probability invalid,
				double price, ArrayList<Task> task, int maxLevel, int level, Dissassemblity dissassemblity, double buildingPrice, double repairPrice,
				HashMultiset<Item> repairItems, Image picture) {
		super(type, featureChangeRate, capacity, name, invalid, price, task, maxLevel, level, false, false, dissassemblity, picture);
		this.buildingPrice = buildingPrice;
		this.repairPrice = repairPrice;
		this.repairItems = repairItems;
	}

	public Tool(){}
}