import com.google.common.collect.HashMultiset;
import javafx.scene.image.Image;

import java.util.ArrayList;
import java.util.HashMap;

public class AllOfFoods {

    AllOfTools allOfTools = new AllOfTools();
    AllOfFruits allOfFruits = new AllOfFruits();
    AllOfAnimalProducts animalProducts = new AllOfAnimalProducts();
    AllOfDrinks allOfDrinks = new AllOfDrinks();
    AllOfProducts allOfProducts = new AllOfProducts();

    public Food frenchFries() {
        HashMultiset<Item> initialIngredients = HashMultiset.create();
        initialIngredients.add(allOfFruits.potato(), 2);
        initialIngredients.add(allOfProducts.oil());
        initialIngredients.add(allOfProducts.salt());
        Food food = new Food(new HashMap<String, Double>() {{
            put("MaxHealth", -15.0);
            put("Energy", 100.0);
        }}, 1, "French Fries", new Probability() {{
        }},
                0, new ArrayList<Tool>() {{
            add(allOfTools.knife());
            add(allOfTools.fryingPan());
        }}, new ArrayList<Task>() {{
        }}, new Dissassemblity(0, initialIngredients,
                true), new Recipe(), new Image("pics/food/frenchFries.png"));
        food.getRecipe().setFood(food);
        return food;
    }

    public Food saladShirazi() {
        HashMultiset<Item> initialIngredients = HashMultiset.create();
        initialIngredients.add(allOfFruits.cucumber());
        initialIngredients.add(allOfFruits.tomato());
        initialIngredients.add(allOfFruits.onion());
        initialIngredients.add(allOfDrinks.lemonJuice());
        Food food = new Food(new HashMap<String, Double>() {{
            put("Health", 60.0);
            put("Energy", 40.0);
            put("MaxHealth", 10.0);
        }}, 1, "Salad Shirazi", new Probability() {{
        }},
                0, new ArrayList<Tool>() {{
            add(allOfTools.knife());
        }}, new ArrayList<Task>() {{
        }}, new Dissassemblity(0, initialIngredients,
                true), new Recipe(), new Image("pics/food/saladShirazi.png"));
        food.getRecipe().setFood(food);
        return food;
    }

    public Food cheeseCake() {
        HashMultiset<Item> initialIngredients = HashMultiset.create();
        initialIngredients.add(animalProducts.milk());
        initialIngredients.add(allOfProducts.cheese());
        initialIngredients.add(animalProducts.egg(), 2);
        initialIngredients.add(allOfProducts.sugar());
        Food food = new Food(new HashMap<String, Double>() {{
            put("Energy", 80.0);
        }}, 1, "Cheese Cake", new Probability() {{
        }},
                0, new ArrayList<Tool>() {{
            add(allOfTools.pot());
            add(allOfTools.foodMixer());
            add(allOfTools.oven());
        }}, new ArrayList<Task>() {{
        }}, new Dissassemblity(0, initialIngredients,
                true), new Recipe(), new Image("pics/food/cheeseCake.png"));
        food.getRecipe().setFood(food);
        return food;
    }

    public Food mirzaGhasemi() {
        HashMultiset<Item> initialIngredients = HashMultiset.create();
        initialIngredients.add(allOfProducts.oil());
        initialIngredients.add(allOfFruits.garlic(), 2);
        initialIngredients.add(allOfFruits.eggplant(), 3);
        initialIngredients.add(allOfFruits.tomato(), 2);
        initialIngredients.add(animalProducts.egg(), 4);
        initialIngredients.add(allOfProducts.salt());
        Food food = new Food(new HashMap<String, Double>() {{
            put("MaxHealth", 10.0);
            put("Energy", 70.0);
            put("Health", 30.0);
        }}, 1, "MirzaGhasemi", new Probability() {{
        }},
                0, new ArrayList<Tool>() {{
            add(allOfTools.knife());
            add(allOfTools.fryingPan());
        }}, new ArrayList<Task>() {{
        }}, new Dissassemblity(0, initialIngredients,
                true), new Recipe(), new Image("pics/food/mirzaGhasemi.png"));
        food.getRecipe().setFood(food);
        return food;
    }
}
