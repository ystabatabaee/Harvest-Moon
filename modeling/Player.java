import java.util.ArrayList;

public class Player extends Person {

    private Backpack backpack;
    private double money;
    private ArrayList<Mission> mission;
    private int missionCapacity;
    private Cell cell;
    private Place place;

    public Backpack getBackpack() {
        return backpack;
    }

    public void setBackpack(Backpack backpack) {
        this.backpack = backpack;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public ArrayList<Mission> getMission() {
        return mission;
    }

    public void setMission(ArrayList<Mission> mission) {
        this.mission = mission;
    }

    public int getMissionCapacity() {
        return missionCapacity;
    }

    public void setMissionCapacity(int missionCapacity) {
        this.missionCapacity = missionCapacity;
    }

    public Cell getCell() {
        return cell;
    }

    public void setCell(Cell cell) {
        this.cell = cell;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    public Player(Backpack backpack, int missionCapacity) throws InstantiationException, IllegalAccessException {
        super("Player");
        this.backpack = backpack;
        this.missionCapacity = missionCapacity;
        mission = new ArrayList<>();
    }

    public Player(String name) {
        super(name);
        AllOfBackpacks allOfBackpacks = new AllOfBackpacks();
        backpack = allOfBackpacks.meduim();
        missionCapacity = 10;
        mission = new ArrayList<>();
    }

    public Player(int level, int experience, ArrayList<Feature> feature, String name, Backpack backpack, double money, int missionCapacity) {
        super(level, experience, feature, name);
        this.backpack = backpack;
        this.money = money;
        this.missionCapacity = missionCapacity;
        mission = new ArrayList<>();
    }

    public Player() {
    }
}