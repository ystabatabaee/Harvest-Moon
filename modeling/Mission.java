import com.google.common.collect.HashMultiset;

import java.util.ArrayList;

public class Mission {

    private String name;
    private ArrayList<Tetrad> tetrad;
    private HashMultiset<Object> price;
    private double contractFee;
    private Time dueTime;
    private HashMultiset<Object> input;
    private Seller applicant;

    public Mission(String name, ArrayList<Tetrad> tetrad, HashMultiset<Object> price, double contractFee, Time dueTime, HashMultiset<Object> input) {
        this.name = name;
        this.tetrad = tetrad;
        this.price = price;
        this.contractFee = contractFee;
        this.dueTime = dueTime;
        this.input = input;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Tetrad> getTetrad() {
        return tetrad;
    }

    public void setTetrad(ArrayList<Tetrad> tetrad) {
        this.tetrad = tetrad;
    }

    public HashMultiset<Object> getPrice() {
        return price;
    }

    public void setPrice(HashMultiset<Object> price) {
        this.price = price;
    }

    public double getContractFee() {
        return contractFee;
    }

    public void setContractFee(double contractFee) {
        this.contractFee = contractFee;
    }

    public Time getDueTime() {
        return dueTime;
    }

    public void setDueTime(Time dueTime) {
        this.dueTime = dueTime;
    }

    public HashMultiset<Object> getInput() {
        return input;
    }

    public void setInput(HashMultiset<Object> input) {
        this.input = input;
    }

    public Seller getApplicant() {
        return applicant;
    }

    public void setApplicant(Seller applicant) {
        this.applicant = applicant;
    }

    public double getProgress() {
        double progress = 0;
        for (Tetrad tetrad: this.tetrad){
            progress += (tetrad.getHappened() / tetrad.getCount()) / this.tetrad.size();
        }
        return progress * 100;
    }
}