import java.util.ArrayList;

public class Season {

    private String type;
    private ArrayList<Task> task;
    private Weather weather;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ArrayList<Task> getTask() {
        return task;
    }

    public void setTask(ArrayList<Task> task) {
        this.task = task;
    }

    public Weather getWeather() {
        return weather;
    }

    public void setWeather(Weather weather) {
        this.weather = weather;
    }

    public Season(String type, ArrayList<Task> task, Weather weather) {
        this.type = type;
        this.task = task;
        this.weather = weather;
    }

    public Season() {
        weather = new Weather();
    }
}



/*
private static double timeMillis;
    private static Date gameDate = new Date();


    private static Game game = new Game();


    private static Timeline timeCalculator = new Timeline(new KeyFrame(new Duration(50d), event -> {
        Time newTime = gameDate.getTime();
        newTime.setSecond(newTime.getSecond() + 1);
        if (newTime.getSecond() == 60) {
            newTime.setSecond(0);
            newTime.setMinute(newTime.getMinute() + 1);
            for (int i = 0; i < 4; i++) {
                if (game.getPlayer().getFeature().get(i).getName().equalsIgnoreCase("Health")) {
                    game.getPlayer().getFeature().get(i).setCurrent(game.getPlayer().getFeature().get(i).getCurrent() -
                            1.0 / 12.0);
                }
                if (game.getPlayer().getFeature().get(i).getName().equalsIgnoreCase("Stamina")) {
                    game.getPlayer().getFeature().get(i).setCurrent(game.getPlayer().getFeature().get(i).getCurrent() -
                            1.0 / 60.0);
                }
                if (game.getPlayer().getFeature().get(i).getName().equalsIgnoreCase("Energy")) {
                    game.getPlayer().getFeature().get(i).setCurrent(game.getPlayer().getFeature().get(i).getCurrent() -
                            1.0 / 3.0);
                }
                if (game.getPlayer().getFeature().get(i).getName().equalsIgnoreCase("Satiety")) {
                    game.getPlayer().getFeature().get(i).setCurrent(game.getPlayer().getFeature().get(i).getCurrent() -
                            1.25);
                }

            }
            if (newTime.getMinute() == 60) {
                newTime.setMinute(0);
                newTime.setHour(newTime.getHour() + 1);
            }
        }
        if (newTime.getHour() == 23) {
            // TODO: 7/6/2017 - faint ;
        }
    }));
*/