import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class JungleUI implements Mover {
    private Group root;
    static double tileSize = 25;
    private Jungle jungle;
    private Game game;
    private Rectangle[][] cellG;
    private Rectangle[][] contentG;


    public JungleUI(Group root, Game game) {
        this.root = root;
        this.jungle = game.getJungle();
        this.game = game;
    }

    private Rectangle rectangleBuilder(double width, double height, Paint fill, double setX, double setY) {
        Rectangle rectangle = new Rectangle(width, height, fill);
        rectangle.setX(setX);
        rectangle.setY(setY);
        return rectangle;
    }


    public Scene sceneBuilder(Stage primaryStage) {
        Image junglePic = new Image("pics/maps/jungle-fall.png");
        Scene jungleScene = new Scene(root, 1000, 700,  new ImagePattern(junglePic));
        Image tepeePic = new Image("pics/tepee.png");
        Image bigBoatPic = new Image("pics/bigBoat.png");

        Rectangle tepee = rectangleBuilder(tileSize * 4, tileSize * 5, new ImagePattern(tepeePic), 1000 - tileSize * 6, tileSize * 10);
        Rectangle bigBoat = rectangleBuilder(tileSize * 15, tileSize * 2, new ImagePattern(bigBoatPic), tileSize * 18, tileSize * 22);
        root.getChildren().addAll(tepee, bigBoat);

        cellG = new Rectangle[30][20];
        for (int i = 1; i < 30; i++) {
            for (int j = 5; j < 20; j++) {
                cellG[i][j] = new Rectangle();
                if (jungle.getCells()[i][j].getContent() instanceof Mineral) {
                    Rectangle content = rectangleBuilder(tileSize * 1, tileSize * 1, new ImagePattern(((Mineral) jungle.getCells()[i][j].getContent()).getPicture()), tileSize * i, tileSize * j);
                    root.getChildren().addAll(content);
                    cellG[i][j] = content;
                }
            }
        }
        contentG = new Rectangle[7][3];
        for (int j = 0; j < 3; j++) {
            for (int i = 0; i < 7; i++) {
                contentG[i][j] = new Rectangle();
                if (jungle.getContent()[i][j] instanceof JungleTree)
                    contentG[i][j] = rectangleBuilder(tileSize * 3, tileSize * 4, new ImagePattern(((JungleTree) jungle.getContent()[i][j]).getPicture()), tileSize * (4 * i + 2), tileSize * (5 * j + 4));
                else if (jungle.getContent()[i][j] instanceof Mineral)
                    contentG[i][j] = rectangleBuilder(tileSize * 2, tileSize * 2, new ImagePattern(((Mineral) jungle.getContent()[i][j]).getPicture()), tileSize * (4 * i + 2), tileSize * (5 * j + 6));
                root.getChildren().addAll(contentG[i][j]);
            }
        }

        return jungleScene;
    }

    public boolean moveUp(Rectangle player) {
        if (((player.getY() + player.getHeight() >= tileSize * 4.25) ||
                (player.getY() + player.getHeight() <= tileSize * 4.25 &&
                        player.getX() + player.getWidth() >= tileSize * 30.75 + 10 &&
                        player.getX() <= tileSize * 31 &&
                        player.getY() + player.getHeight() >= tileSize * 3) ||
                (player.getY() + player.getHeight() <= tileSize * 4.25 &&
                        player.getX() + player.getWidth() >= tileSize * 13 &&
                        player.getX() <= tileSize * 13 &&
                        player.getY() + player.getHeight() >= tileSize * 3)) &&
                !(player.getX() + player.getWidth() >= tileSize * 2 + 10 &&
                        player.getX() <= tileSize * 4 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 8 &&
                        player.getY() + player.getHeight() <= tileSize * 8 + 10) &&
                !(player.getX() + player.getWidth() >= tileSize * 6 + 10 &&
                        player.getX() <= tileSize * 8 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 8 &&
                        player.getY() + player.getHeight() <= tileSize * 8 + 10) &&
                !(player.getX() + player.getWidth() >= tileSize * 10 + 10 &&
                        player.getX() <= tileSize * 12 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 8 &&
                        player.getY() + player.getHeight() <= tileSize * 8 + 10) &&
                !(player.getX() + player.getWidth() >= tileSize * 14 + 10 &&
                        player.getX() <= tileSize * 16 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 8 &&
                        player.getY() + player.getHeight() <= tileSize * 8 + 10) &&
                !(player.getX() + player.getWidth() >= tileSize * 18 + 10 &&
                        player.getX() <= tileSize * 20 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 8 &&
                        player.getY() + player.getHeight() <= tileSize * 8 + 10) &&
                !(player.getX() + player.getWidth() >= tileSize * 22 + 10 &&
                        player.getX() <= tileSize * 24 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 8 &&
                        player.getY() + player.getHeight() <= tileSize * 8 + 10) &&
                !(player.getX() + player.getWidth() >= tileSize * 26 + 10 &&
                        player.getX() <= tileSize * 28 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 8 &&
                        player.getY() + player.getHeight() <= tileSize * 8 + 10) &&
                !(player.getX() + player.getWidth() >= tileSize * 2 + 10 &&
                        player.getX() <= tileSize * 4 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 13 &&
                        player.getY() + player.getHeight() <= tileSize * 13 + 10) &&
                !(player.getX() + player.getWidth() >= tileSize * 6 + 10 &&
                        player.getX() <= tileSize * 8 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 13 &&
                        player.getY() + player.getHeight() <= tileSize * 13 + 10) &&
                !(player.getX() + player.getWidth() >= tileSize * 10 + 10 &&
                        player.getX() <= tileSize * 12 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 13 &&
                        player.getY() + player.getHeight() <= tileSize * 13 + 10) &&
                !(player.getX() + player.getWidth() >= tileSize * 14 + 10 &&
                        player.getX() <= tileSize * 16 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 13 &&
                        player.getY() + player.getHeight() <= tileSize * 13 + 10) &&
                !(player.getX() + player.getWidth() >= tileSize * 18 + 10 &&
                        player.getX() <= tileSize * 20 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 13 &&
                        player.getY() + player.getHeight() <= tileSize * 13 + 10) &&
                !(player.getX() + player.getWidth() >= tileSize * 22 + 10 &&
                        player.getX() <= tileSize * 24 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 13 &&
                        player.getY() + player.getHeight() <= tileSize * 13 + 10) &&
                !(player.getX() + player.getWidth() >= tileSize * 26 + 10 &&
                        player.getX() <= tileSize * 28 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 13 &&
                        player.getY() + player.getHeight() <= tileSize * 13 + 10) &&
                !(player.getX() + player.getWidth() >= tileSize * 2 + 10 &&
                        player.getX() <= tileSize * 4 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 18 &&
                        player.getY() + player.getHeight() <= tileSize * 18 + 10) &&
                !(player.getX() + player.getWidth() >= tileSize * 6 + 10 &&
                        player.getX() <= tileSize * 8 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 18 &&
                        player.getY() + player.getHeight() <= tileSize * 18 + 10) &&
                !(player.getX() + player.getWidth() >= tileSize * 10 + 10 &&
                        player.getX() <= tileSize * 12 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 18 &&
                        player.getY() + player.getHeight() <= tileSize * 18 + 10) &&
                !(player.getX() + player.getWidth() >= tileSize * 14 + 10 &&
                        player.getX() <= tileSize * 16 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 18 &&
                        player.getY() + player.getHeight() <= tileSize * 18 + 10) &&
                !(player.getX() + player.getWidth() >= tileSize * 18 + 10 &&
                        player.getX() <= tileSize * 20 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 18 &&
                        player.getY() + player.getHeight() <= tileSize * 18 + 10) &&
                !(player.getX() + player.getWidth() >= tileSize * 22 + 10 &&
                        player.getX() <= tileSize * 24 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 18 &&
                        player.getY() + player.getHeight() <= tileSize * 18 + 10) &&
                !(player.getX() + player.getWidth() >= tileSize * 26 + 10 &&
                        player.getX() <= tileSize * 28 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 18 &&
                        player.getY() + player.getHeight() <= tileSize * 18 + 10) &&
                !(player.getX() + player.getWidth() >= tileSize * 32 + 10 &&
                        player.getX() <= tileSize * 33 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 11 &&
                        player.getY() + player.getHeight() <= tileSize * 11 + 10) &&
                !(player.getX() + player.getWidth() >= tileSize * 38 + 10 &&
                        player.getX() <= tileSize * 39 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 11 &&
                        player.getY() + player.getHeight() <= tileSize * 11 + 10) &&
                !(player.getX() + player.getWidth() >= tileSize * 34 + 10 &&
                        player.getX() <= tileSize * 38 - 10 &&
                        player.getY() + player.getHeight() >= tileSize * 15 &&
                        player.getY() + player.getHeight() <= tileSize * 15 + 10)) {
            return true;
        }
        return false;
    }

    public boolean moveDown(Rectangle player) {
        if (player.getY() + player.getHeight() <= tileSize * 20.75 &&
                !(player.getX() + player.getWidth() >= tileSize * 2 + 10 &&
                        player.getX() <= tileSize * 4 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 4 &&
                        player.getY() + player.getHeight() <= tileSize * 4 + 10) &&
                !(player.getX() + player.getWidth() >= tileSize * 6 + 10 &&
                        player.getX() <= tileSize * 8 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 4 &&
                        player.getY() + player.getHeight() <= tileSize * 4 + 10) &&
                !(player.getX() + player.getWidth() >= tileSize * 10 + 10 &&
                        player.getX() <= tileSize * 12 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 4 &&
                        player.getY() + player.getHeight() <= tileSize * 4 + 10) &&
                !(player.getX() + player.getWidth() >= tileSize * 14 + 10 &&
                        player.getX() <= tileSize * 16 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 4 &&
                        player.getY() + player.getHeight() <= tileSize * 4 + 10) &&
                !(player.getX() + player.getWidth() >= tileSize * 18 + 10 &&
                        player.getX() <= tileSize * 20 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 4 &&
                        player.getY() + player.getHeight() <= tileSize * 4 + 10) &&
                !(player.getX() + player.getWidth() >= tileSize * 22 + 10 &&
                        player.getX() <= tileSize * 24 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 4 &&
                        player.getY() + player.getHeight() <= tileSize * 4 + 10) &&
                !(player.getX() + player.getWidth() >= tileSize * 26 + 10 &&
                        player.getX() <= tileSize * 28 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 4 &&
                        player.getY() + player.getHeight() <= tileSize * 4 + 10) &&
                !(player.getX() + player.getWidth() >= tileSize * 2 + 10 &&
                        player.getX() <= tileSize * 4 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 9 - 10 &&
                        player.getY() + player.getHeight() <= tileSize * 9) &&
                !(player.getX() + player.getWidth() >= tileSize * 6 + 10 &&
                        player.getX() <= tileSize * 8 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 9 - 10 &&
                        player.getY() + player.getHeight() <= tileSize * 9) &&
                !(player.getX() + player.getWidth() >= tileSize * 10 + 10 &&
                        player.getX() <= tileSize * 12 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 9 - 10 &&
                        player.getY() + player.getHeight() <= tileSize * 9) &&
                !(player.getX() + player.getWidth() >= tileSize * 14 + 10 &&
                        player.getX() <= tileSize * 16 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 9 - 10 &&
                        player.getY() + player.getHeight() <= tileSize * 9) &&
                !(player.getX() + player.getWidth() >= tileSize * 18 + 10 &&
                        player.getX() <= tileSize * 20 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 9 - 10 &&
                        player.getY() + player.getHeight() <= tileSize * 9) &&
                !(player.getX() + player.getWidth() >= tileSize * 22 + 10 &&
                        player.getX() <= tileSize * 24 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 9 - 10 &&
                        player.getY() + player.getHeight() <= tileSize * 9) &&
                !(player.getX() + player.getWidth() >= tileSize * 26 + 10 &&
                        player.getX() <= tileSize * 28 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 9 - 10 &&
                        player.getY() + player.getHeight() <= tileSize * 9) &&
                !(player.getX() + player.getWidth() >= tileSize * 2 + 10 &&
                        player.getX() <= tileSize * 4 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 14 &&
                        player.getY() + player.getHeight() <= tileSize * 14 + 10) &&
                !(player.getX() + player.getWidth() >= tileSize * 6 + 10 &&
                        player.getX() <= tileSize * 8 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 14 &&
                        player.getY() + player.getHeight() <= tileSize * 14 + 10) &&
                !(player.getX() + player.getWidth() >= tileSize * 10 + 10 &&
                        player.getX() <= tileSize * 12 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 14 &&
                        player.getY() + player.getHeight() <= tileSize * 14 + 10) &&
                !(player.getX() + player.getWidth() >= tileSize * 14 + 10 &&
                        player.getX() <= tileSize * 16 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 14 &&
                        player.getY() + player.getHeight() <= tileSize * 14 + 10) &&
                !(player.getX() + player.getWidth() >= tileSize * 18 + 10 &&
                        player.getX() <= tileSize * 20 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 14 &&
                        player.getY() + player.getHeight() <= tileSize * 14 + 10) &&
                !(player.getX() + player.getWidth() >= tileSize * 22 + 10 &&
                        player.getX() <= tileSize * 24 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 14 &&
                        player.getY() + player.getHeight() <= tileSize * 14 + 10) &&
                !(player.getX() + player.getWidth() >= tileSize * 26 + 10 &&
                        player.getX() <= tileSize * 28 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 14 &&
                        player.getY() + player.getHeight() <= tileSize * 14 + 10) &&
                !(player.getX() + player.getWidth() >= tileSize * 32 + 10 &&
                        player.getX() <= tileSize * 33 + 20 &&
                        player.getY() + player.getHeight() >= tileSize * 7 + 10 &&
                        player.getY() + player.getHeight() <= tileSize * 7 + 20) &&
                !(player.getX() + player.getWidth() >= tileSize * 38 + 10 &&
                        player.getX() <= tileSize * 39 + 20 &&
                        player.getY() + player.getHeight() >= tileSize * 7 + 10 &&
                        player.getY() + player.getHeight() <= tileSize * 7 + 20) &&
                !(player.getX() + player.getWidth() >= tileSize * 34 + 10 &&
                        player.getX() <= tileSize * 38 - 10 &&
                        player.getY() + player.getHeight() >= tileSize * 10 &&
                        player.getY() + player.getHeight() <= tileSize * 10 + 10)) {
            return true;
        }
        return false;
    }

    public boolean moveRight(Rectangle player) {
        if (player.getX() + player.getWidth() <= tileSize * 40 &&
                player.getY() + player.getHeight() >= tileSize * 4 &&
                !(player.getX() + player.getWidth() >= tileSize * 2 &&
                        player.getX() + player.getWidth() <= tileSize * 2 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 5 - 20 &&
                        player.getY() + player.getHeight() <= tileSize * 8) &&
                !(player.getX() + player.getWidth() >= tileSize * 6 &&
                        player.getX() + player.getWidth() <= tileSize * 6 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 5 - 20 &&
                        player.getY() + player.getHeight() <= tileSize * 8) &&
                !(player.getX() + player.getWidth() >= tileSize * 10 &&
                        player.getX() + player.getWidth() <= tileSize * 10 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 5 - 20 &&
                        player.getY() + player.getHeight() <= tileSize * 8) &&
                !(player.getX() + player.getWidth() >= tileSize * 14 &&
                        player.getX() + player.getWidth() <= tileSize * 14 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 5 - 20 &&
                        player.getY() + player.getHeight() <= tileSize * 8) &&
                !(player.getX() + player.getWidth() >= tileSize * 18 &&
                        player.getX() + player.getWidth() <= tileSize * 18 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 5 - 20 &&
                        player.getY() + player.getHeight() <= tileSize * 8) &&
                !(player.getX() + player.getWidth() >= tileSize * 22 &&
                        player.getX() + player.getWidth() <= tileSize * 22 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 5 - 20 &&
                        player.getY() + player.getHeight() <= tileSize * 8) &&
                !(player.getX() + player.getWidth() >= tileSize * 26 &&
                        player.getX() + player.getWidth() <= tileSize * 26 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 5 - 20 &&
                        player.getY() + player.getHeight() <= tileSize * 8) &&
                !(player.getX() + player.getWidth() >= tileSize * 2 &&
                        player.getX() + player.getWidth() <= tileSize * 2 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 10 - 20 &&
                        player.getY() + player.getHeight() <= tileSize * 13) &&
                !(player.getX() + player.getWidth() >= tileSize * 6 &&
                        player.getX() + player.getWidth() <= tileSize * 6 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 10 - 20 &&
                        player.getY() + player.getHeight() <= tileSize * 13) &&
                !(player.getX() + player.getWidth() >= tileSize * 10 &&
                        player.getX() + player.getWidth() <= tileSize * 10 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 10 - 20 &&
                        player.getY() + player.getHeight() <= tileSize * 13) &&
                !(player.getX() + player.getWidth() >= tileSize * 14 &&
                        player.getX() + player.getWidth() <= tileSize * 14 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 10 - 20 &&
                        player.getY() + player.getHeight() <= tileSize * 13) &&
                !(player.getX() + player.getWidth() >= tileSize * 18 &&
                        player.getX() + player.getWidth() <= tileSize * 18 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 10 - 20 &&
                        player.getY() + player.getHeight() <= tileSize * 13) &&
                !(player.getX() + player.getWidth() >= tileSize * 22 &&
                        player.getX() + player.getWidth() <= tileSize * 22 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 10 - 20 &&
                        player.getY() + player.getHeight() <= tileSize * 13) &&
                !(player.getX() + player.getWidth() >= tileSize * 26 &&
                        player.getX() + player.getWidth() <= tileSize * 26 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 10 - 20 &&
                        player.getY() + player.getHeight() <= tileSize * 13) &&
                !(player.getX() + player.getWidth() >= tileSize * 2 &&
                        player.getX() + player.getWidth() <= tileSize * 2 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 15 - 20 &&
                        player.getY() + player.getHeight() <= tileSize * 18) &&
                !(player.getX() + player.getWidth() >= tileSize * 6 &&
                        player.getX() + player.getWidth() <= tileSize * 6 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 15 - 20 &&
                        player.getY() + player.getHeight() <= tileSize * 18) &&
                !(player.getX() + player.getWidth() >= tileSize * 10 &&
                        player.getX() + player.getWidth() <= tileSize * 10 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 15 - 20 &&
                        player.getY() + player.getHeight() <= tileSize * 18) &&
                !(player.getX() + player.getWidth() >= tileSize * 14 &&
                        player.getX() + player.getWidth() <= tileSize * 14 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 15 - 20 &&
                        player.getY() + player.getHeight() <= tileSize * 18) &&
                !(player.getX() + player.getWidth() >= tileSize * 18 &&
                        player.getX() + player.getWidth() <= tileSize * 18 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 15 - 20 &&
                        player.getY() + player.getHeight() <= tileSize * 18) &&
                !(player.getX() + player.getWidth() >= tileSize * 22 &&
                        player.getX() + player.getWidth() <= tileSize * 22 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 15 - 20 &&
                        player.getY() + player.getHeight() <= tileSize * 18) &&
                !(player.getX() + player.getWidth() >= tileSize * 26 &&
                        player.getX() + player.getWidth() <= tileSize * 26 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 15 - 20 &&
                        player.getY() + player.getHeight() <= tileSize * 18) &&
                !(player.getX() + player.getWidth() >= tileSize * 32 &&
                        player.getX() + player.getWidth() <= tileSize * 32 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 7 + 20 &&
                        player.getY() + player.getHeight() <= tileSize * 11) &&
                !(player.getX() + player.getWidth() >= tileSize * 38 &&
                        player.getX() + player.getWidth() <= tileSize * 38 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 7 + 20 &&
                        player.getY() + player.getHeight() <= tileSize * 11) &&
                !(player.getX() + player.getWidth() >= tileSize * 34 &&
                        player.getX() + player.getWidth() <= tileSize * 34 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 10 &&
                        player.getY() + player.getHeight() <= tileSize * 15)) {
            return true;
        }
        return false;

    }

    public boolean moveLeft(Rectangle player) {
        if (player.getX() >= tileSize &&
                player.getY() + player.getHeight() >= tileSize * 4 &&
                !(player.getX() >= tileSize * 4 + 10 &&
                        player.getX() <= tileSize * 4 + 20 &&
                        player.getY() + player.getHeight() >= tileSize * 5 - 20 &&
                        player.getY() + player.getHeight() <= tileSize * 8) &&
                !(player.getX() >= tileSize * 8 + 10 &&
                        player.getX() <= tileSize * 8 + 20 &&
                        player.getY() + player.getHeight() >= tileSize * 5 - 20 &&
                        player.getY() + player.getHeight() <= tileSize * 8) &&
                !(player.getX() >= tileSize * 12 + 10 &&
                        player.getX() <= tileSize * 12 + 20 &&
                        player.getY() + player.getHeight() >= tileSize * 5 - 20 &&
                        player.getY() + player.getHeight() <= tileSize * 8) &&
                !(player.getX() >= tileSize * 16 + 10 &&
                        player.getX() <= tileSize * 16 + 20 &&
                        player.getY() + player.getHeight() >= tileSize * 5 - 20 &&
                        player.getY() + player.getHeight() <= tileSize * 8) &&
                !(player.getX() >= tileSize * 20 + 10 &&
                        player.getX() <= tileSize * 20 + 20 &&
                        player.getY() + player.getHeight() >= tileSize * 5 - 20 &&
                        player.getY() + player.getHeight() <= tileSize * 8) &&
                !(player.getX() >= tileSize * 24 + 10 &&
                        player.getX() <= tileSize * 24 + 20 &&
                        player.getY() + player.getHeight() >= tileSize * 5 - 20 &&
                        player.getY() + player.getHeight() <= tileSize * 8) &&
                !(player.getX() >= tileSize * 28 + 10 &&
                        player.getX() <= tileSize * 28 + 20 &&
                        player.getY() + player.getHeight() >= tileSize * 5 - 20 &&
                        player.getY() + player.getHeight() <= tileSize * 8) &&
                !(player.getX() >= tileSize * 4 + 10 &&
                        player.getX() <= tileSize * 4 + 20 &&
                        player.getY() + player.getHeight() >= tileSize * 10 - 20 &&
                        player.getY() + player.getHeight() <= tileSize * 13) &&
                !(player.getX() >= tileSize * 8 + 10 &&
                        player.getX() <= tileSize * 8 + 20 &&
                        player.getY() + player.getHeight() >= tileSize * 10 - 20 &&
                        player.getY() + player.getHeight() <= tileSize * 13) &&
                !(player.getX() >= tileSize * 12 + 10 &&
                        player.getX() <= tileSize * 12 + 20 &&
                        player.getY() + player.getHeight() >= tileSize * 10 - 20 &&
                        player.getY() + player.getHeight() <= tileSize * 13) &&
                !(player.getX() >= tileSize * 16 + 10 &&
                        player.getX() <= tileSize * 16 + 20 &&
                        player.getY() + player.getHeight() >= tileSize * 10 - 20 &&
                        player.getY() + player.getHeight() <= tileSize * 13) &&
                !(player.getX() >= tileSize * 20 + 10 &&
                        player.getX() <= tileSize * 20 + 20 &&
                        player.getY() + player.getHeight() >= tileSize * 10 - 20 &&
                        player.getY() + player.getHeight() <= tileSize * 13) &&
                !(player.getX() >= tileSize * 24 + 10 &&
                        player.getX() <= tileSize * 24 + 20 &&
                        player.getY() + player.getHeight() >= tileSize * 10 - 20 &&
                        player.getY() + player.getHeight() <= tileSize * 13) &&
                !(player.getX() >= tileSize * 28 + 10 &&
                        player.getX() <= tileSize * 28 + 20 &&
                        player.getY() + player.getHeight() >= tileSize * 10 - 20 &&
                        player.getY() + player.getHeight() <= tileSize * 13) &&
                !(player.getX() >= tileSize * 4 + 10 &&
                        player.getX() <= tileSize * 4 + 20 &&
                        player.getY() + player.getHeight() >= tileSize * 15 - 20 &&
                        player.getY() + player.getHeight() <= tileSize * 18) &&
                !(player.getX() >= tileSize * 8 + 10 &&
                        player.getX() <= tileSize * 8 + 20 &&
                        player.getY() + player.getHeight() >= tileSize * 15 - 20 &&
                        player.getY() + player.getHeight() <= tileSize * 18) &&
                !(player.getX() >= tileSize * 12 + 10 &&
                        player.getX() <= tileSize * 12 + 20 &&
                        player.getY() + player.getHeight() >= tileSize * 15 - 20 &&
                        player.getY() + player.getHeight() <= tileSize * 18) &&
                !(player.getX() >= tileSize * 16 + 10 &&
                        player.getX() <= tileSize * 16 + 20 &&
                        player.getY() + player.getHeight() >= tileSize * 15 - 20 &&
                        player.getY() + player.getHeight() <= tileSize * 18) &&
                !(player.getX() >= tileSize * 20 + 10 &&
                        player.getX() <= tileSize * 20 + 20 &&
                        player.getY() + player.getHeight() >= tileSize * 15 - 20 &&
                        player.getY() + player.getHeight() <= tileSize * 18) &&
                !(player.getX() >= tileSize * 24 + 10 &&
                        player.getX() <= tileSize * 24 + 20 &&
                        player.getY() + player.getHeight() >= tileSize * 15 - 20 &&
                        player.getY() + player.getHeight() <= tileSize * 18) &&
                !(player.getX() >= tileSize * 28 + 10 &&
                        player.getX() <= tileSize * 28 + 20 &&
                        player.getY() + player.getHeight() >= tileSize * 15 - 20 &&
                        player.getY() + player.getHeight() <= tileSize * 18) &&
                !(player.getX() >= tileSize * 34 - 10 &&
                        player.getX() <= tileSize * 34 &&
                        player.getY() + player.getHeight() >= tileSize * 7 + 20 &&
                        player.getY() + player.getHeight() <= tileSize * 11) &&
                !(player.getX() >= tileSize * 40 - 10 &&
                        player.getX() <= tileSize * 40 &&
                        player.getY() + player.getHeight() >= tileSize * 7 + 20 &&
                        player.getY() + player.getHeight() <= tileSize * 11) &&
                !(player.getX() >= tileSize * 38 - 10 &&
                        player.getX() <= tileSize * 38 &&
                        player.getY() + player.getHeight() >= tileSize * 10 &&
                        player.getY() + player.getHeight() <= tileSize * 15)) {
            return true;
        }
        return false;
    }

    public boolean closeToRiver(Rectangle player) {
        if (player.getX() + player.getWidth() / 2 >= tileSize * 2 &&
                player.getY() + player.getHeight() <= tileSize * 21 &&
                player.getY() + player.getHeight() >= tileSize * 18) {
            return true;
        }
        return false;
    }

    private int ai;
    private int jey;

    public int getAi() {
        return ai;
    }

    public int getJey() {
        return jey;
    }

    public Mineral objectToCollect(Rectangle player) {
        for (int i = 0; i < 30; i++) {
            for (int j = 0; j < 20; j++) {
                if (cellG[i][j] != null) {
                    if (player.getX() + player.getWidth() / 2 <= cellG[i][j].getX() + cellG[i][j].getWidth() + 30 &&
                            player.getX() + player.getWidth() / 2 >= cellG[i][j].getX() - 30 &&
                            player.getY() + player.getHeight() <= cellG[i][j].getY() + cellG[i][j].getHeight() + 30 &&
                            player.getY() + player.getHeight() >= cellG[i][j].getY() - 30) {
                        if (jungle.getCells()[i][j].getContent() instanceof Mineral) {
                            ai = i;
                            jey = j;
                            return (Mineral) jungle.getCells()[i][j].getContent();
                        }
                    }
                }
            }
        }
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 3; j++) {
                if (contentG[i][j] != null) {
                    if (player.getX() + player.getWidth() / 2 <= contentG[i][j].getX() + contentG[i][j].getWidth() + 30&&
                            player.getX() + player.getWidth() / 2 >= contentG[i][j].getX() - 30&&
                            player.getY() + player.getHeight() <= contentG[i][j].getY() + contentG[i][j].getHeight() + 30 &&
                            player.getY() + player.getHeight() >= contentG[i][j].getY() - 30 && jungle.getContent()[i][j] instanceof Mineral) {
                        ai = i;
                        jey = j;
                        return (Mineral) jungle.getContent()[i][j];
                    }
                }
            }
        }
        return null;
    }

    public void fishing(Item item, boolean[] flags) {
        Fishing fishing = new Fishing();
        if (item instanceof FishingRod) {
            UI.showPopup(fishing.run(game.getPlayer(), (FishingRod) item), root);
        }
        flags[2] = true;
    }

    public void collect(Player player, Mineral mineral, Place place, int i, int j, boolean[] flags) {
        Collect collect = new Collect(new AllOfTools().hand());
        UI.showPopup(collect.run(player, mineral, place, i, j, cellG, root), root);
        flags[2] = true;
    }

    public void collect(Player player, Tool tool, Mineral mineral, Place place, int i, int j, boolean[] flags) {
        Collect collect = new Collect(tool);
        UI.showPopup(collect.run(player, tool, mineral, place, i, j, contentG, root), root);
        flags[2] = true;
    }

    public Group getRoot() {
        return root;
    }

    public Rectangle[][] getCellG() {
        return cellG;
    }
}