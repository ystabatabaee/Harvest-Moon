import java.util.ArrayList;

public class AllOfBackpacks {

    public Backpack mini() {
        return new Backpack(5.0, 1);
    }

    public Backpack small() {
        return new Backpack(10.0, 2);
    }

    public Backpack meduim() {
        return new Backpack(200.0, 3);
    }

    public Backpack large() {
        return new Backpack(50.0, 4);
    }

    public Backpack enormous() {
        return new Backpack(100.0, 5);
    }
}