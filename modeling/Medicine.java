import com.google.common.collect.HashMultiset;
import javafx.scene.image.Image;

import java.util.ArrayList;
import java.util.HashMap;

public class Medicine extends Item {
    public Medicine(String type, HashMap<String, Double> featureChangeRate, double capacity, String name, Probability invalid,
                    double price, ArrayList<Task> task, int maxLevel, int level, boolean eatable, Image picture) {
        super(type, featureChangeRate, capacity, name, invalid, price, task, maxLevel, level,
                eatable, false, new Dissassemblity(0, HashMultiset.<Item>create(), false), picture);
    }

    public Medicine(String name, HashMap<String, Double> featureChangeRate, double price) {
        super("Potion", featureChangeRate, 1, name, new Probability(), price, new ArrayList<>(), 1, 1,
                true, false, new Dissassemblity(0, HashMultiset.<Item>create(), false),
                new Image("pics/medicine/usual.png"));
    }
}