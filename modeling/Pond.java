import java.util.ArrayList;

public class Pond {
    private ArrayList<Task> task;
    public Pond() {
        task = new ArrayList<>();
    }

    public ArrayList<Task> getTask() {
        return task;
    }

    public void setTask(ArrayList<Task> task) {
        this.task = task;
    }
}