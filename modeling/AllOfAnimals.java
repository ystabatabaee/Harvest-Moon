import java.util.ArrayList;

public class AllOfAnimals {

    AllOfAnimalProducts animalProducts = new AllOfAnimalProducts();
    AllOfProducts products = new AllOfProducts();
    AllOfFeatures allOfFeatures = new AllOfFeatures();

    public Animal cow(){
        return new Animal("Cow", new ArrayList<Feature>(){{add(allOfFeatures.health());}}, new ArrayList<Task>(){{}},
                new ArrayList<Product>(){{add(animalProducts.cowMeat());add(animalProducts.milk());}},
                new ArrayList<Item>(){{add(products.alfalfa());}}, 1, 2000);
    }

    public Animal sheep(){
        return new Animal("Sheep", new ArrayList<Feature>(){{add(allOfFeatures.health());}}, new ArrayList<Task>(){{}},
                new ArrayList<Product>(){{add(animalProducts.sheepMeat());add(animalProducts.wool());}},
                new ArrayList<Item>(){{add(products.alfalfa());}}, 3, 1500);
    }

    public Animal fish(){
        return new Animal("Fish", new ArrayList<Feature>(){{add(allOfFeatures.health());}}, new ArrayList<Task>(){{}},
                new ArrayList<Product>(){{add(animalProducts.fishMeat());}}, new ArrayList<Item>(){{}}, 0, 700);

    }

    public Animal chicken(){
        return new Animal("Chicken", new ArrayList<Feature>(){{add(allOfFeatures.health());}}, new ArrayList<Task>(){{}},
                new ArrayList<Product>(){{add(animalProducts.chickenMeat());add(animalProducts.egg());}},
                new ArrayList<Item>(){{add(products.seed());}}, 1, 300);

    }
}
