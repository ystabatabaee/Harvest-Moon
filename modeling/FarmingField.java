import java.util.ArrayList;

public class FarmingField {

    private ArrayList<Task> task;
    private Cell[][] cell = new Cell[3][3];

    public FarmingField() {
        for(int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
                cell[i][j] = new Cell();
        task = new ArrayList<Task>();

    }

    public ArrayList<Task> getTask() {
        return task;
    }

    public void setTask(ArrayList<Task> task) {
        this.task = task;
    }

    public Cell[][] getCell() {
        return cell;
    }

    public void setCell(Cell[][] cell) {
        this.cell = cell;
    }

}