import java.util.ArrayList;

public class AllOFWeathers {

    public Weather spring() {
        return new Weather(0.7, 20, "Spring", new ArrayList<Task>(){{}});
    }

    public Weather summer() {
        return new Weather(0.2, 40, "Summer", new ArrayList<Task>(){{}});
    }

    public Weather fall() {
        return new Weather(0.5, 15, "Fall", new ArrayList<Task>(){{}});
    }

    public Weather winter() {
        return new Weather(0.4, 0, "Winter", new ArrayList<Task>(){{}});
    }

    public Weather tropical() {
        return new Weather(1, 30, "Tropical", new ArrayList<Task>(){{}});
    }
}

// FIXME: 5/6/2017 - Arraylist Task
