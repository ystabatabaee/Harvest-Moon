import com.google.common.collect.HashMultiset;
import com.vdurmont.emoji.EmojiParser;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

public class UI extends Application {

    private static Stage primaryStage;

    private static Scene mainScene;
    private static Scene pauseScene;
    private static Scene networkPauseScene;
    private static Scene settingsScene;
    private static Scene backpackScene;
    private static Scene statusScene;
    private static Scene objectStatusScene;
    private static Scene buyScene;
    private static Scene sellScene;
    private static Scene twoSidedScene;
    private static Scene makeScene;
    private static Scene playerMenuScene;
    private static Scene helpScene;
    private static Scene yesNoScene;
    private static Scene tradeScene;
    private static Scene multiButtonScene;
    private static Scene printScene;
    private static Scene checkShopScene;
    private static Scene customScene;
    private static Scene customShopScene;
    private static Scene customCreationScene;
    private static Scene chatScene;
    private static Scene publicChatScene;
    private static Scene weatherScene;
    private static Scene friendScene;
    private static Scene tableScene;
    private static Scene miniGameScene;

    private static Group mainRoot = new Group();
    private static Group pauseRoot = new Group();
    private static Group networkPauseRoot = new Group();
    private static Group settingsRoot = new Group();
    private static Group backpackRoot = new Group();
    private static Group statusRoot = new Group();
    private static Group objectStatusRoot = new Group();
    private static Group buyRoot = new Group();
    private static Group sellRoot = new Group();
    private static Group twoSidedRoot = new Group();
    private static Group makeRoot = new Group();
    private static Group playerMenuRoot = new Group();
    private static Group helpRoot = new Group();
    private static Group yesNoRoot = new Group();
    private static Group tradeRoot = new Group();
    private static Group multiButtonRoot = new Group();
    private static Group printRoot = new Group();
    private static Group checkShopRoot = new Group();
    private static Group customRoot = new Group();
    private static Group customShopRoot = new Group();
    private static Group weatherRoot = new Group();
    private static Group customCreationRoot = new Group();
    private static Group tableRoot = new Group();
    private static Group miniGameRoot = new Group();
    private static Group jungleRoot = new Group();
    private static Group gymRoot = new Group();
    private static Group farmRoot = new Group();
    private static Group caveRoot = new Group();
    private static Group villageRoot = new Group();
    private static Group cafeRoot = new Group();
    private static Group homeRoot = new Group();
    private static Group clinicRoot = new Group();
    private static Group marketRoot = new Group();
    private static Group laboratoryRoot = new Group();
    private static Group greenhouseRoot = new Group();
    private static Group groceryStoreRoot = new Group();
    private static Group workshopRoot = new Group();
    private static Group barnRoot = new Group();
    private static Group ranchRoot = new Group();
    private static Group butcheryRoot = new Group();
    private static Group generalStoreRoot = new Group();
    private static Group multiPlayerRoot = new Group();
    private static Group hostRoot = new Group();
    private static Group joinRoot = new Group();
    private static GridPane chatRoot = new GridPane();
    private static GridPane publicChatRoot = new GridPane();
    private static Group friendRoot = new Group();
    private static Group choosedRoot = farmRoot;

    static FarmUI farmUI;
    private static CafeUI cafeUI = new CafeUI(cafeRoot);
    private static RanchUI ranchUI;
    private static HomeUI homeUI;
    static GreenhouseUI greenhouseUI;
    private static BarnUI barnUI;
    private static GymUI gymUI;
    private static ClinicUI clinicUI = new ClinicUI(clinicRoot);
    private static GroceryStoreUI groceryStoreUI = new GroceryStoreUI(groceryStoreRoot);
    private static VillageUI villageUI = new VillageUI(villageRoot);
    static JungleUI jungleUI;
    static CaveUI caveUI;
    private static LaboratoryUI laboratoryUI;
    private static MarketUI marketUI = new MarketUI(marketRoot);
    private static WorkshopUI workshopUI;
    private static ButcheryUI butcheryUI = new ButcheryUI(butcheryRoot);
    private static GeneralStoreUI generalStoreUI = new GeneralStoreUI(generalStoreRoot);
    private static ChartUI backpackUI;
    private static ChartUI checkGroceryUI;
    private static ChartUI checkGeneralUI;
    private static ChartUI checkButcheryUI;
    private static ChartUI checkCafeUI;
    private static ChartUI checkClinicUI;
    private static ChartUI checkRanchUI;
    private static ChartUI checkWorkshopUI;
    private static ChartUI checkLaboratoryUI;
    private static ClientUI clientUI = new ClientUI();
    private static ServerUI serverUI = new ServerUI();

    private static Scene farmScene;
    private static Scene cafeScene;
    private static Scene ranchScene;
    private static Scene homeScene;
    private static Scene greenhouseScene;
    private static Scene barnScene;
    private static Scene gymScene;
    private static Scene clinicScene;
    private static Scene groceryStoreScene;
    private static Scene villageScene;
    private static Scene jungleScene;
    private static Scene caveScene;
    private static Scene marketScene;
    private static Scene laboratoryScene;
    private static Scene workshopScene;
    private static Scene butcheryScene;
    private static Scene generalStoreScene;
    private static Scene multiPlayerScene;
    private static Scene hostScene;
    private static Scene joinScene;


    private static boolean[] startProcess = {false};
    private static Server server;
    private static Thread serverThread;
    private static Client client;
    private static Thread clientThread;
    private static int[] size = new int[1];
    private static boolean isServer = false;
    private static boolean isMultiplayer = false;


    private static Image[] playerUpPics = new Image[9];
    private static Image[] playerDownPics = new Image[9];
    private static Image[] playerRightPics = new Image[9];
    private static Image[] playerLeftPics = new Image[9];
    private static Rectangle player;

    private static double timeMillis;
    private static Date gameDate = new Date();
    private static ArrayList<Text> timeText = new ArrayList<>();
    private static ArrayList<Text> healthText = new ArrayList<>();
    private static ArrayList<Text> satietyText = new ArrayList<>();
    private static ArrayList<Text> energyText = new ArrayList<>();
    private static ArrayList<Text> staminaText = new ArrayList<>();
    static Text popupText;
    static Image popupWindowPic;
    static Rectangle popupWindow;

    private static boolean pressed;
    private static Item choosedItem;
    private static String choosedButton;
    private static Mission choosedMission;
    private static Train choosedTrain;
    private static boolean yesNO;
    static Option option = new Option();
    private static int numver = 0;
    private static int nearAnyPlayer = -1;
    static int language = 0;


    static Game game;

    static ArrayList<Tool> tools;
    static ArrayList<Fruit> fruits;
    static ArrayList<Product> products;
    static ArrayList<AnimalProduct> animalProducts;
    static ArrayList<Food> foods;
    static ArrayList<Weather> weathers;
    static ArrayList<Crop> crops;
    static ArrayList<Seed> seeds;
    static ArrayList<GardenTree> gardenTrees;
    static ArrayList<Train> trains;
    static ArrayList<Mineral> minerals;
    static ArrayList<Item> items;
    static ArrayList<Animal> animals;
    static ArrayList<Tool> cookingUtensil;
    static boolean[] taskBoolean;

    static boolean isSatiety0 = false;
    private static Timeline timeCalculator = new Timeline(new KeyFrame(new Duration(50d), event -> {
        Time newTime = gameDate.getTime();
        String health = "";
        String energy = "";
        String satiety = "";
        String stamin = "";
        newTime.setSecond(newTime.getSecond() + 1);
        for (int i = 0; i < 4; i++) {
            if (game.getPlayer().getFeature().get(i).getName().equalsIgnoreCase("Health")) {
                game.getPlayer().getFeature().get(i).setCurrent(Math.max(game.getPlayer().getFeature().get(i).getCurrent() -
                        1.0 / 12.0 * 1.0 / 60.0, 0));
                if (isSatiety0)
                    game.getPlayer().getFeature().get(i).setCurrent(Math.max(game.getPlayer().getFeature().get(i).getCurrent() -
                            1.0 / 12.0 * 1.0 / 30.0, 0));
                health = Double.toString(game.getPlayer().getFeature().get(i).getCurrent());
            }
            if (game.getPlayer().getFeature().get(i).getName().equalsIgnoreCase("Stamina")) {
                game.getPlayer().getFeature().get(i).setCurrent(Math.max(game.getPlayer().getFeature().get(i).getCurrent() -
                        1.0 / 60.0 * 1.0 / 60.0, 0));
                stamin = Double.toString(game.getPlayer().getFeature().get(i).getCurrent());
            }
            if (game.getPlayer().getFeature().get(i).getName().equalsIgnoreCase("Energy")) {
                game.getPlayer().getFeature().get(i).setCurrent(Math.max(game.getPlayer().getFeature().get(i).getCurrent() -
                        1.0 / 3.0 * 1.0 / 60.0, 0));
                energy = Double.toString(game.getPlayer().getFeature().get(i).getCurrent());
            }
            if (game.getPlayer().getFeature().get(i).getName().equalsIgnoreCase("Satiety")) {
                game.getPlayer().getFeature().get(i).setCurrent(Math.max(game.getPlayer().getFeature().get(i).getCurrent() -
                        1.25 * 1.0 / 60.0, 0));
                satiety = Double.toString(game.getPlayer().getFeature().get(i).getCurrent());
                if (game.getPlayer().getFeature().get(i).getCurrent() == 0)
                    isSatiety0 = true;
                else isSatiety0 = false;
            }
        }
        if (newTime.getSecond() == 60) {
            newTime.setSecond(0);
            newTime.setMinute(newTime.getMinute() + 1);
            if (newTime.getMinute() == 60) {
                newTime.setMinute(0);
                newTime.setHour(newTime.getHour() + 1);
            }
        }
        if (newTime.getHour() == 23) {
            Faint faint = new Faint();
            UI.showPopup(faint.run(game.getPlayer(), gameDate, game), choosedRoot);
        }
        for (Feature feature : game.getPlayer().getFeature()) {
            if (feature.getCurrent() < 1 && (feature.getName().equalsIgnoreCase("Energy") || feature.getName().equalsIgnoreCase("Health"))) {
                Faint faint = new Faint();
                UI.showPopup(faint.run(game.getPlayer(), gameDate, game), choosedRoot);
            }
        }
        /*addToRoot(healthText, health, 10, 10);
        addToRoot(healthText, energy, 10, 30);
        addToRoot(healthText, stamin, 10, 50);
        addToRoot(healthText, satiety, 10, 70);
        if (!isMultiplayer) {
            timeText.add(buttonDesigner(newTime.getHour() + " : " + newTime.getMinute() + " : " + newTime.getSecond(),
                    750, 70, 50, Color.SADDLEBROWN));
            barnRoot.getChildren().add(timeText.get(timeText.size() - 1));
            timeText.add(buttonDesigner(newTime.getHour() + " : " + newTime.getMinute() + " : " + newTime.getSecond(),
                    750, 70, 50, Color.SADDLEBROWN));
            butcheryRoot.getChildren().add(timeText.get(timeText.size() - 1));
            timeText.add(buttonDesigner(newTime.getHour() + " : " + newTime.getMinute() + " : " + newTime.getSecond(),
                    750, 70, 50, Color.SADDLEBROWN));
            cafeRoot.getChildren().add(timeText.get(timeText.size() - 1));
            timeText.add(buttonDesigner(newTime.getHour() + " : " + newTime.getMinute() + " : " + newTime.getSecond(),
                    750, 70, 50, Color.SADDLEBROWN));
            caveRoot.getChildren().add(timeText.get(timeText.size() - 1));
            timeText.add(buttonDesigner(newTime.getHour() + " : " + newTime.getMinute() + " : " + newTime.getSecond(),
                    750, 70, 50, Color.SADDLEBROWN));
            clinicRoot.getChildren().add(timeText.get(timeText.size() - 1));
            timeText.add(buttonDesigner(newTime.getHour() + " : " + newTime.getMinute() + " : " + newTime.getSecond(),
                    750, 70, 50, Color.SADDLEBROWN));
            farmRoot.getChildren().add(timeText.get(timeText.size() - 1));
            timeText.add(buttonDesigner(newTime.getHour() + " : " + newTime.getMinute() + " : " + newTime.getSecond(),
                    750, 70, 50, Color.SADDLEBROWN));
            generalStoreRoot.getChildren().add(timeText.get(timeText.size() - 1));
            timeText.add(buttonDesigner(newTime.getHour() + " : " + newTime.getMinute() + " : " + newTime.getSecond(),
                    750, 70, 50, Color.SADDLEBROWN));
            greenhouseRoot.getChildren().add(timeText.get(timeText.size() - 1));
            timeText.add(buttonDesigner(newTime.getHour() + " : " + newTime.getMinute() + " : " + newTime.getSecond(),
                    750, 70, 50, Color.SADDLEBROWN));
            groceryStoreRoot.getChildren().add(timeText.get(timeText.size() - 1));
            timeText.add(buttonDesigner(newTime.getHour() + " : " + newTime.getMinute() + " : " + newTime.getSecond(),
                    750, 70, 50, Color.SADDLEBROWN));
            homeRoot.getChildren().add(timeText.get(timeText.size() - 1));
            timeText.add(buttonDesigner(newTime.getHour() + " : " + newTime.getMinute() + " : " + newTime.getSecond(),
                    750, 70, 50, Color.SADDLEBROWN));
            jungleRoot.getChildren().add(timeText.get(timeText.size() - 1));
            timeText.add(buttonDesigner(newTime.getHour() + " : " + newTime.getMinute() + " : " + newTime.getSecond(),
                    750, 70, 50, Color.SADDLEBROWN));
            laboratoryRoot.getChildren().add(timeText.get(timeText.size() - 1));
            timeText.add(buttonDesigner(newTime.getHour() + " : " + newTime.getMinute() + " : " + newTime.getSecond(),
                    750, 70, 50, Color.SADDLEBROWN));
            marketRoot.getChildren().add(timeText.get(timeText.size() - 1));
            timeText.add(buttonDesigner(newTime.getHour() + " : " + newTime.getMinute() + " : " + newTime.getSecond(),
                    750, 70, 50, Color.SADDLEBROWN));
            ranchRoot.getChildren().add(timeText.get(timeText.size() - 1));
            timeText.add(buttonDesigner(newTime.getHour() + " : " + newTime.getMinute() + " : " + newTime.getSecond(),
                    750, 70, 50, Color.SADDLEBROWN));
            villageRoot.getChildren().add(timeText.get(timeText.size() - 1));
            timeText.add(buttonDesigner(newTime.getHour() + " : " + newTime.getMinute() + " : " + newTime.getSecond(),
                    750, 70, 50, Color.SADDLEBROWN));
            workshopRoot.getChildren().add(timeText.get(timeText.size() - 1));
            numver++;
        }*/
        /*if (numver != 0) {
            remove(timeText);
            remove(healthText);
            remove(satietyText);
            remove(staminaText);
            remove(energyText);
        }*/
    }));

    private static void remove(ArrayList<Text> timeText) {
        if (numver != 0 && !isMultiplayer) {
            workshopRoot.getChildren().remove(timeText.get(timeText.size() - 1));
            timeText.remove(timeText.size() - 1);
            villageRoot.getChildren().remove(timeText.get(timeText.size() - 1));
            timeText.remove(timeText.size() - 1);
            ranchRoot.getChildren().remove(timeText.get(timeText.size() - 1));
            timeText.remove(timeText.size() - 1);
            marketRoot.getChildren().remove(timeText.get(timeText.size() - 1));
            timeText.remove(timeText.size() - 1);
            laboratoryRoot.getChildren().remove(timeText.get(timeText.size() - 1));
            timeText.remove(timeText.size() - 1);
            jungleRoot.getChildren().remove(timeText.get(timeText.size() - 1));
            timeText.remove(timeText.size() - 1);
            homeRoot.getChildren().remove(timeText.get(timeText.size() - 1));
            timeText.remove(timeText.size() - 1);
            groceryStoreRoot.getChildren().remove(timeText.get(timeText.size() - 1));
            timeText.remove(timeText.size() - 1);
            greenhouseRoot.getChildren().remove(timeText.get(timeText.size() - 1));
            timeText.remove(timeText.size() - 1);
            generalStoreRoot.getChildren().remove(timeText.get(timeText.size() - 1));
            timeText.remove(timeText.size() - 1);
            farmRoot.getChildren().remove(timeText.get(timeText.size() - 1));
            timeText.remove(timeText.size() - 1);
            clinicRoot.getChildren().remove(timeText.get(timeText.size() - 1));
            timeText.remove(timeText.size() - 1);
            caveRoot.getChildren().remove(timeText.get(timeText.size() - 1));
            timeText.remove(timeText.size() - 1);
            cafeRoot.getChildren().remove(timeText.get(timeText.size() - 1));
            timeText.remove(timeText.size() - 1);
            butcheryRoot.getChildren().remove(timeText.get(timeText.size() - 1));
            timeText.remove(timeText.size() - 1);
            barnRoot.getChildren().remove(timeText.get(timeText.size() - 1));
            timeText.remove(timeText.size() - 1);
        }
    }

    private static void addToRoot(ArrayList<Text> timeText, String text, int x, int y) {
        if (!isMultiplayer) {
            timeText.add(buttonDesigner(text, x, y, 20, Color.SADDLEBROWN));
            barnRoot.getChildren().add(timeText.get(timeText.size() - 1));
            timeText.add(buttonDesigner(text, x, y, 20, Color.SADDLEBROWN));
            butcheryRoot.getChildren().add(timeText.get(timeText.size() - 1));
            timeText.add(buttonDesigner(text, x, y, 20, Color.SADDLEBROWN));
            cafeRoot.getChildren().add(timeText.get(timeText.size() - 1));
            timeText.add(buttonDesigner(text, x, y, 20, Color.SADDLEBROWN));
            caveRoot.getChildren().add(timeText.get(timeText.size() - 1));
            timeText.add(buttonDesigner(text, x, y, 20, Color.SADDLEBROWN));
            clinicRoot.getChildren().add(timeText.get(timeText.size() - 1));
            timeText.add(buttonDesigner(text, x, y, 20, Color.SADDLEBROWN));
            farmRoot.getChildren().add(timeText.get(timeText.size() - 1));
            timeText.add(buttonDesigner(text, x, y, 20, Color.SADDLEBROWN));
            generalStoreRoot.getChildren().add(timeText.get(timeText.size() - 1));
            timeText.add(buttonDesigner(text, x, y, 20, Color.SADDLEBROWN));
            greenhouseRoot.getChildren().add(timeText.get(timeText.size() - 1));
            timeText.add(buttonDesigner(text, x, y, 20, Color.SADDLEBROWN));
            groceryStoreRoot.getChildren().add(timeText.get(timeText.size() - 1));
            timeText.add(buttonDesigner(text, x, y, 20, Color.SADDLEBROWN));
            homeRoot.getChildren().add(timeText.get(timeText.size() - 1));
            timeText.add(buttonDesigner(text, x, y, 20, Color.SADDLEBROWN));
            jungleRoot.getChildren().add(timeText.get(timeText.size() - 1));
            timeText.add(buttonDesigner(text, x, y, 20, Color.SADDLEBROWN));
            laboratoryRoot.getChildren().add(timeText.get(timeText.size() - 1));
            timeText.add(buttonDesigner(text, x, y, 20, Color.SADDLEBROWN));
            marketRoot.getChildren().add(timeText.get(timeText.size() - 1));
            timeText.add(buttonDesigner(text, x, y, 20, Color.SADDLEBROWN));
            ranchRoot.getChildren().add(timeText.get(timeText.size() - 1));
            timeText.add(buttonDesigner(text, x, y, 20, Color.SADDLEBROWN));
            villageRoot.getChildren().add(timeText.get(timeText.size() - 1));
            timeText.add(buttonDesigner(text, x, y, 20, Color.SADDLEBROWN));
            workshopRoot.getChildren().add(timeText.get(timeText.size() - 1));
            numver++;
        }
    }

    private static ArrayList<String> nameOfTasks;


    private static Rectangle rectangleBuilder(double width, double height, Paint fill, double setX, double setY) {
        Rectangle rectangle = new Rectangle(width, height, fill);
        rectangle.setX(setX);
        rectangle.setY(setY);
        return rectangle;
    }

    public static Text buttonDesigner(String name, double x, double y, double fontSize, Color color) {
        Text text = new Text(x, y, name);
        text.setFont(Font.font("Comic Sans MS", fontSize));
        text.setFill(Color.BLACK);
        text.setStroke(color);
        text.setOnMouseEntered(event -> text.setFill(Color.DARKGREY));
        text.setOnMouseExited(event -> text.setFill(Color.BLACK));
        return text;
    }

    public static ComboBox<String> comboBoxDesigner(String name, double x, double y, Color color) {
        ComboBox<String> comboBox = new ComboBox<>();
        comboBox.setValue(name);
        comboBox.setStyle("-fx-font: 17px \"Comic Sans MS\";");
        comboBox.setBackground(new Background(new BackgroundFill(color, CornerRadii.EMPTY, Insets.EMPTY)));
        comboBox.relocate(x, y);
        return comboBox;
    }

    private static TextField textFieldDesigner(double x, double y, double width, double fontsize, Color color) {
        TextField textField = new TextField();
        textField.setFont(Font.font("Comic Sans MS", fontsize));
        textField.setMaxWidth(width);
        textField.relocate(x, y);
        textField.setText("0");
        textField.setBackground(new Background(new BackgroundFill(color, CornerRadii.EMPTY, Insets.EMPTY)));
        return textField;
    }

    private static CheckBox checkBoxDesigner(String name, double x, double y, double fontsize) {
        CheckBox checkB = new CheckBox(name);
        checkB.setFont(Font.font("Comic Sans MS", fontsize));
        checkB.relocate(x, y);
        return checkB;
    }


    public static void main(String[] args) throws IllegalAccessException, InstantiationException, InvocationTargetException {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws IllegalAccessException, InvocationTargetException, InstantiationException {
        initializeGameObjects();
        game = new Game();
        Image backgroundImage = new Image("pics/menus/menu.png");
        Image backpackImage = new Image("pics/menus/backpack.png");
        popupText = new Text();
        popupWindowPic = new Image("pics/icons/popUp.png");
        popupWindow = rectangleBuilder(500, 60, new ImagePattern(popupWindowPic), 300, 30);

        jungleUI = new JungleUI(jungleRoot, game);
        caveUI = new CaveUI(caveRoot, game);
        farmUI = new FarmUI(farmRoot, game);
        ranchUI = new RanchUI(ranchRoot, game.getVillage().getRanch());
        barnUI = new BarnUI(barnRoot, game.getFarm().getBarn());
        gymUI = new GymUI(gymRoot, game.getVillage().getGym());
        workshopUI = new WorkshopUI(workshopRoot, game.getVillage().getWorkshop());
        laboratoryUI = new LaboratoryUI(laboratoryRoot, game.getVillage().getLaboratory());
        backpackUI = new ChartUI(backpackRoot, game.getPlayer().getBackpack().getItem(), "Backpack");
        checkGroceryUI = new ChartUI(checkShopRoot, game.getVillage().getMarket().getShop().get(0).getGoods(), "Grocery");
        checkGeneralUI = new ChartUI(checkShopRoot, game.getVillage().getMarket().getShop().get(2).getGoods(), "General");
        checkButcheryUI = new ChartUI(checkShopRoot, game.getVillage().getMarket().getShop().get(1).getGoods(), "Butchery");
        checkCafeUI = new ChartUI(checkShopRoot, game.getVillage().getCafe().getGoods(), "Cafe");
        checkClinicUI = new ChartUI(checkShopRoot, game.getVillage().getClinic().getGoods(), "Clinic");
        checkRanchUI = new ChartUI(checkShopRoot, game.getVillage().getRanch().getGoods(), "Ranch");
        checkWorkshopUI = new ChartUI(checkShopRoot, game.getVillage().getWorkshop().getTool(), "Workshop");
        checkLaboratoryUI = new ChartUI(checkShopRoot, game.getVillage().getLaboratory().getGoods(), "Laboratory");
        greenhouseUI = new GreenhouseUI(greenhouseRoot, game.getFarm());
        homeUI = new HomeUI(homeRoot, game.getFarm());

        gameDate.setSeason(new Season("Spring", new ArrayList<Task>(), new AllOFWeathers().spring()));

        timeCalculator.setCycleCount(Animation.INDEFINITE);

        farmScene = farmUI.sceneBuilder(primaryStage);
        cafeScene = cafeUI.sceneBuilder(primaryStage);
        ranchScene = ranchUI.sceneBuilder(primaryStage);
        homeScene = homeUI.sceneBuilder(primaryStage);
        greenhouseScene = greenhouseUI.sceneBuilder(primaryStage);
        barnScene = barnUI.sceneBuilder(primaryStage);
        gymScene = gymUI.sceneBuilder(primaryStage);
        clinicScene = clinicUI.sceneBuilder(primaryStage);
        groceryStoreScene = groceryStoreUI.sceneBuilder(primaryStage);
        villageScene = villageUI.sceneBuilder(primaryStage);
        jungleScene = jungleUI.sceneBuilder(primaryStage);
        caveScene = caveUI.sceneBuilder(primaryStage);
        marketScene = marketUI.sceneBuilder(primaryStage);
        laboratoryScene = laboratoryUI.sceneBuilder(primaryStage);
        workshopScene = workshopUI.sceneBuilder(primaryStage);
        butcheryScene = butcheryUI.sceneBuilder(primaryStage);
        generalStoreScene = generalStoreUI.sceneBuilder(primaryStage);

        miniGameScene = new Scene(miniGameRoot, 1000, 700, Color.BLACK);
        mainScene = new Scene(mainRoot, 1000, 700, new ImagePattern(backgroundImage));
        pauseScene = new Scene(pauseRoot, 1000, 700, new ImagePattern(backgroundImage));
        settingsScene = new Scene(settingsRoot, 1000, 700, new ImagePattern(backgroundImage));
        networkPauseScene = new Scene(networkPauseRoot, 1000, 700, new ImagePattern(backgroundImage));
        customScene = new Scene(customRoot, 1000, 700, new ImagePattern(backpackImage));
        customShopScene = new Scene(customShopRoot, 1000, 700, new ImagePattern(backpackImage));
        customCreationScene = new Scene(customCreationRoot, 1000, 700, new ImagePattern(backpackImage));
        weatherScene = new Scene(weatherRoot, 1000, 700, new ImagePattern(backpackImage));
        statusScene = new Scene(statusRoot, 1000, 700, new ImagePattern(backpackImage));
        objectStatusScene = new Scene(objectStatusRoot, 1000, 700, new ImagePattern(backpackImage));
        buyScene = new Scene(buyRoot, 1000, 700, new ImagePattern(backpackImage));
        sellScene = new Scene(sellRoot, 1000, 700, new ImagePattern(backpackImage));
        twoSidedScene = new Scene(twoSidedRoot, 1000, 700, new ImagePattern(backpackImage));
        makeScene = new Scene(makeRoot, 1000, 700, new ImagePattern(backpackImage));
        playerMenuScene = new Scene(playerMenuRoot, 1000, 700, new ImagePattern(backpackImage));
        helpScene = new Scene(helpRoot, 1000, 700, new ImagePattern(backpackImage));
        yesNoScene = new Scene(yesNoRoot, 1000, 700, new ImagePattern(backpackImage));
        tradeScene = new Scene(tradeRoot, 1000, 700, new ImagePattern(backpackImage));
        multiButtonScene = new Scene(multiButtonRoot, 1000, 700, new ImagePattern(backpackImage));
        printScene = new Scene(printRoot, 1000, 700, new ImagePattern(backpackImage));
        checkShopScene = new Scene(checkShopRoot, 1000, 700, new ImagePattern(backpackImage));
        backpackScene = new Scene(backpackRoot, 1000, 700, new ImagePattern(backpackImage));
        multiPlayerScene = new Scene(multiPlayerRoot, 1000, 700, new ImagePattern(backgroundImage));
        hostScene = new Scene(hostRoot, 1000, 700, new ImagePattern(backgroundImage));
        joinScene = new Scene(joinRoot, 1000, 700, new ImagePattern(backgroundImage));
        chatScene = new Scene(chatRoot, 1000, 700, new ImagePattern(backgroundImage));
        publicChatScene = new Scene(publicChatRoot, 1000, 700, new ImagePattern(backgroundImage));
        friendScene = new Scene(friendRoot, 1000, 700, new ImagePattern(backpackImage));
        tableScene = new Scene(tableRoot, 1000, 700, new ImagePattern(backpackImage));

        Class playerImagesClass = AllOfPlayerImages.class;
        Object obj = playerImagesClass.newInstance();
        Method[] playerImages = playerImagesClass.getDeclaredMethods();
        for (Method image : playerImages) {
            Image actualImage = (Image) image.invoke(obj);
            StringBuilder numBuilder = new StringBuilder();
            if (image.getName().contains("up")) {
                numBuilder.append(image.getName().charAt(2));
                String num = numBuilder.toString();
                playerUpPics[Integer.parseInt(num) - 1] = actualImage;
            } else if (image.getName().contains("down")) {
                numBuilder.append(image.getName().charAt(4));
                String num = numBuilder.toString();
                playerDownPics[Integer.parseInt(num) - 1] = actualImage;
            } else if (image.getName().contains("left")) {
                numBuilder.append(image.getName().charAt(4));
                String num = numBuilder.toString();
                playerLeftPics[Integer.parseInt(num) - 1] = actualImage;
            } else if (image.getName().contains("right")) {
                numBuilder.append(image.getName().charAt(5));
                String num = numBuilder.toString();
                playerRightPics[Integer.parseInt(num) - 1] = actualImage;
            }
        }
        UI.primaryStage = primaryStage;
        sceneHandler();
        sceneBuilder();
        primaryStage.setScene(mainScene);
        primaryStage.setTitle("AP PROJECT :D");
        primaryStage.show();
    }

    public static void initializeGameObjects() throws IllegalAccessException, InstantiationException {
        tools = new ArrayList<>();
        Class toolClass = AllOfTools.class;
        Object obj1 = toolClass.newInstance();
        Method[] methods = toolClass.getDeclaredMethods();
        for (Method method : methods) {
            try {
                tools.add((Tool) method.invoke(obj1));
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        fruits = new ArrayList<>();
        Class fruitClass = AllOfFruits.class;
        Object obj2 = fruitClass.newInstance();
        methods = fruitClass.getDeclaredMethods();
        for (Method method : methods) {
            try {
                fruits.add((Fruit) method.invoke(obj2));
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        foods = new ArrayList<>();
        Class foodClass = AllOfFoods.class;
        Object obj3 = foodClass.newInstance();
        methods = foodClass.getDeclaredMethods();
        for (Method method : methods) {
            try {
                foods.add((Food) method.invoke(obj3));
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        weathers = new ArrayList<>();
        Class weathersClass = AllOFWeathers.class;
        obj3 = weathersClass.newInstance();
        methods = weathersClass.getDeclaredMethods();
        for (Method method : methods) {
            try {
                weathers.add((Weather) method.invoke(obj3));
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        seeds = new ArrayList<>();
        Class seedClass = AllOfSeeds.class;
        obj3 = seedClass.newInstance();
        methods = seedClass.getDeclaredMethods();
        for (Method method : methods) {
            try {
                seeds.add((Seed) method.invoke(obj3));
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        crops = new ArrayList<>();
        Class cropClass = AllOfCrops.class;
        obj3 = cropClass.newInstance();
        methods = cropClass.getDeclaredMethods();
        for (Method method : methods) {
            try {
                crops.add((Crop) method.invoke(obj3));
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        gardenTrees = new ArrayList<>();
        Class treeClass = AllOfGardenTrees.class;
        obj3 = treeClass.newInstance();
        methods = treeClass.getDeclaredMethods();
        for (Method method : methods) {
            try {
                gardenTrees.add((GardenTree) method.invoke(obj3));
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        trains = new ArrayList<>();
        Class trainClass = AllOfTrains.class;
        obj3 = trainClass.newInstance();
        methods = trainClass.getDeclaredMethods();
        for (Method method : methods) {
            try {
                trains.add((Train) method.invoke(obj3));
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        products = new ArrayList<>();
        Class productClass = AllOfProducts.class;
        obj3 = productClass.newInstance();
        methods = productClass.getDeclaredMethods();
        for (Method method : methods) {
            try {
                products.add((Product) method.invoke(obj3));
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }

        animalProducts = new ArrayList<>();
        Class animalProductClass = AllOfAnimalProducts.class;
        obj3 = animalProductClass.newInstance();
        methods = animalProductClass.getDeclaredMethods();
        for (Method method : methods) {
            try {
                animalProducts.add((AnimalProduct) method.invoke(obj3));
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }

        minerals = new ArrayList<>();
        Class mineralClass = AllOfMinerals.class;
        obj3 = mineralClass.newInstance();
        methods = mineralClass.getDeclaredMethods();
        for (Method method : methods) {
            try {
                minerals.add((Mineral) method.invoke(obj3));
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        animals = new ArrayList<>();
        Class animalClass = AllOfAnimals.class;
        obj3 = animalClass.newInstance();
        methods = animalClass.getDeclaredMethods();
        for (Method method : methods) {
            try {
                animals.add((Animal) method.invoke(obj3));
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }

        cookingUtensil = new ArrayList<>();
        Class cookingClass = AllOfTools.class;
        obj3 = cookingClass.newInstance();
        methods = cookingClass.getDeclaredMethods();
        for (Method method : methods) {
            try {
                Tool tool = (Tool) method.invoke(obj3);
                if (tool.getType().contains("Cook"))
                    cookingUtensil.add(tool);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        nameOfTasks = new ArrayList<>();
        nameOfTasks.add("Buy");
        nameOfTasks.add("Collect");
        nameOfTasks.add("Cook");
        nameOfTasks.add("Disassemble");
        nameOfTasks.add("Drop");
        nameOfTasks.add("Faint");
        nameOfTasks.add("Feed");
        nameOfTasks.add("Fill");
        nameOfTasks.add("Fishing");
        nameOfTasks.add("GetProduct");
        nameOfTasks.add("Harvest");
        nameOfTasks.add("HealUp");
        nameOfTasks.add("Make");
        nameOfTasks.add("Plant Seeds");
        nameOfTasks.add("Plow");
        nameOfTasks.add("Put");
        nameOfTasks.add("Sell");
        nameOfTasks.add("Take");
        nameOfTasks.add("Use");
        nameOfTasks.add("Move Right");
        nameOfTasks.add("Move Left");
        nameOfTasks.add("Move Up");
        nameOfTasks.add("Move Down");

        taskBoolean = new boolean[nameOfTasks.size()];

        items = new ArrayList<>();
        items.addAll(products);
        items.addAll(tools);
        items.addAll(fruits);
        items.addAll(minerals);
    }

    public static void missionHandler(String task, String object) {
        outer:
        for (Mission mission : game.getPlayer().getMission()) {
            inner:
            for (Tetrad tetrad : mission.getTetrad()) {
                if (tetrad.getObejct().toLowerCase().equals(object.toLowerCase()) && tetrad.getTask().toLowerCase().equals(task.toLowerCase())) {
                    if (tetrad.getHappened() < tetrad.getCount()) {
                        tetrad.setHappened(tetrad.getHappened() + 1);
                        break outer;
                    }
                }
            }
        }
    }

    public static void sceneHandler() {
        jungleSceneHandler(jungleRoot);
        farmSceneHandler(farmRoot);
        caveSceneHandler(caveRoot);
        villageSceneHandler(villageRoot);
        cafeSceneHandler(cafeRoot);
        homeSceneHandler(homeRoot);
        clinicSceneHandler(clinicRoot);
        marketSceneHandler(marketRoot);
        laboratorySceneHandler(laboratoryRoot);
        greenhouseSceneHandler(greenhouseRoot);
        groceryStoreSceneHandler(groceryStoreRoot);
        workshopSceneHandler(workshopRoot);
        barnSceneHandler(barnRoot);
        ranchSceneHandler(ranchRoot);
        butcherySceneHandler(butcheryRoot);
        generalStoreSceneHandler(generalStoreRoot);
        gymSceneHandler(gymRoot);
    }

    public static void sceneBuilder() {
        mainSceneBuilder(mainRoot);
        multiPlayerSceneBuilder(multiPlayerRoot);
        customSceneBuilder(customRoot);
        customShopSceneBuilder(customShopRoot);
        hostSceneBuilder(hostRoot);
        joinSceneBuilder(joinRoot);
        weatherSceneBuilder(weatherRoot, mainScene);
        miniGameSceneBuilder(miniGameRoot);
        //friendSceneBuilder(friendRoot, playerMenuScene);
    }

    public static void multiPlayerSceneBuilder(Group root) {
        Text host = buttonDesigner("Host", 420, 330, 70, Color.SADDLEBROWN);
        Text join = buttonDesigner("Join", 420, 530, 70, Color.SADDLEBROWN);
        root.getChildren().addAll(host, join);
        host.setOnMouseClicked(event -> primaryStage.setScene(hostScene));
        join.setOnMouseClicked(event -> primaryStage.setScene(joinScene));
    }

    static boolean isAliveH = false;
    static boolean isAliveE = false;
    static boolean isAliveS = false;
    static Process processH;
    static Process processE;
    static Process processS;


    static Timeline heal = new Timeline(new KeyFrame(new Duration(150), event -> {
        if (processH != null && processH.isAlive()) {
            for (int i = 0; i < game.getPlayer().getFeature().size(); i++) {
                if (game.getPlayer().getFeature().get(i).equals("Health")) {
                    game.getPlayer().getFeature().get(i).setCurrent(Math.min(game.getPlayer().getFeature().get(i).getMaxCurrent(),
                            game.getPlayer().getFeature().get(i).getCurrent() + 1.65));
                }
            }
        } else if (processE != null && processE.isAlive()) {
            for (int i = 0; i < game.getPlayer().getFeature().size(); i++) {
                if (game.getPlayer().getFeature().get(i).equals("Energy")) {
                    game.getPlayer().getFeature().get(i).setCurrent(Math.min(game.getPlayer().getFeature().get(i).getMaxCurrent(),
                            game.getPlayer().getFeature().get(i).getCurrent() + 2.2));
                }
            }
        } else if (processS != null && processS.isAlive()) {
            for (int i = 0; i < game.getPlayer().getFeature().size(); i++) {
                if (game.getPlayer().getFeature().get(i).equals("Satiety")) {
                    game.getPlayer().getFeature().get(i).setCurrent(Math.min(game.getPlayer().getFeature().get(i).getMaxCurrent(),
                            game.getPlayer().getFeature().get(i).getCurrent() + 4));
                }
            }
        } else {
            stopHeal();
        }
    }));

    public static void stopHeal() {
        heal.stop();
    }


    public static void miniGameSceneBuilder(Group root) {
        heal.setCycleCount(Animation.INDEFINITE);
        Text[] name = new Text[10];
        name[0] = buttonDesigner("3 FOOT NINJA(100 GIL)`H`", 50, 160, 25, Color.SADDLEBROWN);
        name[1] = buttonDesigner("A DAY OF SLACKING(200 GIL)`H`", 450, 160, 25, Color.SADDLEBROWN);
        name[2] = buttonDesigner("BUG ON A WIRE(173 GIL)`E`", 50, 260, 25, Color.SADDLEBROWN);
        name[3] = buttonDesigner("CELL OUT(26 GIL)`E`", 450, 260, 25, Color.SADDLEBROWN);
        name[4] = buttonDesigner("CUBE BUSTER(150 GIL)`E`", 50, 360, 25, Color.SADDLEBROWN);
        name[5] = buttonDesigner("GYROBALL(264 GIL)`H`", 450, 360, 25, Color.SADDLEBROWN);
        name[6] = buttonDesigner("MAGIC BALLS(110 GIL)`S`", 50, 460, 25, Color.SADDLEBROWN);
        name[7] = buttonDesigner("PENGAPOP(400 GIL)`H`", 450, 460, 25, Color.SADDLEBROWN);
        name[8] = buttonDesigner("SHRUNKEN HEADS(50 GIL)`S`", 50, 560, 25, Color.SADDLEBROWN);
        name[9] = buttonDesigner("ZED(500 GIL)`H`", 450, 560, 25, Color.SADDLEBROWN);
        Text back = buttonDesigner("Back", 50, 50, 20, Color.SADDLEBROWN);
        Text title = buttonDesigner("MINI GAMES", 380, 60, 40, Color.SADDLEBROWN);
        for (int i = 0; i < 10; i++) {
            root.getChildren().add(name[i]);
        }
        root.getChildren().addAll(back, title);
        back.setOnMouseClicked(event -> primaryStage.setScene(cafeScene));

        name[0].setOnMouseClicked(event -> {
            game.getPlayer().setMoney(game.getPlayer().getMoney() - 100);
            try {
                processH = Runtime.getRuntime().exec("games/Miniclip-Game-3-foot-ninja.exe");
                stopHeal();
                isAliveH = true;
                heal.play();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        name[1].setOnMouseClicked(event -> {
            game.getPlayer().setMoney(game.getPlayer().getMoney() - 200);
            try {
                processH = Runtime.getRuntime().exec("games/Miniclip-Game-a-day-of-slacking.exe");
                stopHeal();
                isAliveH = true;
                heal.play();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        name[2].setOnMouseClicked(event -> {
            game.getPlayer().setMoney(game.getPlayer().getMoney() - 173);
            try {
                processE = Runtime.getRuntime().exec("games/Miniclip-Game-bug-on-a-wire.exe");
                stopHeal();
                isAliveE = true;
                heal.play();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        name[3].setOnMouseClicked(event -> {
            game.getPlayer().setMoney(game.getPlayer().getMoney() - 26);
            try {
                processE = Runtime.getRuntime().exec("games/Miniclip-Game-cell-out.exe");
                stopHeal();
                isAliveE = true;
                heal.play();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        name[4].setOnMouseClicked(event -> {
            game.getPlayer().setMoney(game.getPlayer().getMoney() - 150);
            try {
                processE = Runtime.getRuntime().exec("games/Miniclip-Game-gyroball.exe");
                stopHeal();
                isAliveE = true;
                heal.play();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        name[5].setOnMouseClicked(event -> {
            game.getPlayer().setMoney(game.getPlayer().getMoney() - 264);
            try {
                processH = Runtime.getRuntime().exec("games/Miniclip-Game-cube-buster.exe");
                stopHeal();
                isAliveH = true;
                heal.play();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        name[6].setOnMouseClicked(event -> {
            game.getPlayer().setMoney(game.getPlayer().getMoney() - 110);
            try {
                processS = Runtime.getRuntime().exec("games/Miniclip-Game-magic-balls.exe");
                stopHeal();
                isAliveS = true;
                heal.play();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        name[7].setOnMouseClicked(event -> {
            game.getPlayer().setMoney(game.getPlayer().getMoney() - 400);
            try {
                processH = Runtime.getRuntime().exec("games/Miniclip-Game-pengapop.exe");
                stopHeal();
                isAliveH = true;
                heal.play();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        name[8].setOnMouseClicked(event -> {
            game.getPlayer().setMoney(game.getPlayer().getMoney() - 50);
            try {
                processS = Runtime.getRuntime().exec("games/Miniclip-Game-shrunken-heads.exe");
                stopHeal();
                isAliveS = true;
                heal.play();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        name[9].setOnMouseClicked(event -> {
            game.getPlayer().setMoney(game.getPlayer().getMoney() - 500);
            try {
                processH = Runtime.getRuntime().exec("games/Miniclip-Game-zed.exe");
                stopHeal();
                isAliveH = true;
                heal.play();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }


    public static void publicChatSceneBuilder(GridPane rootPane) {
        rootPane.getChildren().clear();
        rootPane.setBackground(new Background(new BackgroundFill(Color.BLACK,
                new CornerRadii(0), new Insets(0))));
        rootPane.setPadding(new Insets(20));
        rootPane.setAlignment(Pos.CENTER);
        ListView<String> chatListView = new ListView<String>();
        if (isServer)
            chatListView.setItems(server.getServerLog());
        else
            chatListView.setItems(client.getClientLog());
        chatListView.setStyle("-fx-background-color: darkgray;-fx-text-fill: darkred;-fx-border-radius: 20px;");
        TextField chatTextField = new TextField();
        chatTextField.setBackground(new Background(new BackgroundFill(Color.DARKGRAY,
                new CornerRadii(20), new Insets(0))));
        chatTextField.setStyle("-fx-text-fill: darkred");
        Button smileEmoji = new Button(EmojiParser.parseToUnicode(":smile:"));
        smileEmoji.setShape(new Circle(15, Color.BLACK));
        smileEmoji.setTranslateX(10);
        smileEmoji.setTranslateY(-180);
        smileEmoji.setStyle("-fx-background-color: #000000;-fx-border-color: darkgray;" +
                "-fx-border-width: 2px;-fx-text-fill: darkgray;");
        Button catEmoji = new Button(EmojiParser.parseToUnicode(":cat:"));
        catEmoji.setShape(new Circle(15, Color.BLACK));
        catEmoji.setTranslateX(40);
        catEmoji.setTranslateY(-180);
        catEmoji.setStyle("-fx-background-color: #000000;-fx-border-color: darkgray;" +
                "-fx-border-width: 2px;-fx-text-fill: darkgray;");
        Button angryEmoji = new Button(EmojiParser.parseToUnicode(":angry:"));
        angryEmoji.setShape(new Circle(15, Color.BLACK));
        angryEmoji.setTranslateX(70);
        angryEmoji.setTranslateY(-180);
        angryEmoji.setStyle("-fx-background-color: #000000;-fx-border-color: darkgray;" +
                "-fx-border-width: 2px;-fx-text-fill: darkgray;");
        Button cryEmoji = new Button(EmojiParser.parseToUnicode(":cry:"));
        cryEmoji.setShape(new Circle(15, Color.BLACK));
        cryEmoji.setTranslateX(100);
        cryEmoji.setTranslateY(-180);
        cryEmoji.setStyle("-fx-background-color: #000000;-fx-border-color: darkgray;" +
                "-fx-border-width: 2px;-fx-text-fill: darkgray;");
        Button carEmoji = new Button(EmojiParser.parseToUnicode(":car:"));
        carEmoji.setShape(new Circle(15, Color.BLACK));
        carEmoji.setTranslateX(10);
        carEmoji.setTranslateY(-150);
        carEmoji.setStyle("-fx-background-color: #000000;-fx-border-color: darkgray;" +
                "-fx-border-width: 2px;-fx-text-fill: darkgray;");
        Button dogEmoji = new Button(EmojiParser.parseToUnicode(":dog:"));
        dogEmoji.setShape(new Circle(15, Color.BLACK));
        dogEmoji.setTranslateX(40);
        dogEmoji.setTranslateY(-150);
        dogEmoji.setStyle("-fx-background-color: #000000;-fx-border-color: darkgray;" +
                "-fx-border-width: 2px;-fx-text-fill: darkgray;");
        Button smileyEmoji = new Button(EmojiParser.parseToUnicode(":smiley:"));
        smileyEmoji.setShape(new Circle(15, Color.BLACK));
        smileyEmoji.setTranslateX(70);
        smileyEmoji.setTranslateY(-150);
        smileyEmoji.setStyle("-fx-background-color: #000000;-fx-border-color: darkgray;" +
                "-fx-border-width: 2px;-fx-text-fill: darkgray;");
        Button grinningEmoji = new Button(EmojiParser.parseToUnicode(":grinning:"));
        grinningEmoji.setShape(new Circle(15, Color.BLACK));
        grinningEmoji.setTranslateX(100);
        grinningEmoji.setTranslateY(-150);
        grinningEmoji.setStyle("-fx-background-color: #000000;-fx-border-color: darkgray;" +
                "-fx-border-width: 2px;-fx-text-fill: darkgray;");
        Button chooseImage = new Button("Choose Image");
        chooseImage.setPrefSize(100, 100);
        chooseImage.setShape(new Circle(90, Color.BLACK));
        chooseImage.setTranslateX(20);
        chooseImage.setTranslateY(-70);
        chooseImage.setStyle("-fx-background-color: #000000;-fx-border-color: darkseagreen;" +
                "-fx-border-width: 2px;-fx-text-fill: darkseagreen;");
        Button chooseGif = new Button("Choose Gif");
        chooseGif.setPrefSize(100, 100);
        chooseGif.setShape(new Circle(90, Color.BLACK));
        chooseGif.setTranslateX(20);
        chooseGif.setTranslateY(40);
        chooseGif.setStyle("-fx-background-color: #000000;-fx-border-color: darkseagreen;" +
                "-fx-border-width: 2px;-fx-text-fill: darkseagreen;");
        Button chooseAudio = new Button("Choose Audio");
        chooseAudio.setPrefSize(100, 100);
        chooseAudio.setShape(new Circle(90, Color.BLACK));
        chooseAudio.setTranslateX(20);
        chooseAudio.setTranslateY(150);
        chooseAudio.setStyle("-fx-background-color: #000000;-fx-border-color: darkseagreen;" +
                "-fx-border-width: 2px;-fx-text-fill: darkseagreen;");

        Button back = new Button("Back");
        back.setPrefSize(100, 100);
        back.setShape(new Circle(90, Color.BLACK));
        back.setTranslateX(-300);
        back.setTranslateY(-250);
        back.setStyle("-fx-background-color: #000000;-fx-border-color: darkseagreen;" +
                "-fx-border-width: 2px;-fx-text-fill: darkseagreen;");


        back.setOnMouseClicked(event -> primaryStage.setScene(networkPauseScene));


        chatTextField.setOnAction(event -> {
            if (isServer) {
                server.sendTextPublic(chatTextField.getText());
            } else {
                client.sendTextPublic(chatTextField.getText());
            }
            chatTextField.clear();
        });
        smileEmoji.setOnAction(event -> chatTextField.appendText(EmojiParser.parseToUnicode(":smile:")));
        catEmoji.setOnAction(event -> chatTextField.appendText(EmojiParser.parseToUnicode(":cat:")));
        angryEmoji.setOnAction(event -> chatTextField.appendText(EmojiParser.parseToUnicode(":angry:")));
        cryEmoji.setOnAction(event -> chatTextField.appendText(EmojiParser.parseToUnicode(":cry:")));
        carEmoji.setOnAction(event -> chatTextField.appendText(EmojiParser.parseToUnicode(":car:")));
        dogEmoji.setOnAction(event -> chatTextField.appendText(EmojiParser.parseToUnicode(":dog:")));
        smileyEmoji.setOnAction(event -> chatTextField.appendText(EmojiParser.parseToUnicode(":smiley:")));
        grinningEmoji.setOnAction(event -> chatTextField.appendText(EmojiParser.parseToUnicode(":grinning:")));
        chooseImage.setOnAction(event -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("View Pictures");
            fileChooser.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                    new FileChooser.ExtensionFilter("PNG", "*.png"));
            File file = fileChooser.showOpenDialog(primaryStage);
            if (file != null)
                if (isServer)
                    server.sendImagePublic(file.getAbsolutePath(), file.getName());
                else client.sendImagePublic(file.getAbsolutePath(), file.getName());
        });
        chooseGif.setOnAction(event -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("View Gifs");
            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("GIF", "*.gif"));
            File file = fileChooser.showOpenDialog(primaryStage);
            if (file != null)
                if (isServer)
                    server.sendImagePublic(file.getAbsolutePath(), file.getName());
                else client.sendImagePublic(file.getAbsolutePath(), file.getName());
        });
        chooseAudio.setOnAction(event -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("View Audios");
            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("MP3", "*.mp3"));
            File file = fileChooser.showOpenDialog(primaryStage);
            if (file != null)
                if (isServer)
                    server.sendAudioPublic(file.getAbsolutePath(), file.getName());
                else client.sendAudioPublic(file.getAbsolutePath(), file.getName());
        });

        rootPane.add(chatListView, 0, 0);
        rootPane.add(chatTextField, 0, 1);
        rootPane.add(smileEmoji, 2, 0);
        rootPane.add(catEmoji, 2, 0);
        rootPane.add(angryEmoji, 2, 0);
        rootPane.add(cryEmoji, 2, 0);
        rootPane.add(carEmoji, 2, 0);
        rootPane.add(dogEmoji, 2, 0);
        rootPane.add(smileyEmoji, 2, 0);
        rootPane.add(grinningEmoji, 2, 0);
        rootPane.add(chooseImage, 2, 0);
        rootPane.add(chooseGif, 2, 0);
        rootPane.add(chooseAudio, 2, 0);
        rootPane.add(back, 0, 0);
    }


    public static void chatSceneBuilder(GridPane rootPane, int numOfPlayer) {
        rootPane.getChildren().clear();
        if (isServer)
            server.getServerLog().clear();
        else client.getClientLog().clear();
        rootPane.setBackground(new Background(new BackgroundFill(Color.BLACK,
                new CornerRadii(0), new Insets(0))));
        rootPane.setPadding(new Insets(20));
        rootPane.setAlignment(Pos.CENTER);
        ListView<String> chatListView = new ListView<String>();
        if (isServer)
            chatListView.setItems(server.getServerLog());
        else
            chatListView.setItems(client.getClientLog());
        chatListView.setStyle("-fx-background-color: darkgray;-fx-text-fill: darkred;-fx-border-radius: 20px;");
        TextField chatTextField = new TextField();
        chatTextField.setBackground(new Background(new BackgroundFill(Color.DARKGRAY,
                new CornerRadii(20), new Insets(0))));
        chatTextField.setStyle("-fx-text-fill: darkred");
        Button smileEmoji = new Button(EmojiParser.parseToUnicode(":smile:"));
        smileEmoji.setShape(new Circle(15, Color.BLACK));
        smileEmoji.setTranslateX(10);
        smileEmoji.setTranslateY(-180);
        smileEmoji.setStyle("-fx-background-color: #000000;-fx-border-color: darkgray;" +
                "-fx-border-width: 2px;-fx-text-fill: darkgray;");
        Button catEmoji = new Button(EmojiParser.parseToUnicode(":cat:"));
        catEmoji.setShape(new Circle(15, Color.BLACK));
        catEmoji.setTranslateX(40);
        catEmoji.setTranslateY(-180);
        catEmoji.setStyle("-fx-background-color: #000000;-fx-border-color: darkgray;" +
                "-fx-border-width: 2px;-fx-text-fill: darkgray;");
        Button angryEmoji = new Button(EmojiParser.parseToUnicode(":angry:"));
        angryEmoji.setShape(new Circle(15, Color.BLACK));
        angryEmoji.setTranslateX(70);
        angryEmoji.setTranslateY(-180);
        angryEmoji.setStyle("-fx-background-color: #000000;-fx-border-color: darkgray;" +
                "-fx-border-width: 2px;-fx-text-fill: darkgray;");
        Button cryEmoji = new Button(EmojiParser.parseToUnicode(":cry:"));
        cryEmoji.setShape(new Circle(15, Color.BLACK));
        cryEmoji.setTranslateX(100);
        cryEmoji.setTranslateY(-180);
        cryEmoji.setStyle("-fx-background-color: #000000;-fx-border-color: darkgray;" +
                "-fx-border-width: 2px;-fx-text-fill: darkgray;");
        Button carEmoji = new Button(EmojiParser.parseToUnicode(":car:"));
        carEmoji.setShape(new Circle(15, Color.BLACK));
        carEmoji.setTranslateX(10);
        carEmoji.setTranslateY(-150);
        carEmoji.setStyle("-fx-background-color: #000000;-fx-border-color: darkgray;" +
                "-fx-border-width: 2px;-fx-text-fill: darkgray;");
        Button dogEmoji = new Button(EmojiParser.parseToUnicode(":dog:"));
        dogEmoji.setShape(new Circle(15, Color.BLACK));
        dogEmoji.setTranslateX(40);
        dogEmoji.setTranslateY(-150);
        dogEmoji.setStyle("-fx-background-color: #000000;-fx-border-color: darkgray;" +
                "-fx-border-width: 2px;-fx-text-fill: darkgray;");
        Button smileyEmoji = new Button(EmojiParser.parseToUnicode(":smiley:"));
        smileyEmoji.setShape(new Circle(15, Color.BLACK));
        smileyEmoji.setTranslateX(70);
        smileyEmoji.setTranslateY(-150);
        smileyEmoji.setStyle("-fx-background-color: #000000;-fx-border-color: darkgray;" +
                "-fx-border-width: 2px;-fx-text-fill: darkgray;");
        Button grinningEmoji = new Button(EmojiParser.parseToUnicode(":grinning:"));
        grinningEmoji.setShape(new Circle(15, Color.BLACK));
        grinningEmoji.setTranslateX(100);
        grinningEmoji.setTranslateY(-150);
        grinningEmoji.setStyle("-fx-background-color: #000000;-fx-border-color: darkgray;" +
                "-fx-border-width: 2px;-fx-text-fill: darkgray;");
        Button chooseImage = new Button("Choose Image");
        chooseImage.setPrefSize(100, 100);
        chooseImage.setShape(new Circle(90, Color.BLACK));
        chooseImage.setTranslateX(20);
        chooseImage.setTranslateY(-70);
        chooseImage.setStyle("-fx-background-color: #000000;-fx-border-color: darkseagreen;" +
                "-fx-border-width: 2px;-fx-text-fill: darkseagreen;");
        Button chooseGif = new Button("Choose Gif");
        chooseGif.setPrefSize(100, 100);
        chooseGif.setShape(new Circle(90, Color.BLACK));
        chooseGif.setTranslateX(20);
        chooseGif.setTranslateY(40);
        chooseGif.setStyle("-fx-background-color: #000000;-fx-border-color: darkseagreen;" +
                "-fx-border-width: 2px;-fx-text-fill: darkseagreen;");
        Button chooseAudio = new Button("Choose Audio");
        chooseAudio.setPrefSize(100, 100);
        chooseAudio.setShape(new Circle(90, Color.BLACK));
        chooseAudio.setTranslateX(20);
        chooseAudio.setTranslateY(150);
        chooseAudio.setStyle("-fx-background-color: #000000;-fx-border-color: darkseagreen;" +
                "-fx-border-width: 2px;-fx-text-fill: darkseagreen;");

        Button back = new Button("Back");
        back.setPrefSize(100, 100);
        back.setShape(new Circle(90, Color.BLACK));
        back.setTranslateX(-300);
        back.setTranslateY(-250);
        back.setStyle("-fx-background-color: #000000;-fx-border-color: darkseagreen;" +
                "-fx-border-width: 2px;-fx-text-fill: darkseagreen;");


        back.setOnMouseClicked(event -> primaryStage.setScene(villageScene));


        chatTextField.setOnAction(event -> {
            if (isServer) {
                server.sendTextPV(chatTextField.getText(), numOfPlayer);
            } else {
                client.sendTextPV(chatTextField.getText(), numOfPlayer);
            }
            chatTextField.clear();
        });
        smileEmoji.setOnAction(event -> chatTextField.appendText(EmojiParser.parseToUnicode(":smile:")));
        catEmoji.setOnAction(event -> chatTextField.appendText(EmojiParser.parseToUnicode(":cat:")));
        angryEmoji.setOnAction(event -> chatTextField.appendText(EmojiParser.parseToUnicode(":angry:")));
        cryEmoji.setOnAction(event -> chatTextField.appendText(EmojiParser.parseToUnicode(":cry:")));
        carEmoji.setOnAction(event -> chatTextField.appendText(EmojiParser.parseToUnicode(":car:")));
        dogEmoji.setOnAction(event -> chatTextField.appendText(EmojiParser.parseToUnicode(":dog:")));
        smileyEmoji.setOnAction(event -> chatTextField.appendText(EmojiParser.parseToUnicode(":smiley:")));
        grinningEmoji.setOnAction(event -> chatTextField.appendText(EmojiParser.parseToUnicode(":grinning:")));
        chooseImage.setOnAction(event -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("View Pictures");
            fileChooser.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                    new FileChooser.ExtensionFilter("PNG", "*.png"));
            File file = fileChooser.showOpenDialog(primaryStage);
            if (file != null)
                if (isServer)
                    server.sendImagePV(file.getAbsolutePath(), file.getName(), numOfPlayer);
                else client.sendImagePV(file.getAbsolutePath(), file.getName(), numOfPlayer);
        });
        chooseGif.setOnAction(event -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("View Gifs");
            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("GIF", "*.gif"));
            File file = fileChooser.showOpenDialog(primaryStage);
            if (file != null)
                if (isServer)
                    server.sendImagePV(file.getAbsolutePath(), file.getName(), numOfPlayer);
                else client.sendImagePV(file.getAbsolutePath(), file.getName(), numOfPlayer);
        });
        chooseAudio.setOnAction(event -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("View Audios");
            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("MP3", "*.mp3"));
            File file = fileChooser.showOpenDialog(primaryStage);
            if (file != null)
                if (isServer)
                    server.sendAudioPV(file.getAbsolutePath(), file.getName(), numOfPlayer);
                else client.sendAudioPV(file.getAbsolutePath(), file.getName(), numOfPlayer);
        });

        rootPane.add(chatListView, 0, 0);
        rootPane.add(chatTextField, 0, 1);
        rootPane.add(smileEmoji, 2, 0);
        rootPane.add(catEmoji, 2, 0);
        rootPane.add(angryEmoji, 2, 0);
        rootPane.add(cryEmoji, 2, 0);
        rootPane.add(carEmoji, 2, 0);
        rootPane.add(dogEmoji, 2, 0);
        rootPane.add(smileyEmoji, 2, 0);
        rootPane.add(grinningEmoji, 2, 0);
        rootPane.add(chooseImage, 2, 0);
        rootPane.add(chooseGif, 2, 0);
        rootPane.add(chooseAudio, 2, 0);
        rootPane.add(back, 0, 0);
    }

    private static void stopNetworkTimeline() {
        networkSceneChanger.stop();
        sendData.setCycleCount(Animation.INDEFINITE);
        sendData.play();
    }

    private static void playSendData() {
        ImagePattern imagePattern = (ImagePattern) player.getFill();
        LocatedImage image = (LocatedImage) imagePattern.getImage();
        String url = image.getUrl();
        if (isServer) {
            server.sendDataPack(new DataPack(player.getX(), player.getY(), player.getWidth(), player.getHeight(), url), -1);
            if (server.getChat() != -1) {
                if (!server.isAcceptChatReq()) {
                    yesNoNetworkSceneBuilder(yesNoRoot, chatScene, server.getNames()[server.getChatReqFrom()] +
                            " wants to chat with you.", 4, "Chat");
                    primaryStage.setScene(yesNoScene);
                } else if (server.isAcceptChatReq()) {
                    chatSceneBuilder(chatRoot, server.getChatReqFrom());
                    server.setAcceptChatReq(false);
                    server.setChat(-1);
                    primaryStage.setScene(chatScene);
                }
            } else if (server.isReceivedStatusReq()) {
                server.setReceivedStatusReq(false);
                PlayerStatus playerStatus = server.getRequestedPlayerStatus();
                statusSceneBuilder(statusRoot, playerMenuScene, playerStatus);
                primaryStage.setScene(statusScene);
            } else if (server.getTrade() != -1) {
                if (!server.isAcceptTradeReq()) {
                    yesNoNetworkSceneBuilder(yesNoRoot, tradeScene, server.getNames()[server.getTradeReqFrom()] +
                            " wants to trade with you.", 4, "Trade");
                    primaryStage.setScene(yesNoScene);
                } else if (server.isAcceptTradeReq()) {
                    tradeSceneBuilder(tradeRoot, game.getPlayer(), villageScene, server.getTrade());
                    server.setAcceptTradeReq(false);
                    server.setTrade(-1);
                    primaryStage.setScene(tradeScene);
                }
            } else if (server.isLockIn()) {
                for (int i = 0; i < server.getLockInFrom().length; i++) {
                    if (server.getLockInFrom()[i] && server.getLockInTo()[i]) {
                        for (Item item : chosedItems.elementSet()) {
                            server.sendItem(item, chosedItems.count(item), i);
                        }
                        server.setLockIn(false);
                        server.getLockInFrom()[i] = false;
                        server.getLockInTo()[i] = false;
                        primaryStage.setScene(villageScene);
                    }
                }
            }
        } else {
            client.sendDataPack(new DataPack(player.getX(), player.getY(), player.getWidth(), player.getHeight(), url));
            if (client.getChat() != -1) {
                if (!client.isAcceptChatReq()) {
                    yesNoNetworkSceneBuilder(yesNoRoot, chatScene, client.getNames()[client.getChatReqFrom()] +
                            " wants to chat with you.", 4, "Chat");
                    primaryStage.setScene(yesNoScene);
                } else if (client.isAcceptChatReq()) {
                    chatSceneBuilder(chatRoot, client.getChatReqFrom());
                    client.setAcceptChatReq(false);
                    client.setChat(-1);
                    primaryStage.setScene(chatScene);
                }
            } else if (client.isReceivedStatusReq()) {
                client.setReceivedStatusReq(false);
                PlayerStatus playerStatus = client.getRequestedPlayerStatus();
                statusSceneBuilder(statusRoot, playerMenuScene, playerStatus);
                primaryStage.setScene(statusScene);
            } else if (client.getTrade() != -1) {
                if (!client.isAcceptTradeReq()) {
                    yesNoNetworkSceneBuilder(yesNoRoot, tradeScene, client.getNames()[client.getTradeReqFrom()] +
                            " wants to trade with you.", 4, "Trade");
                    primaryStage.setScene(yesNoScene);
                } else if (client.isAcceptTradeReq()) {
                    tradeSceneBuilder(tradeRoot, game.getPlayer(), villageScene, client.getTradeReqFrom());
                    client.setAcceptTradeReq(false);
                    client.setTrade(-1);
                    primaryStage.setScene(tradeScene);
                }
            } else if (client.isLockIn()) {
                for (int i = 0; i < client.getLockInFrom().length; i++) {
                    if (client.getLockInFrom()[i] && client.getLockInTo()[i]) {
                        for (Item item : chosedItems.elementSet()) {
                            client.sendItem(item, chosedItems.count(item), i);
                        }
                        client.setLockIn(false);
                        client.getLockInFrom()[i] = false;
                        client.getLockInTo()[i] = false;
                        primaryStage.setScene(villageScene);
                    }
                }
            }
        }
    }

    private static Timeline networkSceneChanger = new Timeline(new KeyFrame(new Duration(500), event -> {
        if (startProcess[0]) {
            primaryStage.setScene(villageScene);
            stopNetworkTimeline();
        }
    }));
    private static Timeline sendData = new Timeline(new KeyFrame(new Duration(100), event -> {
        playSendData();
    }));

    public static void hostSceneBuilder(Group root) {
        TextField nameField = new TextField();
        Label nameLable = new Label("Name");
        TextField portField = new TextField();
        Label portLabel = new Label("Port");
        nameField.setLayoutX(280);
        nameLable.setLayoutX(450);
        portField.setLayoutX(280);
        portLabel.setLayoutX(450);
        nameField.setLayoutY(400);
        nameLable.setLayoutY(400);
        portField.setLayoutY(440);
        portLabel.setLayoutY(440);
        Button submit = new Button("Done");
        submit.setPrefSize(50, 50);
        submit.setShape(new Circle(15, Color.BLACK));
        submit.setStyle("-fx-background-color: #000000;-fx-border-color: darkseagreen;" +
                "-fx-border-width: 2px;-fx-text-fill: darkseagreen;");
        submit.setLayoutX(280);
        submit.setLayoutY(470);
        root.getChildren().addAll(submit, portField, portLabel, nameField, nameLable);
        submit.setOnAction(event -> {
            isServer = true;
            server = new Server(Integer.parseInt(portField.getText()), nameField.getText(), root, startProcess, villageRoot, player, game);
            serverThread = new Thread(server);
            serverThread.setDaemon(true);
            serverThread.start();
            networkSceneChanger.setCycleCount(Animation.INDEFINITE);
            networkSceneChanger.play();
            timeCalculator.play();
        });
    }

    public static void joinSceneBuilder(Group root) {
        TextField nameField = new TextField();
        Label nameLable = new Label("Name");
        TextField portField = new TextField();
        Label portLabel = new Label("Port");
        nameField.setLayoutX(280);
        nameLable.setLayoutX(450);
        portField.setLayoutX(280);
        portLabel.setLayoutX(450);
        nameField.setLayoutY(400);
        nameLable.setLayoutY(400);
        portField.setLayoutY(440);
        portLabel.setLayoutY(440);
        Button submit = new Button("Done");
        submit.setPrefSize(50, 50);
        submit.setShape(new Circle(15, Color.BLACK));
        submit.setStyle("-fx-background-color: #000000;-fx-border-color: darkseagreen;" +
                "-fx-border-width: 2px;-fx-text-fill: darkseagreen;");
        submit.setLayoutX(280);
        submit.setLayoutY(470);
        root.getChildren().addAll(submit, portField, portLabel, nameField, nameLable);
        submit.setOnAction(event -> {
            client = new Client(Integer.parseInt(portField.getText()), nameField.getText(), root, startProcess, villageRoot, player, game);
            clientThread = new Thread(client);
            clientThread.setDaemon(true);
            clientThread.start();
            networkSceneChanger.setCycleCount(Animation.INDEFINITE);
            networkSceneChanger.play();
            timeCalculator.play();
        });
    }


    public static void mainSceneBuilder(Group root) {
        Image lumber = new Image("pics/menus/menuLumber.png");
        Rectangle singlePlayerButton = rectangleBuilder(270, 270, new ImagePattern(lumber), 350, 150);
        Rectangle customButton = rectangleBuilder(270, 270, new ImagePattern(lumber), 150, 270);
        Rectangle multiPlayerButton = rectangleBuilder(270, 270, new ImagePattern(lumber), 550, 270);
        Rectangle settingsButton = rectangleBuilder(270, 270, new ImagePattern(lumber), 230, 450);
        Rectangle exitButton = rectangleBuilder(270, 270, new ImagePattern(lumber), 470, 450);
        root.getChildren().addAll(singlePlayerButton, customButton, multiPlayerButton, settingsButton, exitButton);

        Text singlePlayer = buttonDesigner("Single\nPlayer", 250, 340, 35, Color.DARKGREEN);
        Text custom = buttonDesigner("Custom\n Game", 320, 520, 35, Color.DARKGREEN);
        Text multiPlayer = buttonDesigner(" Multi\nPlayer", 650, 340, 35, Color.DARKGREEN);
        Text settings = buttonDesigner("Setting", 560, 540, 35, Color.DARKGREEN);
        Text exit = buttonDesigner("Exit", 470, 240, 35, Color.DARKGREEN);

        exit.setOnMouseClicked(event -> primaryStage.close());
        settings.setOnMouseClicked(event -> {
            settingsSceneBuilder(settingsRoot, mainScene, game.getPlayer());
            primaryStage.setScene(settingsScene);
        });
        singlePlayer.setOnMouseClicked(event -> {
            primaryStage.setScene(weatherScene);
            timeCalculator.play();
        });
        multiPlayer.setOnMouseClicked(event -> {
            primaryStage.setScene(multiPlayerScene);
            isMultiplayer = true;
        });
        custom.setOnMouseClicked(event -> primaryStage.setScene(customScene));
        root.getChildren().addAll(singlePlayer, custom, multiPlayer, settings, exit);
    }

    public static void weatherSceneBuilder(Group root, Scene scene) {
        Image backImage = new Image("pics/menus/backButton.png");
        Image titleImage = new Image("pics/menus/backpackTitle.png");
        Image statusImage = new Image("pics/menus/weather.png");
        Rectangle backButton = rectangleBuilder(60, 60, new ImagePattern(backImage), 50, 20);
        Rectangle title = rectangleBuilder(180, 60, new ImagePattern(titleImage), 430, 20);
        Rectangle status = rectangleBuilder(1000, 700, new ImagePattern(statusImage), 0, -50);
        root.getChildren().addAll(backButton, title, status);

        Text titleText = buttonDesigner("Weather", 440, 53, 28, Color.SADDLEBROWN);
        Text summer = buttonDesigner("Summer", 230, 130, 28, Color.SADDLEBROWN);
        Text autumn = buttonDesigner("Autumn", 670, 130, 28, Color.SADDLEBROWN);
        Text spring = buttonDesigner("Spring", 670, 380, 28, Color.SADDLEBROWN);
        Text winter = buttonDesigner("Winter", 230, 380, 28, Color.SADDLEBROWN);
        Text back = buttonDesigner("Back", 54.5, 52, 21.5, Color.SADDLEBROWN);
        root.getChildren().addAll(titleText, back, summer, autumn, winter, spring);
        back.setOnMouseClicked(event -> {
            primaryStage.setScene(scene);
            initializeFlags();
        });
        summer.setOnMouseClicked(event -> {
            farmScene.setFill(new ImagePattern(new Image("pics/maps/farm-summer.png")));
            jungleScene.setFill(new ImagePattern(new Image("pics/maps/jungle-summer.png")));
            villageScene.setFill(new ImagePattern(new Image("pics/maps/village-summer.png")));
            primaryStage.setScene(farmScene);
            choosedRoot = farmRoot;
            gameDate.setSeason(new Season("Summer", new ArrayList<Task>(), new AllOFWeathers().summer()));
            gameDate.setMonth(2);
        });
        spring.setOnMouseClicked(event -> {
            farmScene.setFill(new ImagePattern(new Image("pics/maps/farm-spring.png")));
            jungleScene.setFill(new ImagePattern(new Image("pics/maps/jungle-spring.png")));
            villageScene.setFill(new ImagePattern(new Image("pics/maps/village-spring.png")));
            primaryStage.setScene(farmScene);
            choosedRoot = farmRoot;
            gameDate.setSeason(new Season("Spring", new ArrayList<Task>(), new AllOFWeathers().spring()));
            gameDate.setMonth(1);
        });

        winter.setOnMouseClicked(event -> {
            farmScene.setFill(new ImagePattern(new Image("pics/maps/farm-winter.png")));
            jungleScene.setFill(new ImagePattern(new Image("pics/maps/jungle-winter.png")));
            villageScene.setFill(new ImagePattern(new Image("pics/maps/village-winter.png")));
            primaryStage.setScene(farmScene);
            choosedRoot = farmRoot;
            gameDate.setSeason(new Season("Winter", new ArrayList<Task>(), new AllOFWeathers().winter()));
            gameDate.setMonth(4);
        });

        autumn.setOnMouseClicked(event -> {
            farmScene.setFill(new ImagePattern(new Image("pics/maps/farm-fall.png")));
            jungleScene.setFill(new ImagePattern(new Image("pics/maps/jungle-fall.png")));
            villageScene.setFill(new ImagePattern(new Image("pics/maps/village-fall.png")));
            primaryStage.setScene(farmScene);
            choosedRoot = farmRoot;
            gameDate.setSeason(new Season("Fall", new ArrayList<Task>(), new AllOFWeathers().fall()));
            gameDate.setMonth(3);
        });
    }


    public static void customSceneBuilder(Group root) {
        Image backImage = new Image("pics/menus/backButton.png");
        Image titleImage = new Image("pics/menus/backpackTitle.png");
        Rectangle backButton = rectangleBuilder(60, 60, new ImagePattern(backImage), 50, 20);
        Rectangle title = rectangleBuilder(190, 60, new ImagePattern(titleImage), 430, 20);
        Rectangle cropButton = rectangleBuilder(200, 70, new ImagePattern(titleImage), 170, 100);
        Rectangle treeButton = rectangleBuilder(200, 70, new ImagePattern(titleImage), 680, 100);
        Rectangle machineButton = rectangleBuilder(200, 70, new ImagePattern(titleImage), 170, 200);
        Rectangle foodButton = rectangleBuilder(200, 70, new ImagePattern(titleImage), 680, 200);
        Rectangle playerButton = rectangleBuilder(200, 70, new ImagePattern(titleImage), 170, 300);
        Rectangle sportButton = rectangleBuilder(200, 70, new ImagePattern(titleImage), 680, 300);
        Rectangle toolButton = rectangleBuilder(200, 70, new ImagePattern(titleImage), 170, 400);
        Rectangle medicineButton = rectangleBuilder(200, 70, new ImagePattern(titleImage), 680, 400);
        Rectangle missionButton = rectangleBuilder(200, 70, new ImagePattern(titleImage), 170, 500);
        Rectangle shopButton = rectangleBuilder(200, 70, new ImagePattern(titleImage), 680, 500);
        root.getChildren().addAll(backButton, title, cropButton, treeButton, machineButton, foodButton, playerButton,
                sportButton, toolButton, medicineButton, missionButton, shopButton);

        Text titleText = buttonDesigner("Custom Game", 440, 53, 28, Color.SADDLEBROWN);
        Text crop = buttonDesigner("Crop", 220, 140, 30, Color.SADDLEBROWN);
        Text tree = buttonDesigner("Tree", 730, 140, 30, Color.SADDLEBROWN);
        Text machine = buttonDesigner("Machine", 210, 240, 30, Color.SADDLEBROWN);
        Text food = buttonDesigner("Food", 730, 240, 30, Color.SADDLEBROWN);
        Text player = buttonDesigner("Player", 210, 340, 30, Color.SADDLEBROWN);
        Text sport = buttonDesigner("Sport", 730, 340, 30, Color.SADDLEBROWN);
        Text tool = buttonDesigner("Tool", 210, 440, 30, Color.SADDLEBROWN);
        Text medicine = buttonDesigner("Medicine", 730, 440, 30, Color.SADDLEBROWN);
        Text mission = buttonDesigner("Mission", 210, 540, 30, Color.SADDLEBROWN);
        Text shop = buttonDesigner("Shops", 730, 540, 30, Color.SADDLEBROWN);
        Text back = buttonDesigner("Back", 54.5, 52, 21.5, Color.SADDLEBROWN);

        root.getChildren().addAll(titleText, back, crop, tree, machine, food, player, sport, medicine, tool, mission, shop);
        back.setOnMouseClicked(event -> primaryStage.setScene(mainScene));
        food.setOnMouseClicked(event -> {
            customFoodSceneBuilder(customCreationRoot, customScene);
            primaryStage.setScene(customCreationScene);
        });
        crop.setOnMouseClicked(event -> {
            customCropSceneBuilder(customCreationRoot, customScene);
            primaryStage.setScene(customCreationScene);
        });
        tree.setOnMouseClicked(event -> {
            customTreeSceneBuilder(customCreationRoot, customScene);
            primaryStage.setScene(customCreationScene);
        });

        player.setOnMouseClicked(event -> {
            customPlayerSceneBuilder(customCreationRoot, customScene);
            primaryStage.setScene(customCreationScene);
        });

        machine.setOnMouseClicked(event -> {
            customMachineSceneBuilder(customCreationRoot, customScene);
            primaryStage.setScene(customCreationScene);
        });

        sport.setOnMouseClicked(event -> {
            customTrainSceneBuilder(customCreationRoot, customScene, "Train");
            primaryStage.setScene(customCreationScene);
        });

        medicine.setOnMouseClicked(event -> {
            customTrainSceneBuilder(customCreationRoot, customScene, "Medicine");
            primaryStage.setScene(customCreationScene);
        });

        mission.setOnMouseClicked(event -> {
            customMissionSceneBuilder(customCreationRoot, customScene, "Mission");
            primaryStage.setScene(customCreationScene);
        });

        shop.setOnMouseClicked(event -> {
            customShopSceneBuilder(customShopRoot);
            primaryStage.setScene(customShopScene);
        });
    }

    public static void customShopSceneBuilder(Group root) {
        Image backImage = new Image("pics/menus/backButton.png");
        Image titleImage = new Image("pics/menus/backpackTitle.png");
        Rectangle backButton = rectangleBuilder(60, 60, new ImagePattern(backImage), 50, 20);
        Rectangle title = rectangleBuilder(190, 60, new ImagePattern(titleImage), 430, 20);
        Rectangle cropButton = rectangleBuilder(200, 70, new ImagePattern(titleImage), 170, 100);
        Rectangle treeButton = rectangleBuilder(200, 70, new ImagePattern(titleImage), 680, 100);
        Rectangle machineButton = rectangleBuilder(200, 70, new ImagePattern(titleImage), 170, 200);
        Rectangle foodButton = rectangleBuilder(200, 70, new ImagePattern(titleImage), 680, 200);
        Rectangle playerButton = rectangleBuilder(200, 70, new ImagePattern(titleImage), 170, 300);
        Rectangle sportButton = rectangleBuilder(200, 70, new ImagePattern(titleImage), 680, 300);
        root.getChildren().addAll(backButton, title, cropButton, treeButton, machineButton, foodButton, playerButton, sportButton);

        Text titleText = buttonDesigner("Shops", 440, 53, 28, Color.SADDLEBROWN);
        Text crop = buttonDesigner("clinic", 220, 140, 30, Color.SADDLEBROWN);
        Text tree = buttonDesigner("workshop", 730, 140, 30, Color.SADDLEBROWN);
        Text machine = buttonDesigner("lab", 210, 240, 30, Color.SADDLEBROWN);
        Text food = buttonDesigner("general Store", 730, 240, 30, Color.SADDLEBROWN);
        Text player = buttonDesigner("Player", 210, 340, 30, Color.SADDLEBROWN);
        Text sport = buttonDesigner("Cafe", 730, 340, 30, Color.SADDLEBROWN);
        Text back = buttonDesigner("Back", 54.5, 52, 21.5, Color.SADDLEBROWN);

        root.getChildren().addAll(titleText, back, crop, tree, machine, food, player, sport);
        back.setOnMouseClicked(event -> primaryStage.setScene(mainScene));
        food.setOnMouseClicked(event -> {
            customGeneralSceneBuilder(customCreationRoot, customScene);
            primaryStage.setScene(customCreationScene);
        });
        crop.setOnMouseClicked(event -> {
            customClinicSceneBuilder(customCreationRoot, customScene);
            primaryStage.setScene(customCreationScene);
        });
        tree.setOnMouseClicked(event -> {
            customWorkshopSceneBuilder(customCreationRoot, customScene);
            primaryStage.setScene(customCreationScene);
        });

        player.setOnMouseClicked(event -> {
            customPlayerSceneBuilder(customCreationRoot, customScene);
            primaryStage.setScene(customCreationScene);
        });

        machine.setOnMouseClicked(event -> {
            customMachineSceneBuilder(customCreationRoot, customScene);
            primaryStage.setScene(customCreationScene);
        });

        sport.setOnMouseClicked(event -> {
            customCafeSceneBuilder(customCreationRoot, customScene);
            primaryStage.setScene(customCreationScene);
        });


    }

    public static void customCafeSceneBuilder(Group root, Scene scene) {
        root.getChildren().clear();
        Image backImage = new Image("pics/menus/backButton.png");
        Image titleImage = new Image("pics/menus/backpackTitle.png");
        Image statusImage = new Image("pics/menus/machine.png");
        Rectangle status = rectangleBuilder(1000, 700, new ImagePattern(statusImage), 0, 0);
        Rectangle backButton = rectangleBuilder(60, 60, new ImagePattern(backImage), 50, 20);
        Rectangle moveButton = rectangleBuilder(60, 60, new ImagePattern(backImage), 880, 20);
        Rectangle title = rectangleBuilder(190, 60, new ImagePattern(titleImage), 430, 20);
        root.getChildren().addAll(backButton, title, status, moveButton);
        Text[] text = new Text[1];
        HashMultiset<Item> inputs = HashMultiset.create();
        HashMultiset<Item> outputs = HashMultiset.create();

        Text titleText = buttonDesigner("Workshop", 440, 53, 28, Color.SADDLEBROWN);
        Text back = buttonDesigner("Back", 54.5, 52, 21.5, Color.SADDLEBROWN);
        Text move = buttonDesigner("Done", 884, 52, 21.5, Color.SADDLEBROWN);
        Text money = buttonDesigner("Price", 750, 280, 21.5, Color.SADDLEBROWN);
        Text nameText = buttonDesigner("Name", 100, 120, 21.5, Color.SADDLEBROWN);
        TextField moneyNum = textFieldDesigner(860, 250, 70, 21.5, Color.DARKGOLDENROD);
        TextField name = textFieldDesigner(170, 90, 200, 21.5, Color.DARKGOLDENROD);
        root.getChildren().addAll(titleText, back, money, moneyNum, move, name, nameText);
        String[] machineAddress = new String[1];
        machineAddress[0] = "pics/tools/ironShovel.png";

        ComboBox<String> input = comboBoxDesigner("Add", 100, 150, Color.DARKGOLDENROD);
        for (Item item : items) {
            input.getItems().addAll(item.getName());
        }
        input.getSelectionModel().selectedIndexProperty()
                .addListener((ChangeListener<Number>) (ov, value, new_value) -> {
                    text[0] = buttonDesigner(items.get(new_value.intValue()).getName() + " " + (inputs.count(items.get(new_value.intValue())) + 1), 130,
                            30 * (inputs.size() + 10), 20, Color.SADDLEBROWN);
                    inputs.add(items.get(new_value.intValue()));
                    game.getVillage().getCafe().getGoods().add(items.get(new_value.intValue()));
                    root.getChildren().addAll(text[0]);
                });
        ComboBox<String> output = comboBoxDesigner("Remove", 400, 150, Color.DARKGOLDENROD);
        ArrayList<Item> goods = new ArrayList<>();
        for (Object item : game.getVillage().getCafe().getGoods().elementSet()) {
            if(item instanceof Item){
                goods.add((Item)item);
            }
        }
        for(Item item: goods){
            output.getItems().addAll(((Item) item).getName());
        }
        output.getSelectionModel().selectedIndexProperty()
                .addListener((ChangeListener<Number>) (ov, value, new_value) -> {
                    text[0] = buttonDesigner(goods.get(new_value.intValue()).getName() + " " + (outputs.count(goods.get(new_value.intValue())) + 1), 440,
                            30 * (outputs.size() + 10), 20, Color.SADDLEBROWN);
                    outputs.add(goods.get(new_value.intValue()));
                    game.getVillage().getCafe().getGoods().remove(goods.get(new_value.intValue()), game.getVillage().getCafe().getGoods().count(goods.get(new_value.intValue())));
                    root.getChildren().addAll(text[0]);
                });
        move.setOnMouseClicked(event -> {
            primaryStage.setScene(scene);
        });
        root.getChildren().addAll(input, output);
        back.setOnMouseClicked(event -> primaryStage.setScene(scene));
    }

    public static void customClinicSceneBuilder(Group root, Scene scene) {
        root.getChildren().clear();
        Image backImage = new Image("pics/menus/backButton.png");
        Image titleImage = new Image("pics/menus/backpackTitle.png");
        Image statusImage = new Image("pics/menus/machine.png");
        Rectangle status = rectangleBuilder(1000, 700, new ImagePattern(statusImage), 0, 0);
        Rectangle backButton = rectangleBuilder(60, 60, new ImagePattern(backImage), 50, 20);
        Rectangle moveButton = rectangleBuilder(60, 60, new ImagePattern(backImage), 880, 20);
        Rectangle title = rectangleBuilder(190, 60, new ImagePattern(titleImage), 430, 20);
        root.getChildren().addAll(backButton, title, status, moveButton);
        Text[] text = new Text[1];
        HashMultiset<Item> inputs = HashMultiset.create();
        HashMultiset<Item> outputs = HashMultiset.create();

        Text titleText = buttonDesigner("Clinic", 440, 53, 28, Color.SADDLEBROWN);
        Text back = buttonDesigner("Back", 54.5, 52, 21.5, Color.SADDLEBROWN);
        Text move = buttonDesigner("Done", 884, 52, 21.5, Color.SADDLEBROWN);
        Text money = buttonDesigner("Price", 750, 280, 21.5, Color.SADDLEBROWN);
        Text nameText = buttonDesigner("Name", 100, 120, 21.5, Color.SADDLEBROWN);
        TextField moneyNum = textFieldDesigner(860, 250, 70, 21.5, Color.DARKGOLDENROD);
        TextField name = textFieldDesigner(170, 90, 200, 21.5, Color.DARKGOLDENROD);
        root.getChildren().addAll(titleText, back, money, moneyNum, move, name, nameText);
        String[] machineAddress = new String[1];
        machineAddress[0] = "pics/tools/ironShovel.png";

        ComboBox<String> input = comboBoxDesigner("Add", 100, 150, Color.DARKGOLDENROD);
        for (Item item : items) {
            input.getItems().addAll(item.getName());
        }
        input.getSelectionModel().selectedIndexProperty()
                .addListener((ChangeListener<Number>) (ov, value, new_value) -> {
                    text[0] = buttonDesigner(items.get(new_value.intValue()).getName() + " " + (inputs.count(items.get(new_value.intValue())) + 1), 130,
                            30 * (inputs.size() + 10), 20, Color.SADDLEBROWN);
                    inputs.add(items.get(new_value.intValue()));
                    game.getVillage().getClinic().getGoods().add(items.get(new_value.intValue()));
                    root.getChildren().addAll(text[0]);
                });
        ComboBox<String> output = comboBoxDesigner("Remove", 400, 150, Color.DARKGOLDENROD);
        ArrayList<Item> goods = new ArrayList<>();
        for (Object item : game.getVillage().getClinic().getGoods().elementSet()) {
            if(item instanceof Item){
                goods.add((Item)item);
            }
        }
        for(Item item: goods){
            output.getItems().addAll(((Item) item).getName());
        }
        output.getSelectionModel().selectedIndexProperty()
                .addListener((ChangeListener<Number>) (ov, value, new_value) -> {
                    text[0] = buttonDesigner(goods.get(new_value.intValue()).getName() + " " + (outputs.count(goods.get(new_value.intValue())) + 1), 440,
                            30 * (outputs.size() + 10), 20, Color.SADDLEBROWN);
                    outputs.add(goods.get(new_value.intValue()));
                    game.getVillage().getClinic().getGoods().remove(goods.get(new_value.intValue()), game.getVillage().getClinic().getGoods().count(goods.get(new_value.intValue())));
                    root.getChildren().addAll(text[0]);
                });
        move.setOnMouseClicked(event -> {
            primaryStage.setScene(scene);
        });
        root.getChildren().addAll(input, output);
        back.setOnMouseClicked(event -> primaryStage.setScene(scene));
    }

    public static void customGeneralSceneBuilder(Group root, Scene scene) {
        root.getChildren().clear();
        Image backImage = new Image("pics/menus/backButton.png");
        Image titleImage = new Image("pics/menus/backpackTitle.png");
        Image statusImage = new Image("pics/menus/machine.png");
        Rectangle status = rectangleBuilder(1000, 700, new ImagePattern(statusImage), 0, 0);
        Rectangle backButton = rectangleBuilder(60, 60, new ImagePattern(backImage), 50, 20);
        Rectangle moveButton = rectangleBuilder(60, 60, new ImagePattern(backImage), 880, 20);
        Rectangle title = rectangleBuilder(190, 60, new ImagePattern(titleImage), 430, 20);
        root.getChildren().addAll(backButton, title, status, moveButton);
        Text[] text = new Text[1];
        HashMultiset<Item> inputs = HashMultiset.create();
        HashMultiset<Item> outputs = HashMultiset.create();

        Text titleText = buttonDesigner("General Store", 440, 53, 28, Color.SADDLEBROWN);
        Text back = buttonDesigner("Back", 54.5, 52, 21.5, Color.SADDLEBROWN);
        Text move = buttonDesigner("Done", 884, 52, 21.5, Color.SADDLEBROWN);
        Text money = buttonDesigner("Price", 750, 280, 21.5, Color.SADDLEBROWN);
        Text nameText = buttonDesigner("Name", 100, 120, 21.5, Color.SADDLEBROWN);
        TextField moneyNum = textFieldDesigner(860, 250, 70, 21.5, Color.DARKGOLDENROD);
        TextField name = textFieldDesigner(170, 90, 200, 21.5, Color.DARKGOLDENROD);
        root.getChildren().addAll(titleText, back, money, moneyNum, move, name, nameText);
        String[] machineAddress = new String[1];
        machineAddress[0] = "pics/tools/ironShovel.png";

        ComboBox<String> input = comboBoxDesigner("Add", 100, 150, Color.DARKGOLDENROD);
        for (Seed item : seeds) {
            input.getItems().addAll(item.getName());
        }
        input.getSelectionModel().selectedIndexProperty()
                .addListener((ChangeListener<Number>) (ov, value, new_value) -> {
                    text[0] = buttonDesigner(seeds.get(new_value.intValue()).getName() + " " + (inputs.count(seeds.get(new_value.intValue())) + 1), 130,
                            30 * (inputs.size() + 10), 20, Color.SADDLEBROWN);
                    inputs.add(seeds.get(new_value.intValue()));
                    game.getVillage().getMarket().getShop().get(2).getGoods().add(seeds.get(new_value.intValue()));
                    root.getChildren().addAll(text[0]);
                });
        ComboBox<String> output = comboBoxDesigner("Remove", 400, 150, Color.DARKGOLDENROD);
        ArrayList<Item> goods = new ArrayList<>();
        for (Object item : game.getVillage().getMarket().getShop().get(2).getGoods().elementSet()) {
            if(item instanceof Item){
                goods.add((Item)item);
            }
        }
        for(Item item: goods){
            output.getItems().addAll(((Item) item).getName());
        }
        output.getSelectionModel().selectedIndexProperty()
                .addListener((ChangeListener<Number>) (ov, value, new_value) -> {
                    text[0] = buttonDesigner(goods.get(new_value.intValue()).getName() + " " + (outputs.count(goods.get(new_value.intValue())) + 1), 440,
                            30 * (outputs.size() + 10), 20, Color.SADDLEBROWN);
                    outputs.add(goods.get(new_value.intValue()));
                    game.getVillage().getMarket().getShop().get(2).getGoods().remove(goods.get(new_value.intValue()), game.getVillage().getMarket().getShop().get(2).getGoods().count(goods.get(new_value.intValue())));
                    root.getChildren().addAll(text[0]);
                });
        move.setOnMouseClicked(event -> {
            primaryStage.setScene(scene);
        });
        root.getChildren().addAll(input, output);
        back.setOnMouseClicked(event -> primaryStage.setScene(scene));
    }

    public static void customWorkshopSceneBuilder(Group root, Scene scene) {
        root.getChildren().clear();
        Image backImage = new Image("pics/menus/backButton.png");
        Image titleImage = new Image("pics/menus/backpackTitle.png");
        Image statusImage = new Image("pics/menus/machine.png");
        Rectangle status = rectangleBuilder(1000, 700, new ImagePattern(statusImage), 0, 0);
        Rectangle backButton = rectangleBuilder(60, 60, new ImagePattern(backImage), 50, 20);
        Rectangle moveButton = rectangleBuilder(60, 60, new ImagePattern(backImage), 880, 20);
        Rectangle title = rectangleBuilder(190, 60, new ImagePattern(titleImage), 430, 20);
        root.getChildren().addAll(backButton, title, status, moveButton);
        Text[] text = new Text[1];
        HashMultiset<Item> inputs = HashMultiset.create();
        HashMultiset<Item> outputs = HashMultiset.create();

        Text titleText = buttonDesigner("Cafe Menu", 440, 53, 28, Color.SADDLEBROWN);
        Text back = buttonDesigner("Back", 54.5, 52, 21.5, Color.SADDLEBROWN);
        Text move = buttonDesigner("Done", 884, 52, 21.5, Color.SADDLEBROWN);
        Text money = buttonDesigner("Price", 750, 280, 21.5, Color.SADDLEBROWN);
        Text nameText = buttonDesigner("Name", 100, 120, 21.5, Color.SADDLEBROWN);
        TextField moneyNum = textFieldDesigner(860, 250, 70, 21.5, Color.DARKGOLDENROD);
        TextField name = textFieldDesigner(170, 90, 200, 21.5, Color.DARKGOLDENROD);
        root.getChildren().addAll(titleText, back, money, moneyNum, move, name, nameText);
        String[] machineAddress = new String[1];
        machineAddress[0] = "pics/tools/ironShovel.png";

        ComboBox<String> input = comboBoxDesigner("Add", 100, 150, Color.DARKGOLDENROD);
        for (Tool item : tools) {
            input.getItems().addAll(item.getName());
        }
        input.getSelectionModel().selectedIndexProperty()
                .addListener((ChangeListener<Number>) (ov, value, new_value) -> {
                    text[0] = buttonDesigner(tools.get(new_value.intValue()).getName() + " " + (inputs.count(tools.get(new_value.intValue())) + 1), 130,
                            30 * (inputs.size() + 10), 20, Color.SADDLEBROWN);
                    inputs.add(tools.get(new_value.intValue()));
                    game.getVillage().getWorkshop().getTool().add(tools.get(new_value.intValue()));
                    root.getChildren().addAll(text[0]);
                });
        ComboBox<String> output = comboBoxDesigner("Remove", 400, 150, Color.DARKGOLDENROD);
        ArrayList<Item> goods = new ArrayList<>();
        for (Object item : game.getVillage().getWorkshop().getTool().elementSet()) {
            if(item instanceof Item){
                goods.add((Item)item);
            }
        }
        for(Item item: goods){
            output.getItems().addAll(((Item) item).getName());
        }
        output.getSelectionModel().selectedIndexProperty()
                .addListener((ChangeListener<Number>) (ov, value, new_value) -> {
                    text[0] = buttonDesigner(goods.get(new_value.intValue()).getName() + " " + (outputs.count(goods.get(new_value.intValue())) + 1), 440,
                            30 * (outputs.size() + 10), 20, Color.SADDLEBROWN);
                    outputs.add(goods.get(new_value.intValue()));
                    game.getVillage().getWorkshop().getTool().remove(goods.get(new_value.intValue()), game.getVillage().getWorkshop().getTool().count(goods.get(new_value.intValue())));
                    root.getChildren().addAll(text[0]);
                });
        move.setOnMouseClicked(event -> {
            primaryStage.setScene(scene);
        });
        root.getChildren().addAll(input, output);
        back.setOnMouseClicked(event -> primaryStage.setScene(scene));
    }


    public static void tableSceneBuilder(Group root, Scene scene, ArrayList buttons, String name, int num) {
        root.getChildren().clear();
        Image backImage = new Image("pics/menus/backButton.png");
        Image titleImage = new Image("pics/menus/backpackTitle.png");
        Rectangle backButton = rectangleBuilder(60, 60, new ImagePattern(backImage), 50, 20);
        Rectangle title = rectangleBuilder(190, 60, new ImagePattern(titleImage), 430, 20);
        Text titleText = buttonDesigner(name, 440, 53, 28, Color.SADDLEBROWN);
        Text back = buttonDesigner("Back", 54.5, 52, 21.5, Color.SADDLEBROWN);
        root.getChildren().addAll(backButton, title, titleText, back);
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 2; j++) {
                Rectangle button = rectangleBuilder(200, 70, new ImagePattern(titleImage), 170 + 510 * j, 100 * (i + 1));
                root.getChildren().addAll(button);
                try {
                    if (buttons.get(i + 5 * j) instanceof Mission) {
                        Text text = buttonDesigner(((Mission) buttons.get(i + 5 * j)).getName(), 200 + 510 * j, 100 * (i + 1.4), 26, Color.SADDLEBROWN);
                        text.setOnMouseClicked(event -> {
                            for (int k = 0; k < buttons.size(); k++) {
                                if (buttons.get(k) instanceof Mission && ((Mission) buttons.get(k)).getName().equals(text.getText())) {
                                    choosedMission = (Mission) buttons.get(k);
                                    break;
                                }
                            }
                            flags[num] = true;
                            primaryStage.setScene(scene);
                        });
                        root.getChildren().addAll(text);
                    } else if (buttons.get(i + 5 * j) instanceof Train) {
                        Text text = buttonDesigner(((Train) buttons.get(i + 5 * j)).getName(), 200 + 510 * j, 100 * (i + 1.4), 26, Color.SADDLEBROWN);
                        text.setOnMouseClicked(event -> {
                            for (int k = 0; k < buttons.size(); k++) {
                                if (buttons.get(k) instanceof Train && ((Train) buttons.get(k)).getName().equals(text.getText())) {
                                    choosedTrain = (Train) buttons.get(k);
                                    break;
                                }
                            }
                            flags[num] = true;
                            primaryStage.setScene(scene);
                        });
                        root.getChildren().addAll(text);
                    } else if (buttons.get(i + 5 * j) instanceof Machine) {
                        Text text = buttonDesigner(((Machine) buttons.get(i + 5 * j)).getName(), 200 + 510 * j, 100 * (i + 1.4), 26, Color.SADDLEBROWN);
                        text.setOnMouseClicked(event -> {
                            for (int k = 0; k < buttons.size(); k++) {
                                if (buttons.get(k) instanceof Machine && ((Machine) buttons.get(k)).getName().equals(text.getText())) {
                                    choosedItem = (Machine) buttons.get(k);
                                    break;
                                }
                            }
                            flags[num] = true;
                            primaryStage.setScene(scene);
                        });
                        root.getChildren().addAll(text);
                    }
                } catch (Exception e) {
                }

            }
        }
        back.setOnMouseClicked(event -> {
            primaryStage.setScene(scene);
            initializeFlags();
        });
    }

    public static void customFoodSceneBuilder(Group root, Scene scene) {
        root.getChildren().clear();
        Image backImage = new Image("pics/menus/backButton.png");
        Image titleImage = new Image("pics/menus/backpackTitle.png");
        Image statusImage = new Image("pics/menus/food.png");
        Rectangle status = rectangleBuilder(1000, 700, new ImagePattern(statusImage), 0, 0);
        Rectangle backButton = rectangleBuilder(60, 60, new ImagePattern(backImage), 50, 20);
        Rectangle moveButton = rectangleBuilder(60, 60, new ImagePattern(backImage), 880, 20);
        Rectangle title = rectangleBuilder(190, 60, new ImagePattern(titleImage), 430, 20);
        root.getChildren().addAll(backButton, title, status, moveButton);
        Text[] text = new Text[1];
        ArrayList<Tool> chosenTools = new ArrayList<>();
        HashMultiset<Item> chosenIngredients = HashMultiset.create();

        Text titleText = buttonDesigner("Custom Game", 440, 53, 28, Color.SADDLEBROWN);
        Text back = buttonDesigner("Back", 54.5, 52, 21.5, Color.SADDLEBROWN);
        Text move = buttonDesigner("Done", 884, 52, 21.5, Color.SADDLEBROWN);
        Text health = buttonDesigner("Health", 750, 280, 21.5, Color.SADDLEBROWN);
        Text energy = buttonDesigner("Energy", 750, 380, 21.5, Color.SADDLEBROWN);
        Text stamina = buttonDesigner("Stamina", 750, 480, 21.5, Color.SADDLEBROWN);
        Text satiety = buttonDesigner("Satiety", 750, 580, 21.5, Color.SADDLEBROWN);
        Text nameText = buttonDesigner("Name", 100, 120, 21.5, Color.SADDLEBROWN);
        Text foodImage = buttonDesigner("Food Image", 700, 80, 21.5, Color.SADDLEBROWN);
        TextField healthNum = textFieldDesigner(860, 250, 50, 21.5, Color.DARKGOLDENROD);
        TextField energyNum = textFieldDesigner(860, 350, 50, 21.5, Color.DARKGOLDENROD);
        TextField staminaNum = textFieldDesigner(860, 450, 50, 21.5, Color.DARKGOLDENROD);
        TextField satietyNum = textFieldDesigner(860, 550, 50, 21.5, Color.DARKGOLDENROD);
        TextField name = textFieldDesigner(170, 90, 100, 21.5, Color.DARKGOLDENROD);
        root.getChildren().addAll(titleText, back, health, energy, satiety, stamina, healthNum, energyNum, staminaNum,
                satietyNum, move, name, nameText, foodImage);

        String[] foodAddress = new String[1];
        foodAddress[0] = "pics/food/frenchFries.png";


        foodImage.setOnMouseClicked(event -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setInitialDirectory(new File("pics/food"));
            FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.png");
            fileChooser.getExtensionFilters().add(extFilter);
            File file = fileChooser.showOpenDialog(primaryStage);
            Image image = new Image("file:\\\\\\" + file.getAbsolutePath());
            foodAddress[0] = "file:\\\\\\" + file.getAbsolutePath();
            Rectangle rectanglePic = rectangleBuilder(100, 100, new ImagePattern(image), 720, 100);
            root.getChildren().addAll(rectanglePic);
        });

        ComboBox<String> cookingTools = comboBoxDesigner("Cooking Tools", 100, 150, Color.DARKGOLDENROD);
        for (Tool tool : cookingUtensil) {
            cookingTools.getItems().addAll(tool.getName());
        }
        cookingTools.getSelectionModel().selectedIndexProperty()
                .addListener((ChangeListener<Number>) (ov, value, new_value) -> {
                    text[0] = buttonDesigner(cookingUtensil.get(new_value.intValue()).getName(), 140,
                            30 * (chosenTools.size() + 10), 20, Color.SADDLEBROWN);
                    chosenTools.add((Tool) cookingUtensil.get(new_value.intValue()));
                    root.getChildren().addAll(text[0]);
                });
        ComboBox<String> ingredients = comboBoxDesigner("Ingredients", 400, 150, Color.DARKGOLDENROD);
        for (Fruit fruit : fruits) {
            ingredients.getItems().addAll(fruit.getName());
        }
        ingredients.getSelectionModel().selectedIndexProperty()
                .addListener((ChangeListener<Number>) (ov, value, new_value) -> {
                    text[0] = buttonDesigner(fruits.get(new_value.intValue()).getName() + " " + (chosenIngredients.count(fruits.get(new_value.intValue())) + 1), 440,
                            30 * (chosenIngredients.size() + 10), 20, Color.SADDLEBROWN);
                    chosenIngredients.add(fruits.get(new_value.intValue()).clone());
                    root.getChildren().addAll(text[0]);
                });
        root.getChildren().addAll(cookingTools, ingredients);
        move.setOnMouseClicked(event -> {
            Food food = new Food(new HashMap<String, Double>() {{
                put("Health", Double.parseDouble(healthNum.getText().toString()));
                put("Energy", Double.parseDouble(energyNum.getText().toString()));
                put("Stamina", Double.parseDouble(staminaNum.getText().toString()));
                put("Satiety", Double.parseDouble(satietyNum.getText().toString()));
            }}, 1, name.getText().toString(), new Probability() {{
            }},
                    0, chosenTools, new ArrayList<Task>() {{
            }}, new Dissassemblity(0, chosenIngredients,
                    false), new Recipe(), new Image(foodAddress[0]));
            foods.add(food);
            Recipe recipe = new Recipe();
            recipe.setFood(food);
            game.getFarm().getHome().getKitchen().getRecipe().add(recipe);
            primaryStage.setScene(scene);
        });
        back.setOnMouseClicked(event -> primaryStage.setScene(scene));
    }

    public static void customCropSceneBuilder(Group root, Scene scene) {
        root.getChildren().clear();
        Image backImage = new Image("pics/menus/backButton.png");
        Image titleImage = new Image("pics/menus/backpackTitle.png");
        Image statusImage = new Image("pics/menus/fruit.png");
        Rectangle status = rectangleBuilder(1000, 700, new ImagePattern(statusImage), 0, 0);
        Rectangle backButton = rectangleBuilder(60, 60, new ImagePattern(backImage), 50, 20);
        Rectangle moveButton = rectangleBuilder(60, 60, new ImagePattern(backImage), 880, 20);
        Rectangle title = rectangleBuilder(190, 60, new ImagePattern(titleImage), 430, 20);
        root.getChildren().addAll(backButton, title, status, moveButton);
        Weather[] chosenWeather = new Weather[1];

        Text titleText = buttonDesigner("Custom Game", 440, 53, 28, Color.SADDLEBROWN);
        Text back = buttonDesigner("Back", 54.5, 52, 21.5, Color.SADDLEBROWN);
        Text move = buttonDesigner("Done", 884, 52, 21.5, Color.SADDLEBROWN);
        Text health = buttonDesigner("Health", 750, 280, 21.5, Color.SADDLEBROWN);
        Text energy = buttonDesigner("Energy", 750, 380, 21.5, Color.SADDLEBROWN);
        Text stamina = buttonDesigner("Stamina", 750, 480, 21.5, Color.SADDLEBROWN);
        Text satiety = buttonDesigner("Satiety", 750, 580, 21.5, Color.SADDLEBROWN);
        Text harvestLeft = buttonDesigner("Harvest Left", 110, 280, 21.5, Color.SADDLEBROWN);
        Text lifeCycle = buttonDesigner("Life Cycle", 110, 380, 21.5, Color.SADDLEBROWN);
        Text spoilageTime = buttonDesigner("Spoilage Time", 110, 480, 21.5, Color.SADDLEBROWN);
        Text price = buttonDesigner("Price", 110, 580, 21.5, Color.SADDLEBROWN);
        Text fruitPrice = buttonDesigner("Fruit Price", 410, 280, 21.5, Color.SADDLEBROWN);
        Text seedPrice = buttonDesigner("Seed Price", 410, 380, 21.5, Color.SADDLEBROWN);
        Text nameText = buttonDesigner("Name", 100, 120, 21.5, Color.SADDLEBROWN);
        Text cropImage = buttonDesigner("Crop Image", 500, 580, 21.5, Color.SADDLEBROWN);
        Text fruitImage = buttonDesigner("Fruit Image", 700, 80, 21.5, Color.SADDLEBROWN);
        Text seedImage = buttonDesigner("Seed Image", 300, 580, 21.5, Color.SADDLEBROWN);

        String[] fruitAddress = new String[1];
        fruitAddress[0] = "pics/fruits/apple.png";
        String[] seedAddress = new String[1];
        seedAddress[0] = "pics/seed.png";

        TextField healthNum = textFieldDesigner(860, 250, 50, 20, Color.DARKGOLDENROD);
        TextField energyNum = textFieldDesigner(860, 350, 50, 20, Color.DARKGOLDENROD);
        TextField staminaNum = textFieldDesigner(860, 450, 50, 20, Color.DARKGOLDENROD);
        TextField satietyNum = textFieldDesigner(860, 550, 50, 20, Color.DARKGOLDENROD);
        TextField harvestNum = textFieldDesigner(260, 250, 40, 20, Color.DARKGOLDENROD);
        TextField cycleNum = textFieldDesigner(260, 350, 40, 20, Color.DARKGOLDENROD);
        TextField spoilageNum = textFieldDesigner(260, 450, 40, 20, Color.DARKGOLDENROD);
        TextField fruitpriceNum = textFieldDesigner(560, 250, 60, 20, Color.DARKGOLDENROD);
        TextField seedpriceNum = textFieldDesigner(560, 350, 60, 21.5, Color.DARKGOLDENROD);
        TextField priceNum = textFieldDesigner(260, 550, 40, 21.5, Color.DARKGOLDENROD);
        TextField name = textFieldDesigner(170, 90, 100, 21.5, Color.DARKGOLDENROD);
        root.getChildren().addAll(titleText, back, health, energy, satiety, stamina, healthNum, energyNum, staminaNum,
                satietyNum, move, name, nameText, harvestLeft, lifeCycle, spoilageTime, price, harvestNum, cycleNum,
                spoilageNum, priceNum, fruitPrice, seedPrice, fruitpriceNum, seedpriceNum, cropImage, fruitImage, seedImage);

        ComboBox<String> cookingTools = comboBoxDesigner("Weathers", 100, 150, Color.DARKGOLDENROD);
        for (Weather weather : weathers) {
            cookingTools.getItems().addAll(weather.getType());
        }
        cookingTools.getSelectionModel().selectedIndexProperty()
                .addListener((ChangeListener<Number>) (ov, value, new_value) -> {
                    chosenWeather[0] = weathers.get(new_value.intValue());
                });
        root.getChildren().addAll(cookingTools);
        fruitImage.setOnMouseClicked(event -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setInitialDirectory(new File("pics/fruits"));
            FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.png");
            fileChooser.getExtensionFilters().add(extFilter);
            File file = fileChooser.showOpenDialog(primaryStage);
            Image image = new Image("file:\\\\\\" + file.getAbsolutePath());
            fruitAddress[0] = "file:\\\\\\" + file.getAbsolutePath();
            Rectangle rectanglePic = rectangleBuilder(100, 100, new ImagePattern(image), 720, 100);
            root.getChildren().addAll(rectanglePic);
        });
        seedImage.setOnMouseClicked(event -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setInitialDirectory(new File("pics"));
            FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.png");
            fileChooser.getExtensionFilters().add(extFilter);
            File file = fileChooser.showOpenDialog(primaryStage);
            Image image = new Image("file:\\\\\\" + file.getAbsolutePath());
            seedAddress[0] = "file:\\\\\\" + file.getAbsolutePath();
            Rectangle rectanglePic = rectangleBuilder(100, 100, new ImagePattern(image), 320, 450);
            root.getChildren().addAll(rectanglePic);
        });
        cropImage.setOnMouseClicked(event -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setInitialDirectory(new File("pics/fruits"));
            FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.png");
            fileChooser.getExtensionFilters().add(extFilter);
            File file = fileChooser.showOpenDialog(primaryStage);
            Image image = new Image("file:\\\\\\" + file.getAbsolutePath());
            Rectangle rectanglePic = rectangleBuilder(100, 100, new ImagePattern(image), 520, 450);
            root.getChildren().addAll(rectanglePic);
        });
        move.setOnMouseClicked(event -> {
            Fruit fruit = new Fruit(new HashMap<String, Double>() {{
                put("Health", Double.parseDouble(healthNum.getText().toString()));
                put("Energy", Double.parseDouble(energyNum.getText().toString()));
                put("Stamina", Double.parseDouble(staminaNum.getText().toString()));
                put("Satiety", Double.parseDouble(satietyNum.getText().toString()));
            }}, 1, name.getText().toString(),
                    new Probability() {{
                    }}, Double.parseDouble(fruitpriceNum.getText().toString()), new ArrayList<Tool>(), new ArrayList<Task>() {{
            }}, false, chosenWeather[0], new Image(fruitAddress[0]));
            Crop crop = new Crop(name.getText().toString(), chosenWeather[0], Integer.parseInt(cycleNum.getText().toString()),
                    Integer.parseInt(spoilageNum.getText().toString()), Integer.parseInt(harvestNum.getText().toString()), new ArrayList<Task>() {{
            }}, fruit);
            Seed seed = new Seed(name.getText().toString() + " Seed", Double.parseDouble(seedpriceNum.getText().toString()),
                    new ArrayList<Task>() {{
                    }}, crop, new Image(seedAddress[0]));
            fruits.add(fruit);
            crops.add(crop);
            seeds.add(seed);
            game.getVillage().getMarket().getShop().get(2).getGoods().add(seed.clone(), 10);
            primaryStage.setScene(scene);
        });
        back.setOnMouseClicked(event -> primaryStage.setScene(scene));
    }

    public static void customTreeSceneBuilder(Group root, Scene scene) {
        root.getChildren().clear();
        Image backImage = new Image("pics/menus/backButton.png");
        Image titleImage = new Image("pics/menus/backpackTitle.png");
        Image statusImage = new Image("pics/menus/tree.png");
        Rectangle status = rectangleBuilder(1000, 700, new ImagePattern(statusImage), 0, 0);
        Rectangle backButton = rectangleBuilder(60, 60, new ImagePattern(backImage), 50, 20);
        Rectangle moveButton = rectangleBuilder(60, 60, new ImagePattern(backImage), 880, 20);
        Rectangle title = rectangleBuilder(190, 60, new ImagePattern(titleImage), 430, 20);
        root.getChildren().addAll(backButton, title, status, moveButton);
        Weather[] chosenWeather = new Weather[1];

        Text titleText = buttonDesigner("Custom Game", 440, 53, 28, Color.SADDLEBROWN);
        Text back = buttonDesigner("Back", 54.5, 52, 21.5, Color.SADDLEBROWN);
        Text move = buttonDesigner("Done", 884, 52, 21.5, Color.SADDLEBROWN);
        Text health = buttonDesigner("Health", 750, 280, 21.5, Color.SADDLEBROWN);
        Text energy = buttonDesigner("Energy", 750, 380, 21.5, Color.SADDLEBROWN);
        Text stamina = buttonDesigner("Stamina", 750, 480, 21.5, Color.SADDLEBROWN);
        Text satiety = buttonDesigner("Satiety", 750, 580, 21.5, Color.SADDLEBROWN);
        Text min = buttonDesigner("Min Production", 105, 280, 20, Color.SADDLEBROWN);
        Text max = buttonDesigner("Max Production", 105, 380, 20, Color.SADDLEBROWN);
        Text fruitPrice = buttonDesigner("Fruit Price", 110, 480, 21.5, Color.SADDLEBROWN);
        Text price = buttonDesigner("Tree Price", 110, 580, 21.5, Color.SADDLEBROWN);
        Text nameText = buttonDesigner("Name", 100, 120, 21.5, Color.SADDLEBROWN);
        Text treeImage = buttonDesigner("Tree Image", 400, 480, 21.5, Color.SADDLEBROWN);
        Text fruitImage = buttonDesigner("Fruit Image", 700, 80, 21.5, Color.SADDLEBROWN);
        TextField healthNum = textFieldDesigner(860, 250, 50, 20, Color.DARKGOLDENROD);
        TextField energyNum = textFieldDesigner(860, 350, 50, 20, Color.DARKGOLDENROD);
        TextField staminaNum = textFieldDesigner(860, 450, 50, 20, Color.DARKGOLDENROD);
        TextField satietyNum = textFieldDesigner(860, 550, 50, 20, Color.DARKGOLDENROD);
        TextField minNum = textFieldDesigner(260, 250, 60, 20, Color.DARKGOLDENROD);
        TextField maxNum = textFieldDesigner(260, 350, 60, 20, Color.DARKGOLDENROD);
        TextField fruitPriceNum = textFieldDesigner(260, 450, 60, 20, Color.DARKGOLDENROD);
        TextField priceNum = textFieldDesigner(260, 550, 60, 21.5, Color.DARKGOLDENROD);
        TextField name = textFieldDesigner(170, 90, 100, 21.5, Color.DARKGOLDENROD);
        root.getChildren().addAll(titleText, back, health, energy, satiety, stamina, healthNum, energyNum, staminaNum,
                satietyNum, move, name, nameText, min, max, fruitPrice, price, minNum, maxNum,
                fruitPriceNum, priceNum, fruitImage, treeImage);
        String[] fruitAddress = new String[1];
        fruitAddress[0] = "pics/fruits/apple.png";
        String[] treeAddress = new String[1];
        treeAddress[0] = "pics/tree/tree.png";

        fruitImage.setOnMouseClicked(event -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setInitialDirectory(new File("pics/fruits"));
            FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.png");
            fileChooser.getExtensionFilters().add(extFilter);
            File file = fileChooser.showOpenDialog(primaryStage);
            Image image = new Image("file:\\\\\\" + file.getAbsolutePath());
            fruitAddress[0] = "file:\\\\\\" + file.getAbsolutePath();
            Rectangle rectanglePic = rectangleBuilder(100, 100, new ImagePattern(image), 720, 100);
            root.getChildren().addAll(rectanglePic);
        });

        treeImage.setOnMouseClicked(event -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setInitialDirectory(new File("pics/tree"));
            FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.png");
            fileChooser.getExtensionFilters().add(extFilter);
            File file = fileChooser.showOpenDialog(primaryStage);
            Image image = new Image("file:\\\\\\" + file.getAbsolutePath());
            treeAddress[0] = "file:\\\\\\" + file.getAbsolutePath();
            Rectangle rectanglePic = rectangleBuilder(130, 200, new ImagePattern(image), 400, 250);
            root.getChildren().addAll(rectanglePic);
        });

        ComboBox<String> cookingTools = comboBoxDesigner("Weathers", 100, 150, Color.DARKGOLDENROD);
        for (Weather weather : weathers) {
            cookingTools.getItems().addAll(weather.getType());
        }
        cookingTools.getSelectionModel().selectedIndexProperty()
                .addListener((ChangeListener<Number>) (ov, value, new_value) -> {
                    chosenWeather[0] = weathers.get(new_value.intValue());
                });
        root.getChildren().addAll(cookingTools);
        move.setOnMouseClicked(event -> {
            Fruit fruit = new Fruit(new HashMap<String, Double>() {{
                put("Health", Double.parseDouble(healthNum.getText().toString()));
                put("Energy", Double.parseDouble(energyNum.getText().toString()));
                put("Stamina", Double.parseDouble(staminaNum.getText().toString()));
                put("Satiety", Double.parseDouble(satietyNum.getText().toString()));
            }}, 1, name.getText().toString(),
                    new Probability() {{
                    }}, Double.parseDouble(fruitPriceNum.getText().toString()), new ArrayList<Tool>(), new ArrayList<Task>() {{
            }}, false, chosenWeather[0], new Image(fruitAddress[0]));
            GardenTree gardenTree = new GardenTree(name.getText().toString(), chosenWeather[0],
                    new ArrayList<Integer>() {{
                        add(Integer.parseInt(minNum.getText().toString()));
                        add(Integer.parseInt(maxNum.getText().toString()));
                    }},
                    fruit, Integer.parseInt(priceNum.getText().toString()), new Image(treeAddress[0]));
            fruits.add(fruit);
            gardenTrees.add(gardenTree);
            game.getFarm().getGarden().getGardenTree().add(gardenTree.clone());
            primaryStage.setScene(scene);
        });
        back.setOnMouseClicked(event -> primaryStage.setScene(scene));
    }

    public static void customPlayerSceneBuilder(Group root, Scene scene) {
        root.getChildren().clear();
        Image backImage = new Image("pics/menus/backButton.png");
        Image titleImage = new Image("pics/menus/backpackTitle.png");
        Image statusImage = new Image("pics/menus/player.png");
        Rectangle status = rectangleBuilder(1000, 700, new ImagePattern(statusImage), 0, 0);
        Rectangle backButton = rectangleBuilder(60, 60, new ImagePattern(backImage), 50, 20);
        Rectangle moveButton = rectangleBuilder(60, 60, new ImagePattern(backImage), 880, 20);
        Rectangle title = rectangleBuilder(190, 60, new ImagePattern(titleImage), 430, 20);
        root.getChildren().addAll(backButton, title, status, moveButton);
        Text[] text = new Text[1];

        Text titleText = buttonDesigner("Player", 440, 53, 28, Color.SADDLEBROWN);
        Text back = buttonDesigner("Back", 54.5, 52, 21.5, Color.SADDLEBROWN);
        Text move = buttonDesigner("Done", 884, 52, 21.5, Color.SADDLEBROWN);
        Text health = buttonDesigner("Health", 750, 280, 21.5, Color.SADDLEBROWN);
        Text energy = buttonDesigner("Energy", 750, 380, 21.5, Color.SADDLEBROWN);
        Text stamina = buttonDesigner("Stamina", 750, 480, 21.5, Color.SADDLEBROWN);
        Text satiety = buttonDesigner("Satiety", 750, 580, 21.5, Color.SADDLEBROWN);
        Text money = buttonDesigner("Money", 110, 280, 21.5, Color.SADDLEBROWN);
        Text mission = buttonDesigner("Mission Capacity", 105, 380, 18, Color.SADDLEBROWN);
        Text level = buttonDesigner("Level", 110, 480, 21.5, Color.SADDLEBROWN);
        Text experience = buttonDesigner("Experience", 110, 580, 21.5, Color.SADDLEBROWN);
        Text capacity = buttonDesigner("Capacity", 410, 380, 21.5, Color.SADDLEBROWN);
        Text nameText = buttonDesigner("Name", 100, 120, 21.5, Color.SADDLEBROWN);
        TextField healthNum = textFieldDesigner(860, 250, 50, 20, Color.DARKGOLDENROD);
        TextField energyNum = textFieldDesigner(860, 350, 50, 20, Color.DARKGOLDENROD);
        TextField staminaNum = textFieldDesigner(860, 450, 50, 20, Color.DARKGOLDENROD);
        TextField satietyNum = textFieldDesigner(860, 550, 50, 20, Color.DARKGOLDENROD);
        TextField moneyNum = textFieldDesigner(260, 250, 60, 20, Color.DARKGOLDENROD);
        TextField missionNum = textFieldDesigner(260, 350, 60, 20, Color.DARKGOLDENROD);
        TextField levelNum = textFieldDesigner(260, 450, 60, 20, Color.DARKGOLDENROD);
        TextField experienceNum = textFieldDesigner(560, 350, 60, 21.5, Color.DARKGOLDENROD);
        TextField capacityNum = textFieldDesigner(260, 550, 60, 21.5, Color.DARKGOLDENROD);
        TextField name = textFieldDesigner(170, 90, 100, 21.5, Color.DARKGOLDENROD);
        root.getChildren().addAll(titleText, back, health, energy, satiety, stamina, healthNum, energyNum, staminaNum,
                satietyNum, move, name, nameText, money, mission, experience, level, moneyNum, missionNum,
                levelNum, capacityNum, capacity, experienceNum);

        HashMultiset<Item> chosenFacilities = HashMultiset.create();

        ComboBox<String> cookingTools = comboBoxDesigner("Backpack Facilities", 100, 150, Color.DARKGOLDENROD);
        for (Tool tool : tools) {
            cookingTools.getItems().addAll(tool.getName());
        }
        cookingTools.getSelectionModel().selectedIndexProperty()
                .addListener((ChangeListener<Number>) (ov, value, new_value) -> {
                    root.getChildren().remove(text[0]);
                    text[0] = buttonDesigner(tools.get(new_value.intValue()).getName(), 430, 230, 20, Color.SADDLEBROWN);
                    chosenFacilities.add(tools.get(new_value.intValue()));
                    root.getChildren().addAll(text[0]);
                });
        root.getChildren().addAll(cookingTools);
        move.setOnMouseClicked(event -> {
            Backpack backpack = new Backpack(Double.parseDouble(capacityNum.getText().toString()), Integer.parseInt(levelNum.getText().toString()));
            backpack.setItem(chosenFacilities);
            ArrayList<Feature> features = new ArrayList<>();
            features.add(new Feature("Energy", Double.parseDouble(energyNum.getText().toString())));
            features.add(new Feature("Health", Double.parseDouble(healthNum.getText().toString())));
            features.add(new Feature("Stamina", Double.parseDouble(staminaNum.getText().toString())));
            features.add(new Feature("Satiety", Double.parseDouble(staminaNum.getText().toString())));
            Player player = new Player(Integer.parseInt(levelNum.getText().toString()), Integer.parseInt(experienceNum.getText().toString()), features,
                    name.getText().toString(), backpack, Double.parseDouble(moneyNum.getText().toString()), Integer.parseInt(missionNum.getText().toString()));
            game.setPlayer(player);
            primaryStage.setScene(scene);
        });
        back.setOnMouseClicked(event -> primaryStage.setScene(scene));
    }

    public static void customMachineSceneBuilder(Group root, Scene scene) {
        root.getChildren().clear();
        Image backImage = new Image("pics/menus/backButton.png");
        Image titleImage = new Image("pics/menus/backpackTitle.png");
        Image statusImage = new Image("pics/menus/machine.png");
        Rectangle status = rectangleBuilder(1000, 700, new ImagePattern(statusImage), 0, 0);
        Rectangle backButton = rectangleBuilder(60, 60, new ImagePattern(backImage), 50, 20);
        Rectangle moveButton = rectangleBuilder(60, 60, new ImagePattern(backImage), 880, 20);
        Rectangle title = rectangleBuilder(190, 60, new ImagePattern(titleImage), 430, 20);
        root.getChildren().addAll(backButton, title, status, moveButton);
        Text[] text = new Text[1];
        HashMultiset<Tool> inputs = HashMultiset.create();
        HashMultiset<Item> outputs = HashMultiset.create();

        Text titleText = buttonDesigner("Custom Game", 440, 53, 28, Color.SADDLEBROWN);
        Text back = buttonDesigner("Back", 54.5, 52, 21.5, Color.SADDLEBROWN);
        Text move = buttonDesigner("Done", 884, 52, 21.5, Color.SADDLEBROWN);
        Text money = buttonDesigner("Price", 750, 280, 21.5, Color.SADDLEBROWN);
        Text nameText = buttonDesigner("Name", 100, 120, 21.5, Color.SADDLEBROWN);
        Text machineImage = buttonDesigner("Machine Image", 720, 580, 21.5, Color.SADDLEBROWN);
        TextField moneyNum = textFieldDesigner(860, 250, 70, 21.5, Color.DARKGOLDENROD);
        TextField name = textFieldDesigner(170, 90, 200, 21.5, Color.DARKGOLDENROD);
        root.getChildren().addAll(titleText, back, money, moneyNum, move, name, nameText, machineImage);
        String[] machineAddress = new String[1];
        machineAddress[0] = "pics/tools/ironShovel.png";

        machineImage.setOnMouseClicked(event -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setInitialDirectory(new File("pics/tools"));
            FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.png");
            fileChooser.getExtensionFilters().add(extFilter);
            File file = fileChooser.showOpenDialog(primaryStage);
            Image image = new Image("file:\\\\\\" + file.getAbsolutePath());
            machineAddress[0] = "file:\\\\\\" + file.getAbsolutePath();
            Rectangle rectanglePic = rectangleBuilder(150, 180, new ImagePattern(image), 720, 350);
            root.getChildren().addAll(rectanglePic);
        });
        ComboBox<String> input = comboBoxDesigner("Input", 100, 150, Color.DARKGOLDENROD);
        for (Mineral mineral : minerals) {
            input.getItems().addAll(mineral.getName());
        }
        input.getSelectionModel().selectedIndexProperty()
                .addListener((ChangeListener<Number>) (ov, value, new_value) -> {
                    text[0] = buttonDesigner(minerals.get(new_value.intValue()).getName() + " " + (inputs.count(minerals.get(new_value.intValue())) + 1), 130,
                            30 * (inputs.size() + 10), 20, Color.SADDLEBROWN);
                    inputs.add(tools.get(new_value.intValue()));
                    root.getChildren().addAll(text[0]);
                });
        ComboBox<String> output = comboBoxDesigner("Output", 400, 150, Color.DARKGOLDENROD);
        for (Tool tool : tools) {
            output.getItems().addAll(tool.getName());
        }
        output.getSelectionModel().selectedIndexProperty()
                .addListener((ChangeListener<Number>) (ov, value, new_value) -> {
                    text[0] = buttonDesigner(tools.get(new_value.intValue()).getName() + " " + (outputs.count(tools.get(new_value.intValue())) + 1), 440,
                            30 * (outputs.size() + 10), 20, Color.SADDLEBROWN);
                    outputs.add(tools.get(new_value.intValue()));
                    root.getChildren().addAll(text[0]);
                });
        root.getChildren().addAll(input, output);
        move.setOnMouseClicked(event -> {
            Machine machine = new Machine(inputs, outputs);
            machine.setName(name.getText().toString());
            machine.setValid(true);
            machine.setPrice(Double.parseDouble(moneyNum.getText().toString()));
            machine.setPicture(new Image(machineAddress[0]));
            tools.add(machine);
            game.getVillage().getLaboratory().getGoods().add(machine);
            primaryStage.setScene(scene);
        });
        back.setOnMouseClicked(event -> primaryStage.setScene(scene));
    }

    public static void customTrainSceneBuilder(Group root, Scene scene, String titleName) {
        root.getChildren().clear();
        Image backImage = new Image("pics/menus/backButton.png");
        Image titleImage = new Image("pics/menus/backpackTitle.png");
        Image statusImage = new Image("pics/menus/train.png");
        Rectangle status = rectangleBuilder(1000, 700, new ImagePattern(statusImage), 0, 0);
        Rectangle backButton = rectangleBuilder(60, 60, new ImagePattern(backImage), 50, 20);
        Rectangle moveButton = rectangleBuilder(60, 60, new ImagePattern(backImage), 880, 20);
        Rectangle title = rectangleBuilder(190, 60, new ImagePattern(titleImage), 430, 20);
        root.getChildren().addAll(backButton, title, status, moveButton);

        Text titleText = buttonDesigner(titleName, 440, 53, 28, Color.SADDLEBROWN);
        Text back = buttonDesigner("Back", 54.5, 52, 21.5, Color.SADDLEBROWN);
        Text move = buttonDesigner("Done", 884, 52, 21.5, Color.SADDLEBROWN);
        Text consumption = buttonDesigner("Consumption", 120, 280, 21.5, Color.SADDLEBROWN);
        Text refill = buttonDesigner("Refill rate", 120, 380, 21.5, Color.SADDLEBROWN);
        Text max = buttonDesigner("Max", 150, 480, 21.5, Color.SADDLEBROWN);
        Text current = buttonDesigner("Current", 150, 580, 21.5, Color.SADDLEBROWN);
        root.getChildren().addAll(consumption, refill, max, current);
        Text money = buttonDesigner("Price", 750, 150, 21.5, Color.SADDLEBROWN);
        Text nameText = buttonDesigner("Name", 100, 120, 21.5, Color.SADDLEBROWN);
        TextField moneyNum = textFieldDesigner(860, 120, 70, 21.5, Color.DARKGOLDENROD);
        TextField name = textFieldDesigner(170, 90, 200, 21.5, Color.DARKGOLDENROD);
        TextField[] refillRate = new TextField[4];
        TextField[] consumptionRate = new TextField[4];
        TextField[] maximum = new TextField[4];
        TextField[] currentAmount = new TextField[4];
        for (int i = 0; i < 4; i++) {
            consumptionRate[i] = textFieldDesigner(320 + 150 * i, 250, 70, 21.5, Color.DARKGOLDENROD);
            refillRate[i] = textFieldDesigner(320 + 150 * i, 350, 70, 21.5, Color.DARKGOLDENROD);
            maximum[i] = textFieldDesigner(320 + 150 * i, 450, 70, 21.5, Color.DARKGOLDENROD);
            currentAmount[i] = textFieldDesigner(320 + 150 * i, 550, 70, 21.5, Color.DARKGOLDENROD);
            refillRate[i].setEditable(false);
            consumptionRate[i].setEditable(false);
            maximum[i].setEditable(false);
            currentAmount[i].setEditable(false);
            root.getChildren().addAll(refillRate[i], consumptionRate[i], maximum[i], currentAmount[i]);
        }
        root.getChildren().addAll(titleText, back, money, moneyNum, move, name, nameText);
        CheckBox health = checkBoxDesigner("Health", 320, 200, 23);
        CheckBox energy = checkBoxDesigner("Energy", 470, 200, 23);
        CheckBox stamina = checkBoxDesigner("Stamina", 620, 200, 23);
        CheckBox satiety = checkBoxDesigner("Satiety", 770, 200, 23);
        root.getChildren().addAll(health, energy, stamina, satiety);

        health.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue) {
                refillRate[0].setEditable(false);
                consumptionRate[0].setEditable(false);
                maximum[0].setEditable(false);
                currentAmount[0].setEditable(false);
            } else {
                refillRate[0].setEditable(true);
                consumptionRate[0].setEditable(true);
                maximum[0].setEditable(true);
                currentAmount[0].setEditable(true);
            }
        });

        energy.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue) {
                refillRate[1].setEditable(false);
                consumptionRate[1].setEditable(false);
                maximum[1].setEditable(false);
                currentAmount[1].setEditable(false);
            } else {
                refillRate[1].setEditable(true);
                consumptionRate[1].setEditable(true);
                maximum[1].setEditable(true);
                currentAmount[1].setEditable(true);
            }
        });

        stamina.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue) {
                refillRate[2].setEditable(false);
                consumptionRate[2].setEditable(false);
                maximum[2].setEditable(false);
                currentAmount[2].setEditable(false);
            } else {
                refillRate[2].setEditable(true);
                consumptionRate[2].setEditable(true);
                maximum[2].setEditable(true);
                currentAmount[2].setEditable(true);
            }
        });

        satiety.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue) {
                refillRate[3].setEditable(false);
                consumptionRate[3].setEditable(false);
                maximum[3].setEditable(false);
                currentAmount[3].setEditable(false);
            } else {
                refillRate[3].setEditable(true);
                consumptionRate[3].setEditable(true);
                maximum[3].setEditable(true);
                currentAmount[3].setEditable(true);
            }
        });

        move.setOnMouseClicked(event -> {
            primaryStage.setScene(scene);
            if (titleName.equals("Train")) {
                ArrayList<Feature> featureChangeRate = new ArrayList<>();
                if (energy.isSelected()) {
                    Feature energyFeature = new Feature("Energy", Double.parseDouble(currentAmount[1].getText().toString()), Double.parseDouble(maximum[1].getText().toString()),
                            Double.parseDouble(consumptionRate[1].getText().toString()), Double.parseDouble(refillRate[1].getText().toString()));
                    featureChangeRate.add(energyFeature);
                }
                if (health.isSelected()) {
                    Feature healthFeature = new Feature("Health", Double.parseDouble(currentAmount[0].getText().toString()), Double.parseDouble(maximum[0].getText().toString()),
                            Double.parseDouble(consumptionRate[0].getText().toString()), Double.parseDouble(refillRate[0].getText().toString()));
                    featureChangeRate.add(healthFeature);
                }
                if (stamina.isSelected()) {
                    Feature healthFeature = new Feature("Stamina", Double.parseDouble(currentAmount[2].getText().toString()), Double.parseDouble(maximum[2].getText().toString()),
                            Double.parseDouble(consumptionRate[2].getText().toString()), Double.parseDouble(refillRate[2].getText().toString()));
                    featureChangeRate.add(healthFeature);
                }
                if (satiety.isSelected()) {
                    Feature healthFeature = new Feature("Satiety", Double.parseDouble(currentAmount[3].getText().toString()), Double.parseDouble(maximum[3].getText().toString()),
                            Double.parseDouble(consumptionRate[3].getText().toString()), Double.parseDouble(refillRate[3].getText().toString()));
                    featureChangeRate.add(healthFeature);
                }
                Train train = new Train(name.getText().toString(), featureChangeRate, Double.parseDouble(moneyNum.getText().toString()), 10);
                trains.add(train);
                game.getVillage().getGym().getTrain().add(train);
            } else {
                HashMap<String, Double> featureChangeRate = new HashMap();
                if (energy.isSelected()) {
                    featureChangeRate.put("Energy", Double.parseDouble(currentAmount[1].getText().toString()));
                }
                if (health.isSelected()) {
                    featureChangeRate.put("Health", Double.parseDouble(currentAmount[0].getText().toString()));
                }
                if (stamina.isSelected()) {
                    featureChangeRate.put("Stamina", Double.parseDouble(currentAmount[0].getText().toString()));
                }
                if (satiety.isSelected()) {
                    featureChangeRate.put("Satiety", Double.parseDouble(currentAmount[0].getText().toString()));
                }
                Medicine medicine = new Medicine(name.getText().toString(), featureChangeRate, Double.parseDouble(moneyNum.getText().toString()));
                game.getVillage().getClinic().getGoods().add(medicine.clone(), 5);
            }

        });
        back.setOnMouseClicked(event -> primaryStage.setScene(scene));
    }

    public static void customMissionSceneBuilder(Group root, Scene scene, String titleName) {
        root.getChildren().clear();
        Image backImage = new Image("pics/menus/backButton.png");
        Image titleImage = new Image("pics/menus/backpackTitle.png");
        Image statusImage = new Image("pics/menus/food.png");
        Rectangle status = rectangleBuilder(1000, 700, new ImagePattern(statusImage), 0, 0);
        Rectangle backButton = rectangleBuilder(60, 60, new ImagePattern(backImage), 50, 20);
        Rectangle moveButton = rectangleBuilder(60, 60, new ImagePattern(backImage), 880, 20);
        Rectangle title = rectangleBuilder(190, 60, new ImagePattern(titleImage), 430, 20);
        root.getChildren().addAll(backButton, title, status, moveButton);
        Text[] text = new Text[2];
        ArrayList<String> Tasks = new ArrayList<>();
        HashMultiset objects = HashMultiset.create();
        ArrayList<Tetrad> tetrads = new ArrayList<>();

        Text titleText = buttonDesigner("Custom Game", 440, 53, 28, Color.SADDLEBROWN);
        Text back = buttonDesigner("Back", 54.5, 52, 21.5, Color.SADDLEBROWN);
        Text move = buttonDesigner("Done", 884, 52, 21.5, Color.SADDLEBROWN);
        Text contact = buttonDesigner("Contact Fee", 710, 280, 21.5, Color.SADDLEBROWN);
        Text time = buttonDesigner("Due Time", 720, 380, 21.5, Color.SADDLEBROWN);
        Text prize = buttonDesigner("Prize", 750, 480, 21.5, Color.SADDLEBROWN);
        Text exp = buttonDesigner("Experience", 720, 580, 21.5, Color.SADDLEBROWN);
        Text nameText = buttonDesigner("Name", 100, 120, 21.5, Color.SADDLEBROWN);
        TextField contactNum = textFieldDesigner(860, 250, 50, 21.5, Color.DARKGOLDENROD);
        TextField timeNum = textFieldDesigner(860, 350, 50, 21.5, Color.DARKGOLDENROD);
        TextField pricaNum = textFieldDesigner(860, 450, 50, 21.5, Color.DARKGOLDENROD);
        TextField expNum = textFieldDesigner(860, 550, 50, 21.5, Color.DARKGOLDENROD);
        TextField name = textFieldDesigner(170, 90, 100, 21.5, Color.DARKGOLDENROD);
        root.getChildren().addAll(titleText, back, contact, time, prize, contactNum, timeNum, pricaNum
                , move, name, nameText, exp, expNum);


        ComboBox<String> cookingTools = comboBoxDesigner("Tasks", 100, 150, Color.DARKGOLDENROD);
        for (String string : nameOfTasks) {
            cookingTools.getItems().addAll(string);
        }
        int[] taskCount = new int[1];
        int[] objectCount = new int[1];
        ComboBox<String> ingredients = comboBoxDesigner("Related Objects", 400, 150, Color.DARKGOLDENROD);
        taskCount[0] = 1;
        objectCount[0] = 1;
        cookingTools.getSelectionModel().selectedIndexProperty()
                .addListener((ChangeListener<Number>) (ov, value, new_value) -> {
                    ingredients.getItems().clear();
                    text[0] = buttonDesigner(taskCount[0] + ". " + nameOfTasks.get(new_value.intValue()), 140,
                            30 * (Tasks.size() + 10), 20, Color.SADDLEBROWN);
                    Tasks.add(nameOfTasks.get(new_value.intValue()));
                    taskBoolean[new_value.intValue()] = true;
                    System.out.println(new_value.intValue());
                    root.getChildren().addAll(text[0]);
                    taskCount[0]++;
                    if (taskBoolean[0] || taskBoolean[4] || taskBoolean[15] || taskBoolean[16] || taskBoolean[17] || taskBoolean[18]) {
                        for (Item item : items) {
                            ingredients.getItems().addAll(item.getName());
                        }
                        ingredients.getSelectionModel().selectedIndexProperty()
                                .addListener((ChangeListener<Number>) (o, valu, new_valu) -> {
                                    try {
                                        if(taskBoolean[0] || taskBoolean[4] || taskBoolean[15] || taskBoolean[16] || taskBoolean[17] || taskBoolean[18]){
                                            text[1] = buttonDesigner(objectCount[0] + ". " + items.get(new_valu.intValue()).getName(), 440,
                                                    30 * (objects.size() + 10), 20, Color.SADDLEBROWN);
                                            tetrads.add(new Tetrad(nameOfTasks.get(new_value.intValue()), items.get(new_valu.intValue()).getName(), 1));
                                            objects.add(items.get(new_valu.intValue()).clone());
                                            root.getChildren().addAll(text[1]);
                                            objectCount[0]++;
                                            for (int i = 0; i < taskBoolean.length; i++) {
                                                taskBoolean[i] = false;
                                            }
                                        }

                                    } catch (Exception e) {

                                    }
                                });
                    } else if (taskBoolean[3] || taskBoolean[12]) {
                        for (Tool tool : tools) {
                            ingredients.getItems().addAll(tool.getName());
                        }
                        ingredients.getSelectionModel().selectedIndexProperty()
                                .addListener((ChangeListener<Number>) (o, valu, new_valu) -> {
                                    if(taskBoolean[3] || taskBoolean[12]){
                                        text[1] = buttonDesigner(objectCount[0] + ". " + tools.get(new_valu.intValue()).getName(), 440,
                                                30 * (objects.size() + 10), 20, Color.SADDLEBROWN);
                                        tetrads.add(new Tetrad(nameOfTasks.get(new_value.intValue()), tools.get(new_valu.intValue()).getName(), 1));
                                        objects.add(tools.get(new_valu.intValue()).clone());
                                        objectCount[0]++;
                                        root.getChildren().addAll(text[1]);
                                        for (int i = 0; i < taskBoolean.length; i++) {
                                            taskBoolean[i] = false;
                                        }
                                    }

                                });
                    } else if (taskBoolean[5] || taskBoolean[11] || taskBoolean[19] || taskBoolean[20] || taskBoolean[21] || taskBoolean[22]) {
                        ingredients.getItems().addAll("Player");
                        ingredients.getSelectionModel().selectedIndexProperty()
                                .addListener((ChangeListener<Number>) (o, valu, new_valu) -> {
                                    if(taskBoolean[5] || taskBoolean[11] || taskBoolean[19] || taskBoolean[20] || taskBoolean[21] || taskBoolean[22]){
                                        text[1] = buttonDesigner(objectCount[0] + ". " + "Player", 440,
                                                30 * (objects.size() + 10), 20, Color.SADDLEBROWN);
                                        tetrads.add(new Tetrad(nameOfTasks.get(new_value.intValue()), "Player", 1));
                                        objects.add("Player");
                                        objectCount[0]++;
                                        root.getChildren().addAll(text[1]);
                                        for (int i = 0; i < taskBoolean.length; i++) {
                                            taskBoolean[i] = false;
                                        }
                                    }
                                });
                    }else if (taskBoolean[6] || taskBoolean[9]) {
                        for (Animal animal : animals) {
                            ingredients.getItems().addAll(animal.getType());
                        }
                        ingredients.getSelectionModel().selectedIndexProperty()
                                .addListener((ChangeListener<Number>) (o, valu, new_valu) -> {
                                    if(taskBoolean[6] || taskBoolean[9]){
                                        text[1] = buttonDesigner(objectCount[0] + ". " + animals.get(new_valu.intValue()).getType(), 440,
                                                30 * (objects.size() + 10), 20, Color.SADDLEBROWN);
                                        tetrads.add(new Tetrad(nameOfTasks.get(new_value.intValue()), animals.get(new_valu.intValue()).getType(), 1));
                                        objects.add(animals.get(new_valu.intValue()).getType());
                                        objectCount[0]++;
                                        root.getChildren().addAll(text[1]);
                                        for (int i = 0; i < taskBoolean.length; i++) {
                                            taskBoolean[i] = false;
                                        }
                                    }

                                });
                    }else if (taskBoolean[2]) {
                        for (Food food : foods) {
                            ingredients.getItems().addAll(food.getName());
                        }
                        ingredients.getSelectionModel().selectedIndexProperty()
                                .addListener((ChangeListener<Number>) (o, valu, new_valu) -> {
                                    if(taskBoolean[2]){
                                        text[1] = buttonDesigner(objectCount[0] + ". " + foods.get(new_valu.intValue()).getName(), 440,
                                                30 * (objects.size() + 10), 20, Color.SADDLEBROWN);
                                        tetrads.add(new Tetrad(nameOfTasks.get(new_value.intValue()), foods.get(new_valu.intValue()).getName(), 1));
                                        objects.add(foods.get(new_valu.intValue()).getName());
                                        objectCount[0]++;
                                        root.getChildren().addAll(text[1]);
                                        for (int i = 0; i < taskBoolean.length; i++) {
                                            taskBoolean[i] = false;
                                        }
                                    }

                                });
                    }else if (taskBoolean[1]) {
                        for (Mineral mineral : minerals) {
                            ingredients.getItems().addAll(mineral.getName());
                        }
                        ingredients.getSelectionModel().selectedIndexProperty()
                                .addListener((ChangeListener<Number>) (o, valu, new_valu) -> {
                                    if(taskBoolean[1]){
                                        text[1] = buttonDesigner(objectCount[0] + ". " + minerals.get(new_valu.intValue()).getName(), 440,
                                                30 * (objects.size() + 10), 20, Color.SADDLEBROWN);
                                        tetrads.add(new Tetrad(nameOfTasks.get(new_value.intValue()), minerals.get(new_valu.intValue()).getName(), 1));
                                        objects.add(minerals.get(new_valu.intValue()).getName());
                                        objectCount[0]++;
                                        root.getChildren().addAll(text[1]);
                                        for (int i = 0; i < taskBoolean.length; i++) {
                                            taskBoolean[i] = false;
                                        }
                                    }

                                });
                    }else if (taskBoolean[13]) {
                        for (Seed seed : seeds) {
                            ingredients.getItems().addAll(seed.getName());
                        }
                        ingredients.getSelectionModel().selectedIndexProperty()
                                .addListener((ChangeListener<Number>) (o, valu, new_valu) -> {
                                    if(taskBoolean[13]){
                                        text[1] = buttonDesigner(objectCount[0] + ". " + seeds.get(new_valu.intValue()).getName(), 440,
                                                30 * (objects.size() + 10), 20, Color.SADDLEBROWN);
                                        tetrads.add(new Tetrad(nameOfTasks.get(new_value.intValue()), seeds.get(new_valu.intValue()).getName(), 1));
                                        objects.add(seeds.get(new_valu.intValue()).getName());
                                        objectCount[0]++;
                                        root.getChildren().addAll(text[1]);
                                        for (int i = 0; i < taskBoolean.length; i++) {
                                            taskBoolean[i] = false;
                                        }
                                    }

                                });
                    }else if (taskBoolean[10]) {
                        for (Fruit fruit: fruits) {
                            ingredients.getItems().addAll(fruit.getName());
                        }
                        ingredients.getSelectionModel().selectedIndexProperty()
                                .addListener((ChangeListener<Number>) (o, valu, new_valu) -> {
                                    if(taskBoolean[10]){
                                        text[1] = buttonDesigner(objectCount[0] + ". " + fruits.get(new_valu.intValue()).getName(), 440,
                                                30 * (objects.size() + 10), 20, Color.SADDLEBROWN);
                                        tetrads.add(new Tetrad(nameOfTasks.get(new_value.intValue()), fruits.get(new_valu.intValue()).getName(), 1));
                                        objects.add(fruits.get(new_valu.intValue()).getName());
                                        objectCount[0]++;
                                        root.getChildren().addAll(text[1]);
                                        for (int i = 0; i < taskBoolean.length; i++) {
                                            taskBoolean[i] = false;
                                        }
                                    }

                                });
                    }else if (taskBoolean[14]) {
                        ingredients.getItems().addAll("Shovel");
                        ingredients.getSelectionModel().selectedIndexProperty()
                                .addListener((ChangeListener<Number>) (o, valu, new_valu) -> {
                                    if(taskBoolean[14]){
                                        text[1] = buttonDesigner(objectCount[0] + ". " + "Shovel", 440,
                                                30 * (objects.size() + 10), 20, Color.SADDLEBROWN);
                                        tetrads.add(new Tetrad(nameOfTasks.get(new_value.intValue()), "Shovel", 1));
                                        objects.add("Shovel");
                                        objectCount[0]++;
                                        root.getChildren().addAll(text[1]);
                                        for (int i = 0; i < taskBoolean.length; i++) {
                                            taskBoolean[i] = false;
                                        }
                                    }
                                });
                    }else if (taskBoolean[7]) {
                        ingredients.getItems().addAll("Watering Can");
                        ingredients.getSelectionModel().selectedIndexProperty()
                                .addListener((ChangeListener<Number>) (o, valu, new_valu) -> {
                                    if(taskBoolean[7]){
                                        text[1] = buttonDesigner(objectCount[0] + ". " + "Watering Can", 440,
                                                30 * (objects.size() + 10), 20, Color.SADDLEBROWN);
                                        tetrads.add(new Tetrad(nameOfTasks.get(new_value.intValue()), "Watering Can", 1));
                                        objects.add("Watering Can");
                                        objectCount[0]++;
                                        root.getChildren().addAll(text[1]);
                                        for (int i = 0; i < taskBoolean.length; i++) {
                                            taskBoolean[i] = false;
                                        }
                                    }
                                });
                    }else if (taskBoolean[8]) {
                        ingredients.getItems().addAll("Fishing Rod");
                        ingredients.getSelectionModel().selectedIndexProperty()
                                .addListener((ChangeListener<Number>) (o, valu, new_valu) -> {
                                    if(taskBoolean[8]){
                                        text[1] = buttonDesigner(objectCount[0] + ". " + "Fishing Rod", 440,
                                                30 * (objects.size() + 10), 20, Color.SADDLEBROWN);
                                        tetrads.add(new Tetrad(nameOfTasks.get(new_value.intValue()), "Fishing Rod", 1));
                                        objects.add("Fishing Rod");
                                        objectCount[0]++;
                                        root.getChildren().addAll(text[1]);
                                        for (int i = 0; i < taskBoolean.length; i++) {
                                            taskBoolean[i] = false;
                                        }
                                    }
                                });
                    }
                });
        root.getChildren().addAll(cookingTools, ingredients);
        move.setOnMouseClicked(event -> {
            Time dueTime = new Time();
            dueTime.setHour((int)(Double.parseDouble(timeNum.getText().toString())));
            HashMultiset price = HashMultiset.create();
            price.add(Double.parseDouble(pricaNum.getText().toString()) + " Gils!");
            Mission mission = new Mission(name.getText().toString(), tetrads, price, Double.parseDouble(contactNum.getText().toString()),
                    dueTime, HashMultiset.create());
            game.getVillage().getCafe().getBoard().getAvailableMission().add(9, mission);
            primaryStage.setScene(scene);
        });
        back.setOnMouseClicked(event -> primaryStage.setScene(scene));
    }


    public static void pauseSceneBuilder(Group root, Scene scene, Player player) {
        Image lumber = new Image("pics/menus/menuLumber.png");
        Rectangle continueButton = rectangleBuilder(270, 270, new ImagePattern(lumber), 500, 200);
        Rectangle statusButton = rectangleBuilder(270, 270, new ImagePattern(lumber), 200, 200);
        Rectangle mainMenuButton = rectangleBuilder(270, 270, new ImagePattern(lumber), 200, 400);
        Rectangle backpackButton = rectangleBuilder(270, 270, new ImagePattern(lumber), 500, 400);
        root.getChildren().addAll(continueButton, statusButton, mainMenuButton, backpackButton);

        Text continueGame = buttonDesigner("Continue", 280, 290, 35, Color.DARKGREEN);
        Text status = buttonDesigner(" Show\nStatus", 290, 460, 35, Color.DARKGREEN);
        Text mainMenu = buttonDesigner("Main\nMenu", 610, 270, 35, Color.DARKGREEN);
        Text backpack = buttonDesigner("  Show\nBackpack", 580, 460, 35, Color.DARKGREEN);
        backpack.setOnMouseClicked(event -> {
            backpackUI.backpackSceneBuilder(primaryStage, backpackRoot, pauseScene);
            backpackSceneHandler(backpackRoot);
            primaryStage.setScene(backpackScene);
        });
        status.setOnMouseClicked(event -> {
            statusSceneBuilder(statusRoot, pauseScene, player);
            primaryStage.setScene(statusScene);
        });
        //settings.setOnMouseClicked(event -> primaryStage.setScene(settingsScene));
        mainMenu.setOnMouseClicked(event -> primaryStage.setScene(mainScene));
        continueGame.setOnMouseClicked(event -> {
            primaryStage.setScene(scene);
            timeCalculator.play();
        });
        root.getChildren().addAll(continueGame, status, mainMenu, backpack);
    }

    public static void networkPauseSceneBuilder(Group root, Scene scene, Player player) {
        Image lumber = new Image("pics/menus/menuLumber.png");
        Rectangle singlePlayerButton = rectangleBuilder(270, 270, new ImagePattern(lumber), 350, 150);
        Rectangle customButton = rectangleBuilder(270, 270, new ImagePattern(lumber), 150, 270);
        Rectangle multiPlayerButton = rectangleBuilder(270, 270, new ImagePattern(lumber), 550, 270);
        Rectangle settingsButton = rectangleBuilder(270, 270, new ImagePattern(lumber), 230, 450);
        Rectangle exitButton = rectangleBuilder(270, 270, new ImagePattern(lumber), 470, 450);
        Rectangle chatButton = rectangleBuilder(270, 270, new ImagePattern(lumber), 350, 300);
        root.getChildren().addAll(singlePlayerButton, customButton, chatButton, multiPlayerButton, settingsButton, exitButton);

        Text continueGame = buttonDesigner("Continue", 240, 350, 35, Color.DARKGREEN);
        Text status = buttonDesigner(" Show\nStatus", 320, 520, 35, Color.DARKGREEN);
        Text mainMenu = buttonDesigner("Main\nMenu", 650, 340, 35, Color.DARKGREEN);
        Text backpack = buttonDesigner("  Show\nBackpack", 560, 520, 35, Color.DARKGREEN);
        Text friends = buttonDesigner("Friends", 450, 240, 35, Color.DARKGREEN);
        Text chatroom = buttonDesigner("Chat\nRoom", 450, 370, 35, Color.DARKGREEN);

        chatroom.setOnMouseClicked(event -> {
            publicChatSceneBuilder(publicChatRoot);
            primaryStage.setScene(publicChatScene);
        });

        backpack.setOnMouseClicked(event -> {
            if (!isMultiplayer)
                backpackUI.backpackSceneBuilder(primaryStage, backpackRoot, pauseScene);
            else backpackUI.backpackSceneBuilder(primaryStage, backpackRoot, networkPauseScene);
            backpackSceneHandler(backpackRoot);
            primaryStage.setScene(backpackScene);
        });
        //settings.setOnMouseClicked(event -> primaryStage.setScene(settingsScene));
        mainMenu.setOnMouseClicked(event -> primaryStage.setScene(mainScene));
        continueGame.setOnMouseClicked(event -> {
            primaryStage.setScene(scene);
            timeCalculator.play();
        });

        friends.setOnMouseClicked(event -> {
            friendSceneBuilder(friendRoot, networkPauseScene);
            primaryStage.setScene(friendScene);
        });

        status.setOnMouseClicked(event -> {
            statusSceneBuilder(statusRoot, pauseScene, player);
            primaryStage.setScene(statusScene);
        });
        root.getChildren().addAll(continueGame, status, mainMenu, backpack, friends, chatroom);
    }

    public static void settingsSceneBuilder(Group root, Scene scene, Player player) {
        Image lumber = new Image("pics/menus/menuLumber.png");
        CheckBox sound = checkBoxDesigner("Sound", 300, 270, 32);
        CheckBox popUp = checkBoxDesigner("Show PopUp", 520, 270, 32);
        ComboBox<String> cookingTools = comboBoxDesigner("Languages", 300, 370, Color.GREEN);
        Rectangle exitButton = rectangleBuilder(270, 270, new ImagePattern(lumber), 350, 450);
        Text backpack = buttonDesigner("Main\nMenu", 450, 520, 35, Color.DARKGREEN);
        cookingTools.setStyle("-fx-font: 28px \"Comic Sans MS\";");
        String[] strings = {"English", "French", "Spanish"};
        for (String string : strings) {
            cookingTools.getItems().addAll(string);
        }
        cookingTools.getSelectionModel().selectedIndexProperty()
                .addListener((ChangeListener<Number>) (ov, value, new_value) -> {
                    language = new_value.intValue();
                });
        backpack.setOnMouseClicked(event -> {
            primaryStage.setScene(mainScene);
            initializeFlags();
        });

        root.getChildren().addAll(sound, popUp, cookingTools, exitButton, backpack);
    }

    public static void statusSceneBuilder(Group root, Scene scene, PlayerStatus playerStatus) {
        root.getChildren().clear();
        Image backImage = new Image("pics/menus/backButton.png");
        Image titleImage = new Image("pics/menus/backpackTitle.png");
        Image statusImage = new Image("pics/menus/status.png");
        Rectangle backButton = rectangleBuilder(60, 60, new ImagePattern(backImage), 50, 20);
        Rectangle title = rectangleBuilder(180, 60, new ImagePattern(titleImage), 430, 20);
        Rectangle status = rectangleBuilder(1000, 700, new ImagePattern(statusImage), 50, 0);
        root.getChildren().addAll(backButton, title, status);

        Text titleText = buttonDesigner(game.getPlayer().getName() + " Status", 440, 53, 28, Color.SADDLEBROWN);
        Text back = buttonDesigner("Back", 54.5, 52, 21.5, Color.SADDLEBROWN);
        Text money = buttonDesigner("Money : " + playerStatus.getMoney().intValue(), 230, 230, 21.5, Color.SADDLEBROWN);
        Text time = buttonDesigner("Animal Count : " + playerStatus.getAnimalCount() + "\nDate :    /    / \nSeason : ", 600, 230, 21.5, Color.SADDLEBROWN);
        Text health = buttonDesigner("Health", 170, 480, 21.5, Color.GOLD);
        Text energy = buttonDesigner("Energy", 170, 530, 21.5, Color.GOLD);
        Text stamina = buttonDesigner("Stamina", 160, 580, 21.5, Color.GOLD);
        Text satiety = buttonDesigner("Satiety", 165, 630, 21.5, Color.GOLD);
        Text consumption = buttonDesigner("Consumption Rate", 260, 430, 16, Color.MAROON);
        Text refill = buttonDesigner("Refill Rate", 435, 430, 16, Color.MAROON);
        Text current = buttonDesigner("Current Amount", 565, 430, 16, Color.MAROON);
        Text max = buttonDesigner("Max Amount", 730, 430, 16, Color.MAROON);
        root.getChildren().addAll(titleText, back, money, time, health, energy, stamina, satiety, consumption, refill, current, max);

        Text energyCons = buttonDesigner(Integer.toString(playerStatus.getEnergyConsumption().intValue()), 300, 480 + 50 * 1, 21.5, Color.BLACK);
        Text energyRefil = buttonDesigner(Integer.toString(playerStatus.getEnergyRefill().intValue()), 450, 480 + 50 * 1, 21.5, Color.BLACK);
        Text energyCur = buttonDesigner(Integer.toString(playerStatus.getEnergyCurrent().intValue()), 600, 480 + 50 * 1, 21.5, Color.BLACK);
        Text energyMax = buttonDesigner(Integer.toString(playerStatus.getEnergyMaxCurrent().intValue()), 750, 480 + 50 * 1, 21.5, Color.BLACK);
        root.getChildren().addAll(energyCons, energyRefil, energyCur, energyMax);

        Text healthCons = buttonDesigner(Integer.toString(playerStatus.getHealthConsumption().intValue()), 300, 480 + 50 * 0, 21.5, Color.BLACK);
        Text healthRefil = buttonDesigner(Integer.toString(playerStatus.getHealthMaxRefill().intValue()), 450, 480 + 50 * 0, 21.5, Color.BLACK);
        Text healthCur = buttonDesigner(Integer.toString(playerStatus.getHealthCurrent().intValue()), 600, 480 + 50 * 0, 21.5, Color.BLACK);
        Text healthMax = buttonDesigner(Integer.toString(playerStatus.getHealthMaxCurrent().intValue()), 750, 480 + 50 * 0, 21.5, Color.BLACK);
        root.getChildren().addAll(healthCons, healthRefil, healthCur, healthMax);

        Text staminaCons = buttonDesigner(Integer.toString(playerStatus.getStaminaConsumption().intValue()), 300, 480 + 50 * 2, 21.5, Color.BLACK);
        Text staminaRefil = buttonDesigner(Integer.toString(playerStatus.getStaminaRefill().intValue()), 450, 480 + 50 * 2, 21.5, Color.BLACK);
        Text staminaCur = buttonDesigner(Integer.toString(playerStatus.getStaminaCurrent().intValue()), 600, 480 + 50 * 2, 21.5, Color.BLACK);
        Text staminaMax = buttonDesigner(Integer.toString(playerStatus.getStaminaMaxCurrent().intValue()), 750, 480 + 50 * 2, 21.5, Color.BLACK);
        root.getChildren().addAll(staminaCons, staminaRefil, staminaCur, staminaMax);

        Text satityCons = buttonDesigner(Integer.toString(playerStatus.getSatietyConsumption().intValue()), 300, 480 + 50 * 3, 21.5, Color.BLACK);
        Text satityRefil = buttonDesigner(Integer.toString(playerStatus.getSatietyRefill().intValue()), 450, 480 + 50 * 3, 21.5, Color.BLACK);
        Text satityCur = buttonDesigner(Integer.toString(playerStatus.getSatietyCurrent().intValue()), 600, 480 + 50 * 3, 21.5, Color.BLACK);
        Text satityMax = buttonDesigner(Integer.toString(playerStatus.getSatietyMaxCurrent().intValue()), 750, 480 + 50 * 3, 21.5, Color.BLACK);
        root.getChildren().addAll(satityCons, satityRefil, satityCur, satityMax);


        back.setOnMouseClicked(event -> {
            primaryStage.setScene(scene);
            initializeFlags();
        });
    }

    public static void statusSceneBuilder(Group root, Scene scene, Player player) {
        root.getChildren().clear();
        Image backImage = new Image("pics/menus/backButton.png");
        Image titleImage = new Image("pics/menus/backpackTitle.png");
        Image statusImage = new Image("pics/menus/status.png");
        Rectangle backButton = rectangleBuilder(60, 60, new ImagePattern(backImage), 50, 20);
        Rectangle title = rectangleBuilder(180, 60, new ImagePattern(titleImage), 430, 20);
        Rectangle status = rectangleBuilder(1000, 700, new ImagePattern(statusImage), 50, 0);
        root.getChildren().addAll(backButton, title, status);

        Text titleText = buttonDesigner(game.getPlayer().getName() + " Status", 440, 53, 28, Color.SADDLEBROWN);
        Text back = buttonDesigner("Back", 54.5, 52, 21.5, Color.SADDLEBROWN);
        Text money = buttonDesigner("Money : " + (int) player.getMoney(), 230, 230, 21.5, Color.SADDLEBROWN);
        Text time = buttonDesigner("Time : " + (int) gameDate.getTime().getHour() + " : " + gameDate.getTime().getMinute() +
                " : " + gameDate.getTime().getSecond() + "\nDate : " + gameDate.getYear() + " / " + gameDate.getMonth() +
                " / " + gameDate.getDay() + "\nSeason : " + gameDate.getSeason().getType(), 600, 230, 21.5, Color.SADDLEBROWN);
        Text consumption = buttonDesigner("Consumption Rate", 260, 430, 16, Color.MAROON);
        Text refill = buttonDesigner("Refill Rate", 435, 430, 16, Color.MAROON);
        Text current = buttonDesigner("Current Amount", 565, 430, 16, Color.MAROON);
        Text max = buttonDesigner("Max Amount", 730, 430, 16, Color.MAROON);
        for (int i = 0; i < player.getFeature().size(); i++) {
            Text health = buttonDesigner(player.getFeature().get(i).getName(), 170, 480 + 50 * i, 21.5, Color.GOLD);
            Text cons = buttonDesigner(Integer.toString((int) player.getFeature().get(i).getConsumptionRate()), 300, 480 + 50 * i, 21.5, Color.BLACK);
            Text refil = buttonDesigner(Integer.toString((int) player.getFeature().get(i).getRefillRate()), 450, 480 + 50 * i, 21.5, Color.BLACK);
            Text cur = buttonDesigner(Integer.toString((int) player.getFeature().get(i).getCurrent()), 600, 480 + 50 * i, 21.5, Color.BLACK);
            Text maxi = buttonDesigner(Integer.toString((int) player.getFeature().get(i).getMaxCurrent()), 750, 480 + 50 * i, 21.5, Color.BLACK);
            root.getChildren().addAll(cons, refil, cur, maxi, health);
        }
        root.getChildren().addAll(titleText, back, money, time, consumption, refill, current, max);
        back.setOnMouseClicked(event -> {
            primaryStage.setScene(scene);
            initializeFlags();
        });
    }


    public static void objectStatusSceneBuilder(Group root, Item item, Scene scene, int num) {
        root.getChildren().clear();
        Status statusi = new Status();
        flags[num] = true;
        Image backImage = new Image("pics/menus/backButton.png");
        Image titleImage = new Image("pics/menus/backpackTitle.png");
        Image statusImage = new Image("pics/menus/objectStatus.png");
        Rectangle backButton = rectangleBuilder(60, 60, new ImagePattern(backImage), 50, 20);
        Rectangle title = rectangleBuilder(180, 60, new ImagePattern(titleImage), 430, 20);
        Rectangle status = rectangleBuilder(1000, 700, new ImagePattern(statusImage), 0, 0);
        Rectangle picture = rectangleBuilder(150, 150, new ImagePattern(item.getPicture()), 700, 300);
        root.getChildren().addAll(backButton, title, status, picture);

        Text titleText = buttonDesigner("Status", 470, 53, 30, Color.SADDLEBROWN);
        Text back = buttonDesigner("Back", 54.5, 52, 21.5, Color.SADDLEBROWN);
        Text data = null;
        try {
            data = buttonDesigner(statusi.object(item), 190, 250, 24, Color.SADDLEBROWN);
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        back.setOnMouseClicked(event -> {
            primaryStage.setScene(scene);
            initializeFlags();
        });
        root.getChildren().addAll(titleText, back, data);
    }

    public static void animalStatusSceneBuilder(Group root, Animal animal, Scene scene, boolean inShop) {
        root.getChildren().clear();
        Status statusi = new Status();
        flags[2] = true;
        Image backImage = new Image("pics/menus/backButton.png");
        Image titleImage = new Image("pics/menus/backpackTitle.png");
        Image statusImage = new Image("pics/menus/objectStatus.png");
        Rectangle backButton = rectangleBuilder(60, 60, new ImagePattern(backImage), 50, 20);
        Rectangle title = rectangleBuilder(180, 60, new ImagePattern(titleImage), 430, 20);
        Rectangle status = rectangleBuilder(1000, 700, new ImagePattern(statusImage), 0, 0);
        Rectangle picture = rectangleBuilder(150, 100, new ImagePattern(animal.getPicture()), 700, 300);
        root.getChildren().addAll(backButton, title, status, picture);

        Text titleText = buttonDesigner("Status", 470, 53, 30, Color.SADDLEBROWN);
        Text back = buttonDesigner("Back", 54.5, 52, 21.5, Color.SADDLEBROWN);
        Text data = null;
        if (inShop)
            data = buttonDesigner(statusi.shopAnimal(animal), 190, 250, 24, Color.SADDLEBROWN);
        else
            data = buttonDesigner(statusi.BarnAnimal(animal), 190, 250, 24, Color.SADDLEBROWN);
        back.setOnMouseClicked(event -> {
            primaryStage.setScene(scene);
            initializeFlags();
        });
        root.getChildren().addAll(titleText, back, data);
    }

    public static void buySceneBuilder(Group root, Item item, Scene scene, Group sourceRoot, Player player, Shop shop) {
        root.getChildren().clear();
        Status itemStatus = new Status();
        Image backImage = new Image("pics/menus/backButton.png");
        Image titleImage = new Image("pics/menus/backpackTitle.png");
        Image statusImage = new Image("pics/menus/buy.png");
        Rectangle backButton = rectangleBuilder(60, 60, new ImagePattern(backImage), 50, 20);
        Rectangle title = rectangleBuilder(180, 60, new ImagePattern(titleImage), 430, 20);
        Rectangle status = rectangleBuilder(1000, 700, new ImagePattern(statusImage), 0, 0);
        Rectangle picture = rectangleBuilder(150, 150, new ImagePattern(item.getPicture()), 700, 300);
        Rectangle buyButton = rectangleBuilder(60, 60, new ImagePattern(backImage), 750, 550);
        root.getChildren().addAll(backButton, title, status, picture, buyButton);

        Text titleText = buttonDesigner("Status", 470, 53, 30, Color.SADDLEBROWN);
        Text back = buttonDesigner("Back", 54.5, 52, 21.5, Color.SADDLEBROWN);
        Text data = null;
        try {
            data = buttonDesigner(itemStatus.item(item), 190, 240, 24, Color.SADDLEBROWN);
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        Text buy = buttonDesigner("Buy", 762, 582, 21.5, Color.SADDLEBROWN);
        Text price = buttonDesigner("Price : " + item.getPrice(), 300, 580, 24, Color.SADDLEBROWN);
        root.getChildren().addAll(titleText, back, data, buy, price);

        back.setOnMouseClicked(event -> {
            primaryStage.setScene(scene);
            initializeFlags();
        });
        buy.setOnMouseClicked(event -> {
            initializeFlags();
            Buy buy1 = new Buy();
            primaryStage.setScene(scene);
            showPopup(buy1.run(shop, item, player, 1), sourceRoot);
        });
    }

    public static void sellSceneBuilder(Group root, Item item, Scene scene, Group sourceRoot, Player player, Shop shop) {
        root.getChildren().clear();
        Status itemStatus = new Status();
        Image backImage = new Image("pics/menus/backButton.png");
        Image titleImage = new Image("pics/menus/backpackTitle.png");
        Image statusImage = new Image("pics/menus/buy.png");
        Rectangle backButton = rectangleBuilder(60, 60, new ImagePattern(backImage), 50, 20);
        Rectangle title = rectangleBuilder(180, 60, new ImagePattern(titleImage), 430, 20);
        Rectangle status = rectangleBuilder(1000, 700, new ImagePattern(statusImage), 0, 0);
        Rectangle picture = rectangleBuilder(150, 150, new ImagePattern(item.getPicture()), 700, 300);
        Rectangle buyButton = rectangleBuilder(60, 60, new ImagePattern(backImage), 750, 550);
        root.getChildren().addAll(backButton, title, status, picture, buyButton);

        Text titleText = buttonDesigner("Status", 470, 53, 30, Color.SADDLEBROWN);
        Text back = buttonDesigner("Back", 54.5, 52, 21.5, Color.SADDLEBROWN);
        Text data = null;
        try {
            data = buttonDesigner(itemStatus.item(item), 190, 240, 24, Color.SADDLEBROWN);
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        Text sell = buttonDesigner("Sell", 762, 582, 21.5, Color.SADDLEBROWN);
        Text price = buttonDesigner("Price(Sell) : " + (item.getPrice() * 1.25), 280, 580, 24, Color.SADDLEBROWN);
        root.getChildren().addAll(titleText, back, data, sell, price);

        back.setOnMouseClicked(event -> {
            primaryStage.setScene(scene);
            initializeFlags();
        });
        sell.setOnMouseClicked(event -> {
            initializeFlags();
            Sell sell1 = new Sell();
            primaryStage.setScene(scene);
            showPopup(sell1.run(shop, item, player, 1), sourceRoot);
        });
    }

    public static void twoSidedSceneBuilder(Group root, Scene scene, HashMultiset<Item> items1, HashMultiset<Item> items2,
                                            String name1, String name2) {
        root.getChildren().clear();
        Image backImage = new Image("pics/menus/backButton.png");
        Image titleImage = new Image("pics/menus/backpackTitle.png");
        Image statusImage = new Image("pics/menus/2sidedMenu.png");
        Rectangle backButton = rectangleBuilder(60, 60, new ImagePattern(backImage), 50, 20);
        Rectangle title = rectangleBuilder(180, 60, new ImagePattern(titleImage), 430, 20);
        Rectangle status = rectangleBuilder(1000, 700, new ImagePattern(statusImage), 0, 50);
        Rectangle moveButton = rectangleBuilder(60, 60, new ImagePattern(backImage), 880, 20);
        root.getChildren().addAll(backButton, title, status, moveButton);

        Text titleText = buttonDesigner("Exchange", 450, 53, 30, Color.SADDLEBROWN);
        Text back = buttonDesigner("Back", 54.5, 52, 21.5, Color.SADDLEBROWN);
        Text move = buttonDesigner("Move", 884, 52, 21.5, Color.SADDLEBROWN);
        Text name1Text = buttonDesigner(name1, 260, 130, 16, Color.SADDLEBROWN);
        Text name2Text = buttonDesigner(name2, 655, 130, 16, Color.SADDLEBROWN);
        int count = 0;
        for (Item item : items1.elementSet()) {
            StringBuilder data = new StringBuilder();
            count++;
            data.append(count);
            data.append(". ");
            data.append(item.getName());
            if (items1.count(item) > 1) {
                data.append(" x");
                data.append(items1.count(item));
            }
            data.append("\n");
            Text itemName = buttonDesigner(data.toString(), 180, 30 * (count + 6), 21.5, Color.SADDLEBROWN);
            itemName.setOnMouseClicked(event -> {
                choosedItem = item;
                choosedButton = "Put";
                flags[1] = true;
                primaryStage.setScene(scene);
            });
            root.getChildren().addAll(itemName);
        }
        count = 0;
        for (Item item : items2.elementSet()) {
            StringBuilder data = new StringBuilder();
            count++;
            data.append(count);
            data.append(". ");
            data.append(item.getName());
            if (items2.count(item) > 1) {
                data.append(" x");
                data.append(items2.count(item));
            }
            data.append("\n");
            Text itemName = buttonDesigner(data.toString(), 600, 30 * (count + 6), 21.5, Color.SADDLEBROWN);
            itemName.setOnMouseClicked(event -> {
                choosedItem = item;
                choosedButton = "Take";
                flags[1] = true;
                primaryStage.setScene(scene);
            });
            root.getChildren().addAll(itemName);
        }
        root.getChildren().addAll(titleText, back, move, name1Text, name2Text);

        back.setOnMouseClicked(event -> {
            primaryStage.setScene(scene);
            pressed = false;
        });
    }

    public static void friendSceneBuilder(Group root, Scene scene) {
        root.getChildren().clear();
        Image backImage = new Image("pics/menus/backButton.png");
        Image titleImage = new Image("pics/menus/backpackTitle.png");
        Image statusImage = new Image("pics/menus/friend.png");
        Rectangle backButton = rectangleBuilder(60, 60, new ImagePattern(backImage), 50, 20);
        Rectangle title = rectangleBuilder(180, 60, new ImagePattern(titleImage), 430, 20);
        Rectangle status = rectangleBuilder(1000, 700, new ImagePattern(statusImage), 0, 0);
        Rectangle moveButton = rectangleBuilder(60, 60, new ImagePattern(backImage), 880, 20);
        root.getChildren().addAll(backButton, title, status, moveButton);

        Text titleText = buttonDesigner("Friends", 450, 53, 30, Color.SADDLEBROWN);
        Text back = buttonDesigner("Back", 54.5, 52, 21.5, Color.SADDLEBROWN);
        //Text move = buttonDesigner("Move", 884, 52, 21.5, Color.SADDLEBROWN);
        Text name1Text = buttonDesigner("Friends", 263, 75, 18, Color.SADDLEBROWN);
        Text name2Text = buttonDesigner("Requests", 658, 75, 19, Color.SADDLEBROWN);
        root.getChildren().addAll(titleText, back, name1Text, name2Text);
        int cnt = -1;
        if (isServer) {
            for (int i = 0; i < server.clientsAdd.length; i++) {
                if (server.getFriend()[i]) {
                    cnt++;
                    Text text = buttonDesigner(server.getNames()[i], 180, 50 * (cnt + 3.5), 18, Color.SADDLEBROWN);
                    Text button = buttonDesigner("R", 470, 50 * (cnt + 3.5), 18, Color.SADDLEBROWN);
                    int finalI = i;
                    button.setOnMouseClicked(event -> {
                        server.sendRequest(new Request("Remove", finalI, server.clientsAdd.length));
                        server.getFriend()[finalI] = false;
                    });
                    text.setOnMouseClicked(event -> {
                        playerMenuSceneBuilder(playerMenuRoot, new Player(), finalI);
                        primaryStage.setScene(playerMenuScene);
                    });
                    root.getChildren().addAll(text, button);
                }
            }
            cnt = 0;
            for (int i = 0; i < server.clientsAdd.length; i++) {
                if (server.getFriendReq()[i]) {
                    cnt++;
                    Text text = buttonDesigner(server.getNames()[i], 600, 50 * (cnt + 3.5), 18, Color.SADDLEBROWN);
                    Text accept = buttonDesigner("A", 870, 50 * (cnt + 3.5), 18, Color.SADDLEBROWN);
                    Text decline = buttonDesigner("D", 920, 50 * (cnt + 3.5), 18, Color.SADDLEBROWN);
                    int finalI = i;
                    accept.setOnMouseClicked(event -> {
                        server.sendRequest(new Request("Friend", finalI, true,
                                server.clientsAdd.length));
                        server.getFriend()[finalI] = true;
                        server.getFriendReq()[finalI] = false;
                    });
                    decline.setOnMouseClicked(event -> {
                        server.getFriendReq()[finalI] = false;
                    });
                    root.getChildren().addAll(text, accept, decline);
                }
            }
        } else {
            for (int i = 0; i < client.getNames().length; i++) {
                if (client.getFriend()[i]) {
                    cnt++;
                    Text text = buttonDesigner(client.getNames()[i], 180, 50 * (cnt + 3.5), 18, Color.SADDLEBROWN);
                    Text button = buttonDesigner("R", 470, 50 * (cnt + 3.5), 18, Color.SADDLEBROWN);
                    int finalI = i;
                    button.setOnMouseClicked(event -> {
                        client.sendRequest(new Request("Remove", finalI, client.getIndexOf()));
                        client.getFriend()[finalI] = false;
                    });
                    text.setOnMouseClicked(event -> {
                        playerMenuSceneBuilder(playerMenuRoot, new Player(), finalI);
                        primaryStage.setScene(playerMenuScene);
                    });
                    root.getChildren().addAll(text, button);
                }
            }
            cnt = 0;
            for (int i = 0; i < client.getNames().length; i++) {
                if (client.getFriendReq()[i]) {
                    Text text = buttonDesigner(client.getNames()[i], 600, 50 * (cnt + 3.5), 18, Color.SADDLEBROWN);
                    Text accept = buttonDesigner("A", 870, 50 * (cnt + 3.5), 18, Color.SADDLEBROWN);
                    Text decline = buttonDesigner("D", 920, 50 * (cnt + 3.5), 18, Color.SADDLEBROWN);
                    int finalI = i;
                    accept.setOnMouseClicked(event -> {
                        client.sendRequest(new Request("Friend", finalI, true,
                                client.getIndexOf()));
                        client.getFriend()[finalI] = true;
                        client.getFriendReq()[finalI] = false;
                    });
                    decline.setOnMouseClicked(event -> {
                        client.getFriendReq()[finalI] = false;
                    });
                    root.getChildren().addAll(text, accept, decline);
                }
            }
        }
        back.setOnMouseClicked(event -> {
            primaryStage.setScene(scene);
            pressed = false;
        });
    }

    public static void makeSceneBuilder(Group root, Scene scene, HashMultiset<Item> items1, HashMultiset<Item> items2,
                                        String name1, String name2, Group sourceRoot, String buttonName, int num) {
        root.getChildren().clear();
        Image backImage = new Image("pics/menus/backButton.png");
        Image titleImage = new Image("pics/menus/backpackTitle.png");
        Image statusImage = new Image("pics/menus/make.png");
        Rectangle backButton = rectangleBuilder(60, 60, new ImagePattern(backImage), 50, 20);
        Rectangle title = rectangleBuilder(180, 60, new ImagePattern(titleImage), 430, 20);
        Rectangle status = rectangleBuilder(1000, 700, new ImagePattern(statusImage), 0, 50);
        Rectangle moveButton = rectangleBuilder(60, 60, new ImagePattern(backImage), 880, 20);
        root.getChildren().addAll(backButton, title, status, moveButton);

        Text titleText = buttonDesigner(name2, 450, 53, 30, Color.SADDLEBROWN);
        Text back = buttonDesigner("Back", 54.5, 52, 21.5, Color.SADDLEBROWN);
        Text make = buttonDesigner(buttonName, 884, 52, 21.5, Color.SADDLEBROWN);
        Text name1Text = buttonDesigner(name1, 260, 130, 16, Color.SADDLEBROWN);
        Text name2Text = buttonDesigner(name2, 655, 130, 16, Color.SADDLEBROWN);
        HashMultiset<Item> chosedItems = HashMultiset.create();
        Text[] text = new Text[1];
        int count = 0;
        for (Item item : items1.elementSet()) {
            StringBuilder data = new StringBuilder();
            StringBuilder data1 = new StringBuilder();
            count++;
            data.append(count + ". ");
            data.append(item.getName());
            data1.append(item.getName());
            if (items1.count(item) > 1) {
                data.append(" x");
                data1.append(" x");
                data.append(items1.count(item));
                data1.append(items2.count(item));
            }
            data.append("\n");
            Text itemName = buttonDesigner(data.toString(), 180, 30 * (count + 6), 21.5, Color.SADDLEBROWN);
            itemName.setOnMouseClicked(event -> {
                size[0] += 1;
                if (items2.count(item) == 0) {
                    showPopup("Wrong Item was choosed!", sourceRoot);
                    initializeFlags();
                    primaryStage.setScene(scene);
                } else if (items2.count(item) > items1.count(item)) {
                    showPopup("You don't have enough " + item.getName(), sourceRoot);
                    initializeFlags();
                    primaryStage.setScene(scene);
                }
                text[0] = buttonDesigner(data1.toString(), 600, 30 * (chosedItems.elementSet().size() + 16), 21.5, Color.SADDLEBROWN);
                chosedItems.add(item, items2.count(item));
                root.getChildren().addAll(text[0]);
            });
            root.getChildren().addAll(itemName);
        }
        count = 0;
        for (Item item : items2.elementSet()) {
            StringBuilder data = new StringBuilder();
            count++;
            data.append(count);
            data.append(". ");
            data.append(item.getName());
            if (items2.count(item) > 1) {
                data.append(" x");
                data.append(items2.count(item));
            }
            data.append("\n");
            Text itemName = buttonDesigner(data.toString(), 600, 30 * (count + 6), 21.5, Color.SADDLEBROWN);
            root.getChildren().addAll(itemName);
        }
        root.getChildren().addAll(titleText, back, make, name1Text, name2Text);

        back.setOnMouseClicked(event -> {
            primaryStage.setScene(scene);
            initializeFlags();
        });

        make.setOnMouseClicked(event -> {
            if (chosedItems.elementSet().size() != items2.elementSet().size()) {
                showPopup("You didn't choosed the item completely!", sourceRoot);
                initializeFlags();
            }
            primaryStage.setScene(scene);
            flags[num] = true;
        });
    }


    public static void playerMenuSceneBuilder(Group root, Player player, int numberOfPlayer) {
        root.getChildren().clear();
        Image backImage = new Image("pics/menus/backButton.png");
        Image titleImage = new Image("pics/menus/backpackTitle.png");
        Image statusImage = new Image("pics/menus/playerMenu.png");
        Rectangle backButton = rectangleBuilder(60, 60, new ImagePattern(backImage), 50, 20);
        Rectangle title = rectangleBuilder(180, 60, new ImagePattern(titleImage), 430, 20);
        Rectangle status = rectangleBuilder(1000, 700, new ImagePattern(statusImage), 0, 0);
        Rectangle playerImage = rectangleBuilder(100, 140, new ImagePattern(playerDownPics[0]), 750, 300);
        root.getChildren().addAll(backButton, title, status, playerImage);

        Text titleText;
        if (isServer)
            titleText = buttonDesigner(server.getNames()[numberOfPlayer], 480, 53, 30, Color.SADDLEBROWN);
        else titleText = buttonDesigner(client.getNames()[numberOfPlayer], 480, 53, 30, Color.SADDLEBROWN);
        Text back = buttonDesigner("Back", 54.5, 52, 21.5, Color.SADDLEBROWN);
        Text chat = buttonDesigner("Chat", 180, 280, 30, Color.SADDLEBROWN);
        Text showStatus = buttonDesigner("Show Status", 435, 280, 27, Color.SADDLEBROWN);
        Text friend = buttonDesigner("Add/Remove Friend", 115, 480, 23, Color.SADDLEBROWN);
        Text trade = buttonDesigner("Invite to Trade", 425, 480, 26, Color.SADDLEBROWN);

        back.setOnMouseClicked(event -> primaryStage.setScene(villageScene));
        chat.setOnMouseClicked(event -> {
            if (isServer) {
                Request request = new Request("Chat", numberOfPlayer, server.clientsAdd.length);
                server.sendRequest(request);
            } else {
                Request request = new Request("Chat", numberOfPlayer, client.getIndexOf());
                client.sendRequest(request);
            }
        });

        showStatus.setOnMouseClicked(event -> {
            if (isServer) {
                Request request = new Request("Status", numberOfPlayer, server.clientsAdd.length);
                server.sendRequest(request);
            } else {
                Request request = new Request("Status", numberOfPlayer, client.getIndexOf());
                client.sendRequest(request);
            }
        });

        friend.setOnMouseClicked(event -> {
            if (isServer) {
                Request request = new Request("Friend", numberOfPlayer, server.clientsAdd.length);
                server.sendRequest(request);
            } else {
                Request request = new Request("Friend", numberOfPlayer, client.getIndexOf());
                client.sendRequest(request);
            }
        });

        trade.setOnMouseClicked(event -> {
            if (isServer) {
                Request request = new Request("Trade", numberOfPlayer, server.clientsAdd.length);
                server.sendRequest(request);
            } else {
                Request request = new Request("Trade", numberOfPlayer, client.getIndexOf());
                client.sendRequest(request);
            }
        });

        root.getChildren().addAll(titleText, back, chat, showStatus, friend, trade);
    }

    public static void helpSceneBuilder(Group root, Scene scene) {
        root.getChildren().clear();
        Image backImage = new Image("pics/menus/backButton.png");
        Image titleImage = new Image("pics/menus/backpackTitle.png");
        Image helpImage = new Image("pics/menus/help.png");
        Rectangle backButton = rectangleBuilder(60, 60, new ImagePattern(backImage), 50, 20);
        Rectangle title = rectangleBuilder(180, 60, new ImagePattern(titleImage), 430, 20);
        Rectangle status = rectangleBuilder(1000, 700, new ImagePattern(helpImage), 0, 0);
        root.getChildren().addAll(backButton, title, status);

        Text titleText = buttonDesigner("Help", 480, 53, 30, Color.SADDLEBROWN);
        Text back = buttonDesigner("Back", 54.5, 52, 21.5, Color.SADDLEBROWN);
        root.getChildren().addAll(titleText, back);

        back.setOnMouseClicked(event -> primaryStage.setScene(scene));
    }

    public static void yesNoSceneBuilder(Group root, Scene scene, String question, int num) {
        root.getChildren().clear();
        Image backImage = new Image("pics/menus/backButton.png");
        Image titleImage = new Image("pics/menus/backpackTitle.png");
        Image helpImage = new Image("pics/menus/yesno.png");
        Rectangle backButton = rectangleBuilder(60, 60, new ImagePattern(backImage), 50, 20);
        Rectangle title = rectangleBuilder(180, 60, new ImagePattern(titleImage), 430, 20);
        Rectangle status = rectangleBuilder(1000, 700, new ImagePattern(helpImage), 0, 0);
        root.getChildren().addAll(backButton, title, status);
        Text titleText = buttonDesigner("Question", 460, 53, 30, Color.SADDLEBROWN);
        Text back = buttonDesigner("Back", 54.5, 52, 21.5, Color.SADDLEBROWN);
        Text yes = buttonDesigner("Yes", 270, 480, 24, Color.SADDLEBROWN);
        Text no = buttonDesigner("No", 670, 480, 24, Color.SADDLEBROWN);
        Text data = buttonDesigner(question, 200, 270, 25, Color.SADDLEBROWN);
        root.getChildren().addAll(titleText, back, data, yes, no);

        back.setOnMouseClicked(event -> {
            primaryStage.setScene(scene);
            initializeFlags();
        });

        yes.setOnMouseClicked(event -> {
            flags[num] = true;
            yesNO = true;
            primaryStage.setScene(scene);
        });

        no.setOnMouseClicked(event -> {
            yesNO = false;
            initializeFlags();
            primaryStage.setScene(scene);
        });
    }

    public static void yesNoNetworkSceneBuilder(Group root, Scene scene, String question, int num, String req) {
        root.getChildren().clear();
        Image backImage = new Image("pics/menus/backButton.png");
        Image titleImage = new Image("pics/menus/backpackTitle.png");
        Image helpImage = new Image("pics/menus/yesno.png");
        Rectangle backButton = rectangleBuilder(60, 60, new ImagePattern(backImage), 50, 20);
        Rectangle title = rectangleBuilder(180, 60, new ImagePattern(titleImage), 430, 20);
        Rectangle status = rectangleBuilder(1000, 700, new ImagePattern(helpImage), 0, 0);
        root.getChildren().addAll(backButton, title, status);

        Text titleText = buttonDesigner("Question", 460, 53, 30, Color.SADDLEBROWN);
        Text back = buttonDesigner("Back", 54.5, 52, 21.5, Color.SADDLEBROWN);
        Text yes = buttonDesigner("Accept", 255, 480, 24, Color.SADDLEBROWN);
        Text no = buttonDesigner("Decline", 655, 480, 24, Color.SADDLEBROWN);
        Text data = buttonDesigner(question, 200, 270, 25, Color.SADDLEBROWN);
        root.getChildren().addAll(titleText, back, data, yes, no);

        back.setOnMouseClicked(event -> {
            primaryStage.setScene(villageScene);
            if (isServer) {
                if (req.equals("Chat"))
                    server.setChat(-1);
                else server.setTrade(-1);
            } else {
                if (req.equals("Chat"))
                    client.setChat(-1);
                else client.setTrade(-1);
            }
            initializeFlags();
        });

        yes.setOnMouseClicked(event -> {
            if (isServer) {
                if (req.equals("Chat")) {
                    chatSceneBuilder(chatRoot, server.getChatReqFrom());
                    if (server.getChat() != -1) {
                        server.sendRequest(new Request(req, server.getChatReqFrom(), true, server.clientsAdd.length));
                        server.setChat(-1);
                    }
                } else {
                    tradeSceneBuilder(tradeRoot, game.getPlayer(), villageScene, server.getTradeReqFrom());
                    if (server.getTrade() != -1) {
                        server.sendRequest(new Request(req, server.getTradeReqFrom(), true, server.clientsAdd.length));
                        server.setTrade(-1);
                    }
                }
            } else {
                if (req.equals("Chat")) {
                    chatSceneBuilder(chatRoot, client.getChatReqFrom());
                    if (client.getChat() != -1) {
                        client.sendRequest(new Request(req, client.getChatReqFrom(), true, client.getIndexOf()));
                        client.setChat(-1);
                    }
                } else {
                    tradeSceneBuilder(tradeRoot, game.getPlayer(), villageScene, client.getTradeReqFrom());
                    if (client.getTrade() != -1) {
                        client.sendRequest(new Request(req, client.getTradeReqFrom(), true, client.getIndexOf()));
                        client.setTrade(-1);
                    }
                }
            }
            flags[num] = true;
            yesNO = true;
            primaryStage.setScene(scene);
        });

        no.setOnMouseClicked(event -> {
            yesNO = false;
            if (isServer) {
                if (req.equals("Chat"))
                    server.setChat(-1);
                else server.setTrade(-1);
            } else {
                if (req.equals("Chat"))
                    client.setChat(-1);
                else client.setTrade(-1);
            }
            //initializeFlags();
            primaryStage.setScene(villageScene);
        });
    }

    public static void printSceneBuilder(Group root, ArrayList<String> data, String name, Scene scene, int num) {
        root.getChildren().clear();
        Image backImage = new Image("pics/menus/backButton.png");
        Image titleImage = new Image("pics/menus/backpackTitle.png");
        Image helpImage = new Image("pics/menus/help.png");
        Rectangle backButton = rectangleBuilder(60, 60, new ImagePattern(backImage), 50, 20);
        Rectangle title = rectangleBuilder(180, 60, new ImagePattern(titleImage), 430, 20);
        Rectangle status = rectangleBuilder(1000, 700, new ImagePattern(helpImage), 0, 0);
        root.getChildren().addAll(backButton, title, status);

        Text titleText = buttonDesigner(name, 445, 53, 28, Color.SADDLEBROWN);
        Text back = buttonDesigner("Back", 54.5, 52, 21.5, Color.SADDLEBROWN);
        root.getChildren().addAll(titleText, back);

        for (int i = 0; i < data.size(); i++) {
            Text dataText = buttonDesigner(data.get(i), 200, 38 * (i + 5), 21.5, Color.SADDLEBROWN);
            dataText.setOnMouseClicked(event -> {
                if (name.equals("ToolShelf")) {
                    for (Tool tool : game.getFarm().getHome().getKitchen().getToolshelf().getExistingTools()) {
                        if (dataText.getText().contains(tool.getName())) {
                            choosedItem = tool;
                        }
                    }
                }
                choosedButton = dataText.getText();
                flags[num] = true;
                primaryStage.setScene(scene);
            });
            root.getChildren().addAll(dataText);
        }

        back.setOnMouseClicked(event -> {
            initializeFlags();
            primaryStage.setScene(scene);
        });
    }

    public static void multiButtonSceneBuilder(Group root, ArrayList<String> buttons, String name, Scene scene, int num) {
        root.getChildren().clear();
        Image backImage = new Image("pics/menus/backButton.png");
        Image titleImage = new Image("pics/menus/backpackTitle.png");
        Rectangle backButton = rectangleBuilder(60, 60, new ImagePattern(backImage), 50, 20);
        Rectangle title = rectangleBuilder(200, 60, new ImagePattern(titleImage), 430, 20);
        root.getChildren().addAll(backButton, title);

        for (int i = 0; i < buttons.size(); i++) {
            Rectangle button = rectangleBuilder(350, 90, new ImagePattern(titleImage), 360, 120 * (i + 1));
            Text buttonText = buttonDesigner(buttons.get(i), 380, 120 * (i + 1.4), 28, Color.SADDLEBROWN);
            buttonText.setOnMouseClicked(event -> {
                choosedButton = buttonText.getText();
                flags[num] = true;
                primaryStage.setScene(scene);
            });
            root.getChildren().addAll(button, buttonText);
        }

        Text titleText = buttonDesigner(name, 450, 53, 30, Color.SADDLEBROWN);
        Text back = buttonDesigner("Back", 54.5, 52, 21.5, Color.SADDLEBROWN);
        root.getChildren().addAll(titleText, back);

        back.setOnMouseClicked(event -> {
            initializeFlags();
            primaryStage.setScene(scene);
        });
    }

    static HashMultiset<Item> chosedItems = HashMultiset.create();

    public static void tradeSceneBuilder(Group root, Player player1, Scene scene, int numOfPlayer) {
        root.getChildren().clear();
        chosedItems.clear();
        if (isServer) {
            server.setLockIn(false);
            for (int i = 0; i < server.clientsAdd.length + 1; i++) {
                server.getLockInFrom()[i] = false;
                server.getLockInTo()[i] = false;
            }
        } else {
            client.setLockIn(false);
            for (int i = 0; i < client.getNames().length; i++) {
                client.getLockInFrom()[i] = false;
                client.getLockInTo()[i] = false;
            }
        }
        Image backImage = new Image("pics/menus/backButton.png");
        Image titleImage = new Image("pics/menus/backpackTitle.png");
        Image statusImage = new Image("pics/menus/2sidedMenu.png");
        Rectangle backButton = rectangleBuilder(60, 60, new ImagePattern(backImage), 50, 20);
        Rectangle title = rectangleBuilder(180, 60, new ImagePattern(titleImage), 430, 20);
        Rectangle status = rectangleBuilder(1000, 700, new ImagePattern(statusImage), 0, 50);
        Rectangle moveButton = rectangleBuilder(60, 60, new ImagePattern(backImage), 880, 20);
        Rectangle playerImage1 = rectangleBuilder(100, 140, new ImagePattern(playerDownPics[0]), 850, 300);
        root.getChildren().addAll(backButton, title, status, moveButton, playerImage1);

        Text titleText = buttonDesigner("Trade", 450, 53, 30, Color.SADDLEBROWN);
        Text back = buttonDesigner("Back", 54.5, 52, 21.5, Color.SADDLEBROWN);
        Text move = buttonDesigner("Move", 884, 52, 21.5, Color.SADDLEBROWN);
        Text player1Name = buttonDesigner("Backpack", 275, 130, 21.5, Color.SADDLEBROWN);
        Text player2Name = buttonDesigner("choosed", 660, 130, 21.5, Color.SADDLEBROWN);
        Text[] text = new Text[1];
        int count = 0;
        for (Item item : player1.getBackpack().getItem().elementSet()) {
            StringBuilder data = new StringBuilder();
            count++;
            data.append(count + ". ");
            data.append(item.getName());
            if (player1.getBackpack().getItem().count(item) > 1) {
                data.append(" x");
                data.append(player1.getBackpack().getItem().count(item));
            }
            data.append("\n");
            Text itemName = buttonDesigner(data.toString(), 180, 30 * (count + 6), 21.5, Color.SADDLEBROWN);
            itemName.setOnMouseClicked(event -> {
                boolean flag = false;
                for (Item iitteemm : player1.getBackpack().getItem().elementSet()) {
                    if (iitteemm.equals(item))
                        flag = true;
                }
                if (flag) {
                    text[0] = buttonDesigner(item.getName(), 600, 30 * (chosedItems.size() + 7), 21.5, Color.SADDLEBROWN);
                    chosedItems.add(item);
                    player1.getBackpack().getItem().remove(item);
                    root.getChildren().addAll(text[0]);
                }
            });
            root.getChildren().addAll(itemName);
        }
        root.getChildren().addAll(titleText, back, move, player1Name, player2Name);
        move.setOnMouseClicked(event -> {
            if (isServer) {
                server.sendRequest(new Request("LockIn", numOfPlayer, server.clientsAdd.length));
                server.setLockIn(true);
                server.getLockInTo()[numOfPlayer] = true;
            } else {
                client.sendRequest(new Request("LockIn", numOfPlayer, client.getIndexOf()));
                client.setLockIn(true);
                client.getLockInTo()[numOfPlayer] = true;
            }
        });
        back.setOnMouseClicked(event -> primaryStage.setScene(scene));
    }

    final static boolean[] move = {false};
    static double durationMillis = 20d;
    static double movementSpeed = 1d;

    public static void move(Mover mover, KeyEvent event, Rectangle player) {
        if (!move[0]) {
            if (event.getCode() == KeyCode.UP) {
                player.setFill(new ImagePattern(playerUpPics[0]));
                if (mover.moveUp(player)) { // in 10 ha darvaghe 10 barabare movement speed hastan
                    move[0] = true;
                    final int[] cnt = {0};
                    Timeline timeline = new Timeline(new KeyFrame(Duration.millis(durationMillis), event1 -> {
                        player.setFill(new ImagePattern(playerUpPics[cnt[0]]));
                        cnt[0]++;
                        cnt[0] %= 9;
                        player.setY(player.getY() - movementSpeed);
                        player.setHeight(player.getHeight() - 0.05d);
                        player.setWidth(player.getWidth() - 0.0325d);
                        player.setX(player.getX() + 0.01625d);
                        if (cnt[0] == 0)
                            move[0] = false;
                    }));
                    timeline.setCycleCount(10);
                    timeline.play();
                    missionHandler("Move Up", "Player");
                }
            } else if (event.getCode() == KeyCode.DOWN) {
                player.setFill(new ImagePattern(playerDownPics[0]));
                if (mover.moveDown(player)) {
                    move[0] = true;
                    final int[] cnt = {0};
                    Timeline timeline = new Timeline(new KeyFrame(Duration.millis(durationMillis), event1 -> {
                        player.setFill(new ImagePattern(playerDownPics[cnt[0]]));
                        cnt[0]++;
                        cnt[0] %= 9;
                        player.setY(player.getY() + movementSpeed);
                        player.setHeight(player.getHeight() + 0.05d);
                        player.setWidth(player.getWidth() + 0.0325d);
                        player.setX(player.getX() - 0.01625d);
                        if (cnt[0] == 0)
                            move[0] = false;
                    }));
                    timeline.setCycleCount(10);
                    timeline.play();
                    missionHandler("Move Down", "Player");
                }
            } else if (event.getCode() == KeyCode.RIGHT) {
                player.setFill(new ImagePattern(playerRightPics[0]));
                if (mover.moveRight(player)) {
                    move[0] = true;
                    final int[] cnt = {0};
                    Timeline timeline = new Timeline(new KeyFrame(Duration.millis(durationMillis), event1 -> {
                        player.setFill(new ImagePattern(playerRightPics[cnt[0]]));
                        cnt[0]++;
                        cnt[0] %= 9;
                        player.setX(player.getX() + movementSpeed);
                        if (cnt[0] == 0)
                            move[0] = false;
                    }));
                    timeline.setCycleCount(10);
                    timeline.play();
                    missionHandler("Move Right", "Player");
                }
            } else if (event.getCode() == KeyCode.LEFT) {
                player.setFill(new ImagePattern(playerLeftPics[0]));
                if (mover.moveLeft(player)) {
                    move[0] = true;
                    final int[] cnt = {0};
                    Timeline timeline = new Timeline(new KeyFrame(Duration.millis(durationMillis), event1 -> {
                        player.setFill(new ImagePattern(playerLeftPics[cnt[0]]));
                        cnt[0]++;
                        cnt[0] %= 9;
                        player.setX(player.getX() - movementSpeed);
                        if (cnt[0] == 0)
                            move[0] = false;
                    }));
                    timeline.setCycleCount(10);
                    timeline.play();
                    missionHandler("Move Left", "Player");
                }
            }
        }
    }


    public static void backpackSceneHandler(String name, Scene scene, Group root, int num) {
        final int[] choosen = new int[1];
        Image backImage = new Image("pics/menus/backButton.png");
        Rectangle backButton = rectangleBuilder(60, 60, new ImagePattern(backImage), 50, 20);
        Text back = buttonDesigner("Back", 54.5, 52, 21.5, Color.SADDLEBROWN);
        backpackRoot.getChildren().addAll(backButton, back);
        back.setOnMouseClicked(event -> {
            initializeFlags();
            primaryStage.setScene(scene);
        });

        backpackScene.setOnMouseClicked(event -> {
            backpackUI.writeName(event);
            choosen[0] = backpackUI.clickOnRightPlace(event);
        });
        backpackScene.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ENTER) {
                if (choosen[0] != -1) {
                    if (backpackUI.choose(name, choosen[0]) != null) {
                        choosedItem = backpackUI.choose(name, choosen[0]);
                        flags[num] = true;
                    } else {
                        showPopup("Wrong Item", root);
                        initializeFlags();
                    }
                    primaryStage.setScene(scene);
                }
            }
        });
    }

    public static void backpackSceneHandler(Scene scene, Group root, String type) {
        final int[] choosen = new int[1];
        Image backImage = new Image("pics/menus/backButton.png");
        Rectangle backButton = rectangleBuilder(60, 60, new ImagePattern(backImage), 50, 20);
        Text back = buttonDesigner("Back", 54.5, 52, 21.5, Color.SADDLEBROWN);
        backpackRoot.getChildren().addAll(backButton, back);
        back.setOnMouseClicked(event -> {
            initializeFlags();
            primaryStage.setScene(scene);
        });

        backpackScene.setOnMouseClicked(event -> {
            backpackUI.writeName(event);
            choosen[0] = backpackUI.clickOnRightPlace(event);
        });
        backpackScene.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ENTER) {
                if (choosen[0] != -1) {
                    if (backpackUI.choose(choosen[0]) != null) {
                        choosedItem = backpackUI.choose(choosen[0]);
                        flags[1] = true;
                    }
                    if (type.equals("Animal Medicine") && !choosedItem.getType().contains(type)) {
                        showPopup("You can't choose this item to heal animals!", root);
                        initializeFlags();
                    } else if (!choosedItem.getType().contains(type)) {
                        showPopup("You can't sell this Item at this shop", root);
                        initializeFlags();
                    }
                    primaryStage.setScene(scene);
                }
            }
        });
    }

    public static void backpackSceneHandler(Scene scene, Group root, int num) {
        final int[] choosen = new int[1];
        Image backImage = new Image("pics/menus/backButton.png");
        Rectangle backButton = rectangleBuilder(60, 60, new ImagePattern(backImage), 50, 20);
        Text back = buttonDesigner("Back", 54.5, 52, 21.5, Color.SADDLEBROWN);
        backpackRoot.getChildren().addAll(backButton, back);
        back.setOnMouseClicked(event -> {
            initializeFlags();
            primaryStage.setScene(scene);
        });

        backpackScene.setOnMouseClicked(event -> {
            backpackUI.writeName(event);
            choosen[0] = backpackUI.clickOnRightPlace(event);
        });
        backpackScene.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ENTER) {
                if (choosen[0] != -1) {
                    if (backpackUI.choose(choosen[0]) != null) {
                        choosedItem = backpackUI.choose(choosen[0]);
                        flags[num] = true;
                    }
                    if (!(choosedItem instanceof Tool)) {
                        showPopup("You need to choose a tool!", root);
                        initializeFlags();
                    }
                    primaryStage.setScene(scene);
                }
            }
        });
    }

    public static void backpackSceneHandler(Scene scene, Group root, boolean valid) {
        final int[] choosen = new int[1];
        Image backImage = new Image("pics/menus/backButton.png");
        Rectangle backButton = rectangleBuilder(60, 60, new ImagePattern(backImage), 50, 20);
        Text back = buttonDesigner("Back", 54.5, 52, 21.5, Color.SADDLEBROWN);
        backpackRoot.getChildren().addAll(backButton, back);
        back.setOnMouseClicked(event -> {
            initializeFlags();
            primaryStage.setScene(scene);
        });

        backpackScene.setOnMouseClicked(event -> {
            backpackUI.writeName(event);
            choosen[0] = backpackUI.clickOnRightPlace(event);
        });
        backpackScene.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ENTER) {
                if (choosen[0] != -1) {
                    if (backpackUI.choose(choosen[0]) != null) {
                        choosedItem = backpackUI.choose(choosen[0]);
                        flags[1] = true;
                    }
                    if (!(choosedItem instanceof Tool)) {
                        showPopup("You need to choose a tool!", root);
                        initializeFlags();
                    } else if (choosedItem.isValid() != valid) {
                        showPopup("You need to choose a broken tool!", root);
                        initializeFlags();
                    }
                    primaryStage.setScene(scene);
                }
            }
        });
    }

    public static void backpackSceneHandler(Group root) {
        final int[] choosen = new int[1];
        Image backImage = new Image("pics/menus/backButton.png");
        Rectangle backButton = rectangleBuilder(60, 60, new ImagePattern(backImage), 50, 20);
        Text back = buttonDesigner("Back", 54.5, 52, 21.5, Color.SADDLEBROWN);
        backpackRoot.getChildren().addAll(backButton, back);
        back.setOnMouseClicked(event -> {
            initializeFlags();
            if (!isMultiplayer)
                primaryStage.setScene(pauseScene);
            else primaryStage.setScene(networkPauseScene);
        });

        backpackScene.setOnMouseClicked(event -> {
            backpackUI.writeName(event);
            choosen[0] = backpackUI.clickOnRightPlace(event);
        });
        backpackScene.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ENTER) {
                if (choosen[0] != -1) {
                    if (backpackUI.choose(choosen[0]) != null) {
                        choosedItem = backpackUI.choose(choosen[0]);
                        pressed = true;
                        if (flags[4]) {
                            timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                                if (flags[0]) {
                                    multiButtonSceneBuilder(multiButtonRoot, option.inspectItem(choosedItem),
                                            choosedItem.getName(), backpackScene, 1);
                                    primaryStage.setScene(multiButtonScene);
                                    flags[0] = false;
                                } else if (flags[1]) {
                                    if (choosedButton.equals("Drop this item")) {
                                        game.getPlayer().getBackpack().getItem().remove(choosedItem);
                                        backpackUI.backpackSceneBuilder(primaryStage, backpackRoot, backpackScene);
                                        showPopup(choosedItem.getName() + " was removed from backpack!", root);
                                        backpackRoot.getChildren().addAll(backButton, back);
                                        flags[2] = true;
                                    } else if (choosedButton.equals("Status")) {
                                        objectStatusSceneBuilder(objectStatusRoot, choosedItem, backpackScene, 2);
                                        primaryStage.setScene(objectStatusScene);
                                    } else if (choosedButton.equals("Use")) {
                                        backpackUI.backpackSceneBuilder(primaryStage, backpackRoot, backpackScene);
                                        backpackUI.use(choosedItem, backpackScene, game.getPlayer(), flags);
                                        backpackRoot.getChildren().addAll(backButton, back);
                                    }
                                    flags[1] = false;
                                } else if (flags[2]) {
                                    pressed = false;
                                    flags[0] = true;
                                    flags[2] = false;
                                    stopTimeline();
                                }
                            }));
                            timeline.setCycleCount(Animation.INDEFINITE);
                        }
                        if (flags[4]) {
                            timeline.play();
                            flags[4] = false;
                        }
                    }
                }
            }
        });
    }

    public static void checkShopSceneHandler(Scene scene, Group root, ChartUI chartUI) {
        final int[] choosen = new int[1];
        Image backImage = new Image("pics/menus/backButton.png");
        Rectangle backButton = rectangleBuilder(60, 60, new ImagePattern(backImage), 50, 20);
        Text back = buttonDesigner("Back", 54.5, 52, 21.5, Color.SADDLEBROWN);
        root.getChildren().addAll(backButton, back);
        back.setOnMouseClicked(event -> {
            initializeFlags();
            primaryStage.setScene(scene);
        });
        checkShopScene.setOnMouseClicked(event -> {
            chartUI.writeName(event);
            choosen[0] = chartUI.clickOnRightPlace(event);
        });
        checkShopScene.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ENTER) {
                if (choosen[0] != -1) {
                    if (chartUI.choose(choosen[0]) != null) {
                        choosedItem = chartUI.choose(choosen[0]);
                        flags[1] = true;
                    } else {
                        showPopup("Wrong Item", root);
                        pressed = false;
                    }
                    primaryStage.setScene(scene);
                }
            }
        });
    }

    public static void showPopup(String text, Group root) {
        //root.getChildren().removeAll(popupWindow, popupText);
        popupText = buttonDesigner(text, 360, 52, 17, Color.DARKRED);
        root.getChildren().addAll(popupWindow, popupText);
        Timeline timeline = new Timeline(new KeyFrame(Duration.millis(4000), event1 -> {
            root.getChildren().removeAll(popupWindow, popupText);
        }));
        timeline.play();
    }


    static boolean[] flags = {true, false, false, false, true};

    public static void stopTimeline() {
        timeline.stop();
        flags[4] = true;
    }

    public static void initializeFlags() {
        stopTimeline();
        pressed = false;
        flags[0] = true;
        flags[1] = false;
        flags[2] = false;
        flags[3] = false;

    }

    static Timeline timeline = new Timeline();
    static KeyEvent keyEvent;

    public static void farmSceneHandler(Group root) {
        double tileSize = farmUI.tileSize;
        Rectangle player = rectangleBuilder(tileSize * 1.5, tileSize * 2, new ImagePattern(playerDownPics[0]), tileSize * 14.25, tileSize * 1.5);
        root.getChildren().add(player);
        final Cell[][][] cells = {new Cell[3][3]};
        final int[] cnt1 = {0};
        final int[] cnt2 = {0};
        farmScene.setOnKeyPressed(event -> {
            if (pressed)
                cnt1[0]++;
            keyEvent = event;
            int gardenField = farmUI.chosenFieldNum(player);
            if ((event.getCode() == KeyCode.P || pressed) && farmUI.rightPlace(player)) {
                pressed = true;
                if (flags[4]) {
                    timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                        if (flags[0]) {
                            backpackUI.backpackSceneBuilder(primaryStage, backpackRoot, farmScene);
                            primaryStage.setScene(backpackScene);
                            backpackSceneHandler("Shovel", farmScene, root, 1);
                            flags[0] = false;
                        } else if (flags[1]) {
                            if (cnt2[0] == cnt1[0]) {
                                cells[0] = farmUI.chooseCell((Shovel) choosedItem, keyEvent, player, flags);
                                cnt2[0]++;
                            }
                        } else if (flags[2]) {
                            farmUI.plow((Shovel) choosedItem, cells[0], game.getPlayer(), flags);
                            flags[2] = false;
                        } else if (flags[3]) {
                            pressed = false;
                            flags[3] = false;
                            flags[0] = true;
                            stopTimeline();
                        }
                    }));
                    timeline.setCycleCount(Animation.INDEFINITE);
                }
                if (flags[4]) {
                    timeline.play();
                    flags[4] = false;
                }
            } else if ((event.getCode() == KeyCode.W || pressed) && farmUI.rightPlace(player)) {
                pressed = true;
                if (flags[4]) {
                    timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                        if (flags[0]) {
                            backpackUI.backpackSceneBuilder(primaryStage, backpackRoot, farmScene);
                            primaryStage.setScene(backpackScene);
                            backpackSceneHandler("Watering Can", farmScene, root, 1);
                            flags[0] = false;
                        } else if (flags[1]) {
                            if (cnt2[0] == cnt1[0]) {
                                cells[0] = farmUI.chooseCell((WateringCan) choosedItem, keyEvent, player, flags);
                                cnt2[0]++;
                            }
                        } else if (flags[2]) {
                            farmUI.watering((WateringCan) choosedItem, cells[0], game.getPlayer(), flags);
                            flags[2] = false;
                        } else if (flags[3]) {
                            pressed = false;
                            flags[3] = false;
                            flags[0] = true;
                            stopTimeline();
                        }
                    }));
                    timeline.setCycleCount(Animation.INDEFINITE);
                }
                if (flags[4]) {
                    timeline.play();
                    flags[4] = false;
                }
            } else if ((event.getCode() == KeyCode.W || pressed) && gardenField != -1) {
                pressed = true;
                if (flags[4]) {
                    timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                        if (flags[0]) {
                            backpackUI.backpackSceneBuilder(primaryStage, backpackRoot, farmScene);
                            primaryStage.setScene(backpackScene);
                            backpackSceneHandler("Watering Can", farmScene, root, 1);
                            flags[0] = false;
                        } else if (flags[1]) {
                            farmUI.waterTree((WateringCan) choosedItem, flags, gardenField / 3, gardenField % 3, game.getPlayer());
                            flags[1] = false;
                        } else if (flags[2]) {
                            pressed = false;
                            flags[0] = true;
                            flags[2] = false;
                            stopTimeline();
                        }
                    }));
                    timeline.setCycleCount(Animation.INDEFINITE);
                }
                if (flags[4]) {
                    timeline.play();
                    flags[4] = false;
                }
            } else if ((event.getCode() == KeyCode.D || pressed) && farmUI.rightPlace(player)) {
                pressed = true;
                if (flags[4]) {
                    timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                        if (flags[0]) {
                            flags[0] = false;
                            flags[1] = true;
                        } else if (flags[1]) {
                            if (cnt2[0] == cnt1[0]) {
                                cells[0] = farmUI.chooseCell(new AllOfTools().hand(), keyEvent, player, flags);
                                cnt2[0]++;
                            }
                        } else if (flags[2]) {
                            farmUI.destroyCrops(cells[0], game.getPlayer(), flags);
                            flags[2] = false;
                        } else if (flags[3]) {
                            pressed = false;
                            flags[3] = false;
                            flags[0] = true;
                            stopTimeline();
                        }
                    }));
                    timeline.setCycleCount(Animation.INDEFINITE);
                }
                if (flags[4]) {
                    timeline.play();
                    flags[4] = false;
                }
            } else if ((event.getCode() == KeyCode.H || pressed) && farmUI.rightPlace(player)) {
                pressed = true;
                if (flags[4]) {
                    timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                        if (flags[0]) {
                            flags[0] = false;
                            flags[1] = true;
                        } else if (flags[1]) {
                            if (cnt2[0] == cnt1[0]) {
                                cells[0] = farmUI.chooseCell(new AllOfTools().hand(), keyEvent, player, flags);
                                cnt2[0]++;
                            }
                        } else if (flags[2]) {
                            farmUI.harvestCrops(cells[0], game.getPlayer(), flags);
                            flags[2] = false;
                        } else if (flags[3]) {
                            pressed = false;
                            flags[3] = false;
                            flags[0] = true;
                            stopTimeline();
                        }
                    }));
                    timeline.setCycleCount(Animation.INDEFINITE);
                }
                if (flags[4]) {
                    timeline.play();
                    flags[4] = false;
                }
            } else if ((event.getCode() == KeyCode.S || pressed) && farmUI.rightPlace(player)) {
                pressed = true;
                if (flags[4]) {
                    timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                        if (flags[0]) {
                            backpackUI.backpackSceneBuilder(primaryStage, backpackRoot, farmScene);
                            primaryStage.setScene(backpackScene);
                            backpackSceneHandler("Seed", farmScene, root, 1);
                            flags[0] = false;
                        } else if (flags[1]) {
                            if (cnt2[0] == cnt1[0]) {
                                cells[0] = farmUI.chooseCell(choosedItem, keyEvent, player, flags);
                                cnt2[0]++;
                            }
                        } else if (flags[2]) {
                            farmUI.plantSeeds((Seed) choosedItem, cells[0], game.getPlayer(), flags, gameDate.getSeason().getWeather());
                            flags[2] = false;
                        } else if (flags[3]) {
                            pressed = false;
                            flags[3] = false;
                            flags[0] = true;
                            stopTimeline();
                        }
                    }));
                    timeline.setCycleCount(Animation.INDEFINITE);
                }
                if (flags[4]) {
                    timeline.play();
                    flags[4] = false;
                }
            } else if ((event.getCode() == KeyCode.F || pressed) && farmUI.rightPlaceForFill(player)) {
                pressed = true;
                if (flags[4]) {
                    timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                        if (flags[0]) {
                            backpackUI.backpackSceneBuilder(primaryStage, backpackRoot, farmScene);
                            primaryStage.setScene(backpackScene);
                            backpackSceneHandler("Watering Can", farmScene, root, 1);
                            flags[0] = false;
                        } else if (flags[1]) {
                            farmUI.fill((WateringCan) choosedItem, game.getPlayer(), flags);
                            flags[1] = false;
                        } else if (flags[2]) {
                            pressed = false;
                            flags[2] = false;
                            flags[0] = true;
                            stopTimeline();
                        }
                    }));
                    timeline.setCycleCount(Animation.INDEFINITE);
                }
                if (flags[4]) {
                    timeline.play();
                    flags[4] = false;
                }
            } else if ((event.getCode() == KeyCode.R || pressed) && farmUI.closeToGreenhouse(player)) {
                pressed = true;
                if (flags[4]) {
                    timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                        if (flags[0]) {
                            makeSceneBuilder(makeRoot, farmScene, game.getPlayer().getBackpack().getItem(),
                                    game.getFarm().getGreenhouse().getRepairObjects(), "Backpack",
                                    "Greenhouse", root, "Repair", 1);
                            primaryStage.setScene(makeScene);
                            flags[0] = false;
                        } else if (flags[1]) {
                            yesNoSceneBuilder(yesNoRoot, farmScene, "Do you want to repair Greenhouse for " +
                                    game.getFarm().getGreenhouse().getRepairPrice() + " Gils?", 2);
                            primaryStage.setScene(yesNoScene);
                            flags[1] = false;
                        } else if (flags[2]) {
                            if (yesNO) {
                                farmUI.repair(game.getPlayer(), flags);
                            }
                            flags[2] = false;
                        } else if (flags[3]) {
                            pressed = false;
                            flags[3] = false;
                            flags[0] = true;
                            stopTimeline();
                        }
                    }));
                    timeline.setCycleCount(Animation.INDEFINITE);
                }
                if (flags[4]) {
                    timeline.play();
                    flags[4] = false;
                }
            } else if ((event.getCode() == KeyCode.B || pressed) && gardenField != -1) {
                pressed = true;
                if (flags[4]) {
                    timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                        if (flags[0]) {
                            multiButtonSceneBuilder(multiButtonRoot, option.inspectFruitGarden(game.getFarm().getGarden(),
                                    gameDate.getSeason().getWeather()), "Buy Tree", farmScene, 1);
                            primaryStage.setScene(multiButtonScene);
                            flags[0] = false;
                        } else if (flags[1]) {
                            yesNoSceneBuilder(yesNoRoot, farmScene, "You will buy " + choosedButton + " for " +
                                    300 + " Gils." + " Is this okay?", 2);
                            primaryStage.setScene(yesNoScene);
                            flags[1] = false;
                        } else if (flags[2]) {
                            if (yesNO) {
                                farmUI.plantTree(choosedButton, flags, gardenField / 3, gardenField % 3);
                                game.getPlayer().setMoney(game.getPlayer().getMoney() - 300);
                                flags[2] = false;
                            } else
                                initializeFlags();
                        } else if (flags[3]) {
                            pressed = false;
                            flags[3] = false;
                            flags[0] = true;
                            stopTimeline();
                        }
                    }));
                    timeline.setCycleCount(Animation.INDEFINITE);
                }
                if (flags[4]) {
                    timeline.play();
                    flags[4] = false;
                }
            } else if ((event.getCode() == KeyCode.C || pressed) && gardenField != -1) {
                pressed = true;
                if (flags[4]) {
                    timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                        if (flags[0]) {
                            farmUI.collectFruit(flags, gardenField / 3, gardenField % 3, game.getPlayer());
                            flags[0] = false;
                        } else if (flags[1]) {
                            pressed = false;
                            flags[0] = true;
                            flags[1] = false;
                            stopTimeline();
                        }
                    }));
                    timeline.setCycleCount(Animation.INDEFINITE);
                }
                if (flags[4]) {
                    timeline.play();
                    flags[4] = false;
                }
            } else if (event.getCode() == KeyCode.E) {
                if (player.getX() + player.getWidth() / 2 <= tileSize * 5 &&
                        player.getX() + player.getWidth() / 2 >= tileSize * 2 &&
                        player.getY() + player.getHeight() <= tileSize * 8 &&
                        player.getY() + player.getHeight() >= tileSize * 6) {
                    choosedRoot = homeRoot;
                    primaryStage.setScene(homeScene);
                } else if (player.getX() + player.getWidth() / 2 <= tileSize * 11 &&
                        player.getX() + player.getWidth() / 2 >= tileSize * 8 &&
                        player.getY() + player.getHeight() <= tileSize * 8 &&
                        player.getY() + player.getHeight() >= tileSize * 6) {
                    choosedRoot = barnRoot;
                    primaryStage.setScene(barnScene);
                } else if (player.getX() + player.getWidth() / 2 <= tileSize * 22 &&
                        player.getX() + player.getWidth() / 2 >= tileSize * 19 &&
                        player.getY() + player.getHeight() <= tileSize * 9 &&
                        player.getY() + player.getHeight() >= tileSize * 7) {
                    if (!game.getFarm().getGreenhouse().isRuin()) {
                        choosedRoot = greenhouseRoot;
                        primaryStage.setScene(greenhouseScene);
                    } else {
                        UI.showPopup("Greenhouse is not repaired!", root);
                    }
                } else if (player.getX() + player.getWidth() / 2 <= tileSize * 16 &&
                        player.getX() + player.getWidth() / 2 >= tileSize * 13 &&
                        player.getY() + player.getHeight() <= tileSize * 3 &&
                        player.getY() + player.getHeight() >= tileSize * -2) {
                    choosedRoot = jungleRoot;
                    primaryStage.setScene(jungleScene);
                } else if (player.getX() + player.getWidth() / 2 <= tileSize * 15.5 &&
                        player.getX() + player.getWidth() / 2 >= tileSize * 13.5 &&
                        player.getY() + player.getHeight() <= tileSize * 30 &&
                        player.getY() + player.getHeight() >= tileSize * 19) {
                    choosedRoot = villageRoot;
                    primaryStage.setScene(villageScene);
                }
            } else if (event.getCode() == KeyCode.SPACE) {
                if (!isMultiplayer) {
                    timeCalculator.stop();
                    pauseSceneBuilder(pauseRoot, farmScene, game.getPlayer());
                    primaryStage.setScene(pauseScene);
                } else {
                    networkPauseSceneBuilder(networkPauseRoot, farmScene, game.getPlayer());
                    primaryStage.setScene(networkPauseScene);
                }
            } else
                move(farmUI, event, player);
        });
    }

    public static void jungleSceneHandler(Group root) {
        double tileSize = jungleUI.tileSize;
        Rectangle player = rectangleBuilder(tileSize * 1.5, tileSize * 2, new ImagePattern(playerDownPics[0]), tileSize * 30.25, tileSize * 2.5);
        root.getChildren().add(player);

        jungleScene.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.E) {
                keyEvent = event;
                if (player.getX() + player.getWidth() / 2 <= tileSize * 33 &&
                        player.getX() + player.getWidth() / 2 >= tileSize * 29 &&
                        player.getY() + player.getHeight() <= tileSize * 4 &&
                        player.getY() + player.getHeight() >= tileSize) {
                    choosedRoot = farmRoot;
                    primaryStage.setScene(farmScene);
                } else if (player.getX() + player.getWidth() / 2 <= tileSize * 13.5 &&
                        player.getX() + player.getWidth() / 2 >= tileSize * 12.5 &&
                        player.getY() + player.getHeight() <= tileSize * 5 &&
                        player.getY() + player.getHeight() >= tileSize * 2.5) {
                    choosedRoot = caveRoot;
                    primaryStage.setScene(caveScene);
                } else if (player.getX() + player.getWidth() / 2 <= tileSize * 2 &&
                        player.getX() + player.getWidth() / 2 >= tileSize * 1 &&
                        player.getY() + player.getHeight() <= tileSize * 30 &&
                        player.getY() + player.getHeight() >= tileSize * 4) {
                    choosedRoot = villageRoot;
                    primaryStage.setScene(villageScene);
                }
            } else if (event.getCode() == KeyCode.F || pressed && jungleUI.closeToRiver(player)) {
                pressed = true;
                if (flags[4]) {
                    timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                        if (flags[0]) {
                            backpackUI.backpackSceneBuilder(primaryStage, backpackRoot, jungleScene);
                            primaryStage.setScene(backpackScene);
                            backpackSceneHandler("Fishing Rod", jungleScene, root, 1);
                            flags[0] = false;
                        } else if (flags[1]) {
                            jungleUI.fishing(choosedItem, flags);
                            flags[1] = false;
                        } else if (flags[2]) {
                            pressed = false;
                            flags[2] = false;
                            flags[0] = true;
                            stopTimeline();
                        }
                    }));
                    timeline.setCycleCount(Animation.INDEFINITE);
                }
                if (flags[4]) {
                    timeline.play();
                    flags[3] = false;
                    flags[4] = false;
                }
            } else if (event.getCode() == KeyCode.C || pressed) {
                pressed = true;
                if (jungleUI.objectToCollect(player) != null) {
                    if ((jungleUI.objectToCollect(player)).getName().equals("Branch") || ((Item) jungleUI.objectToCollect(player)).getName().equals("Stone")) {
                        jungleUI.collect(game.getPlayer(), jungleUI.objectToCollect(player), game.getJungle(), jungleUI.getAi(), jungleUI.getJey(), flags);
                        initializeFlags();
                    } else {
                        if (flags[4]) {
                            timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                                if (flags[0]) {
                                    backpackUI.backpackSceneBuilder(primaryStage, backpackRoot, jungleScene);
                                    primaryStage.setScene(backpackScene);
                                    backpackSceneHandler("Axe", jungleScene, jungleRoot, 1);
                                    flags[0] = false;
                                } else if (flags[1]) {
                                    jungleUI.collect(game.getPlayer(), (Tool) choosedItem, jungleUI.objectToCollect(player), game.getJungle(), jungleUI.getAi(), jungleUI.getJey(), flags);
                                    flags[1] = false;
                                } else if (flags[2]) {
                                    pressed = false;
                                    flags[2] = false;
                                    flags[0] = true;
                                    stopTimeline();
                                }
                            }));
                            timeline.setCycleCount(Animation.INDEFINITE);
                        }
                        if (flags[4]) {
                            timeline.play();
                            flags[3] = false;
                            flags[4] = false;
                        }
                    }
                } else {
                    pressed = false;
                }
            } else if (event.getCode() == KeyCode.SPACE) {
                if (!isMultiplayer) {
                    timeCalculator.stop();
                    pauseSceneBuilder(pauseRoot, jungleScene, game.getPlayer());
                    primaryStage.setScene(pauseScene);
                } else {
                    networkPauseSceneBuilder(networkPauseRoot, jungleScene, game.getPlayer());
                    primaryStage.setScene(networkPauseScene);
                }
            } else
                move(jungleUI, event, player);
        });
    }

    public static void caveSceneHandler(Group root) {
        double tileSize = caveUI.tileSize;
        Rectangle player = rectangleBuilder(tileSize * 1.5, tileSize * 2, new ImagePattern(playerDownPics[0]), tileSize * 14.25, tileSize * 2);
        root.getChildren().add(player);

        caveScene.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.E) {
                if (player.getX() + player.getWidth() / 2 <= tileSize * 11 &&
                        player.getX() + player.getWidth() / 2 >= tileSize * 9 &&
                        player.getY() + player.getHeight() <= tileSize * 4 &&
                        player.getY() + player.getHeight() >= tileSize * 2) {
                    primaryStage.setScene(jungleScene);
                }
            } else if (event.getCode() == KeyCode.C || pressed) {
                pressed = true;
                if (caveUI.objectToCollect(player) != null) {
                    if ((caveUI.objectToCollect(player)).getName().equals("Stone")) {
                        caveUI.collect(game.getPlayer(), caveUI.objectToCollect(player), game.getCave(), caveUI.getAi(), caveUI.getJey(), flags);
                        initializeFlags();
                    } else {
                        if (flags[4]) {
                            timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                                if (flags[0]) {
                                    backpackUI.backpackSceneBuilder(primaryStage, backpackRoot, caveScene);
                                    primaryStage.setScene(backpackScene);
                                    backpackSceneHandler("Pickaxe", caveScene, caveRoot, 1);
                                    flags[0] = false;
                                } else if (flags[1]) {
                                    caveUI.collect(game.getPlayer(), (Tool) choosedItem, caveUI.objectToCollect(player), game.getCave(), caveUI.getAi(), caveUI.getJey(), flags);
                                    flags[1] = false;
                                } else if (flags[2]) {
                                    pressed = false;
                                    flags[2] = false;
                                    flags[0] = true;
                                    stopTimeline();
                                }
                            }));
                            timeline.setCycleCount(Animation.INDEFINITE);
                        }
                        if (flags[4]) {
                            timeline.play();
                            flags[3] = false;
                            flags[4] = false;
                        }
                    }
                } else {
                    pressed = false;
                }
            } else if (event.getCode() == KeyCode.SPACE) {
                if (!isMultiplayer) {
                    timeCalculator.stop();
                    pauseSceneBuilder(pauseRoot, caveScene, game.getPlayer());
                    primaryStage.setScene(pauseScene);
                } else {
                    networkPauseSceneBuilder(networkPauseRoot, caveScene, game.getPlayer());
                    primaryStage.setScene(networkPauseScene);
                }
            } else
                move(caveUI, event, player);
        });
    }

    public static void villageSceneHandler(Group root) {
        double tileSize = villageUI.tileSize;
        player = rectangleBuilder(tileSize * 1.5, tileSize * 2, new ImagePattern(playerRightPics[0]), 0, tileSize * 8);
        root.getChildren().add(player);

        villageScene.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.E) {
                if (player.getX() + player.getWidth() / 2 <= tileSize * 39 &&
                        player.getX() + player.getWidth() / 2 >= tileSize * 37 &&
                        player.getY() + player.getHeight() >= tileSize * 7 &&
                        player.getY() + player.getHeight() <= tileSize * 9) {
                    choosedRoot = jungleRoot;
                    primaryStage.setScene(jungleScene);
                } else if (player.getX() + player.getWidth() / 2 <= tileSize * 32 &&
                        player.getX() + player.getWidth() / 2 >= tileSize * 28 &&
                        player.getY() + player.getHeight() <= tileSize * 23.5 &&
                        player.getY() + player.getHeight() >= tileSize * 22) {
                    choosedRoot = cafeRoot;
                    primaryStage.setScene(cafeScene);
                } else if (player.getX() + player.getWidth() / 2 <= tileSize * 4 &&
                        player.getX() + player.getWidth() / 2 >= tileSize * 2 &&
                        player.getY() + player.getHeight() <= tileSize * 7 &&
                        player.getY() + player.getHeight() >= tileSize * 5) {
                    choosedRoot = clinicRoot;
                    primaryStage.setScene(clinicScene);
                } else if ((player.getX() + player.getWidth() / 2 <= tileSize * 8 &&
                        player.getX() + player.getWidth() / 2 >= tileSize * 6 &&
                        player.getY() + player.getHeight() <= tileSize * 16 &&
                        player.getY() + player.getHeight() >= tileSize * 14) ||
                        (player.getX() + player.getWidth() / 2 <= tileSize * 10 &&
                                player.getX() + player.getWidth() / 2 >= tileSize * 4 &&
                                player.getY() + player.getHeight() <= tileSize * 27 &&
                                player.getY() + player.getHeight() >= tileSize * 24)) {
                    choosedRoot = marketRoot;
                    primaryStage.setScene(marketScene);
                } else if (player.getX() + player.getWidth() / 2 <= tileSize * 18 &&
                        player.getX() + player.getWidth() / 2 >= tileSize * 16 &&
                        player.getY() + player.getHeight() <= tileSize * 22 &&
                        player.getY() + player.getHeight() >= tileSize * 20) {
                    choosedRoot = laboratoryRoot;
                    primaryStage.setScene(laboratoryScene);
                } else if (player.getX() + player.getWidth() / 2 <= tileSize * 22 &&
                        player.getX() + player.getWidth() / 2 >= tileSize * 20 &&
                        player.getY() + player.getHeight() <= tileSize * 6 &&
                        player.getY() + player.getHeight() >= tileSize * 4) {
                    choosedRoot = ranchRoot;
                    primaryStage.setScene(ranchScene);
                } else if (player.getX() + player.getWidth() / 2 <= tileSize * 15 &&
                        player.getX() + player.getWidth() / 2 >= tileSize * 13 &&
                        player.getY() + player.getHeight() <= tileSize * 8 &&
                        player.getY() + player.getHeight() >= tileSize * 6) {
                    choosedRoot = gymRoot;
                    primaryStage.setScene(gymScene);
                } else if (player.getX() + player.getWidth() / 2 <= tileSize * 24 &&
                        player.getX() + player.getWidth() / 2 >= tileSize * 21 &&
                        player.getY() + player.getHeight() <= tileSize * 26 &&
                        player.getY() + player.getHeight() >= tileSize * 24) {
                    choosedRoot = workshopRoot;
                    primaryStage.setScene(workshopScene);
                } else if (player.getX() + player.getWidth() / 2 <= tileSize * 31.5 &&
                        player.getX() + player.getWidth() / 2 >= tileSize * 29.5 &&
                        player.getY() + player.getHeight() <= tileSize * 29 &&
                        player.getY() + player.getHeight() >= tileSize * 26) {
                    choosedRoot = farmRoot;
                    primaryStage.setScene(farmScene);
                } else if (player.getX() + player.getWidth() / 2 <= tileSize * 46 &&
                        player.getX() + player.getWidth() / 2 >= tileSize * 36) {
                    choosedRoot = jungleRoot;
                    primaryStage.setScene(jungleScene);
                }
            } else if (event.getCode() == KeyCode.Q) {
                if (isServer) {
                    if (server.nearAnyPlayer() != -1) {
                        nearAnyPlayer = server.nearAnyPlayer();
                        playerMenuSceneBuilder(playerMenuRoot, game.getPlayer(), nearAnyPlayer);
                        primaryStage.setScene(playerMenuScene);

                    }
                } else if (!isServer) {
                    if (client.nearAnyPlayer() != -1) {
                        nearAnyPlayer = client.nearAnyPlayer();
                        playerMenuSceneBuilder(playerMenuRoot, game.getPlayer(), nearAnyPlayer);
                        primaryStage.setScene(playerMenuScene);

                    }
                }
            } else if (event.getCode() == KeyCode.SPACE) {
                if (!isMultiplayer) {
                    timeCalculator.stop();
                    pauseSceneBuilder(pauseRoot, villageScene, game.getPlayer());
                    primaryStage.setScene(pauseScene);
                } else {
                    networkPauseSceneBuilder(networkPauseRoot, villageScene, game.getPlayer());
                    primaryStage.setScene(networkPauseScene);
                }
            } else
                move(villageUI, event, player);
        });
    }

    public static void cafeSceneHandler(Group root) {
        double tileSize = cafeUI.tileSize;
        Rectangle player = rectangleBuilder(tileSize * 1.5, tileSize * 2, new ImagePattern(playerDownPics[0]), tileSize * 17.75, tileSize * 0.5);
        root.getChildren().add(player);

        cafeScene.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.E) {
                if (player.getX() + player.getWidth() / 2 <= tileSize * 19 &&
                        player.getX() + player.getWidth() / 2 >= tileSize * 18 &&
                        player.getY() + player.getHeight() <= tileSize * 3 &&
                        player.getY() + player.getHeight() >= tileSize * 2) {
                    choosedRoot = villageRoot;
                    primaryStage.setScene(villageScene);
                }
            } else if (event.getCode() == KeyCode.P) {
                primaryStage.setScene(miniGameScene);
            } else if ((event.getCode() == KeyCode.C || pressed) && cafeUI.closeToDinningTable(player)) {
                pressed = true;
                if (flags[4]) {
                    timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                        if (flags[0]) {
                            checkShopSceneHandler(cafeScene, checkShopRoot, checkCafeUI);
                            checkCafeUI.backpackSceneBuilder(primaryStage, checkShopRoot, cafeScene);
                            primaryStage.setScene(checkShopScene);
                            flags[0] = false;
                        } else if (flags[1]) {
                            objectStatusSceneBuilder(objectStatusRoot, choosedItem, cafeScene, 2);
                            primaryStage.setScene(objectStatusScene);
                            flags[1] = false;
                        } else if (flags[2]) {
                            pressed = false;
                            flags[2] = false;
                            flags[0] = true;
                            stopTimeline();
                        }
                    }));
                    timeline.setCycleCount(Animation.INDEFINITE);
                }
                if (flags[4]) {
                    timeline.play();
                    flags[4] = false;
                }
            } else if ((event.getCode() == KeyCode.B || pressed) && cafeUI.closeToDinningTable(player)) {
                pressed = true;
                if (flags[4]) {
                    timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                        if (flags[0]) {
                            checkShopSceneHandler(cafeScene, checkShopRoot, checkCafeUI);
                            checkCafeUI.backpackSceneBuilder(primaryStage, checkShopRoot, cafeScene);
                            primaryStage.setScene(checkShopScene);
                            flags[0] = false;
                        } else if (flags[1]) {
                            // TODO: 7/12/2017 ghazasho hamin ja bokhore:|
                            buySceneBuilder(buyRoot, choosedItem, cafeScene, cafeRoot, game.getPlayer(),
                                    game.getVillage().getCafe());
                            primaryStage.setScene(buyScene);
                            flags[1] = false;
                        } else if (flags[2]) {
                            pressed = false;
                            flags[2] = false;
                            flags[0] = true;
                            stopTimeline();
                        }
                    }));
                    timeline.setCycleCount(Animation.INDEFINITE);
                }
                if (flags[4]) {
                    timeline.play();
                    flags[4] = false;
                }
            } else if ((event.getCode() == KeyCode.M || pressed) && cafeUI.closeToBoard(player)) {
                pressed = true;
                if (flags[4]) {
                    timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                        if (flags[0]) {
                            multiButtonSceneBuilder(multiButtonRoot, option.inspectBoard(),
                                    "MissionBoard", cafeScene, 1);
                            primaryStage.setScene(multiButtonScene);
                            flags[0] = false;
                        } else if (flags[1]) {
                            if (choosedButton.toLowerCase().contains("available")) {
                                tableSceneBuilder(tableRoot, cafeScene, game.getVillage().getCafe().getBoard().getAvailableMission(), "Missions", 2);
                                primaryStage.setScene(tableScene);
                            } else if (choosedButton.toLowerCase().contains("active")) {
                                tableSceneBuilder(tableRoot, cafeScene, game.getVillage().getCafe().getBoard().getActiveMission(), "Active Missions", 2);
                                primaryStage.setScene(tableScene);
                            }
                            flags[1] = false;
                        } else if (flags[2]) {
                            if (choosedButton.toLowerCase().contains("available")) {
                                multiButtonSceneBuilder(multiButtonRoot, option.inspectAvailableMission(), choosedMission.getName(), cafeScene, 3);
                                primaryStage.setScene(multiButtonScene);
                            } else if (choosedButton.toLowerCase().contains("active")) {
                                multiButtonSceneBuilder(multiButtonRoot, option.inspectActiveMissions(choosedMission), choosedMission.getName(), cafeScene, 3);
                                primaryStage.setScene(multiButtonScene);
                            }
                            flags[2] = false;
                        } else if (flags[3]) {
                            if (choosedButton.toLowerCase().contains("brief")) {
                                printSceneBuilder(printRoot, option.missionBriefing(choosedMission), choosedMission.getName(), cafeScene, 4);
                                primaryStage.setScene(printScene);
                                initializeFlags();
                            } else if (choosedButton.toLowerCase().contains("accept")) {
                                cafeUI.acceptMission(choosedMission, game.getPlayer(), game.getVillage().getCafe());
                                initializeFlags();
                            } else if (choosedButton.toLowerCase().contains("progress")) {
                                printSceneBuilder(printRoot, option.checkMissionProgress(choosedMission), choosedMission.getName(), cafeScene, 4);
                                primaryStage.setScene(printScene);
                                initializeFlags();
                            } else if (choosedButton.toLowerCase().contains("remove")) {
                                cafeUI.removeMission(choosedMission, game.getPlayer(), game.getVillage().getCafe());
                                initializeFlags();
                            }
                        }

                    }));
                    timeline.setCycleCount(Animation.INDEFINITE);
                }
                if (flags[4]) {
                    timeline.play();
                    flags[4] = false;
                }
            } else if (event.getCode() == KeyCode.SPACE) {
                if (!isMultiplayer) {
                    timeCalculator.stop();
                    pauseSceneBuilder(pauseRoot, cafeScene, game.getPlayer());
                    primaryStage.setScene(pauseScene);
                } else {
                    networkPauseSceneBuilder(networkPauseRoot, cafeScene, game.getPlayer());
                    primaryStage.setScene(networkPauseScene);
                }
            } else
                move(cafeUI, event, player);
        });
    }

    public static void homeSceneHandler(Group root) {
        double tileSize = homeUI.tileSize;
        Rectangle player = rectangleBuilder(tileSize * 1.5, tileSize * 2, new ImagePattern(playerDownPics[0]), tileSize * 16.25, tileSize * 3);
        root.getChildren().add(player);

        homeScene.setOnKeyPressed(event -> {
            if ((event.getCode() == KeyCode.B || pressed) && homeUI.closeToBed(player)) {
                pressed = true;
                if (flags[4]) {
                    timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                        if (flags[0]) {
                            multiButtonSceneBuilder(multiButtonRoot, option.inspectBed(new Bed()),
                                    "Bed", homeScene, 1);
                            primaryStage.setScene(multiButtonScene);
                            flags[0] = false;
                        } else if (flags[1]) {
                            homeUI.sleep(game.getPlayer(), flags, game, gameDate);
                            flags[1] = false;
                        } else if (flags[2]) {
                            pressed = false;
                            flags[0] = true;
                            flags[2] = false;
                            stopTimeline();
                        }
                    }));
                    timeline.setCycleCount(Animation.INDEFINITE);
                }
                if (flags[4]) {
                    timeline.play();
                    flags[4] = false;
                }
            } else if ((event.getCode() == KeyCode.S || pressed) && homeUI.closeToStorageBox(player)) {
                pressed = true;
                if (flags[4]) {
                    timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                        if (flags[0]) {
                            twoSidedSceneBuilder(twoSidedRoot, homeScene, game.getPlayer().getBackpack().getItem(),
                                    game.getFarm().getHome().getStorageBox().getItem(), "BackPack", "StorageBox");
                            primaryStage.setScene(twoSidedScene);
                            flags[0] = false;
                        } else if (flags[1]) {
                            if (choosedButton.equals("Take")) {
                                homeUI.takeFromStorageBox(choosedItem, game.getPlayer(), flags);
                            } else if (choosedButton.equals("Put")) {
                                homeUI.putIntoStorageBox(choosedItem, game.getPlayer(), flags);
                            }
                            flags[1] = false;
                        } else if (flags[2]) {
                            pressed = false;
                            flags[0] = true;
                            flags[2] = false;
                            stopTimeline();
                        }
                    }));
                    timeline.setCycleCount(Animation.INDEFINITE);
                }
                if (flags[4]) {
                    timeline.play();
                    flags[4] = false;
                }
            } else if ((event.getCode() == KeyCode.T || pressed) && homeUI.closeToToolShelf(player)) {
                pressed = true;
                if (flags[4]) {
                    Item[] tool = new Item[1];
                    boolean[] exist = new boolean[1];
                    timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                        if (flags[0]) {
                            printSceneBuilder(printRoot, option.inspectToolShelf(game.getFarm().getHome().getKitchen().
                                    getToolshelf()), "ToolShelf", homeScene, 1);
                            primaryStage.setScene(printScene);
                            flags[0] = false;
                        } else if (flags[1]) {
                            if (choosedButton.contains("No")) {
                                multiButtonSceneBuilder(multiButtonRoot, option.inspectCookingTool(false),
                                        "CookingTool", homeScene, 2);
                                primaryStage.setScene(multiButtonScene);
                                exist[0] = false;
                            } else if (choosedButton.contains("Yes")) {
                                multiButtonSceneBuilder(multiButtonRoot, option.inspectCookingTool(true),
                                        "CookingTool", homeScene, 2);
                                exist[0] = true;
                                primaryStage.setScene(multiButtonScene);
                            }
                            flags[1] = false;
                        } else if (flags[2]) {
                            if (exist[0]) {
                                tool[0] = choosedItem;
                                if (choosedButton.contains("Replace")) {
                                    backpackUI.backpackSceneBuilder(primaryStage, backpackRoot, homeScene);
                                    primaryStage.setScene(backpackScene);
                                    backpackSceneHandler(homeScene, root, 3);
                                } else if (choosedButton.contains("Remove")) {
                                    homeUI.remove(choosedItem, game.getPlayer(), flags);
                                    initializeFlags();
                                } else if (choosedButton.contains("Status")) {
                                    objectStatusSceneBuilder(objectStatusRoot, choosedItem, homeScene, 3);
                                    primaryStage.setScene(objectStatusScene);
                                    initializeFlags();
                                }
                            } else {
                                if (choosedButton.contains("Put")) {
                                    backpackUI.backpackSceneBuilder(primaryStage, backpackRoot, homeScene);
                                    primaryStage.setScene(backpackScene);
                                    backpackSceneHandler(choosedItem.getName(), homeScene, root, 3);
                                }
                            }
                            flags[2] = false;
                        } else if (flags[3]) {
                            if (exist[0]) {
                                homeUI.replace(choosedItem, tool[0], game.getPlayer(), flags);
                            } else {
                                homeUI.putIntoToolShelf(choosedItem, game.getPlayer(), flags);
                            }
                            pressed = false;
                            flags[3] = false;
                            flags[0] = true;
                            stopTimeline();
                        }
                    }));
                    timeline.setCycleCount(Animation.INDEFINITE);
                }
                if (flags[4]) {
                    timeline.play();
                    flags[4] = false;
                }
            } else if ((event.getCode() == KeyCode.R || pressed) && homeUI.closeToTable(player)) {
                pressed = true;
                if (flags[4]) {
                    timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                        if (flags[0]) {
                            multiButtonSceneBuilder(multiButtonRoot, option.printFoodsName(game.getFarm().getHome().getKitchen()),
                                    "Recipe", homeScene, 1);
                            primaryStage.setScene(multiButtonScene);
                            flags[0] = false;
                        } else if (flags[1]) {
                            for (Food food : foods) {
                                if (food.getName().equals(choosedButton)) {
                                    System.out.println(choosedButton);
                                    choosedItem = food;
                                }
                            }
                            if (choosedItem instanceof Food)
                                printSceneBuilder(printRoot, new Status().recipe((((Food) (choosedItem)).getRecipe())),
                                        "Recipe", homeScene, 2);
                            primaryStage.setScene(printScene);
                            flags[1] = false;
                        } else if (flags[2]) {
                            pressed = false;
                            flags[0] = true;
                            flags[2] = false;
                            stopTimeline();
                        }
                    }));
                    timeline.setCycleCount(Animation.INDEFINITE);
                }
                if (flags[4]) {
                    timeline.play();
                    flags[4] = false;
                }
            } else if ((event.getCode() == KeyCode.C || pressed) && homeUI.inThekitchen(player)) {
                pressed = true;
                if (flags[4]) {
                    timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                        if (flags[0]) {
                            multiButtonSceneBuilder(multiButtonRoot, option.printFoodsName(game.getFarm().getHome().getKitchen()),
                                    "Recipe", homeScene, 1);
                            primaryStage.setScene(multiButtonScene);
                            flags[0] = false;
                        } else if (flags[1]) {
                            for (Food food : foods) {
                                if (choosedButton.equals(food.getName())) {
                                    choosedItem = food;
                                }
                            }
                            makeSceneBuilder(makeRoot, homeScene, game.getPlayer().getBackpack().getItem(),
                                    choosedItem.getDissassemblity().getInitialIngredients(), "Backpack",
                                    choosedItem.getName(), root, "Cook", 2);
                            primaryStage.setScene(makeScene);
                            flags[1] = false;
                        } else if (flags[2]) {
                            homeUI.cook(choosedItem, game.getPlayer(), flags);
                            flags[2] = false;
                        } else if (flags[3]) {
                            pressed = false;
                            flags[3] = false;
                            flags[0] = true;
                            stopTimeline();
                        }
                    }));
                    timeline.setCycleCount(Animation.INDEFINITE);
                }
                if (flags[4]) {
                    timeline.play();
                    flags[4] = false;
                }
            } else if (event.getCode() == KeyCode.E) {
                if (player.getX() + player.getWidth() / 2 <= tileSize * 18 &&
                        player.getX() + player.getWidth() / 2 >= tileSize * 16 &&
                        player.getY() + player.getHeight() <= tileSize * 5 &&
                        player.getY() + player.getHeight() >= tileSize * 4) {
                    choosedRoot = farmRoot;
                    primaryStage.setScene(farmScene);
                }
            } else if (event.getCode() == KeyCode.SPACE) {
                if (!isMultiplayer) {
                    timeCalculator.stop();
                    pauseSceneBuilder(pauseRoot, homeScene, game.getPlayer());
                    primaryStage.setScene(pauseScene);
                } else {
                    networkPauseSceneBuilder(networkPauseRoot, homeScene, game.getPlayer());
                    primaryStage.setScene(networkPauseScene);
                }
            } else
                move(homeUI, event, player);
        });
    }

    public static void gymSceneHandler(Group root) {
        double tileSize = gymUI.tileSize;
        Rectangle player = rectangleBuilder(tileSize * 1.5, tileSize * 2, new ImagePattern(playerDownPics[0]), tileSize * 15.25, tileSize * 2);
        root.getChildren().add(player);

        gymScene.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.E) {
                if (player.getX() + player.getWidth() / 2 <= tileSize * 16 &&
                        player.getX() + player.getWidth() / 2 >= tileSize * 14 &&
                        player.getY() + player.getHeight() <= tileSize * 5 &&
                        player.getY() + player.getHeight() >= tileSize * 4) {
                    choosedRoot = villageRoot;
                    primaryStage.setScene(villageScene);
                }
            } else if ((event.getCode() == KeyCode.T || pressed) && gymUI.closeToBoard(player)) {
                pressed = true;
                if (flags[4]) {
                    timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                        if (flags[0]) {
                            tableSceneBuilder(tableRoot, gymScene, game.getVillage().getGym().getTrain(), "Trains", 1);
                            primaryStage.setScene(tableScene);
                            flags[0] = false;
                        } else if (flags[1]) {
                            yesNoSceneBuilder(yesNoRoot, gymScene, "You will Train " + choosedTrain.getName() + "for " +
                                    choosedTrain.getPrice() + " Gils." + " Is this okay?", 2);
                            primaryStage.setScene(yesNoScene);
                            flags[1] = false;
                        } else if (flags[2]) {
                            if (yesNO) {
                                gymUI.train(choosedTrain, game.getPlayer(), flags);
                            }
                            flags[2] = false;
                        } else if (flags[3]) {
                            pressed = false;
                            flags[3] = false;
                            flags[0] = true;
                            stopTimeline();
                        }
                    }));
                    timeline.setCycleCount(Animation.INDEFINITE);
                }
                if (flags[4]) {
                    timeline.play();
                    flags[4] = false;
                }
            } else if (event.getCode() == KeyCode.SPACE) {
                if (!isMultiplayer) {
                    timeCalculator.stop();
                    pauseSceneBuilder(pauseRoot, gymScene, game.getPlayer());
                    primaryStage.setScene(pauseScene);
                } else {
                    networkPauseSceneBuilder(networkPauseRoot, gymScene, game.getPlayer());
                    primaryStage.setScene(networkPauseScene);
                }
            } else
                move(gymUI, event, player);
        });
    }

    public static void clinicSceneHandler(Group root) {
        double tileSize = clinicUI.tileSize;
        Rectangle player = rectangleBuilder(tileSize * 1.5, tileSize * 2, new ImagePattern(playerDownPics[0]), tileSize * 16.75, tileSize * 1.5);
        root.getChildren().add(player);

        clinicScene.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.E) {
                if (player.getX() + player.getWidth() / 2 <= tileSize * 18 &&
                        player.getX() + player.getWidth() / 2 >= tileSize * 17 &&
                        player.getY() + player.getHeight() <= tileSize * 4 &&
                        player.getY() + player.getHeight() >= tileSize * 3) {
                    choosedRoot = villageRoot;
                    primaryStage.setScene(villageScene);
                }
            } else if ((event.getCode() == KeyCode.C || pressed) && clinicUI.closeToDrugStore(player)) {
                pressed = true;
                if (flags[4]) {
                    timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                        if (flags[0]) {
                            checkClinicUI.backpackSceneBuilder(primaryStage, checkShopRoot, clinicScene);
                            checkShopSceneHandler(clinicScene, checkShopRoot, checkClinicUI);
                            primaryStage.setScene(checkShopScene);
                            flags[0] = false;
                        } else if (flags[1]) {
                            objectStatusSceneBuilder(objectStatusRoot, choosedItem, clinicScene, 2);
                            primaryStage.setScene(objectStatusScene);
                            flags[1] = false;
                        } else if (flags[2]) {
                            pressed = false;
                            flags[2] = false;
                            flags[0] = true;
                            stopTimeline();
                        }
                    }));
                    timeline.setCycleCount(Animation.INDEFINITE);
                }
                if (flags[4]) {
                    timeline.play();
                    flags[4] = false;
                }
            } else if ((event.getCode() == KeyCode.B || pressed) && clinicUI.closeToDrugStore(player)) {
                pressed = true;
                if (flags[4]) {
                    timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                        if (flags[0]) {
                            checkClinicUI.backpackSceneBuilder(primaryStage, checkShopRoot, clinicScene);
                            checkShopSceneHandler(clinicScene, checkShopRoot, checkClinicUI);
                            primaryStage.setScene(checkShopScene);
                            flags[0] = false;
                        } else if (flags[1]) {
                            buySceneBuilder(buyRoot, choosedItem, clinicScene, clinicRoot, game.getPlayer(),
                                    game.getVillage().getClinic());
                            primaryStage.setScene(buyScene);
                            flags[1] = false;
                        } else if (flags[2]) {
                            pressed = false;
                            flags[2] = false;
                            flags[0] = true;
                            stopTimeline();
                        }
                    }));
                    timeline.setCycleCount(Animation.INDEFINITE);
                }
                if (flags[4]) {
                    timeline.play();
                    flags[4] = false;
                }
            } else if ((event.getCode() == KeyCode.H || pressed) && clinicUI.closeToBed(player)) {
                pressed = true;
                if (flags[4]) {
                    timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                        if (flags[0]) {
                            yesNoSceneBuilder(yesNoRoot, clinicScene, "You will Heal up for 500 Gil. " +
                                    "Is this okay?", 1);
                            primaryStage.setScene(yesNoScene);
                            flags[0] = false;
                        } else if (flags[1]) {
                            if (yesNO) {
                                clinicUI.heal(game.getPlayer(), flags);
                            }
                            flags[1] = false;
                        } else if (flags[2]) {
                            pressed = false;
                            flags[2] = false;
                            flags[0] = true;
                            stopTimeline();
                        }
                    }));
                    timeline.setCycleCount(Animation.INDEFINITE);
                }
                if (flags[4]) {
                    timeline.play();
                    flags[4] = false;
                }
            } else if (event.getCode() == KeyCode.SPACE) {
                if (!isMultiplayer) {
                    timeCalculator.stop();
                    pauseSceneBuilder(pauseRoot, clinicScene, game.getPlayer());
                    primaryStage.setScene(pauseScene);
                } else {
                    networkPauseSceneBuilder(networkPauseRoot, clinicScene, game.getPlayer());
                    primaryStage.setScene(networkPauseScene);
                }
            } else
                move(clinicUI, event, player);
        });
    }

    public static void marketSceneHandler(Group root) {
        double tileSize = marketUI.tileSize;
        Rectangle player = rectangleBuilder(tileSize * 1.5, tileSize * 2, new ImagePattern(playerDownPics[0]), tileSize * 18, tileSize * 10);
        root.getChildren().add(player);

        marketScene.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.E) {
                if ((player.getX() + player.getWidth() / 2 <= tileSize * 20.2 &&
                        player.getX() + player.getWidth() / 2 >= tileSize * 18 &&
                        player.getY() + player.getHeight() <= tileSize * 14 &&
                        player.getY() + player.getHeight() >= tileSize * 10) ||
                        (player.getX() + player.getWidth() / 2 <= tileSize * 2 &&
                                player.getX() + player.getWidth() / 2 >= tileSize * 0 &&
                                player.getY() + player.getHeight() <= tileSize * 14 &&
                                player.getY() + player.getHeight() >= tileSize * 10)) {
                    choosedRoot = villageRoot;
                    primaryStage.setScene(villageScene);
                } else if (player.getX() + player.getWidth() / 2 <= tileSize * 5.5 &&
                        player.getX() + player.getWidth() / 2 >= tileSize * 2.5 &&
                        player.getY() + player.getHeight() <= tileSize * 14 &&
                        player.getY() + player.getHeight() >= tileSize * 10) {
                    choosedRoot = butcheryRoot;
                    primaryStage.setScene(butcheryScene);

                } else if (player.getX() + player.getWidth() / 2 <= tileSize * 11.5 &&
                        player.getX() + player.getWidth() / 2 >= tileSize * 8.5 &&
                        player.getY() + player.getHeight() <= tileSize * 14 &&
                        player.getY() + player.getHeight() >= tileSize * 10) {
                    choosedRoot = groceryStoreRoot;
                    primaryStage.setScene(groceryStoreScene);
                } else if (player.getX() + player.getWidth() / 2 <= tileSize * 17.5 &&
                        player.getX() + player.getWidth() / 2 >= tileSize * 14.5 &&
                        player.getY() + player.getHeight() <= tileSize * 14 &&
                        player.getY() + player.getHeight() >= tileSize * 10) {
                    choosedRoot = generalStoreRoot;
                    primaryStage.setScene(generalStoreScene);
                }
            } else if (event.getCode() == KeyCode.SPACE) {
                if (!isMultiplayer) {
                    timeCalculator.stop();
                    pauseSceneBuilder(pauseRoot, marketScene, game.getPlayer());
                    primaryStage.setScene(pauseScene);
                } else {
                    networkPauseSceneBuilder(networkPauseRoot, marketScene, game.getPlayer());
                    primaryStage.setScene(networkPauseScene);
                }
            } else
                move(marketUI, event, player);
        });
    }

    public static void laboratorySceneHandler(Group root) {
        double tileSize = laboratoryUI.tileSize;
        Rectangle player = rectangleBuilder(tileSize * 1.5, tileSize * 2, new ImagePattern(playerDownPics[0]), tileSize * 16.75, tileSize * 1.5);
        root.getChildren().add(player);

        laboratoryScene.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.E) {
                if (player.getX() + player.getWidth() / 2 <= tileSize * 18 &&
                        player.getX() + player.getWidth() / 2 >= tileSize * 17 &&
                        player.getY() + player.getHeight() <= tileSize * 4 &&
                        player.getY() + player.getHeight() >= tileSize * 3) {
                    primaryStage.setScene(villageScene);
                    choosedRoot = villageRoot;
                }
            } else if (event.getCode() == KeyCode.C || pressed) {
                pressed = true;
                if (flags[4]) {
                    timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                        if (flags[0]) {
                            checkLaboratoryUI.backpackSceneBuilder(primaryStage, checkShopRoot, laboratoryScene);
                            checkShopSceneHandler(laboratoryScene, checkShopRoot, checkLaboratoryUI);
                            primaryStage.setScene(checkShopScene);
                            flags[0] = false;
                        } else if (flags[1]) {
                            objectStatusSceneBuilder(objectStatusRoot, choosedItem, laboratoryScene, 2);
                            primaryStage.setScene(objectStatusScene);
                            flags[1] = false;
                        } else if (flags[2]) {
                            pressed = false;
                            flags[2] = false;
                            flags[0] = true;
                            stopTimeline();
                        }
                    }));
                    timeline.setCycleCount(Animation.INDEFINITE);
                }
                if (flags[4]) {
                    timeline.play();
                    flags[4] = false;
                }
            } else if ((event.getCode() == KeyCode.M || pressed)) {
                pressed = true;
                if (flags[4]) {
                    timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                        if (flags[0]) {
                            checkLaboratoryUI.backpackSceneBuilder(primaryStage, checkShopRoot, laboratoryScene);
                            checkShopSceneHandler(laboratoryScene, checkShopRoot, checkLaboratoryUI);
                            primaryStage.setScene(checkShopScene);
                            flags[0] = false;
                        } else if (flags[1]) {
                            makeSceneBuilder(makeRoot, laboratoryScene, game.getPlayer().getBackpack().getItem(),
                                    choosedItem.getDissassemblity().getInitialIngredients(), "Backpack",
                                    choosedItem.getName(), root, "Build", 2);
                            primaryStage.setScene(makeScene);
                            flags[1] = false;
                        } else if (flags[2]) {
                            laboratoryUI.make(choosedItem, game.getPlayer(), game.getFarm().getBarn(), flags, barnUI);
                            flags[2] = false;
                        } else if (flags[3]) {
                            pressed = false;
                            flags[3] = false;
                            flags[0] = true;
                            stopTimeline();
                        }
                    }));
                    timeline.setCycleCount(Animation.INDEFINITE);
                }
                if (flags[4]) {
                    timeline.play();
                    flags[4] = false;
                }
            } else if (event.getCode() == KeyCode.SPACE) {
                if (!isMultiplayer) {
                    timeCalculator.stop();
                    pauseSceneBuilder(pauseRoot, laboratoryScene, game.getPlayer());
                    primaryStage.setScene(pauseScene);
                } else {
                    networkPauseSceneBuilder(networkPauseRoot, laboratoryScene, game.getPlayer());
                    primaryStage.setScene(networkPauseScene);
                }
            } else
                move(laboratoryUI, event, player);
        });
    }

    public static void greenhouseSceneHandler(Group root) {
        double tileSize = greenhouseUI.tileSize;
        Rectangle player = rectangleBuilder(tileSize * 1.5, tileSize * 2, new ImagePattern(playerDownPics[0]), tileSize * 15.75, tileSize * 2);
        root.getChildren().add(player);
        final Cell[][][] cells = {new Cell[3][3]};
        final int[] cnt1 = {0};
        final int[] cnt2 = {0};

        greenhouseScene.setOnKeyPressed(event -> {
            if (pressed)
                cnt1[0]++;
            keyEvent = event;
            int fieldNum = greenhouseUI.chosenFieldNum(player);
            if ((event.getCode() == KeyCode.P || pressed) && fieldNum != -1) {
                pressed = true;
                if (flags[4]) {
                    timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                        if (flags[0]) {
                            backpackUI.backpackSceneBuilder(primaryStage, backpackRoot, greenhouseScene);
                            primaryStage.setScene(backpackScene);
                            backpackSceneHandler("Shovel", greenhouseScene, root, 1);
                            flags[0] = false;
                        } else if (flags[1]) {
                            if (cnt2[0] == cnt1[0]) {
                                cells[0] = greenhouseUI.chooseCell((Shovel) choosedItem, keyEvent, player, flags, fieldNum);
                                cnt2[0]++;
                            }
                        } else if (flags[2]) {
                            greenhouseUI.plow((Shovel) choosedItem, cells[0], game.getPlayer(), flags, fieldNum);
                            flags[2] = false;
                        } else if (flags[3]) {
                            pressed = false;
                            flags[3] = false;
                            flags[0] = true;
                            stopTimeline();
                        }
                    }));
                    timeline.setCycleCount(Animation.INDEFINITE);
                }
                if (flags[4]) {
                    timeline.play();
                    flags[4] = false;
                }
            } else if ((event.getCode() == KeyCode.W || pressed) && fieldNum != -1) {
                pressed = true;
                if (flags[4]) {
                    timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                        if (flags[0]) {
                            backpackUI.backpackSceneBuilder(primaryStage, backpackRoot, greenhouseScene);
                            primaryStage.setScene(backpackScene);
                            backpackSceneHandler("Watering Can", greenhouseScene, root, 1);
                            flags[0] = false;
                        } else if (flags[1]) {
                            if (cnt2[0] == cnt1[0]) {
                                cells[0] = greenhouseUI.chooseCell((WateringCan) choosedItem, keyEvent, player, flags, fieldNum);
                                cnt2[0]++;
                            }
                        } else if (flags[2]) {
                            greenhouseUI.watering((WateringCan) choosedItem, cells[0], game.getPlayer(), flags, fieldNum);
                            flags[2] = false;
                        } else if (flags[3]) {
                            pressed = false;
                            flags[3] = false;
                            flags[0] = true;
                            stopTimeline();
                        }
                    }));
                    timeline.setCycleCount(Animation.INDEFINITE);
                }
                if (flags[4]) {
                    timeline.play();
                    flags[4] = false;
                }
            } else if ((event.getCode() == KeyCode.D || pressed) && fieldNum != -1) {
                pressed = true;
                if (flags[4]) {
                    timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                        if (flags[0]) {
                            flags[0] = false;
                            flags[1] = true;
                        } else if (flags[1]) {
                            if (cnt2[0] == cnt1[0]) {
                                cells[0] = greenhouseUI.chooseCell(new AllOfTools().hand(), keyEvent, player, flags, fieldNum);
                                cnt2[0]++;
                            }
                        } else if (flags[2]) {
                            greenhouseUI.destroyCrops(cells[0], game.getPlayer(), flags, fieldNum);
                            flags[2] = false;
                        } else if (flags[3]) {
                            pressed = false;
                            flags[3] = false;
                            flags[0] = true;
                            stopTimeline();
                        }
                    }));
                    timeline.setCycleCount(Animation.INDEFINITE);
                }
                if (flags[4]) {
                    timeline.play();
                    flags[4] = false;
                }
            } else if ((event.getCode() == KeyCode.H || pressed) && fieldNum != -1) {
                pressed = true;
                if (flags[4]) {
                    timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                        if (flags[0]) {
                            flags[0] = false;
                            flags[1] = true;
                        } else if (flags[1]) {
                            if (cnt2[0] == cnt1[0]) {
                                cells[0] = greenhouseUI.chooseCell(new AllOfTools().hand(), keyEvent, player, flags, fieldNum);
                                cnt2[0]++;
                            }
                        } else if (flags[2]) {
                            greenhouseUI.harvestCrops(cells[0], game.getPlayer(), flags, fieldNum);
                            flags[2] = false;
                        } else if (flags[3]) {
                            pressed = false;
                            flags[3] = false;
                            flags[0] = true;
                            stopTimeline();
                        }
                    }));
                    timeline.setCycleCount(Animation.INDEFINITE);
                }
                if (flags[4]) {
                    timeline.play();
                    flags[4] = false;
                }
            } else if ((event.getCode() == KeyCode.S || pressed) && fieldNum != -1) {
                pressed = true;
                if (flags[4]) {
                    timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                        if (flags[0]) {
                            backpackUI.backpackSceneBuilder(primaryStage, backpackRoot, farmScene);
                            primaryStage.setScene(backpackScene);
                            backpackSceneHandler("Seed", greenhouseScene, root, 1);
                            flags[0] = false;
                        } else if (flags[1]) {
                            if (cnt2[0] == cnt1[0]) {
                                cells[0] = greenhouseUI.chooseCell(choosedItem, keyEvent, player, flags, fieldNum);
                                cnt2[0]++;
                            }
                        } else if (flags[2]) {
                            greenhouseUI.plantSeeds((Seed) choosedItem, cells[0], game.getPlayer(), flags, game.getFarm().getGreenhouse().getWeather(), fieldNum);
                            flags[2] = false;
                        } else if (flags[3]) {
                            pressed = false;
                            flags[3] = false;
                            flags[0] = true;
                            stopTimeline();
                        }
                    }));
                    timeline.setCycleCount(Animation.INDEFINITE);
                }
                if (flags[4]) {
                    timeline.play();
                    flags[4] = false;
                }
            } else if ((event.getCode() == KeyCode.M || pressed) && greenhouseUI.closeToMachine(player)) {
                pressed = true;
                if (flags[4]) {
                    timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                        if (flags[0]) {
                            multiButtonSceneBuilder(multiButtonRoot, option.inspectWeatherMachine(),
                                    "Weather", greenhouseScene, 1);
                            primaryStage.setScene(multiButtonScene);
                            flags[0] = false;
                        } else if (flags[1]) {
                            greenhouseUI.setWeather(choosedButton, flags);
                            flags[1] = false;
                        } else if (flags[2]) {
                            pressed = false;
                            flags[2] = false;
                            flags[0] = true;
                            stopTimeline();
                        }
                    }));
                    timeline.setCycleCount(Animation.INDEFINITE);
                }
                if (flags[4]) {
                    timeline.play();
                    flags[4] = false;
                }
            } else if ((event.getCode() == KeyCode.E || pressed) && greenhouseUI.closeToFields(player)) {
                pressed = true;
                if (flags[4]) {
                    timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                        if (flags[0]) {
                            makeSceneBuilder(makeRoot, greenhouseScene, game.getPlayer().getBackpack().getItem(),
                                    game.getFarm().getGreenhouse().getRepairObjects(), "Backpack",
                                    "Extend Fields", root, "Extend", 1);
                            primaryStage.setScene(makeScene);
                            flags[0] = false;
                        } else if (flags[1]) {
                            yesNoSceneBuilder(yesNoRoot, greenhouseScene, "Do you want to extend Greenhouse for " +
                                    game.getFarm().getGreenhouse().getRepairPrice() + " Gils?", 2);
                            primaryStage.setScene(yesNoScene);
                            flags[1] = false;
                        } else if (flags[2]) {
                            if (yesNO) {
                                greenhouseUI.extend(game.getPlayer(), flags);
                                root.getChildren().remove(player);
                                root.getChildren().addAll(player);
                            }
                            flags[2] = false;
                        } else if (flags[3]) {
                            pressed = false;
                            flags[3] = false;
                            flags[0] = true;
                            stopTimeline();
                        }
                    }));
                    timeline.setCycleCount(Animation.INDEFINITE);
                }
                if (flags[4]) {
                    timeline.play();
                    flags[4] = false;
                }
            } else if (event.getCode() == KeyCode.E) {
                if (player.getX() + player.getWidth() / 2 <= tileSize * 17 &&
                        player.getX() + player.getWidth() / 2 >= tileSize * 16 &&
                        player.getY() + player.getHeight() <= tileSize * 4 &&
                        player.getY() + player.getHeight() >= tileSize * 3) {
                    choosedRoot = farmRoot;
                    primaryStage.setScene(farmScene);
                }
            } else if (event.getCode() == KeyCode.SPACE) {
                if (!isMultiplayer) {
                    timeCalculator.stop();
                    pauseSceneBuilder(pauseRoot, greenhouseScene, game.getPlayer());
                    primaryStage.setScene(pauseScene);
                } else {
                    networkPauseSceneBuilder(networkPauseRoot, greenhouseScene, game.getPlayer());
                    primaryStage.setScene(networkPauseScene);
                }
            }
            if (!pressed)
                move(greenhouseUI, event, player);
        });
    }

    public static void groceryStoreSceneHandler(Group root) {
        double tileSize = groceryStoreUI.tileSize;
        Rectangle player = rectangleBuilder(tileSize * 1.5, tileSize * 2, new ImagePattern(playerDownPics[0]), tileSize * 16.25, tileSize * 1.5);
        root.getChildren().add(player);

        groceryStoreScene.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.E) {
                if (player.getX() + player.getWidth() / 2 <= tileSize * 18 &&
                        player.getX() + player.getWidth() / 2 >= tileSize * 16 &&
                        player.getY() + player.getHeight() <= tileSize * 4 &&
                        player.getY() + player.getHeight() >= tileSize * 3) {
                    choosedRoot = marketRoot;
                    primaryStage.setScene(marketScene);
                }
            } else if (event.getCode() == KeyCode.B || pressed) {
                String choosen = "";
                if (groceryStoreUI.rightPlace(player) != 0) {
                    pressed = true;
                    if (event.getCode() == KeyCode.ENTER)
                        pressed = false;
                    choosen = groceryStoreUI.buy(event, groceryStoreUI.rightPlace(player));
                }
                if (!pressed) {
                    try {
                        Class fruitsClass = AllOfFruits.class;
                        Object obj = fruitsClass.newInstance();
                        Method[] fruits = fruitsClass.getDeclaredMethods();
                        for (Method fruit : fruits) {
                            Fruit thisFruit = (Fruit) fruit.invoke(obj);
                            if (choosen.equals(thisFruit.getName())) {
                                buySceneBuilder(buyRoot, thisFruit, groceryStoreScene, groceryStoreRoot, game.getPlayer(),
                                        game.getVillage().getMarket().getShop().get(0));
                                primaryStage.setScene(buyScene);
                                break;
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else if (event.getCode() == KeyCode.S || pressed) {
                pressed = true;
                if (flags[4]) {
                    timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                        if (flags[0]) {
                            backpackUI.backpackSceneBuilder(primaryStage, backpackRoot, groceryStoreScene);
                            primaryStage.setScene(backpackScene);
                            backpackSceneHandler(groceryStoreScene, root, "Fruit");
                            flags[0] = false;
                        } else if (flags[1]) {
                            sellSceneBuilder(sellRoot, choosedItem, groceryStoreScene, groceryStoreRoot, game.getPlayer(),
                                    game.getVillage().getMarket().getShop().get(0));
                            primaryStage.setScene(sellScene);
                            flags[1] = false;
                        } else if (flags[2]) {
                            pressed = false;
                            flags[2] = false;
                            flags[0] = true;
                            stopTimeline();
                        }
                    }));
                    timeline.setCycleCount(Animation.INDEFINITE);
                }
                if (flags[4]) {
                    timeline.play();
                    flags[4] = false;
                }
            } else if (event.getCode() == KeyCode.C || pressed) {
                pressed = true;
                if (flags[4]) {
                    timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                        if (flags[0]) {
                            checkShopSceneHandler(groceryStoreScene, checkShopRoot, checkGroceryUI);
                            checkGroceryUI.backpackSceneBuilder(primaryStage, checkShopRoot, groceryStoreScene);
                            primaryStage.setScene(checkShopScene);
                            flags[0] = false;
                        } else if (flags[1]) {
                            objectStatusSceneBuilder(objectStatusRoot, choosedItem, groceryStoreScene, 2);
                            primaryStage.setScene(objectStatusScene);
                            flags[1] = false;
                        } else if (flags[2]) {
                            pressed = false;
                            flags[2] = false;
                            flags[0] = true;
                            stopTimeline();
                        }
                    }));
                    timeline.setCycleCount(Animation.INDEFINITE);
                }
                if (flags[4]) {
                    timeline.play();
                    flags[4] = false;
                }
            } else if (event.getCode() == KeyCode.SPACE) {
                if (!isMultiplayer) {
                    timeCalculator.stop();
                    pauseSceneBuilder(pauseRoot, groceryStoreScene, game.getPlayer());
                    primaryStage.setScene(pauseScene);
                } else {
                    networkPauseSceneBuilder(networkPauseRoot, groceryStoreScene, game.getPlayer());
                    primaryStage.setScene(networkPauseScene);
                }
            } else
                move(groceryStoreUI, event, player);
        });
    }

    public static void workshopSceneHandler(Group root) {
        double tileSize = workshopUI.tileSize;
        Rectangle player = rectangleBuilder(tileSize * 1.5, tileSize * 2, new ImagePattern(playerDownPics[0]), tileSize * 16.75, tileSize * 1.5);
        root.getChildren().add(player);

        workshopScene.setOnKeyPressed(event -> {
            int chosenTask = workshopUI.rightPlace(player);
            if (event.getCode() == KeyCode.E) {
                if (player.getX() + player.getWidth() / 2 <= tileSize * 18 &&
                        player.getX() + player.getWidth() / 2 >= tileSize * 17 &&
                        player.getY() + player.getHeight() <= tileSize * 4 &&
                        player.getY() + player.getHeight() >= tileSize * 3) {
                    choosedRoot = villageRoot;
                    primaryStage.setScene(villageScene);
                }
            } else if ((event.getCode() == KeyCode.C || pressed) && chosenTask == 1) {
                pressed = true;
                if (flags[4]) {
                    timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                        if (flags[0]) {
                            checkWorkshopUI.backpackSceneBuilder(primaryStage, checkShopRoot, workshopScene);
                            checkShopSceneHandler(workshopScene, checkShopRoot, checkWorkshopUI);
                            primaryStage.setScene(checkShopScene);
                            flags[0] = false;
                        } else if (flags[1]) {
                            objectStatusSceneBuilder(objectStatusRoot, choosedItem, workshopScene, 2);
                            primaryStage.setScene(objectStatusScene);
                            flags[1] = false;
                        } else if (flags[2]) {
                            pressed = false;
                            flags[2] = false;
                            flags[0] = true;
                            stopTimeline();
                        }
                    }));
                    timeline.setCycleCount(Animation.INDEFINITE);
                }
                if (flags[4]) {
                    timeline.play();
                    flags[4] = false;
                }
            } else if ((event.getCode() == KeyCode.M || pressed) && chosenTask == 2) {
                pressed = true;
                if (flags[4]) {
                    timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                        if (flags[0]) {
                            checkWorkshopUI.backpackSceneBuilder(primaryStage, checkShopRoot, workshopScene);
                            checkShopSceneHandler(workshopScene, checkShopRoot, checkWorkshopUI);
                            primaryStage.setScene(checkShopScene);
                            flags[0] = false;
                        } else if (flags[1]) {
                            makeSceneBuilder(makeRoot, workshopScene, game.getPlayer().getBackpack().getItem(),
                                    choosedItem.getDissassemblity().getInitialIngredients(), "Backpack",
                                    choosedItem.getName(), root, "Make", 2);
                            primaryStage.setScene(makeScene);
                            flags[1] = false;
                        } else if (flags[2]) {
                            workshopUI.make(choosedItem, game.getPlayer(), game.getFarm().getBarn(), flags, barnUI);
                            flags[2] = false;
                        } else if (flags[3]) {
                            pressed = false;
                            flags[3] = false;
                            flags[0] = true;
                            stopTimeline();
                        }
                    }));
                    timeline.setCycleCount(Animation.INDEFINITE);
                }
                if (flags[4]) {
                    timeline.play();
                    flags[4] = false;
                }
            } else if ((event.getCode() == KeyCode.R || pressed) && chosenTask == 4) {
                pressed = true;
                if (flags[4]) {
                    timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                        if (flags[0]) {
                            backpackUI.backpackSceneBuilder(primaryStage, backpackRoot, workshopScene);
                            primaryStage.setScene(backpackScene);
                            backpackSceneHandler(workshopScene, root, false);
                            flags[0] = false;
                        } else if (flags[1]) {
                            if (choosedItem instanceof Tool && !choosedItem.isValid()) {
                                makeSceneBuilder(makeRoot, workshopScene, game.getPlayer().getBackpack().getItem(),
                                        ((Tool) choosedItem).getRepairItems(), "Backpack", choosedItem.getName(), root, "Repair", 2);
                                primaryStage.setScene(makeScene);
                            }
                            flags[1] = false;
                        } else if (flags[2]) {
                            workshopUI.repair(choosedItem, game.getPlayer(), flags);
                            flags[2] = false;
                        } else if (flags[3]) {
                            pressed = false;
                            flags[3] = false;
                            flags[0] = true;
                            stopTimeline();
                        }
                    }));
                    timeline.setCycleCount(Animation.INDEFINITE);
                }
                if (flags[4]) {
                    timeline.play();
                    flags[4] = false;
                }
            } else if ((event.getCode() == KeyCode.D || pressed) && chosenTask == 3) {
                pressed = true;
                if (flags[4]) {
                    timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                        if (flags[0]) {
                            backpackUI.backpackSceneBuilder(primaryStage, backpackRoot, workshopScene);
                            primaryStage.setScene(backpackScene);
                            backpackSceneHandler(workshopScene, root, 1);
                            flags[0] = false;
                        } else if (flags[1]) {
                            yesNoSceneBuilder(yesNoRoot, workshopScene, "Do you wnat to dissassemble " + choosedItem.getName() +
                                    " for " + choosedItem.getDissassemblity().getPrice() + " Gils?", 2);
                            primaryStage.setScene(yesNoScene);
                            flags[1] = false;
                        } else if (flags[2]) {
                            workshopUI.dissassemble(choosedItem, game.getPlayer(), flags);
                            flags[2] = false;
                        } else if (flags[3]) {
                            pressed = false;
                            flags[3] = false;
                            flags[0] = true;
                            stopTimeline();
                        }
                    }));
                    timeline.setCycleCount(Animation.INDEFINITE);
                }
                if (flags[4]) {
                    timeline.play();
                    flags[4] = false;
                }
            } else if (event.getCode() == KeyCode.SPACE) {
                if (!isMultiplayer) {
                    timeCalculator.stop();
                    pauseSceneBuilder(pauseRoot, workshopScene, game.getPlayer());
                    primaryStage.setScene(pauseScene);
                } else {
                    networkPauseSceneBuilder(networkPauseRoot, workshopScene, game.getPlayer());
                    primaryStage.setScene(networkPauseScene);
                }
            } else
                move(workshopUI, event, player);
        });
    }

    public static void barnSceneHandler(Group root) {
        double tileSize = barnUI.tileSize;
        Rectangle player = rectangleBuilder(tileSize * 1.5, tileSize * 2, new ImagePattern(playerLeftPics[0]), tileSize * 18, tileSize * 11.5);
        root.getChildren().add(player);

        barnScene.setOnKeyPressed(event -> {
            int animalNum = barnUI.chosenAnimal(player);
            if (event.getCode() == KeyCode.E) {
                if (player.getX() + player.getWidth() / 2 <= tileSize * 20.2 &&
                        player.getX() + player.getWidth() / 2 >= tileSize * 17 &&
                        player.getY() + player.getHeight() <= tileSize * 14 &&
                        player.getY() + player.getHeight() >= tileSize * 10) {
                    choosedRoot = farmRoot;
                    primaryStage.setScene(farmScene);
                }
            } else if ((event.getCode() == KeyCode.F || pressed) && animalNum != -1) {
                pressed = true;
                if (flags[4]) {
                    timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                        if (flags[0]) {
                            backpackUI.backpackSceneBuilder(primaryStage, backpackRoot, barnScene);
                            primaryStage.setScene(backpackScene);
                            if (animalNum < 12)
                                backpackSceneHandler("Alfalfa", barnScene, root, 1);
                            else
                                backpackSceneHandler("Seed", barnScene, root, 1);
                            flags[0] = false;
                        } else if (flags[1]) {
                            barnUI.feed(animalNum, choosedItem, game.getPlayer(), flags);
                            flags[1] = false;
                        } else if (flags[2]) {
                            pressed = false;
                            flags[0] = true;
                            flags[2] = false;
                            stopTimeline();
                        }
                    }));
                    timeline.setCycleCount(Animation.INDEFINITE);
                }
                if (flags[4]) {
                    timeline.play();
                    flags[4] = false;
                }
            } else if ((event.getCode() == KeyCode.H || pressed) && animalNum != -1) {
                pressed = true;
                if (flags[4]) {
                    timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                        if (flags[0]) {
                            backpackUI.backpackSceneBuilder(primaryStage, backpackRoot, barnScene);
                            primaryStage.setScene(backpackScene);
                            backpackSceneHandler(barnScene, root, "Animal Medicine");
                            flags[0] = false;
                        } else if (flags[1]) {
                            barnUI.heal(animalNum, choosedItem, game.getPlayer(), flags);
                            flags[1] = false;
                        } else if (flags[2]) {
                            pressed = false;
                            flags[0] = true;
                            flags[2] = false;
                            stopTimeline();
                        }
                    }));
                    timeline.setCycleCount(Animation.INDEFINITE);
                }
                if (flags[4]) {
                    timeline.play();
                    flags[4] = false;
                }
            } else if ((event.getCode() == KeyCode.S || pressed) && animalNum != -1) {
                pressed = true;
                Animal animal[] = new Animal[1];
                if (flags[4]) {
                    timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                        if (flags[0]) {
                            animal[0] = barnUI.chosedAnimal(animalNum, flags);
                            flags[0] = false;
                        } else if (flags[1]) {
                            animalStatusSceneBuilder(objectStatusRoot, animal[0], barnScene, false);
                            primaryStage.setScene(objectStatusScene);
                            flags[1] = false;
                        } else if (flags[2]) {
                            pressed = false;
                            flags[0] = true;
                            flags[2] = false;
                            stopTimeline();
                        }
                    }));
                    timeline.setCycleCount(Animation.INDEFINITE);
                }
                if (flags[4]) {
                    timeline.play();
                    flags[4] = false;
                }
            } else if ((event.getCode() == KeyCode.M || pressed)) {
                pressed = true;
                if (flags[4]) {
                    timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                        if (flags[0]) {
                            tableSceneBuilder(tableRoot, barnScene, game.getFarm().getBarn().getMachines(), "Machines", 1);
                            primaryStage.setScene(tableScene);
                            flags[0] = false;
                        } else if (flags[1]) {
                            multiButtonSceneBuilder(multiButtonRoot, option.inspectMachine(),
                                    choosedItem.getName(), barnScene, 2);
                            primaryStage.setScene(multiButtonScene);
                            flags[1] = false;
                        } else if (flags[2]) {
                            if (choosedButton.contains("Status")) {
                                objectStatusSceneBuilder(objectStatusRoot, choosedItem, barnScene, 3);
                                primaryStage.setScene(objectStatusScene);
                            } else if (choosedButton.contains("Use")) {
                                if (choosedItem.getName().contains("Tomato")) {
                                    backpackUI.backpackSceneBuilder(primaryStage, backpackRoot, barnScene);
                                    primaryStage.setScene(backpackScene);
                                    backpackSceneHandler("Tomato", barnScene, root, 3);
                                } else if (choosedItem.getName().contains("Cheese")) {
                                    backpackUI.backpackSceneBuilder(primaryStage, backpackRoot, barnScene);
                                    primaryStage.setScene(backpackScene);
                                    backpackSceneHandler("Milk", barnScene, root, 3);
                                } else if (choosedItem.getName().contains("Wheel")) {
                                    backpackUI.backpackSceneBuilder(primaryStage, backpackRoot, barnScene);
                                    primaryStage.setScene(backpackScene);
                                    backpackSceneHandler("Wool", barnScene, root, 3);
                                }
                            }
                            flags[2] = false;
                        } else if (flags[3]) {
                            if (choosedButton.contains("Use")) {
                                barnUI.useMachine(choosedItem, flags, game.getPlayer());
                            }
                            pressed = false;
                            flags[0] = true;
                            flags[3] = false;
                            stopTimeline();
                        }
                    }));
                    timeline.setCycleCount(Animation.INDEFINITE);
                }
                if (flags[4]) {
                    timeline.play();
                    flags[4] = false;
                }
            } else if ((event.getCode() == KeyCode.G || pressed) && animalNum != -1) {
                pressed = true;
                Animal animal[] = new Animal[1];
                if (flags[4]) {
                    timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                        if (flags[0]) {
                            animal[0] = barnUI.chosedAnimal(animalNum, flags);
                            flags[0] = false;
                        } else if (flags[1]) {
                            backpackUI.backpackSceneBuilder(primaryStage, backpackRoot, barnScene);
                            if (animal[0].getType().equals("Cow")) {
                                backpackSceneHandler("Milker", barnScene, root, 2);
                                primaryStage.setScene(backpackScene);
                            } else if (animal[0].getType().equals("Sheep")) {
                                backpackSceneHandler("Scissors", barnScene, root, 2);
                                primaryStage.setScene(backpackScene);
                            } else if (animal[0].getType().equals("Chicken")) {
                                flags[2] = true;
                            }
                            flags[1] = false;
                        } else if (flags[2]) {
                            barnUI.getProduct(animalNum, choosedItem, game.getPlayer(), flags);
                            flags[2] = false;
                        } else if (flags[3]) {
                            pressed = false;
                            flags[0] = true;
                            flags[3] = false;
                            stopTimeline();
                        }
                    }));
                    timeline.setCycleCount(Animation.INDEFINITE);
                }
                if (flags[4]) {
                    timeline.play();
                    flags[4] = false;
                }
            } else if (event.getCode() == KeyCode.SPACE) {
                if (!isMultiplayer) {
                    timeCalculator.stop();
                    pauseSceneBuilder(pauseRoot, barnScene, game.getPlayer());
                    primaryStage.setScene(pauseScene);
                } else {
                    networkPauseSceneBuilder(networkPauseRoot, barnScene, game.getPlayer());
                    primaryStage.setScene(networkPauseScene);
                }
            } else
                move(barnUI, event, player);
        });
    }

    public static void ranchSceneHandler(Group root) {
        double tileSize = ranchUI.tileSize;
        Rectangle player = rectangleBuilder(tileSize * 1.5, tileSize * 2, new ImagePattern(playerDownPics[0]), tileSize * 16.75, tileSize * 1.5);
        root.getChildren().add(player);
        ranchScene.setOnKeyPressed(event -> {
            int animalNum = ranchUI.chosenAnimal(player);
            if (event.getCode() == KeyCode.E) {
                if (player.getX() + player.getWidth() / 2 <= tileSize * 18 &&
                        player.getX() + player.getWidth() / 2 >= tileSize * 17 &&
                        player.getY() + player.getHeight() <= tileSize * 4 &&
                        player.getY() + player.getHeight() >= tileSize * 3) {
                    choosedRoot = villageRoot;
                    primaryStage.setScene(villageScene);
                }
            } else if (event.getCode() == KeyCode.C || pressed) {
                pressed = true;
                if (flags[4]) {
                    timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                        if (flags[0]) {
                            checkRanchUI.backpackSceneBuilder(primaryStage, checkShopRoot, ranchScene);
                            checkShopSceneHandler(ranchScene, checkShopRoot, checkRanchUI);
                            primaryStage.setScene(checkShopScene);
                            flags[0] = false;
                        } else if (flags[1]) {
                            objectStatusSceneBuilder(objectStatusRoot, choosedItem, ranchScene, 2);
                            primaryStage.setScene(objectStatusScene);
                            flags[1] = false;
                        } else if (flags[2]) {
                            pressed = false;
                            flags[2] = false;
                            flags[0] = true;
                            stopTimeline();
                        }
                    }));
                    timeline.setCycleCount(Animation.INDEFINITE);
                }
                if (flags[4]) {
                    timeline.play();
                    flags[4] = false;
                }
            } else if ((event.getCode() == KeyCode.B || pressed) && ranchUI.rightPlaceToBuyItem(player)) {
                pressed = true;
                if (flags[4]) {
                    timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                        if (flags[0]) {
                            checkRanchUI.backpackSceneBuilder(primaryStage, checkShopRoot, ranchScene);
                            checkShopSceneHandler(ranchScene, checkShopRoot, checkRanchUI);
                            primaryStage.setScene(checkShopScene);
                            flags[0] = false;
                        } else if (flags[1]) {
                            buySceneBuilder(buyRoot, choosedItem, ranchScene, ranchRoot, game.getPlayer(),
                                    game.getVillage().getRanch());
                            primaryStage.setScene(buyScene);
                            flags[1] = false;
                        } else if (flags[2]) {
                            pressed = false;
                            flags[2] = false;
                            flags[0] = true;
                            stopTimeline();
                        }
                    }));
                    timeline.setCycleCount(Animation.INDEFINITE);
                }
                if (flags[4]) {
                    timeline.play();
                    flags[4] = false;
                }
            } else if ((event.getCode() == KeyCode.B || pressed) && animalNum != -1) {
                pressed = true;
                Animal animal[] = new Animal[1];
                if (flags[4]) {
                    timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                        if (flags[0]) {
                            animal[0] = RanchUI.chosedAnimal(animalNum, flags);
                            flags[0] = false;
                        } else if (flags[1]) {
                            yesNoSceneBuilder(yesNoRoot, ranchScene, "Do you wnat to buy " + animal[0].getType() +
                                    " for " + animal[0].getPrice() + " Gils?", 2);
                            primaryStage.setScene(yesNoScene);
                            flags[1] = false;
                        } else if (flags[2]) {
                            ranchUI.buy(animal[0], game.getPlayer(), flags, game.getFarm().getBarn(), animalNum, barnUI);
                            flags[2] = false;
                        } else if (flags[3]) {
                            pressed = false;
                            flags[3] = false;
                            flags[0] = true;
                            stopTimeline();
                        }
                    }));
                    timeline.setCycleCount(Animation.INDEFINITE);
                }
                if (flags[4]) {
                    timeline.play();
                    flags[4] = false;
                }
            } else if (event.getCode() == KeyCode.SPACE) {
                if (!isMultiplayer) {
                    timeCalculator.stop();
                    pauseSceneBuilder(pauseRoot, ranchScene, game.getPlayer());
                    primaryStage.setScene(pauseScene);
                } else {
                    networkPauseSceneBuilder(networkPauseRoot, ranchScene, game.getPlayer());
                    primaryStage.setScene(networkPauseScene);
                }
            } else
                move(ranchUI, event, player);
        });
    }

    public static void butcherySceneHandler(Group root) {
        double tileSize = butcheryUI.tileSize;
        Rectangle player = rectangleBuilder(tileSize * 1.5, tileSize * 2, new ImagePattern(playerDownPics[0]), tileSize * 15.25, tileSize * 2);
        root.getChildren().add(player);

        butcheryScene.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.E) {
                if (player.getX() + player.getWidth() / 2 <= tileSize * 16 &&
                        player.getX() + player.getWidth() / 2 >= tileSize * 14 &&
                        player.getY() + player.getHeight() <= tileSize * 5 &&
                        player.getY() + player.getHeight() >= tileSize * 4) {
                    choosedRoot = marketRoot;
                    primaryStage.setScene(marketScene);
                }
            } else if (event.getCode() == KeyCode.C || pressed) {
                pressed = true;
                if (flags[4]) {
                    timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                        if (flags[0]) {
                            checkShopSceneHandler(butcheryScene, checkShopRoot, checkButcheryUI);
                            checkButcheryUI.backpackSceneBuilder(primaryStage, checkShopRoot, butcheryScene);
                            primaryStage.setScene(checkShopScene);
                            flags[0] = false;
                        } else if (flags[1]) {
                            objectStatusSceneBuilder(objectStatusRoot, choosedItem, butcheryScene, 2);
                            primaryStage.setScene(objectStatusScene);
                            flags[1] = false;
                        } else if (flags[2]) {
                            pressed = false;
                            flags[2] = false;
                            flags[0] = true;
                            stopTimeline();
                        }
                    }));
                    timeline.setCycleCount(Animation.INDEFINITE);
                }
                if (flags[4]) {
                    timeline.play();
                    flags[4] = false;
                }
            } else if (event.getCode() == KeyCode.B || pressed) {
                pressed = true;
                if (flags[4]) {
                    timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                        if (flags[0]) {
                            checkShopSceneHandler(butcheryScene, checkShopRoot, checkButcheryUI);
                            checkButcheryUI.backpackSceneBuilder(primaryStage, checkShopRoot, butcheryScene);
                            primaryStage.setScene(checkShopScene);
                            flags[0] = false;
                        } else if (flags[1]) {
                            buySceneBuilder(buyRoot, choosedItem, butcheryScene, butcheryRoot, game.getPlayer(),
                                    game.getVillage().getMarket().getShop().get(1));
                            primaryStage.setScene(buyScene);
                            flags[1] = false;
                        } else if (flags[2]) {
                            pressed = false;
                            flags[2] = false;
                            flags[0] = true;
                            stopTimeline();
                        }
                    }));
                    timeline.setCycleCount(Animation.INDEFINITE);
                }
                if (flags[4]) {
                    timeline.play();
                    flags[4] = false;
                }
            } else if (event.getCode() == KeyCode.S || pressed) {
                pressed = true;
                if (flags[4]) {
                    timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                        if (flags[0]) {
                            backpackUI.backpackSceneBuilder(primaryStage, backpackRoot, butcheryScene);
                            primaryStage.setScene(backpackScene);
                            backpackSceneHandler(butcheryScene, root, "Product");
                            flags[0] = false;
                        } else if (flags[1]) {
                            sellSceneBuilder(sellRoot, choosedItem, butcheryScene, butcheryRoot, game.getPlayer(),
                                    game.getVillage().getMarket().getShop().get(1));
                            primaryStage.setScene(sellScene);
                            flags[1] = false;
                        } else if (flags[2]) {
                            pressed = false;
                            flags[2] = false;
                            flags[0] = true;
                            stopTimeline();
                        }
                    }));
                    timeline.setCycleCount(Animation.INDEFINITE);
                }
                if (flags[4]) {
                    timeline.play();
                    flags[4] = false;
                }
            } else if (event.getCode() == KeyCode.SPACE) {
                if (!isMultiplayer) {
                    timeCalculator.stop();
                    pauseSceneBuilder(pauseRoot, butcheryScene, game.getPlayer());
                    primaryStage.setScene(pauseScene);
                } else {
                    networkPauseSceneBuilder(networkPauseRoot, butcheryScene, game.getPlayer());
                    primaryStage.setScene(networkPauseScene);
                }
            } else
                move(butcheryUI, event, player);
        });
    }


    public static void generalStoreSceneHandler(Group root) {
        double tileSize = generalStoreUI.tileSize;
        Rectangle player = rectangleBuilder(tileSize * 1.5, tileSize * 2, new ImagePattern(playerDownPics[0]), tileSize * 17, tileSize * 2);
        root.getChildren().add(player);

        generalStoreScene.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.E) {
                if (player.getX() + player.getWidth() / 2 <= tileSize * 18 &&
                        player.getX() + player.getWidth() / 2 >= tileSize * 17 &&
                        player.getY() + player.getHeight() <= tileSize * 4 &&
                        player.getY() + player.getHeight() >= tileSize * 3) {
                    choosedRoot = marketRoot;
                    primaryStage.setScene(marketScene);
                }
            } else if (event.getCode() == KeyCode.C || pressed) {
                pressed = true;
                if (flags[4]) {
                    timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                        if (flags[0]) {
                            checkShopSceneHandler(generalStoreScene, checkShopRoot, checkGeneralUI);
                            checkGeneralUI.backpackSceneBuilder(primaryStage, checkShopRoot, generalStoreScene);
                            primaryStage.setScene(checkShopScene);
                            flags[0] = false;
                        } else if (flags[1]) {
                            objectStatusSceneBuilder(objectStatusRoot, choosedItem, generalStoreScene, 2);
                            primaryStage.setScene(objectStatusScene);
                            flags[1] = false;
                        } else if (flags[2]) {
                            pressed = false;
                            flags[2] = false;
                            flags[0] = true;
                            stopTimeline();
                        }
                    }));
                    timeline.setCycleCount(Animation.INDEFINITE);
                }
                if (flags[4]) {
                    timeline.play();
                    flags[4] = false;
                }
            } else if (event.getCode() == KeyCode.B || pressed) {
                pressed = true;
                if (flags[4]) {
                    timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                        if (flags[0]) {
                            checkShopSceneHandler(generalStoreScene, checkShopRoot, checkGeneralUI);
                            checkGeneralUI.backpackSceneBuilder(primaryStage, checkShopRoot, generalStoreScene);
                            primaryStage.setScene(checkShopScene);
                            flags[0] = false;
                        } else if (flags[1]) {
                            buySceneBuilder(buyRoot, choosedItem, generalStoreScene, generalStoreRoot, game.getPlayer(),
                                    game.getVillage().getMarket().getShop().get(2));
                            primaryStage.setScene(buyScene);
                            flags[1] = false;
                        } else if (flags[2]) {
                            pressed = false;
                            flags[2] = false;
                            flags[0] = true;
                            stopTimeline();
                        }
                    }));
                    timeline.setCycleCount(Animation.INDEFINITE);
                }
                if (flags[4]) {
                    timeline.play();
                    flags[4] = false;
                }
            } else if (event.getCode() == KeyCode.S || pressed) {
                pressed = true;
                if (flags[4]) {
                    timeline = new Timeline(new KeyFrame(new Duration(100), event1 -> {
                        if (flags[0]) {
                            backpackUI.backpackSceneBuilder(primaryStage, backpackRoot, generalStoreScene);
                            primaryStage.setScene(backpackScene);
                            backpackSceneHandler(generalStoreScene, root, "Product");
                            flags[0] = false;
                        } else if (flags[1]) {
                            sellSceneBuilder(sellRoot, choosedItem, generalStoreScene, generalStoreRoot, game.getPlayer(),
                                    game.getVillage().getMarket().getShop().get(2));
                            primaryStage.setScene(sellScene);
                            flags[1] = false;
                        } else if (flags[2]) {
                            pressed = false;
                            flags[2] = false;
                            flags[0] = true;
                            stopTimeline();
                        }
                    }));
                    timeline.setCycleCount(Animation.INDEFINITE);
                }
                if (flags[4]) {
                    timeline.play();
                    flags[4] = false;
                }
            } else if (event.getCode() == KeyCode.SPACE) {
                if (!isMultiplayer) {
                    timeCalculator.stop();
                    pauseSceneBuilder(pauseRoot, generalStoreScene, game.getPlayer());
                    primaryStage.setScene(pauseScene);
                } else {
                    networkPauseSceneBuilder(networkPauseRoot, generalStoreScene, game.getPlayer());
                    primaryStage.setScene(networkPauseScene);
                }
            } else
                move(generalStoreUI, event, player);
        });

    }
}

// TODO: 7/5/2017 cafe khorojesh az dar moshkel dare
// TODO: 7/12/2017 vase kharido forosh to ghasabi barno mikhaim
