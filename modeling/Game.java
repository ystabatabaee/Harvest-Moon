import com.google.common.collect.HashMultiset;

import java.util.ArrayList;

public class Game {
    private Farm farm;
    private Village village;
    private Jungle jungle;
    private Cave cave;
    private Player player;
    private Date date;

    public Farm getFarm() {
        return farm;
    }

    public void setFarm(Farm farm) {
        this.farm = farm;
    }

    public Village getVillage() {
        return village;
    }

    public void setVillage(Village village) {
        this.village = village;
    }

    public Jungle getJungle() {
        return jungle;
    }

    public void setJungle(Jungle jungle) {
        this.jungle = jungle;
    }

    public Cave getCave() {
        return cave;
    }

    public void setCave(Cave cave) {
        this.cave = cave;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }



    public Game() {
        AllOfShops allOfShops = new AllOfShops();
        AllOfAnimals allOfAnimals = new AllOfAnimals();
        AllOfBackpacks allOfBackpacks = new AllOfBackpacks();
        AllOfTools allOfTools = new AllOfTools();
        AllOfFruits allOfFruits = new AllOfFruits();
        AllOfSeeds allOfSeeds = new AllOfSeeds();
        AllOfMinerals allOfMinerals = new AllOfMinerals();
        AllOFWeathers allOFWeathers = new AllOFWeathers();
        AllOfProducts allOfProducts = new AllOfProducts();
        AllOfMedicines allOfMedicines = new AllOfMedicines();
        player = new Player("Hello!");
        try {
            farm = new Farm();
            village = new Village();
            jungle = new Jungle();
            cave = new Cave();
        } catch (Exception e){
            e.printStackTrace();
        }

        player.setMoney(10000);

        /*AllOfTrees allOfTrees = new AllOfTrees();
        jungle.getJungleTree().add(allOfTrees.oak(), (int)(Math.random() * 100));
        jungle.getJungleTree().add(allOfTrees.oldTree(), (int)(Math.random() * 100));
        jungle.getJungleTree().add(allOfTrees.pine(), (int)(Math.random() * 100));
        jungle.getMineral().add(allOfMinerals.stone(), (int)(Math.random() * 100));
        jungle.getMineral().add(allOfMinerals.oldLumber(), (int)(Math.random() * 100));
        jungle.getMineral().add(allOfMinerals.pineLumber(), (int)(Math.random() * 100));
        jungle.getMineral().add(allOfMinerals.oakLumber(), (int)(Math.random() * 100));
        jungle.getMineral().add(allOfMinerals.branch(), (int)(Math.random() * 100));


        cave.getMineral().add(allOfMinerals.ironOre(), (int)(Math.random() * 100));
        cave.getMineral().add(allOfMinerals.adamantiumOre(), (int)(Math.random() * 100));
        cave.getMineral().add(allOfMinerals.silverOre(), (int)(Math.random() * 100));
        cave.getMineral().add(allOfMinerals.stone(), (int)(Math.random() * 100));*/

        HashMultiset<Item> backpackContent = HashMultiset.create();
        //backpackContent.add(allOfTools.pot());
        //backpackContent.add(allOfTools.fryingPan());
        //backpackContent.add(allOfTools.knife());
        //backpackContent.add(allOfTools.foodMixer());
        //backpackContent.add(allOfTools.oven());
        //backpackContent.add(allOfTools.adamantiumPickaxe());
        //backpackContent.add(allOfTools.silverWateringCan());
        backpackContent.add(new AllOfAnimalProducts().milk());
        backpackContent.add(allOfFruits.tomato(), 3);
        backpackContent.add(allOfProducts.oil(), 2);
        backpackContent.add(allOfProducts.salt(), 2);
        backpackContent.add(allOfSeeds.lettuce(), 4);
        backpackContent.add(allOfMinerals.stone(), 4);
        backpackContent.add(allOfTools.ironShovel());
        backpackContent.add(allOfTools.pineFishingRod(), 1);
        backpackContent.add(allOfTools.adamantiumPickaxe());
        WateringCan wateringCan = allOfTools.ironWateringCan();
        backpackContent.add(allOfMinerals.ironOre(), 10);
        backpackContent.add(allOfMinerals.branch(), 10);
        backpackContent.add(allOfMinerals.stone(), 10);
        backpackContent.add(allOfTools.milker());
        backpackContent.add(allOfMedicines.animalMedicine());
        backpackContent.add(allOfSeeds.pea());
        backpackContent.add(allOfMinerals.oldLumber(), 60);
        backpackContent.add(allOfMinerals.ironOre(), 50);
        wateringCan.setWaterLevel(11);
        backpackContent.add(wateringCan, 1);
        player.getBackpack().setItem(backpackContent);

        date = new Date(new Time(0, 0, 0, new ArrayList<>()), 1, 1, 1,
                new Season("Spring", new ArrayList<>(), allOFWeathers.spring()),
                new Time(0, 0, 0, new ArrayList<>()));



    }
}
