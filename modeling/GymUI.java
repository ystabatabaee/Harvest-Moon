import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class GymUI implements Mover{
        Group root = new Group();
        static double tileSize = 50;
        private static Gym gym;


        private Rectangle rectangleBuilder(double width, double height, Paint fill, double setX, double setY) {
            Rectangle rectangle = new Rectangle(width, height, fill);
            rectangle.setX(setX);
            rectangle.setY(setY);
            return rectangle;
        }

        public GymUI(Group root, Gym gym) {
            this.root = root;
            this.gym = gym;
        }

        public Scene sceneBuilder(Stage primaryStage){
            Scene homeScene = new Scene(root, 1000, 700, Color.BLACK);
            Image homePic = new Image("pics/maps/gym.png");

            Rectangle rectangle = new Rectangle(1000, 700, new ImagePattern(homePic));
            root.getChildren().add(rectangle);


            return homeScene;
        }

        public boolean moveUp(Rectangle player){
            return true;
        }

        public boolean moveDown(Rectangle player){
           return true;
        }

        public boolean moveRight(Rectangle player){
           return true;
        }

        public boolean moveLeft(Rectangle player){
          return true;
        }

    public boolean closeToBoard(Rectangle player) {
        if (player.getX() + player.getWidth() / 2 >= tileSize * 7 &&
                player.getX() + player.getWidth() / 2 <= tileSize * 13 &&
                player.getY() + player.getHeight() <= tileSize * 10 &&
                player.getY() + player.getHeight() >= tileSize * 4) {
            return true;
        }
        return false;
    }

    public void train(Train choosedTrain, Player player, boolean[] flags) {
        UI.showPopup(choosedTrain.run(player), root);
        flags[3] = true;
    }
}
