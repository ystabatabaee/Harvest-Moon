import java.util.ArrayList;

public class AllOfFeatures {

    public Feature health(){
              return new Feature("Health", 100.0, 200.0, 100, 50, 500, 1000, 100,  new ArrayList<Task>());
    }

    public Feature energy(){
        return new Feature("Energy", 100.0, 200.0, 100, 50, 500, 1000, 100, new ArrayList<Task>());
    }

    public Feature stamina(){
        return new Feature("Stamina", 30.0, 60.0, 100, 50, 100, 200, 100, new ArrayList<Task>());
    }

    public Feature satiety(){
        return new Feature("Satiety", 100.0, 200.0, 10, 5, 500, 1000, 100, new ArrayList<Task>());
    }


// TODO: 5/14/2017 energy and health datas
// TODO: 5/14/2017 tasks
}
