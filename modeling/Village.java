public class Village implements Place {

    private Market market;
    private Workshop workshop;
    private Clinic clinic;
    private Laboratory laboratory;
    private Gym gym;
    private Cafe cafe;
    private Ranch ranch;


    public Village() {
        AllOfShops allOfShops = new AllOfShops();
        market = new Market();
        try {
            workshop = allOfShops.workshop();
            cafe = allOfShops.cafe();
            ranch = allOfShops.ranch();
            clinic = allOfShops.clinic();
            laboratory = allOfShops.laboratory();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
        gym = new Gym();
    }

    public Market getMarket() {
        return market;
    }

    public void setMarket(Market market) {
        this.market = market;
    }

    public Workshop getWorkshop() {
        return workshop;
    }

    public void setWorkshop(Workshop workshop) {
        this.workshop = workshop;
    }

    public Clinic getClinic() {
        return clinic;
    }

    public void setClinic(Clinic clinic) {
        this.clinic = clinic;
    }

    public Laboratory getLaboratory() {
        return laboratory;
    }

    public void setLaboratory(Laboratory laboratory) {
        this.laboratory = laboratory;
    }

    public Gym getGym() {
        return gym;
    }

    public void setGym(Gym gym) {
        this.gym = gym;
    }

    public Cafe getCafe() {
        return cafe;
    }

    public void setCafe(Cafe cafe) {
        this.cafe = cafe;
    }

    public Ranch getRanch() {
        return ranch;
    }

    public void setRanch(Ranch ranch) {
        this.ranch = ranch;
    }
}