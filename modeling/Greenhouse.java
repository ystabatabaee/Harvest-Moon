import com.google.common.collect.HashMultiset;

import java.util.ArrayList;

public class Greenhouse extends Building {
    private ArrayList<FarmingField> farmingField;
    private Tool weatherMachine;
    private Weather weather;

    public Greenhouse(Probability ruin) throws IllegalAccessException, InstantiationException {
        super(true, ruin, HashMultiset.create(), 5000);
        AllOfMinerals allOfMinerals = new AllOfMinerals();
        HashMultiset repairObjects = HashMultiset.create();
        repairObjects.add(allOfMinerals.oldLumber(), 20);
        repairObjects.add(allOfMinerals.ironOre(), 20);
        setRepairObjects(repairObjects);
        weather = new Weather();
        farmingField = new ArrayList<>();
        farmingField.add(new FarmingField());
        farmingField.add(new FarmingField());
        farmingField.add(new FarmingField());
        farmingField.add(new FarmingField());
    }

    public ArrayList<FarmingField> getFarmingField() {
        return farmingField;
    }

    public void setFarmingField(ArrayList<FarmingField> farmingField) {
        this.farmingField = farmingField;
    }

    public Tool getWeatherMachine() {
        return weatherMachine;
    }

    public void setWeatherMachine(Tool weatherMachine) {
        this.weatherMachine = weatherMachine;
    }

    public Weather getWeather() {
        return weather;
    }

    public void setWeather(Weather weather) {
        this.weather = weather;
    }

}