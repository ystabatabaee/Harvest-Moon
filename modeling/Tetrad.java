
public class Tetrad {
    private String task;
    private String obejct;
    private int count;
    private int happened;

    public Tetrad(String task, String obejct, Integer count) {
        this.task = task;
        this.obejct = obejct;
        this.count = count;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public String getObejct() {
        return obejct;
    }

    public void setObejct(String obejct) {
        this.obejct = obejct;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getHappened() {
        return happened;
    }

    public void setHappened(int happened) {
        this.happened = happened;
    }


}
