import java.util.HashMap;

public class Faint extends Task{

    NextDay nextDay;

    public Faint() {
        super.name = "Faint";
        super.featureChangeRate = new HashMap<String, Double>();
        super.executionTime = new Date();
    }

    public String run(Player player, Date date, Game game) {
        UI.missionHandler("Faint", "Player");
        for(Feature feature: player.getFeature()){
            if(feature.getName().equals("Energy") && feature.getCurrent() < 1) {
                feature.setCurrent(feature.getMaxCurrent() * 0.75);
                feature.setCurrent(feature.getCurrent() * 0.75);
                date.setDay(date.getDay() + 1);
                date.getTime().setHour(10);
                nextDay = new NextDay();
                nextDay.run(game, date, player);
                return ("You passed-out for one day!");
            }
            else if(feature.getName().equals("Health") && feature.getCurrent() < 1) {
                nextDay = new NextDay();
                feature.setCurrent(feature.getMaxCurrent() * 0.5);
                feature.setCurrent(feature.getMaxCurrent() * 0.5);
                date.setDay(date.getDay() + 7);
                for (int i = 0; i < 7; i++) {
                    nextDay.run(game, date, player);
                }
                return ("You passed-out for one week!");
            }
        }
        date.getTime().setHour(8);
        nextDay = new NextDay();
        date.setDay(date.getDay() + 1);
        return "You fainted for 9 hours!";

    }
}
