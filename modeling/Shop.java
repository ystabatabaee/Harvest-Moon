import com.google.common.collect.HashMultiset;

public class Shop extends Building {

    private Seller seller;
    private HashMultiset goods;
    private String name;

    public Shop(HashMultiset goods, String name) {
        super(false, new Probability(), HashMultiset.create(), 0);
        this.goods = goods;
        this.name = name;
    }

    public Shop(boolean isRuin, Probability ruin, HashMultiset repairObjects, double repairPrice, HashMultiset goods, String name) {
        super(isRuin, ruin, repairObjects, repairPrice);
        this.goods = goods;
        this.name = name;
    }

    public Seller getSeller() {
        return seller;
    }

    public void setSeller(Seller seller) {
        this.seller = seller;
    }

    public HashMultiset getGoods() {
        return goods;
    }

    public void setGoods(HashMultiset goods) {
        this.goods = goods;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}