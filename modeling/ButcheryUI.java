import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class ButcheryUI implements Mover {
    Group root = new Group();
    static double tileSize = 50;
    private static Rectangle chair;
    private static Rectangle table;
    private static Rectangle chickenMeatTable;
    private static Rectangle meatTable;
    private static Rectangle fishTable;
    private static Rectangle hay;
    private static Rectangle chickenHay;

    private Rectangle rectangleBuilder(double width, double height, Paint fill, double setX, double setY) {
        Rectangle rectangle = new Rectangle(width, height, fill);
        rectangle.setX(setX);
        rectangle.setY(setY);
        return rectangle;
    }

    public ButcheryUI(Group root) {
        this.root = root;
    }

    public Scene sceneBuilder(Stage primaryStage) {
        Scene butcheryScene = new Scene(root, 1000, 700, Color.BLACK);

        Image butcheryPic = new Image("pics/maps/butcheryMap.png");
        Image tablePic = new Image("pics/dinningTable.png");
        Image chairPic = new Image("pics/rightChair.png");
        Image meatTablePic = new Image("pics/meatTable.png");
        Image chickenMeatTablePic = new Image("pics/chickenMeatTable.png");
        Image fishTablePic = new Image("pics/fishTable.png");
        Image hayPic = new Image("pics/hay.png");
        Image chickenHayPic = new Image("pics/chickenHay.png");
        Image doorPic = new Image("pics/doors/butcheryDoor.png");


        Rectangle rectangle = new Rectangle(1000, 700, new ImagePattern(butcheryPic));
        root.getChildren().add(rectangle);

        table = rectangleBuilder(tileSize * 2, tileSize * 3, new ImagePattern(tablePic), 1000 - tileSize * 4, tileSize * 5);
        chair = rectangleBuilder(tileSize, tileSize, new ImagePattern(chairPic), 1000 - tileSize * 2, tileSize * 6);
        meatTable = rectangleBuilder(tileSize * 6, tileSize * 3, new ImagePattern(meatTablePic), tileSize * 12, tileSize * 9);
        chickenMeatTable = rectangleBuilder(tileSize * 6, tileSize * 3, new ImagePattern(chickenMeatTablePic), tileSize * 2, tileSize * 9);
        meatTable = rectangleBuilder(tileSize * 6, tileSize * 3, new ImagePattern(meatTablePic), tileSize * 12, tileSize * 9);
        fishTable = rectangleBuilder(tileSize * 3, tileSize * 3, new ImagePattern(fishTablePic), tileSize * 3, tileSize * 5);
        hay = rectangleBuilder(tileSize * 2, tileSize * 3, new ImagePattern(hayPic), tileSize * 9, tileSize * 5);
        chickenHay = rectangleBuilder(tileSize * 2, tileSize, new ImagePattern(chickenHayPic), tileSize * 11, tileSize * 6);
        Rectangle door = rectangleBuilder(tileSize * 4, tileSize * 4, new ImagePattern(doorPic), 1000 - tileSize * 6, 0);
        root.getChildren().addAll(table, chair, door, meatTable, chickenMeatTable, fishTable, hay, chickenHay);

        return butcheryScene;
    }

    public boolean moveUp(Rectangle player) {
        if (player.getY() + player.getHeight() >= tileSize * 4.4 &&
                !(player.getX() + player.getWidth() >= table.getX() + tileSize * 0.8 &&
                        player.getX() <= table.getX() + table.getWidth() - tileSize * 0.8 &&
                        player.getY() + player.getHeight() >= table.getY() + table.getHeight() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= table.getY() + table.getHeight() + tileSize * 0.4) &&
                !(player.getX() + player.getWidth() >= chair.getX() + tileSize * 0.4 &&
                        player.getX() <= chair.getX() + chair.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= chair.getY() + chair.getHeight() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= chair.getY() + chair.getHeight() + tileSize * 0.4) &&
                !(player.getX() + player.getWidth() >= meatTable.getX() + tileSize * 0.6 &&
                        player.getX() <= meatTable.getX() + meatTable.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= meatTable.getY() + meatTable.getHeight() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() <= meatTable.getY() + meatTable.getHeight() - tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= chickenMeatTable.getX() + tileSize * 0.6 &&
                        player.getX() <= chickenMeatTable.getX() + chickenMeatTable.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= chickenMeatTable.getY() + chickenMeatTable.getHeight() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() <= chickenMeatTable.getY() + chickenMeatTable.getHeight() - tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= fishTable.getX() + tileSize * 0.6 &&
                        player.getX() <= fishTable.getX() + fishTable.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= fishTable.getY() + fishTable.getHeight() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= fishTable.getY() + fishTable.getHeight() + tileSize * 0.4) &&
                !(player.getX() + player.getWidth() >= chickenHay.getX() + tileSize * 1 &&
                        player.getX() <= chickenHay.getX() + chickenHay.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= chickenHay.getY() + chickenHay.getHeight() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= chickenHay.getY() + chickenHay.getHeight() + tileSize * 0.4) &&
                !(player.getX() + player.getWidth() >= hay.getX() + tileSize * 0.6 &&
                        player.getX() <= hay.getX() + hay.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= hay.getY() + hay.getHeight() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= hay.getY() + hay.getHeight() + tileSize * 0.4)) {
            return true;
        }
        return false;
    }

    public boolean moveDown(Rectangle player) {
        if (player.getY() + player.getHeight() <= tileSize * 14 &&
                !(player.getX() + player.getWidth() >= table.getX() + tileSize * 0.6 &&
                        player.getX() <= table.getX() + table.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= table.getY() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= table.getY() + tileSize * 0.4) &&
                !(player.getX() + player.getWidth() >= chair.getX() + tileSize * 0.4 &&
                        player.getX() <= chair.getX() + chair.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= chair.getY() - tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= chair.getY()) &&
                !(player.getX() + player.getWidth() >= meatTable.getX() + tileSize * 0.6 &&
                        player.getX() <= meatTable.getX() + meatTable.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= meatTable.getY() - tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= meatTable.getY()) &&
                !(player.getX() + player.getWidth() >= chickenMeatTable.getX() + tileSize * 0.6 &&
                        player.getX() <= chickenMeatTable.getX() + chickenMeatTable.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= chickenMeatTable.getY() - tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= chickenMeatTable.getY()) &&
                !(player.getX() + player.getWidth() >= fishTable.getX() + tileSize * 0.4 &&
                        player.getX() <= fishTable.getX() + fishTable.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= fishTable.getY() &&
                        player.getY() + player.getHeight() <= fishTable.getY() + tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= chickenHay.getX() + tileSize * 1 &&
                        player.getX() <= chickenHay.getX() + chickenHay.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= chickenHay.getY() &&
                        player.getY() + player.getHeight() <= chickenHay.getY() + tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= hay.getX() + tileSize * 0.4 &&
                        player.getX() <= hay.getX() + hay.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= hay.getY() + tileSize * 0.4 &&
                        player.getY() + player.getHeight() <= hay.getY() + tileSize * 0.6)) {
            return true;
        }
        return false;
    }

    public boolean moveRight(Rectangle player) {
        if (player.getX() + player.getWidth() <= tileSize * 20.2 &&
                !(player.getX() + player.getWidth() >= table.getX() + tileSize * 0.4 &&
                        player.getX() + player.getWidth() <= table.getX() + tileSize * 0.8 &&
                        player.getY() + player.getHeight() >= table.getY() + tileSize * 0.4 &&
                        player.getY() + player.getHeight() <= table.getY() + table.getHeight() + tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= chair.getX() + tileSize * 0.2 &&
                        player.getX() + player.getWidth() <= chair.getX() + tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= chair.getY() &&
                        player.getY() + player.getHeight() <= chair.getY() + chair.getHeight() + tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= meatTable.getX() + tileSize * 0.4 &&
                        player.getX() + player.getWidth() <= meatTable.getX() + tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= meatTable.getY() &&
                        player.getY() + player.getHeight() <= meatTable.getY() + meatTable.getHeight() - tileSize * 0.4) &&
                !(player.getX() + player.getWidth() >= chickenMeatTable.getX() + tileSize * 0.4 &&
                        player.getX() + player.getWidth() <= chickenMeatTable.getX() + tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= chickenMeatTable.getY() &&
                        player.getY() + player.getHeight() <= chickenMeatTable.getY() + chickenMeatTable.getHeight() - tileSize * 0.4) &&
                !(player.getX() + player.getWidth() >= fishTable.getX() + tileSize * 0.2 &&
                        player.getX() + player.getWidth() <= fishTable.getX() + tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= fishTable.getY() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= fishTable.getY() + fishTable.getHeight() + tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= chickenHay.getX() + tileSize * 0.8 &&
                        player.getX() + player.getWidth() <= chickenHay.getX() + tileSize * 1 &&
                        player.getY() + player.getHeight() >= chickenHay.getY() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= chickenHay.getY() + chickenHay.getHeight() + tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= hay.getX() + tileSize * 0.2 &&
                        player.getX() + player.getWidth() <= hay.getX() + tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= hay.getY() + tileSize * 0.6 &&
                        player.getY() + player.getHeight() <= hay.getY() + hay.getHeight() + tileSize * 0.2)) {
            return true;
        }
        return false;
    }

    public boolean moveLeft(Rectangle player) {
        if (player.getX() >= -tileSize * 0.2 &&
                !(player.getX() >= table.getX() + table.getWidth() - tileSize * 0.8 &&
                        player.getX() <= table.getX() + table.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= table.getY() + tileSize * 0.4 &&
                        player.getY() + player.getHeight() <= table.getY() + table.getHeight() + tileSize * 0.2) &&
                !(player.getX() >= chair.getX() + chair.getWidth() - tileSize * 0.4 &&
                        player.getX() <= chair.getX() + chair.getWidth() - tileSize * 0.2 &&
                        player.getY() + player.getHeight() >= chair.getY() &&
                        player.getY() + player.getHeight() <= chair.getY() + chair.getHeight() + tileSize * 0.2) &&
                !(player.getX() >= meatTable.getX() + meatTable.getWidth() - tileSize * 0.6 &&
                        player.getX() <= meatTable.getX() + meatTable.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= meatTable.getY() &&
                        player.getY() + player.getHeight() <= meatTable.getY() + meatTable.getHeight() - tileSize * 0.4) &&
                !(player.getX() >= chickenMeatTable.getX() + chickenMeatTable.getWidth() - tileSize * 0.6 &&
                        player.getX() <= chickenMeatTable.getX() + chickenMeatTable.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= chickenMeatTable.getY() &&
                        player.getY() + player.getHeight() <= chickenMeatTable.getY() + chickenMeatTable.getHeight() - tileSize * 0.4) &&
                !(player.getX() >= fishTable.getX() + fishTable.getWidth() - tileSize * 0.6 &&
                        player.getX() <= fishTable.getX() + fishTable.getWidth() - tileSize * 0.2 &&
                        player.getY() + player.getHeight() >= fishTable.getY() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= fishTable.getY() + fishTable.getHeight() + tileSize * 0.2) &&
                !(player.getX() >= chickenHay.getX() + chickenHay.getWidth() - tileSize * 0.4 &&
                        player.getX() <= chickenHay.getX() + chickenHay.getWidth() - tileSize * 0.2 &&
                        player.getY() + player.getHeight() >= chickenHay.getY() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= chickenHay.getY() + chickenHay.getHeight() + tileSize * 0.2) &&
                !(player.getX() >= hay.getX() + hay.getWidth() - tileSize * 0.6 &&
                        player.getX() <= hay.getX() + hay.getWidth() - tileSize * 0.2 &&
                        player.getY() + player.getHeight() >= hay.getY() + tileSize * 0.6 &&
                        player.getY() + player.getHeight() <= hay.getY() + hay.getHeight() + tileSize * 0.2)) {
            return true;
        }
        return false;
    }
}
