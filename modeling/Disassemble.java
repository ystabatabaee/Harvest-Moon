import java.util.HashMap;

public class Disassemble extends Task {
    public Disassemble() {
        super.name = "Disassemble";
        super.featureChangeRate = new HashMap<String, Double>();
        super.executionTime = new Date();
    }

    public String run(Tool tool, Player player) {
        if (player.getBackpack().getCapacity() < player.getBackpack().getCurrentFullness() +
                tool.getDissassemblity().getInitialIngredients().elementSet().size()) {
           return ("Backpack doesn't have enough space!");
        }
        else {
            if (tool.isValid()) {
                for (Item item : tool.getDissassemblity().getInitialIngredients().elementSet()) {
                    player.getBackpack().getItem().add(item, tool.getDissassemblity().getInitialIngredients().count(item));
                }
                player.getBackpack().getItem().remove(tool);
            } else {
                for (Item item : tool.getDissassemblity().getInitialIngredients().elementSet()) {
                    if (tool.getDissassemblity().getInitialIngredients().count(item) >= 3)
                        player.getBackpack().getItem().add(item, tool.getDissassemblity().getInitialIngredients().count(item) - 2);
                    else if (tool.getDissassemblity().getInitialIngredients().count(item) >= 2)
                        player.getBackpack().getItem().add(item, tool.getDissassemblity().getInitialIngredients().count(item) - 1);
                }
                player.getBackpack().getItem().remove(tool);
            }
        }
        UI.missionHandler(name, tool.getName());
        return ( tool.getName() + " Disassembled!");
    }
}
