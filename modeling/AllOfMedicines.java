import javafx.scene.image.Image;

import java.util.ArrayList;
import java.util.HashMap;

public class AllOfMedicines {

    public Potion usual(){
        return new Potion(new HashMap<String, Double>(){{put("Health", 100.0);}}, 1, "Usual", new Probability(){{}},
                150, new ArrayList<Task>(){{}}, 1, new Image("pics/medicine/usual.png"));
    }

    public Potion strong(){
        return new Potion(new HashMap<String, Double>(){{put("Health", 250.0);}}, 1, "Strong", new Probability(){{}},
        350, new ArrayList<Task>(){{}}, 2, new Image("pics/medicine/strong.png"));
    }

    public Potion superPotion(){
        return new Potion(new HashMap<String, Double>(){{put("Health", 500.0);}}, 1, "Super Potion", new Probability(){{}},
                600, new ArrayList<Task>(){{}}, 3, new Image("pics/medicine/superPotion.png"));
    }

    public AnimalMedicine animalMedicine(){
        return new AnimalMedicine(new HashMap<String, Double>(){{put("Health", 500.0);}}, 1, "Animal Medicine", new Probability(){{}}
        , 250,  new ArrayList<Task>(){{}}, 1, 1, new Image("pics/medicine/animalMedicine.png"));
    }
}
