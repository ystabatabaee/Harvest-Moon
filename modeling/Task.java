import java.util.HashMap;

public class Task {

    protected String name;
    protected HashMap<String, Double> featureChangeRate;
    protected Date executionTime;

    public Task(String name, HashMap<String, Double> featureChangeRate) {
        this.name = name;
        this.featureChangeRate = featureChangeRate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public HashMap<String, Double> getFeatureChangeRate() {
        return featureChangeRate;
    }

    public void setFeatureChangeRate(HashMap<String, Double> featureChangeRate) {
        this.featureChangeRate = featureChangeRate;
    }

    public Date getExecutionTime() {
        return executionTime;
    }

    public void setExecutionTime(Date executionTime) {
        this.executionTime = executionTime;
    }

    public Task(String name, HashMap<String, Double> featureChangeRate, Date executionTime) {
        this.name = name;
        this.featureChangeRate = featureChangeRate;
        this.executionTime = executionTime;
    }

    public Task() {
    }
}