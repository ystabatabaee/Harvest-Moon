import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

import java.lang.reflect.InvocationTargetException;

public class HomeUI implements Mover {
    Group root = new Group();
    static double tileSize = 50;
    private static Rectangle bed;
    private static Rectangle drawer;
    private static Rectangle dinningTable;
    private static Rectangle cabinet;
    private static Rectangle toolShelf;
    private static Rectangle storageBox;
    private static Rectangle rightChair;
    private static Rectangle leftChair;
    private static Rectangle kitchenLeftChair;
    private static Rectangle kitchenRightChair;
    private static Rectangle kitchenTable;
    private static Home home;


    private Rectangle rectangleBuilder(double width, double height, Paint fill, double setX, double setY) {
        Rectangle rectangle = new Rectangle(width, height, fill);
        rectangle.setX(setX);
        rectangle.setY(setY);
        return rectangle;
    }

    public HomeUI(Group root, Farm farm) {
        this.root = root;
        this.home = farm.getHome();
    }

    public Scene sceneBuilder(Stage primaryStage) throws IllegalAccessException, InstantiationException, InvocationTargetException {
        Scene homeScene = new Scene(root, 1000, 700, Color.BLACK);
        Image homePic = new Image("pics/maps/homeMap.png");
        Image cabinetPic = new Image("pics/cafeCabinet.png");
        Image doorPic = new Image("pics/doors/homeDoor.png");
        Image bedPic = new Image("pics/bed.png");
        Image drawerPic = new Image("pics/drawer.png");
        Image dinningTablePic = new Image("pics/dinningTable.png");
        Image kitchenTablePic = new Image("pics/kitchenTable.png");
        Image flowerPic1 = new Image("pics/flowerPot1.png");
        Image toolShelfPic = new Image("pics/cookingToolShelf.png");
        Image closedStorageBox = new Image("pics/storageBox-closed.png");
        Image openedStorageBox = new Image("pics/storageBox-opened.png");
        Image flowerPic2 = new Image("pics/flowerPot3.png");
        Image leftChairPic = new Image("pics/leftChair.png");
        Image rightChairPic = new Image("pics/rightChair.png");
        Image woodenLeftChairPic = new Image("pics/woodenLeftChair.png");
        Image woodenRightChairPic = new Image("pics/woodenRightChair.png");

        Rectangle rectangle = new Rectangle(1000, 700, new ImagePattern(homePic));
        root.getChildren().add(rectangle);

        Rectangle door = rectangleBuilder(tileSize * 2, tileSize * 4, new ImagePattern(doorPic), 1000 - tileSize * 4, 0);
        drawer = rectangleBuilder(tileSize * 5, tileSize * 3, new ImagePattern(drawerPic), tileSize * 10, tileSize * 3);
        dinningTable = rectangleBuilder(tileSize * 2, tileSize * 3, new ImagePattern(dinningTablePic), 1000 - tileSize * 5, 700 - tileSize * 4);
        leftChair = rectangleBuilder(tileSize, tileSize, new ImagePattern(leftChairPic), 1000 - tileSize * 6, 700 - tileSize * 3);
        rightChair = rectangleBuilder(tileSize, tileSize, new ImagePattern(rightChairPic), 1000 - tileSize * 3, 700 - tileSize * 3);
        storageBox = rectangleBuilder(tileSize * 2, tileSize, new ImagePattern(closedStorageBox), tileSize * 10, 700 - tileSize * 2);
        root.getChildren().addAll(door, drawer, dinningTable, leftChair, rightChair, storageBox);

        //kitchen facilities
        cabinet = rectangleBuilder(tileSize * 2, tileSize * 2, new ImagePattern(cabinetPic), tileSize, tileSize * 3);
        toolShelf = rectangleBuilder(tileSize, tileSize * 2, new ImagePattern(toolShelfPic), tileSize * 4, tileSize * 3);
        kitchenTable = rectangleBuilder(tileSize, tileSize, new ImagePattern(kitchenTablePic), tileSize * 2, tileSize * 7);
        kitchenLeftChair = rectangleBuilder(tileSize, tileSize, new ImagePattern(woodenLeftChairPic), tileSize, tileSize * 7);
        kitchenRightChair = rectangleBuilder(tileSize, tileSize, new ImagePattern(woodenRightChairPic), tileSize * 3, tileSize * 7);
        root.getChildren().addAll(cabinet, toolShelf, kitchenTable, kitchenLeftChair, kitchenRightChair);

        //room facilities
        bed = rectangleBuilder(tileSize * 2, tileSize * 3, new ImagePattern(bedPic), tileSize, 700 - tileSize * 5);
        Rectangle flowerPot1 = rectangleBuilder(tileSize, tileSize, new ImagePattern(flowerPic2), tileSize * 2, 700 - tileSize);
        Rectangle flowerPot2 = rectangleBuilder(tileSize, tileSize, new ImagePattern(flowerPic2), tileSize * 5, 700 - tileSize);
        root.getChildren().addAll(bed, flowerPot1, flowerPot2);


        for (int i = 0; i < 4; i++) {
            Rectangle flower1 = rectangleBuilder(tileSize, tileSize * 2, new ImagePattern(flowerPic1), 1000 - tileSize, tileSize * 2 * (i + 2));
            root.getChildren().add(flower1);
        }

        return homeScene;
    }

    public boolean moveUp(Rectangle player){
        if (player.getY() + player.getHeight() >= tileSize * 4.5 &&
                !(player.getX() + player.getWidth() >= drawer.getX() + tileSize * 0.4 &&
                        player.getX() <= drawer.getX() + drawer.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= drawer.getY() + drawer.getHeight() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() <= drawer.getY() + drawer.getHeight() - tileSize * 0.4) &&
                !(player.getX() + player.getWidth() >= tileSize * 8 + 20 &&
                        player.getX() <= tileSize * 8 - 10 &&
                        player.getY() + player.getHeight() >= tileSize * 7.5 - 10 &&
                        player.getY() + player.getHeight() <= tileSize * 7.5) &&
                !(player.getX() + player.getWidth() >= dinningTable.getX() + tileSize * 0.6 &&
                        player.getX() <= dinningTable.getX() + dinningTable.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= dinningTable.getY() + dinningTable.getHeight() &&
                        player.getY() + player.getHeight() <= dinningTable.getY() + dinningTable.getHeight() + tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= storageBox.getX() + tileSize * 0.4 &&
                        player.getX() <= storageBox.getX() + storageBox.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= storageBox.getY() + storageBox.getHeight() &&
                        player.getY() + player.getHeight() <= storageBox.getY() + storageBox.getHeight() + tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= rightChair.getX() + tileSize * 0.6 &&
                        player.getX() <= rightChair.getX() + rightChair.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= rightChair.getY() + rightChair.getHeight() &&
                        player.getY() + player.getHeight() <= rightChair.getY() + rightChair.getHeight() + tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= leftChair.getX() + tileSize * 0.6 &&
                        player.getX() <= leftChair.getX() + leftChair.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= leftChair.getY() + leftChair.getHeight() &&
                        player.getY() + player.getHeight() <= leftChair.getY() + leftChair.getHeight() + tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= 0 &&
                        player.getX() <= tileSize * 5.6 &&
                        player.getY() + player.getHeight() >= tileSize * 9.4 &&
                        player.getY() + player.getHeight() <= tileSize * 9.6) &&
                !(player.getX() + player.getWidth() >= bed.getX() + tileSize * 0.6 &&
                        player.getX() <= bed.getX() + bed.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= bed.getY() + bed.getHeight() &&
                        player.getY() + player.getHeight() <= bed.getY() + bed.getHeight() + tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= kitchenTable.getX() + tileSize * 0.4 &&
                        player.getX() <= kitchenTable.getX() + kitchenTable.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= kitchenTable.getY() + kitchenTable.getHeight() &&
                        player.getY() + player.getHeight() <= kitchenTable.getY() + kitchenTable.getHeight() + tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= kitchenRightChair.getX() + tileSize * 0.6 &&
                        player.getX() <= kitchenRightChair.getX() + kitchenRightChair.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= kitchenRightChair.getY() + kitchenRightChair.getHeight() &&
                        player.getY() + player.getHeight() <= kitchenRightChair.getY() + kitchenRightChair.getHeight() + tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= kitchenLeftChair.getX() + tileSize * 0.6 &&
                        player.getX() <= kitchenLeftChair.getX() + kitchenLeftChair.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= kitchenLeftChair.getY() + kitchenLeftChair.getHeight() &&
                        player.getY() + player.getHeight() <= kitchenLeftChair.getY() + kitchenLeftChair.getHeight() + tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= cabinet.getX() + tileSize * 0.4 &&
                        player.getX() <= cabinet.getX() + cabinet.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= cabinet.getY() + cabinet.getHeight() &&
                        player.getY() + player.getHeight() <= cabinet.getY() + cabinet.getHeight() + tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= toolShelf.getX() + tileSize * 0.4 &&
                        player.getX() <= toolShelf.getX() + toolShelf.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= toolShelf.getY() + toolShelf.getHeight() &&
                        player.getY() + player.getHeight() <= toolShelf.getY() + toolShelf.getHeight() + tileSize * 0.2)){
            return true;
        }
        return false;
    }

    public boolean moveDown(Rectangle player){
        if (player.getY() + player.getHeight() <= tileSize * 13.6 &&
                !(player.getX() + player.getWidth() >= tileSize * 8 + 20 &&
                        player.getX() <= tileSize * 8 - 10 &&
                        player.getY() + player.getHeight() >= tileSize * 9 &&
                        player.getY() + player.getHeight() <= tileSize * 9.2) &&
                !(player.getX() + player.getWidth() >= dinningTable.getX() + tileSize * 0.6 &&
                        player.getX() <= dinningTable.getX() + dinningTable.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= dinningTable.getY() + tileSize * 0.4 &&
                        player.getY() + player.getHeight() <= dinningTable.getY() + tileSize * 0.6) &&
                !(player.getX() + player.getWidth() >= storageBox.getX() + tileSize * 0.4 &&
                        player.getX() <= storageBox.getX() + storageBox.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= storageBox.getY() &&
                        player.getY() + player.getHeight() <= storageBox.getY() + tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= rightChair.getX() + tileSize * 0.6 &&
                        player.getX() <= rightChair.getX() + rightChair.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= rightChair.getY() &&
                        player.getY() + player.getHeight() <= rightChair.getY() + tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= leftChair.getX() + tileSize * 0.6 &&
                        player.getX() <= leftChair.getX() + leftChair.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= leftChair.getY() &&
                        player.getY() + player.getHeight() <= leftChair.getY() + tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= 0 &&
                        player.getX() <= tileSize * 5.6 &&
                        player.getY() + player.getHeight() >= tileSize * 9 &&
                        player.getY() + player.getHeight() <= tileSize * 9.2) &&
                !(player.getX() + player.getWidth() >= tileSize * 6.2 &&
                        player.getX() <= tileSize * 5.6 &&
                        player.getY() + player.getHeight() >= tileSize * 7 &&
                        player.getY() + player.getHeight() <= tileSize * 7.2) &&
                !(player.getX() + player.getWidth() >= kitchenTable.getX() + tileSize * 0.4 &&
                        player.getX() <= kitchenTable.getX() + kitchenTable.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= kitchenTable.getY() &&
                        player.getY() + player.getHeight() <= kitchenTable.getY() + tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= kitchenRightChair.getX() + tileSize * 0.6 &&
                        player.getX() <= kitchenRightChair.getX() + kitchenRightChair.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= kitchenRightChair.getY() &&
                        player.getY() + player.getHeight() <= kitchenRightChair.getY() + tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= kitchenLeftChair.getX() + tileSize * 0.6 &&
                        player.getX() <= kitchenLeftChair.getX() + kitchenLeftChair.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= kitchenLeftChair.getY() &&
                        player.getY() + player.getHeight() <= kitchenLeftChair.getY() + tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= tileSize * 2.4 &&
                        player.getX() <= tileSize * 2.6 &&
                        player.getY() + player.getHeight() >= tileSize * 13 &&
                        player.getY() + player.getHeight() <= tileSize * 13.2) &&
                !(player.getX() + player.getWidth() >= tileSize * 5.4 &&
                        player.getX() <= tileSize * 5.6 &&
                        player.getY() + player.getHeight() >= tileSize * 13 &&
                        player.getY() + player.getHeight() <= tileSize * 13.2)){
            return true;
        }
        return false;
    }

    public boolean moveRight(Rectangle player){
        if (player.getX() + player.getWidth() <= tileSize * 19.2 &&
                !(player.getX() + player.getWidth() >= drawer.getX() + tileSize * 0.2 &&
                        player.getX() + player.getWidth() <= drawer.getX() + tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= drawer.getY() &&
                        player.getY() + player.getHeight() <= drawer.getY() + drawer.getHeight() - tileSize * 0.6) &&
                !(player.getX() + player.getWidth() >= tileSize * 8.2 &&
                        player.getX() + player.getWidth() <= tileSize * 8.4 &&
                        player.getY() + player.getHeight() >= tileSize * 0 &&
                        player.getY() + player.getHeight() <= tileSize * 7.2) &&
                !(player.getX() + player.getWidth() >= dinningTable.getX() + tileSize * 0.4 &&
                        player.getX() + player.getWidth() <= dinningTable.getX() + tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= dinningTable.getY() + tileSize * 0.6 &&
                        player.getY() + player.getHeight() <= dinningTable.getY() + dinningTable.getHeight()) &&
                !(player.getX() + player.getWidth() >= storageBox.getX() + tileSize * 0.2 &&
                        player.getX() + player.getWidth() <= storageBox.getX() + tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= storageBox.getY() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= storageBox.getY() + storageBox.getHeight()) &&
                !(player.getX() + player.getWidth() >= rightChair.getX() + tileSize * 0.4 &&
                        player.getX() + player.getWidth() <= rightChair.getX() + tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= rightChair.getY() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= rightChair.getY() + rightChair.getHeight()) &&
                !(player.getX() + player.getWidth() >= leftChair.getX() + tileSize * 0.4 &&
                        player.getX() + player.getWidth() <= leftChair.getX() + tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= leftChair.getY() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= leftChair.getY() + leftChair.getHeight()) &&
                !(player.getX() + player.getWidth() >= tileSize * 6 &&
                        player.getX() + player.getWidth() <= tileSize * 6.2 &&
                        player.getY() + player.getHeight() >= tileSize * 7.2 &&
                        player.getY() + player.getHeight() <= tileSize * 9) &&
                !(player.getX() + player.getWidth() >= kitchenRightChair.getX() + tileSize * 0.4 &&
                        player.getX() + player.getWidth() <= kitchenRightChair.getX() + tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= kitchenRightChair.getY() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= kitchenRightChair.getY() + kitchenRightChair.getHeight()) &&
                !(player.getX() + player.getWidth() >= kitchenLeftChair.getX() + tileSize * 0.4 &&
                        player.getX() + player.getWidth() <= kitchenLeftChair.getX() + tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= kitchenLeftChair.getY() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= kitchenLeftChair.getY() + kitchenLeftChair.getHeight()) &&
                !(player.getX() + player.getWidth() >= toolShelf.getX() + tileSize * 0.2 &&
                        player.getX() + player.getWidth() <= toolShelf.getX() + tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= toolShelf.getY() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= toolShelf.getY() + toolShelf.getHeight()) &&
                !(player.getX() + player.getWidth() >= cabinet.getX() + tileSize * 0.2 &&
                        player.getX() + player.getWidth() <= cabinet.getX() + tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= cabinet.getY() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= cabinet.getY() + cabinet.getHeight()) &&
                !(player.getX() + player.getWidth() >= bed.getX() + tileSize * 0.4 &&
                        player.getX() + player.getWidth() <= bed.getX() + tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= bed.getY() + tileSize * 0.6 &&
                        player.getY() + player.getHeight() <= bed.getY() + bed.getHeight()) &&
                !(player.getX() + player.getWidth() >= tileSize * 8.2 &&
                        player.getX() + player.getWidth() <= tileSize * 8.4 &&
                        player.getY() + player.getHeight() >= tileSize * 9.2 &&
                        player.getY() + player.getHeight() <= tileSize * 14) &&
                !(player.getX() + player.getWidth() >= tileSize * 2.2 &&
                        player.getX() + player.getWidth() <= tileSize * 2.4 &&
                        player.getY() + player.getHeight() >= tileSize * 13.2 &&
                        player.getY() + player.getHeight() <= tileSize * 14) &&
                !(player.getX() + player.getWidth() >= tileSize * 5.2 &&
                        player.getX() + player.getWidth() <= tileSize * 5.4 &&
                        player.getY() + player.getHeight() >= tileSize * 13.2 &&
                        player.getY() + player.getHeight() <= tileSize * 14)){
            return true;
        }
        return false;
    }

    public boolean moveLeft(Rectangle player){
        if (player.getX() >= -tileSize * 0.2 &&
                !(player.getX() >= drawer.getX() + drawer.getWidth() - tileSize * 0.4 &&
                        player.getX() <= drawer.getX() + drawer.getWidth() - tileSize * 0.2 &&
                        player.getY() + player.getHeight() >= drawer.getY() &&
                        player.getY() + player.getHeight() <= drawer.getY() + drawer.getHeight() - tileSize * 0.6) &&
                !(player.getX() >= tileSize * 7.8 &&
                        player.getX() <= tileSize * 8 &&
                        player.getY() + player.getHeight() >= tileSize * 0 &&
                        player.getY() + player.getHeight() <= tileSize * 7.2) &&
                !(player.getX() >= dinningTable.getX() + dinningTable.getWidth() - tileSize * 0.6 &&
                        player.getX() <= dinningTable.getX() + dinningTable.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= dinningTable.getY() + tileSize * 0.6 &&
                        player.getY() + player.getHeight() <= dinningTable.getY() + dinningTable.getHeight()) &&
                !(player.getX() >= storageBox.getX() + storageBox.getWidth() - tileSize * 0.4 &&
                        player.getX() <= storageBox.getX() + storageBox.getWidth() - tileSize * 0.2 &&
                        player.getY() + player.getHeight() >= storageBox.getY() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= storageBox.getY() + storageBox.getHeight()) &&
                !(player.getX() >= rightChair.getX() + rightChair.getWidth() - tileSize * 0.6 &&
                        player.getX() <= rightChair.getX() + rightChair.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= rightChair.getY() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= rightChair.getY() + rightChair.getHeight()) &&
                !(player.getX() >= leftChair.getX() + leftChair.getWidth() - tileSize * 0.6 &&
                        player.getX() <= leftChair.getX() + leftChair.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= leftChair.getY() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= leftChair.getY() + leftChair.getHeight()) &&
                !(player.getX() >= tileSize * 5.6 &&
                        player.getX() <= tileSize * 5.8 &&
                        player.getY() + player.getHeight() >= tileSize * 7.2 &&
                        player.getY() + player.getHeight() <= tileSize * 9.4) &&
                !(player.getX() >= kitchenRightChair.getX() + kitchenRightChair.getWidth() - tileSize * 0.6 &&
                        player.getX() <= kitchenRightChair.getX() + kitchenRightChair.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= kitchenRightChair.getY() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= kitchenRightChair.getY() + kitchenRightChair.getHeight()) &&
                !(player.getX() >= kitchenLeftChair.getX() + kitchenLeftChair.getWidth() - tileSize * 0.6 &&
                        player.getX() <= kitchenLeftChair.getX() + kitchenLeftChair.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= kitchenLeftChair.getY() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= kitchenLeftChair.getY() + kitchenLeftChair.getHeight()) &&
                !(player.getX() >= toolShelf.getX() + toolShelf.getWidth() - tileSize * 0.4 &&
                        player.getX() <= toolShelf.getX() + toolShelf.getWidth() - tileSize * 0.2 &&
                        player.getY() + player.getHeight() >= toolShelf.getY() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= toolShelf.getY() + toolShelf.getHeight()) &&
                !(player.getX() >= cabinet.getX() + cabinet.getWidth() - tileSize * 0.4 &&
                        player.getX() <= cabinet.getX() + cabinet.getWidth() - tileSize * 0.2 &&
                        player.getY() + player.getHeight() >= cabinet.getY() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= cabinet.getY() + cabinet.getHeight()) &&
                !(player.getX() >= bed.getX() + bed.getWidth() - tileSize * 0.6 &&
                        player.getX() <= bed.getX() + bed.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= bed.getY() + tileSize * 0.6 &&
                        player.getY() + player.getHeight() <= bed.getY() + bed.getHeight()) &&
                !(player.getX() >= tileSize * 7.8 &&
                        player.getX() <= tileSize * 8 &&
                        player.getY() + player.getHeight() >= tileSize * 9.2 &&
                        player.getY() + player.getHeight() <= tileSize * 14) &&
                !(player.getX() >= tileSize * 2.6 &&
                        player.getX() <= tileSize * 2.8 &&
                        player.getY() + player.getHeight() >= tileSize * 13.2 &&
                        player.getY() + player.getHeight() <= tileSize * 14) &&
                !(player.getX() >= tileSize * 5.6 &&
                        player.getX() <= tileSize * 5.8 &&
                        player.getY() + player.getHeight() >= tileSize * 13.2 &&
                        player.getY() + player.getHeight() <= tileSize * 14)){
            return true;
        }
        return false;
    }

    public boolean closeToBed(Rectangle player) {
        if (player.getX() + player.getWidth() / 2 >= tileSize &&
                player.getX() + player.getWidth() / 2 <= tileSize * 5 &&
                player.getY() + player.getHeight() <= tileSize * 13 &&
                player.getY() + player.getHeight() >= tileSize * 10) {
            return true;
        }
        return false;
    }

    public boolean closeToStorageBox(Rectangle player) {
        if (player.getX() + player.getWidth() / 2 >= tileSize * 9&&
                player.getX() + player.getWidth() / 2 <= tileSize * 15 &&
                player.getY() + player.getHeight() <= tileSize * 14 &&
                player.getY() + player.getHeight() >= tileSize * 11) {
            return true;
        }
        return false;
    }

    public boolean closeToToolShelf(Rectangle player) {
        if (player.getX() + player.getWidth() / 2 >= tileSize * 1 &&
                player.getX() + player.getWidth() / 2 <= tileSize * 4 &&
                player.getY() + player.getHeight() <= tileSize * 6 &&
                player.getY() + player.getHeight() >= tileSize * 4) {
            return true;
        }
        return false;
    }

    public boolean closeToTable(Rectangle player) {
        if (player.getX() + player.getWidth() / 2 >= tileSize * 1 &&
                player.getX() + player.getWidth() / 2 <= tileSize * 5 &&
                player.getY() + player.getHeight() <= tileSize * 9 &&
                player.getY() + player.getHeight() >= tileSize * 6) {
            return true;
        }
        return false;
    }

    public boolean inThekitchen(Rectangle player) {
        if (player.getX() + player.getWidth() / 2 >= tileSize * 3 &&
                player.getX() + player.getWidth() / 2 <= tileSize * 7 &&
                player.getY() + player.getHeight() <= tileSize * 10 &&
                player.getY() + player.getHeight() >= tileSize * 4) {
            return true;
        }
        return false;
    }

    public void takeFromStorageBox(Item choosedItem, Player player, boolean[] flags) {
        Take take = new Take();
        UI.showPopup(take.run(home.getStorageBox(), choosedItem, player), root);
        flags[2] = true;
    }

    public void putIntoStorageBox(Item choosedItem, Player player, boolean[] flags) {
        Put put = new Put();
        UI.showPopup(put.run(home.getStorageBox(), choosedItem, player), root);
        flags[2] = true;
    }


    public void putIntoToolShelf(Item choosedItem, Player player, boolean[] flags) {
        Put put = new Put();
        UI.showPopup(put.run(home.getKitchen().getToolshelf(), (Tool)choosedItem, player), root);
        flags[2] = true;
    }


    public void cook(Item choosedItem, Player player, boolean[] flags) {
       Cook cook = new Cook();
        UI.showPopup(cook.run((Food)choosedItem, player, home.getKitchen().getToolshelf()), root);
        flags[3] = true;
    }

    public void sleep(Player player, boolean[] flags, Game game, Date date) {
        NextDay nextDay = new NextDay();
        UI.showPopup(nextDay.run(game, date, player), root);
        flags[2] = true;
    }

    public void replace(Item newItem, Item oldItem, Player player, boolean[] flags) {
        Replace replace = new Replace();
        UI.showPopup(replace.run(newItem, oldItem, player, home.getKitchen().getToolshelf()), root);
        flags[4] = true;

    }

    public void remove(Item choosedItem, Player player, boolean[] flags) {
        player.getBackpack().getItem().add(choosedItem);
        home.getKitchen().getToolshelf().getExistingTools().remove(choosedItem);
        UI.showPopup(choosedItem.getName() + " was removed from toolShelf & was added to backpack!", root);

    }

}
