import java.io.Serializable;

public class Request implements Serializable {
    String req;
    Integer to = -1;
    Integer from = -1;
    Boolean accept;

    public Request(String req, Integer to, Integer from) {
        this.req = req;
        this.to = to;
        this.from = from;
    }

    public Request(String req, Integer to, Boolean accept, Integer from) {
        this.req = req;
        this.to = to;
        this.accept = accept;
        this.from = from;
    }

    public String getReq() {
        return req;
    }

    public Integer getTo() {
        return to;
    }

    public Boolean getAccept() {
        return accept;
    }

    public Integer getFrom() {
        return from;
    }


    public void setReq(String req) {
        this.req = req;
    }

    public void setTo(Integer to) {
        this.to = to;
    }

    public void setFrom(Integer from) {
        this.from = from;
    }

    public void setAccept(Boolean accept) {
        this.accept = accept;
    }
}
