import com.google.common.collect.HashMultiset;

import java.lang.reflect.Method;
import java.util.ArrayList;

public class AllOfShops {

    public Shop groceriesStore() throws IllegalAccessException, InstantiationException {
        HashMultiset goods = HashMultiset.<Item>create();
        Class fruitsClass = AllOfFruits.class;
        Object obj = fruitsClass.newInstance();
        Method[] fruits = fruitsClass.getDeclaredMethods();
        for(Method fruit : fruits) {
            try {
                goods.add(fruit.invoke(obj), (int)(Math.random() * 9) + 1);
            }
            catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        return new Shop(goods, "Groceries Store");
    }

    public Shop generalStore() throws IllegalAccessException, InstantiationException {
        HashMultiset goods = HashMultiset.<Item>create();
        Class productsClass = AllOfProducts.class;
        Object obj = productsClass.newInstance();
        Method[] products = productsClass.getDeclaredMethods();
        for(Method method : products) {
            try {
                Product product = (Product)method.invoke(obj);
                if(product.getType().equals("Product"))
                    goods.add(product, (int)(Math.random() * 5) + 1);
            }
            catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }

        Class seedsClass = AllOfSeeds.class;
        Object obj1 = seedsClass.newInstance();
        Method[] methods1 = seedsClass.getDeclaredMethods();
        for(Method method : methods1) {
            try {
                Seed seed = (Seed) method.invoke(obj1);
                goods.add(seed, (int)(Math.random() * 9) + 1);
            }
            catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }


        return new Shop(goods, "General Store");
    }

    public Shop butchery() throws IllegalAccessException, InstantiationException {
        HashMultiset goods = HashMultiset.<Item>create();
        Class animalProductsClass = AllOfAnimalProducts.class;
        Object obj = animalProductsClass.newInstance();
        Method[] products = animalProductsClass.getDeclaredMethods();
        for(Method product : products) {
            try {
                AnimalProduct animalProduct = (AnimalProduct)product.invoke(obj);
                if(animalProduct.getName().contains("Meat"))
                    goods.add(animalProduct, (int)(Math.random() * 5) + 1);
            }
            catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        return new Shop(goods, "Butchery");
    }

    public Cafe cafe() throws IllegalAccessException, InstantiationException {
        HashMultiset goods = HashMultiset.<Item>create();
        Class drinksClass = AllOfDrinks.class;
        Object obj = drinksClass.newInstance();
        Method[] drinks = drinksClass.getDeclaredMethods();
        for(Method drink : drinks) {
            try {
                goods.add(drink.invoke(obj), (int)(Math.random() * 9) + 1);
            }
            catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        AllOfMissions allOfMissions = new AllOfMissions();
        ArrayList<Mission> availabeMissions = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            availabeMissions.add(allOfMissions.generator());
        }
        Board board = new Board(new ArrayList<Mission>(), availabeMissions, new ArrayList<Mission>(), new ArrayList<Task>());
        return new Cafe(goods, board);
    }

    public Ranch ranch() throws IllegalAccessException, InstantiationException {
        HashMultiset goods = HashMultiset.<Item>create();
        Class productClass = AllOfProducts.class;
        Object obj = productClass.newInstance();
        Method[] methods = productClass.getDeclaredMethods();
        for(Method method : methods) {
            try {
                Product product = (Product)method.invoke(obj);
                if(product.getType().equals("AnimalFood"))
                    goods.add(product, (int)(Math.random() * 9) + 1);
            }
            catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }

        Class medicineClass = AllOfMedicines.class;
        Object obj1 = medicineClass.newInstance();
        Method[] methods1 = medicineClass.getDeclaredMethods();
        for(Method method : methods1) {
            try {
                Medicine medicine = (Medicine)method.invoke(obj1);
                if(medicine.getType().equals("Animal Medicine"))
                    goods.add(medicine, (int)(Math.random() * 9) + 1);
            }
            catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }

        HashMultiset animals = HashMultiset.<Animal>create();
        Class animalClass = AllOfAnimals.class;
        Object obj2 = animalClass.newInstance();
        Method[] methods2 = animalClass.getDeclaredMethods();
        for(Method method : methods2) {
            try {
                Animal animal = (Animal)method.invoke(obj2);
                if(!animal.getType().equals("Fish"))
                    animals.add(animal, (int)(Math.random() * 3) + 1);
            }
            catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        return new Ranch(goods, animals);
    }

    public Clinic clinic() throws IllegalAccessException, InstantiationException {
        HashMultiset goods = HashMultiset.<Item>create();
        Class medicineClass = AllOfMedicines.class;
        Object obj = medicineClass.newInstance();
        Method[] medicines = medicineClass.getDeclaredMethods();
        for(Method medicine : medicines) {
            try {
                goods.add(medicine.invoke(obj), (int)(Math.random() * 9) + 1);
            }
            catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        return new Clinic(goods);
    }

    public Workshop workshop() throws IllegalAccessException, InstantiationException {
        HashMultiset<Item> goods = HashMultiset.create();
        Class toolClass = AllOfTools.class;
        Object obj = toolClass.newInstance();
        Method[] tools = toolClass.getDeclaredMethods();
        for(Method tool : tools) {
            try {
                Tool machine = (Tool) tool.invoke(obj);
                if(!machine.getType().equals("Machine"))
                    goods.add((Tool)tool.invoke(obj));
            }
            catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        return new Workshop(goods);
    }

    public Laboratory laboratory() throws IllegalAccessException, InstantiationException {
        HashMultiset goods = HashMultiset.<Item>create();
        ArrayList<Tool> machines = new ArrayList<>();
        Class toolClass = AllOfTools.class;
        Object obj = toolClass.newInstance();
        Method[] tools = toolClass.getDeclaredMethods();
        for(Method tool : tools) {
            try {
                Tool machine = (Tool) tool.invoke(obj);
                if(machine.getType().equals("Machine")) {
                    goods.add(machine, (int) (Math.random() * 9) + 1);
                    machines.add(machine);
                }
            }
            catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        return new Laboratory(goods, machines);
    }





}
