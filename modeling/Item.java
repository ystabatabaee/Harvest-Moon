

import javafx.scene.image.Image;

import java.util.ArrayList;
import java.util.HashMap;


public class Item {

    private boolean valid;
    private String type;
    private HashMap<String, Double> featureChangeRate;
    private double capacity;
    private String name;
    private int useCount;
    private Probability invalid;
    private double price;
    private ArrayList<Task> task;
    private int maxLevel;
    private int level;
    private boolean eatable;
    private boolean accumulation;
    private Dissassemblity dissassemblity;
    private Image picture;

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public HashMap<String, Double> getFeatureChangeRate() {
        return featureChangeRate;
    }

    public void setFeatureChangeRate(HashMap<String, Double> featureChangeRate) {
        this.featureChangeRate = featureChangeRate;
    }

    public double getCapacity() {
        return capacity;
    }

    public void setCapacity(double capacity) {
        this.capacity = capacity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getUseCount() {
        return useCount;
    }

    public void setUseCount(int useCount) {
        this.useCount = useCount;
    }

    public Probability getInvalid() {
        return invalid;
    }

    public void setInvalid(Probability invalid) {
        this.invalid = invalid;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public ArrayList<Task> getTask() {
        return task;
    }

    public void setTask(ArrayList<Task> task) {
        this.task = task;
    }

    public int getMaxLevel() {
        return maxLevel;
    }

    public void setMaxLevel(int maxLevel) {
        this.maxLevel = maxLevel;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public boolean isEatable() {
        return eatable;
    }

    public void setEatable(boolean eatable) {
        this.eatable = eatable;
    }

    public Dissassemblity getDissassemblity() {
        return dissassemblity;
    }

    public void setDissassemblity(Dissassemblity dissassemblity) {
        this.dissassemblity = dissassemblity;
    }

    public boolean isAccumulation() {
        return accumulation;
    }

    public void setAccumulation(boolean accumulation) {
        this.accumulation = accumulation;
    }

    public Image getPicture() {
        return picture;
    }

    public void setPicture(Image picture) {
        this.picture = picture;
    }

    public Item(String type, HashMap<String, Double> featureChangeRate, double capacity, String name,
                Probability invalid, double price, ArrayList<Task> task, int maxLevel, int level,
                boolean eatable, boolean accumulation, Dissassemblity dissassemblity, Image picture) {
        this.valid = true;
        this.type = type;
        this.featureChangeRate = featureChangeRate;
        this.capacity = capacity;
        this.name = name;
        this.useCount = 0;
        this.invalid = invalid;
        this.price = price;
        this.task = task;
        this.maxLevel = maxLevel;
        this.level = level;
        this.eatable = eatable;
        this.accumulation = accumulation;
        this.dissassemblity = dissassemblity;
        this.picture = picture;
    }

    public Item() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Item)) return false;

        Item item = (Item) o;

        if (level != item.level) return false;
        return name.equals(item.name);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + level;
        return result;
    }

    public Item clone() {
        Item clonedItem = new Item(type, featureChangeRate, capacity, name, invalid, price, task, maxLevel, level,
                eatable, accumulation, dissassemblity, picture);
        return clonedItem;
    }
}