import javafx.scene.paint.ImagePattern;

import java.util.HashMap;

public class Make extends Task {
    public Make() {
        super.name = "Make";
        super.featureChangeRate = new HashMap<String, Double>();
        super.executionTime = new Date();
    }

    public String  run(Tool tool, Player player, Building building, Barn barn, BarnUI barnUI) {
        player.setMoney(player.getMoney() - tool.getBuildingPrice());
        if (building instanceof Workshop) {
            player.getBackpack().getItem().add(tool);
            ((Workshop) building).getTool().remove(tool);
        }
        else if (building instanceof Laboratory && (tool.getName().contains("Cheese") || tool.getName().contains("Tomato") || tool.getName().contains("Wheel"))) {
            int x = barn.getMachines().size() % 3;
            int y = barn.getMachines().size() / 3;
            barn.getMachines().add((Machine) tool);
            barnUI.getMachineG()[x][y].setFill(new ImagePattern(tool.getPicture()));
            ((Laboratory) building).getMachine().remove(tool);
        }
        else if (building instanceof Laboratory) {
            player.getBackpack().getItem().add(tool);
            ((Laboratory) building).getMachine().remove(tool);
        }
        for (Item item : tool.getDissassemblity().getInitialIngredients().elementSet()) {
            player.getBackpack().getItem().remove(item, tool.getDissassemblity().getInitialIngredients().count(item));
        }
        UI.missionHandler(name, tool.getName());
        return tool.getName() + " was made successfully!";
    }
}
