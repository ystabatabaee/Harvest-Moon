public class Probability {

    private double rate;
    private double probability;

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public double getProbability() {
        //this.probability = Math.exp(rate) / 2;
        return this.probability;
    }

    public void setProbability(double... var) {
        if (var.length == 1)
            this.probability = var[0];
    }

}