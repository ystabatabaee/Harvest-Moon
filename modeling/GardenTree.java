import javafx.scene.image.Image;

import java.util.*;

public class GardenTree {

    private String type;
    private Weather weather;
    private ArrayList<Integer> fruitProduction;
    private boolean free;
    private double price;
    private boolean watered;
    private Fruit fruit;
    private ArrayList<Fruit> availableFruits;
    private Image picture;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Weather getWeather() {
        return weather;
    }

    public void setWeather(Weather weather) {
        this.weather = weather;
    }

    public ArrayList<Integer> getFruitProduction() {
        return fruitProduction;
    }

    public void setFruitProduction(ArrayList<Integer> fruitProduction) {
        this.fruitProduction = fruitProduction;
    }

    public boolean isFree() {
        return free;
    }

    public void setFree(boolean free) {
        this.free = free;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean isWatered() {
        return watered;
    }

    public void setWatered(boolean watered) {
        this.watered = watered;
    }

    public Fruit getFruit() {
        return fruit;
    }

    public void setFruit(Fruit fruit) {
        this.fruit = fruit;
    }

    public ArrayList<Fruit> getAvailableFruits() {
        return availableFruits;
    }

    public void setAvailableFruits(ArrayList<Fruit> availableFruits) {
        this.availableFruits = availableFruits;
    }

    public Image getPicture() {
        return picture;
    }

    public void setPicture(Image picture) {
        this.picture = picture;
    }


    public int fruitPro() {
        Random random = new Random(new java.util.Date().getTime());
        int index = random.nextInt() % 2;
        return fruitProduction.get(index);
    }

    public GardenTree(String type, Weather weather, ArrayList<Integer> fruitProduction, Fruit fruit) {
        this.type = type;
        this.weather = weather;
        this.fruitProduction = fruitProduction;
        availableFruits = new ArrayList<>();
        for (int i = 0; i < fruitProduction.get(0); i++) {
            availableFruits.add(fruit);
        }
        this.fruit = fruit;
        this.price = (int)(Math.random() * 600);
        picture = new Image("pics/tree/" + type.toLowerCase() + ".png");

    }

    public GardenTree(String type, Weather weather, ArrayList<Integer> fruitProduction, Fruit fruit, double price, Image picture) {
        this.type = type;
        this.weather = weather;
        this.fruitProduction = fruitProduction;
        availableFruits = new ArrayList<>();
        for (int i = 0; i < fruitProduction.get(0); i++) {
            availableFruits.add(fruit);
        }
        this.fruit = fruit;
        this.price = price;
        this.picture = picture;
    }

    public GardenTree clone(){
        return new GardenTree(type, weather, fruitProduction, fruit, price, picture);
    }
}