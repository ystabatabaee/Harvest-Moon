import com.google.common.collect.HashMultiset;

public class Clinic extends Shop {
    public Clinic(boolean isRuin, Probability ruin, HashMultiset repairObjects, double repairPrice, HashMultiset goods) {
        super(isRuin, ruin, repairObjects, repairPrice, goods, "Clinic");
    }

    public Clinic(HashMultiset goods) {
        super(goods, "Clinic");
    }
}