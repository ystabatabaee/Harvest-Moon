import com.google.common.collect.HashMultiset;

import java.util.ArrayList;

public class Building implements Place {

    private boolean isRuin;
    private Probability ruin;
    private ArrayList<Task> task;
    private HashMultiset repairObjects;
    private double repairPrice;

    public boolean isRuin() {
        return isRuin;
    }

    public void setIsRuin(boolean ruin) {
        isRuin = ruin;
    }

    public Probability getRuin() {
        return ruin;
    }

    public void setRuin(Probability ruin) {
        this.ruin = ruin;
    }

    public ArrayList<Task> getTask() {
        return task;
    }

    public void setTask(ArrayList<Task> task) {
        this.task = task;
    }

    public HashMultiset getRepairObjects() {
        return repairObjects;
    }

    public void setRepairObjects(HashMultiset repairObjects) {
        this.repairObjects = repairObjects;
    }

    public double getRepairPrice() {
        return repairPrice;
    }

    public void setRepairPrice(double repairPrice) {
        this.repairPrice = repairPrice;
    }

    public Building(boolean isRuin, Probability ruin, HashMultiset repairObjects, double repairPrice) {
        this.isRuin = isRuin;
        this.ruin = ruin;
        this.repairObjects = repairObjects;
        this.repairPrice = repairPrice;
    }

    public Building(){
    }
}