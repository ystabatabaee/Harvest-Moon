import com.google.common.collect.HashMultiset;

import java.util.ArrayList;

public class AllOfMissions {

    static int id = 0;
    ArrayList<String> nameOfTasks;
    ArrayList<String> nameOfMissions;


    public AllOfMissions() {
        ArrayList<Tetrad> tetrad = new ArrayList<>();
        HashMultiset<Object> price = HashMultiset.create();
        HashMultiset<Object> input = HashMultiset.create();
        this.nameOfTasks = new ArrayList<>();
        //this.nameOfObjects = new ArrayList<>();
        this.nameOfMissions = new ArrayList<>();
        nameOfTasks.add("Buy");
        nameOfTasks.add("Collect");
        nameOfTasks.add("Cook");
        nameOfTasks.add("Disassemble");
        nameOfTasks.add("Drop");
        nameOfTasks.add("Faint");
        nameOfTasks.add("Feed");
        nameOfTasks.add("Fill");
        nameOfTasks.add("Fishing");
        nameOfTasks.add("GetProduct");
        nameOfTasks.add("Harvest");
        nameOfTasks.add("HealUp");
        nameOfTasks.add("Make");
        nameOfTasks.add("Plant Seeds");
        nameOfTasks.add("Plow");
        nameOfTasks.add("Put");
        nameOfTasks.add("Sell");
        nameOfTasks.add("Take");
        nameOfTasks.add("Use");
        nameOfTasks.add("Move Right");
        nameOfTasks.add("Move Left");
        nameOfTasks.add("Move Up");
        nameOfTasks.add("Move Down");

        nameOfMissions.add("Happy");
        nameOfMissions.add("Sad");
        nameOfMissions.add("Bear");
        nameOfMissions.add("Snake");
        nameOfMissions.add("Something");
        nameOfMissions.add("Board");
        nameOfMissions.add("Mission");
        nameOfMissions.add("Onion");
        nameOfMissions.add("Confuse");
        nameOfMissions.add("Lover");
        nameOfMissions.add("Walk");


    }

    public Mission stoneLover() {
        ArrayList<Tetrad> tetrad = new ArrayList<>();
        HashMultiset<Object> price = HashMultiset.create();
        HashMultiset<Object> input = HashMultiset.create();
        tetrad.clear();
        price.clear();
        input.clear();
        tetrad.add(new Tetrad("Collect", "Stone", 10));
        price.add(new AllOfFoods().mirzaGhasemi());
        return new Mission("Stone Lover", tetrad, price, 40, new Time(24, 0, 0), input);
    }

    public Mission walkRight() {
        ArrayList<Tetrad> tetrad = new ArrayList<>();
        HashMultiset<Object> price = HashMultiset.create();
        HashMultiset<Object> input = HashMultiset.create();
        tetrad.clear();
        price.clear();
        input.clear();
        tetrad.add(new Tetrad("Move Right", "Player", 50));
        price.add(new AllOfCrops().eggplant(), 5);
        price.add(new AllOfCrops().onion(), 2);
        return new Mission("Walk Right", tetrad, price, 100, new Time(5, 0, 0), input);
    }

    public Mission plowww() {
        ArrayList<Tetrad> tetrad = new ArrayList<>();
        HashMultiset<Object> price = HashMultiset.create();
        HashMultiset<Object> input = HashMultiset.create();
        tetrad.clear();
        price.clear();
        input.clear();
        tetrad.add(new Tetrad("Plow", "Shovel", 5));
        price.add(new AllOfFruits().peach(), 8);
        price.add(new AllOfFruits().strawberry(), 5);
        input.add(new AllOfFruits().peach());
        return new Mission("Plowww", tetrad, price, 150, new Time(72, 0, 0), input);
    }

    public Mission generator() {
        ArrayList<Tetrad> tetrad = new ArrayList<>();
        HashMultiset<Object> price = HashMultiset.create();
        HashMultiset<Object> input = HashMultiset.create();
        tetrad.clear();
        price.clear();
        input.clear();
        String tetradName = "";
        String tetradObjectName = "";
        int count = 0;
        StringBuilder missionName = new StringBuilder();
        int contractFee = 0;
        int hour = 0;
        int minute = 0;
        int second = 0;
        int random = 0;
        int randomC = (int) (Math.random() * 4) + 1;
        for (int i = 0; i < randomC; i++) {
            random = (int) (Math.random() * nameOfTasks.size());
            tetradName = nameOfTasks.get(random);
            if (nameOfTasks.get(random).equals("Buy")) {
                random = (int) (Math.random() * 6);
                if (random == 0) {
                    random = (int) (Math.random() * UI.fruits.size());
                    tetradObjectName = UI.fruits.get(random).getName();
                    random = (int) (Math.random() * 10) + 1;
                    count = random;
                } else if (random == 1) {
                    random = (int) (Math.random() * UI.products.size());
                    tetradObjectName = UI.products.get(random).getName();
                    random = (int) (Math.random() * 6) + 1;
                    count = random;
                } else if (random == 2) {
                    random = (int) (Math.random() * UI.foods.size());
                    tetradObjectName = UI.foods.get(random).getName();
                    random = (int) (Math.random() * 3) + 1;
                    count = random;
                } else if (random == 3) {
                    random = (int) (Math.random() * UI.seeds.size());
                    tetradObjectName = UI.seeds.get(random).getName();
                    random = (int) (Math.random() * 10) + 1;
                    count = random;
                } else if (random == 4) {
                    random = (int) (Math.random() * UI.gardenTrees.size());
                    tetradObjectName = UI.gardenTrees.get(random).getType() + "Tree";
                    random = (int) (Math.random() + 1);
                    count = random;
                } else if (random == 5) {
                    random = (int) (Math.random() * UI.animals.size());
                    tetradObjectName = UI.animals.get(random).getType();
                    random = (int) (Math.random() + 1);
                    count = random;
                }
            } else if (nameOfTasks.get(random).equals("Collect")) {
                random = (int) (Math.random() * 2);
                if (random == 0) {
                    random = (int) (Math.random() * UI.fruits.size());
                    tetradObjectName = UI.fruits.get(random).getName();
                    random = (int) (Math.random() * 10) + 1;
                    count = random;
                } else if (random == 1) {
                    random = (int) (Math.random() * UI.minerals.size());
                    tetradObjectName = UI.minerals.get(random).getName();
                    random = (int) (Math.random() * 6) + 1;
                    count = random;
                }
            } else if (nameOfTasks.get(random).equals("Cook")) {
                random = (int) (Math.random() * UI.foods.size());
                tetradObjectName = UI.foods.get(random).getName();
                random = (int) (Math.random() * 3) + 1;
                count = random;
            } else if (nameOfTasks.get(random).equals("Disassemble")) {
                random = (int) (Math.random() * UI.tools.size());
                tetradObjectName = UI.tools.get(random).getName();
                random = (int) (Math.random() * 3) + 1;
                count = random;
            } else if (nameOfTasks.get(random).equals("Drop")) {
                random = (int) (Math.random() * UI.items.size());
                tetradObjectName = UI.items.get(random).getName();
                random = (int) (Math.random() * 6) + 1;
                count = random;
            } else if (nameOfTasks.get(random).equals("Faint")) {
                tetradObjectName = "Player";
                random = (int) (Math.random() * 3) + 1;
                count = random;
            } else if (nameOfTasks.get(random).equals("Feed")) {
                random = (int) (Math.random() * UI.animals.size());
                tetradObjectName = UI.animals.get(random).getType();
                random = (int) (Math.random() * 4) + 1;
                count = random;
            } else if (nameOfTasks.get(random).equals("Fill")) {
                tetradObjectName = "Watering Can";
                random = (int) (Math.random() * 15) + 1;
                count = random;
            } else if (nameOfTasks.get(random).equals("Fishing")) {
                tetradObjectName = "Fishing Rod";
                random = (int) (Math.random() * 8) + 1;
                count = random;
            } else if (nameOfTasks.get(random).equals("GetProduct")) {
                random = (int) (Math.random() * UI.animalProducts.size());
                tetradObjectName = UI.animalProducts.get(random).getName();
                random = (int) (Math.random() * 6) + 1;
                count = random;
            } else if (nameOfTasks.get(random).equals("Harvest")) {
                random = (int) (Math.random() * UI.crops.size());
                tetradObjectName = UI.crops.get(random).getType();
                random = (int) (Math.random() * 7) + 1;
                count = random;
            } else if (nameOfTasks.get(random).equals("HealUp")) {
                tetradObjectName = "Player";
                random = (int) (Math.random() * 2) + 1;
                count = random;
            } else if (nameOfTasks.get(random).equals("Make")) {
                random = (int) (Math.random() * UI.tools.size());
                tetradObjectName = UI.tools.get(random).getType();
                random = (int) (Math.random() * 2) + 1;
                count = random;
            } else if (nameOfTasks.get(random).equals("Plant Seeds")) {
                random = (int) (Math.random() * UI.seeds.size());
                tetradObjectName = UI.seeds.get(random).getType();
                random = (int) (Math.random() * 4) + 1;
                count = random;
            } else if (nameOfTasks.get(random).equals("Plow")) {
                tetradObjectName = "Shovel";
                random = (int) (Math.random() * 7) + 1;
                count = random;
            } else if (nameOfTasks.get(random).equals("Put")) {
                random = (int) (Math.random() * 3);
                if (random == 1)
                    tetradObjectName = "Backpack";
                else if (random == 2)
                    tetradObjectName = "Storage Box";
                else if (random == 3)
                    tetradObjectName = "Tool Shelf";
                random = (int) (Math.random() * 3) + 1;
                count = random;
            } else if (nameOfTasks.get(random).equals("Sell")) {
                random = (int) (Math.random() * 3);
                if (random == 0) {
                    random = (int) (Math.random() * UI.fruits.size());
                    tetradObjectName = UI.fruits.get(random).getName();
                    random = (int) (Math.random() * 10) + 1;
                    count = random;
                } else if (random == 1) {
                    random = (int) (Math.random() * UI.products.size());
                    tetradObjectName = UI.products.get(random).getName();
                    random = (int) (Math.random() * 6) + 1;
                    count = random;
                } else if (random == 2) {
                    random = (int) (Math.random() * UI.seeds.size());
                    tetradObjectName = UI.seeds.get(random).getName();
                    random = (int) (Math.random() * 10) + 1;
                    count = random;
                }
            } else if (nameOfTasks.get(random).equals("Take")) {
                random = (int) (Math.random() * 3);
                if (random == 1)
                    tetradObjectName = "Backpack";
                else if (random == 2)
                    tetradObjectName = "Storage Box";
                else if (random == 3)
                    tetradObjectName = "Tool Shelf";
                random = (int) (Math.random() * 3) + 1;
                count = random;
            } else if (nameOfTasks.get(random).equals("Use")) {
                random = (int) (Math.random() * 2);
                if (random == 0) {
                    random = (int) (Math.random() * UI.fruits.size());
                    tetradObjectName = UI.fruits.get(random).getName();
                    random = (int) (Math.random() * 10) + 1;
                    count = random;
                } else if (random == 1) {
                    random = (int) (Math.random() * UI.foods.size());
                    tetradObjectName = UI.foods.get(random).getName();
                    random = (int) (Math.random() * 6) + 1;
                    count = random;
                }
            } else if (nameOfTasks.get(random).contains("Move")) {
                tetradObjectName = "Player";
                random = (int) (Math.random() * 100) + 1;
                count = random;
            }
            Tetrad tetrad1 = new Tetrad(tetradName, tetradObjectName, count);
            tetrad.add(tetrad1);
        }
        random = (int) (Math.random() * 3);
        if (random == 0) {
            missionName.append("?");
        } else if (random == 1) {
            random = (int) (Math.random() * nameOfMissions.size());
            missionName.append(nameOfMissions.get(random));
        } else if (random == 2) {
            random = (int) (Math.random() * nameOfMissions.size());
            missionName.append(nameOfMissions.get(random));
            missionName.append(" ");
            random = (int) (Math.random() * nameOfMissions.size());
            missionName.append(nameOfMissions.get(random));
        }
        random = (int) (Math.random() * 500) + 31;
        contractFee = random;
        random = (int) (Math.random() * 81) + 10;
        hour = random;
        random = (int) (Math.random() * 60);
        minute = random;
        random = (int) (Math.random() * 60);
        second = random;
        randomC = (int) (Math.random() * 3) + 1;
        for (int i = 0; i < randomC; i++) {
            random = (int) (Math.random() * UI.items.size());
            input.add(UI.items.get(random), (int) (Math.random() * 3 + 1));
        }

        randomC = (int) (Math.random() * 3) + 1;
        for (int i = 0; i < randomC; i++) {
            random = (int) (Math.random() * 5);
            if (random == 0) {
                random = (int) (Math.random() * UI.fruits.size());
                input.add(UI.items.get(random), (int) (Math.random() * 2 + 1));
            } else if (random == 1) {
                random = (int) (Math.random() * UI.products.size());
                input.add(UI.items.get(random), (int) (Math.random() * 2 + 1));
            } else if (random == 2) {
                random = (int) (Math.random() * UI.foods.size());
                input.add(UI.items.get(random), (int) (Math.random() * 2 + 1));
            } else if (random == 3) {
                random = (int) (Math.random() * UI.seeds.size());
                input.add(UI.items.get(random), (int) (Math.random() * 2 + 1));
            } else if (random == 4) {
                random = (int) (Math.random() * UI.items.size());
                input.add(UI.items.get(random), (int) (Math.random() * 2 + 1));
            } else if (random == 5) {
                random = (int) (Math.random() * 400) + 200;
                input.add(random);
            }
        }


        randomC = (int) (Math.random() * 5) + 1;
        for (int i = 0; i < randomC; i++) {
            random = (int) (Math.random() * 5);
            if (random == 0) {
                random = (int) (Math.random() * UI.fruits.size());
                price.add(UI.items.get(random), (int) (Math.random() * 2 + 1));
            } else if (random == 1) {
                random = (int) (Math.random() * UI.products.size());
                price.add(UI.items.get(random), (int) (Math.random() * 2 + 1));
            } else if (random == 2) {
                random = (int) (Math.random() * UI.foods.size());
                price.add(UI.items.get(random), (int) (Math.random() * 2 + 1));
            } else if (random == 3) {
                random = (int) (Math.random() * UI.seeds.size());
                price.add(UI.items.get(random), (int) (Math.random() * 2 + 1));
            } else if (random == 4) {
                random = (int) (Math.random() * UI.items.size());
                price.add(UI.items.get(random), (int) (Math.random() * 2 + 1));
            }
        }
        missionName.append(" ");
        missionName.append(id);
        id++;
        return new Mission(missionName.toString(), tetrad, price, contractFee, new Time(hour, minute, second), input);
    }
}
