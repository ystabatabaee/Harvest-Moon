import java.util.ArrayList;

public class AllOfCrops {

    AllOFWeathers allOFWeathers = new AllOFWeathers();
    AllOfFruits allOfFruits = new AllOfFruits();

    public Crop tomato() {
        return new Crop("Tomato", allOFWeathers.fall(), 3, 10 , 3, new ArrayList<Task>(){{}}, allOfFruits.tomato());
    }

    public Crop garlic() {
        return new Crop("Garlic", allOFWeathers.spring(), 1, 2 , 1, new ArrayList<Task>(){{}}, allOfFruits.garlic());
    }

    public Crop pea() {
        return new Crop("Pea", allOFWeathers.spring(), 1, 4 , 3, new ArrayList<Task>(){{}}, allOfFruits.pea());
    }

    public Crop lettuce() {
        return new Crop("Lettuce", allOFWeathers.spring(), 2, 3 , 1, new ArrayList<Task>(){{}}, allOfFruits.lettuce());
    }

    public Crop eggplant() {
        return new Crop("Eggplant", allOFWeathers.spring(), 2, 7 , 3, new ArrayList<Task>(){{}}, allOfFruits.eggplant());
    }

    public Crop potato() {
        return new Crop("Potato", allOFWeathers.fall(), 2, 3, 1, new ArrayList<Task>(){{}}, allOfFruits.potato());
    }

    public Crop carrot() {
        return new Crop("Carrot", allOFWeathers.fall(), 2, 3 , 1, new ArrayList<Task>(){{}}, allOfFruits.carrot());
    }

    public Crop melon() {
        return new Crop("Melon", allOFWeathers.fall(), 1, 4 , 3, new ArrayList<Task>(){{}}, allOfFruits.melon());
    }

    public Crop cucumber() {
        return new Crop("Cucumber", allOFWeathers.summer() , 3, 10, 3, new ArrayList<Task>(){{}}, allOfFruits.cucumber());
    }

    public Crop watermelon() {
        return new Crop("Watermelon", allOFWeathers.summer() , 1, 4, 3, new ArrayList<Task>(){{}}, allOfFruits.watermelon());
    }

    public Crop onion() {
        return new Crop("Onion", allOFWeathers.summer() , 2, 3, 1, new ArrayList<Task>(){{}}, allOfFruits.onion());
    }

    public Crop turnip() {
        return new Crop("Turnip", allOFWeathers.summer() , 3, 4, 1, new ArrayList<Task>(){{}}, allOfFruits.turnip());
    }

    public Crop pineapple() {
        return new Crop("Pineapple", allOFWeathers.tropical() , 1, 4, 3, new ArrayList<Task>(){{}}, allOfFruits.pineapple());
    }

    public Crop strawberry() {
        return new Crop("Strawberry", allOFWeathers.tropical() , 1, 4, 3, new ArrayList<Task>(){{}}, allOfFruits.strawberry());
    }

    public Crop capsicum() {
        return new Crop("Capsicum", allOFWeathers.tropical() , 1, 4, 3, new ArrayList<Task>(){{}}, allOfFruits.capsicum());
    }

}
