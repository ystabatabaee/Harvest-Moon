import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

import java.lang.reflect.InvocationTargetException;



public class CafeUI implements Mover {
    Group root = new Group();
    static double tileSize = 50;
    private static Rectangle cabinet;
    private static Rectangle coffee;
    private static Rectangle crack;
    private static Rectangle door;
    private static Rectangle piano;
    private static Rectangle dinningTable1;
    private static Rectangle dinningTable2;
    private static Rectangle missionBoard;
    private static Rectangle leftChair1;
    private static Rectangle leftChair2;
    private static Rectangle rightChair1;
    private static Rectangle rightChair2;

    private Rectangle rectangleBuilder(double width, double height, Paint fill, double setX, double setY) {
        Rectangle rectangle = new Rectangle(width, height, fill);
        rectangle.setX(setX);
        rectangle.setY(setY);
        return rectangle;
    }

    public CafeUI(Group root) {
        this.root = root;
    }

    public Scene sceneBuilder(Stage primaryStage) throws IllegalAccessException, InstantiationException, InvocationTargetException {
        Scene cafeScene = new Scene(root, 1000, 700, Color.BLACK);
        Image cafePic = new Image("pics/maps/cafeMap.png");
        Image cabinetPic = new Image("pics/cafeCabinet.png");
        Image coffeePic = new Image("pics/coffee.png");
        Image crackPic = new Image("pics/crack.png");
        Image doorPic = new Image("pics/doors/cafeDoor.png");
        Image pianoPic = new Image("pics/piano.png");
        Image dinningTablePic = new Image("pics/dinningTable.png");
        Image flowerPic1 = new Image("pics/flowerPot1.png");
        Image flowerPic2 = new Image("pics/flowerPot2.png");
        Image leftChairPic = new Image("pics/leftChair.png");
        Image rightChairPic = new Image("pics/rightChair.png");
        Image missionBoardPic = new Image("pics/missionBoard.png");

        Rectangle rectangle = new Rectangle(1000, 700, new ImagePattern(cafePic));
        root.getChildren().add(rectangle);

        cabinet = rectangleBuilder(tileSize * 2, tileSize * 2, new ImagePattern(cabinetPic), tileSize * 2, tileSize);
        coffee = rectangleBuilder(tileSize * 2, tileSize * 2, new ImagePattern(coffeePic), tileSize * 5, tileSize);
        crack = rectangleBuilder(tileSize * 2, tileSize * 2, new ImagePattern(crackPic), tileSize * 7, tileSize);
        door = rectangleBuilder(tileSize, tileSize * 2, new ImagePattern(doorPic), 1000 - tileSize * 2, 0);
        piano = rectangleBuilder(tileSize * 3, tileSize * 3, new ImagePattern(pianoPic), 13 * tileSize, tileSize * 3);
        dinningTable1 = rectangleBuilder(tileSize * 2, tileSize * 3, new ImagePattern(dinningTablePic), tileSize * 2, tileSize * 4);
        dinningTable2 = rectangleBuilder(tileSize * 2, tileSize * 3, new ImagePattern(dinningTablePic), tileSize * 2, tileSize * 10);
        missionBoard = rectangleBuilder(tileSize * 3, tileSize * 2, new ImagePattern(missionBoardPic), tileSize * 10, tileSize);
        root.getChildren().addAll(cabinet, coffee, crack, door, piano, dinningTable1, dinningTable2, missionBoard);

        for (int i = 0; i < 4; i++) {
            Rectangle flower1 = rectangleBuilder(tileSize, tileSize * 2, new ImagePattern(flowerPic1), 1000 - tileSize * 2, tileSize * (3 * (i + 1)));
            root.getChildren().add(flower1);
        }
        for (int i = 0; i < 4; i++) {
            Rectangle flower2 = rectangleBuilder(tileSize, tileSize * 2, new ImagePattern(flowerPic2), tileSize * (2 * i + 1), tileSize * 7);
            root.getChildren().add(flower2);
        }

        leftChair1 = rectangleBuilder(tileSize, tileSize, new ImagePattern(leftChairPic), tileSize, tileSize * 5);
        leftChair2 = rectangleBuilder(tileSize, tileSize, new ImagePattern(leftChairPic), tileSize, tileSize * 11);
        rightChair1 = rectangleBuilder(tileSize, tileSize, new ImagePattern(rightChairPic), tileSize * 4, tileSize * 5);
        rightChair2 = rectangleBuilder(tileSize, tileSize, new ImagePattern(rightChairPic), tileSize * 4, tileSize * 11);
        root.getChildren().addAll(leftChair1, leftChair2, rightChair1, rightChair2);

        return cafeScene;
    }

    public boolean moveUp(Rectangle player) {
        if (player.getY() + player.getHeight() >= tileSize * 2.4 &&
                !(player.getX() + player.getWidth() >= piano.getX() + tileSize * 1 &&
                        player.getX() <= piano.getX() + piano.getWidth() - tileSize * 1 &&
                        player.getY() + player.getHeight() >= piano.getY() + piano.getHeight() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() <= piano.getY() + piano.getHeight() - tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= missionBoard.getX() + tileSize * 0.6 &&
                        player.getX() <= missionBoard.getX() + missionBoard.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= missionBoard.getY() + missionBoard.getHeight() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= missionBoard.getY() + missionBoard.getHeight() + tileSize * 0.4) &&
                !(player.getX() + player.getWidth() >= coffee.getX() + tileSize * 0.8 &&
                        player.getX() <= coffee.getX() + coffee.getWidth() - tileSize * 0.8 &&
                        player.getY() + player.getHeight() >= coffee.getY() + coffee.getHeight() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= coffee.getY() + coffee.getHeight() + tileSize * 0.4) &&
                !(player.getX() + player.getWidth() >= crack.getX() + tileSize * 0.8 &&
                        player.getX() <= crack.getX() + crack.getWidth() - tileSize * 0.8 &&
                        player.getY() + player.getHeight() >= crack.getY() + crack.getHeight() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= crack.getY() + crack.getHeight() + tileSize * 0.4) &&
                !(player.getX() + player.getWidth() >= cabinet.getX() + tileSize * 0.4 &&
                        player.getX() <= cabinet.getX() + cabinet.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= cabinet.getY() + cabinet.getHeight() &&
                        player.getY() + player.getHeight() <= cabinet.getY() + cabinet.getHeight() + tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= dinningTable1.getX() + tileSize * 0.8 &&
                        player.getX() <= dinningTable1.getX() + dinningTable1.getWidth() - tileSize * 0.8 &&
                        player.getY() + player.getHeight() >= dinningTable1.getY() + dinningTable1.getHeight() &&
                        player.getY() + player.getHeight() <= dinningTable1.getY() + dinningTable1.getHeight() + tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= dinningTable2.getX() + tileSize * 0.8 &&
                        player.getX() <= dinningTable2.getX() + dinningTable2.getWidth() - tileSize * 0.8 &&
                        player.getY() + player.getHeight() >= dinningTable2.getY() + dinningTable2.getHeight() &&
                        player.getY() + player.getHeight() <= dinningTable2.getY() + dinningTable2.getHeight() + tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= leftChair1.getX() + tileSize * 0.6 &&
                        player.getX() <= leftChair1.getX() + leftChair1.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= leftChair1.getY() + leftChair1.getHeight() &&
                        player.getY() + player.getHeight() <= leftChair1.getY() + leftChair1.getHeight() + tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= leftChair2.getX() + tileSize * 0.6 &&
                        player.getX() <= leftChair2.getX() + leftChair2.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= leftChair2.getY() + leftChair2.getHeight() &&
                        player.getY() + player.getHeight() <= leftChair2.getY() + leftChair2.getHeight() + tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= rightChair1.getX() + tileSize * 0.6 &&
                        player.getX() <= rightChair1.getX() + rightChair1.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= rightChair1.getY() + rightChair1.getHeight() &&
                        player.getY() + player.getHeight() <= rightChair1.getY() + rightChair1.getHeight() + tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= rightChair2.getX() + tileSize * 0.6 &&
                        player.getX() <= rightChair2.getX() + rightChair2.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= rightChair2.getY() + rightChair2.getHeight() &&
                        player.getY() + player.getHeight() <= rightChair2.getY() + rightChair2.getHeight() + tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= tileSize * 1.6 &&
                        player.getX() <= tileSize * 1.4 &&
                        player.getY() + player.getHeight() >= tileSize * 9.2 &&
                        player.getY() + player.getHeight() <= tileSize * 9.4) &&
                !(player.getX() + player.getWidth() >= tileSize * 3.6 &&
                        player.getX() <= tileSize * 3.4 &&
                        player.getY() + player.getHeight() >= tileSize * 9.2 &&
                        player.getY() + player.getHeight() <= tileSize * 9.4) &&
                !(player.getX() + player.getWidth() >= tileSize * 5.6 &&
                        player.getX() <= tileSize * 5.4 &&
                        player.getY() + player.getHeight() >= tileSize * 9.2 &&
                        player.getY() + player.getHeight() <= tileSize * 9.4) &&
                !(player.getX() + player.getWidth() >= tileSize * 7.6 &&
                        player.getX() <= tileSize * 7.4 &&
                        player.getY() + player.getHeight() >= tileSize * 9.2 &&
                        player.getY() + player.getHeight() <= tileSize * 9.4) &&
                !(player.getX() + player.getWidth() >= tileSize * 18.6 &&
                        player.getX() <= tileSize * 18.4 &&
                        player.getY() + player.getHeight() >= tileSize * 5.2 &&
                        player.getY() + player.getHeight() <= tileSize * 5.4) &&
                !(player.getX() + player.getWidth() >= tileSize * 18.6 &&
                        player.getX() <= tileSize * 18.4 &&
                        player.getY() + player.getHeight() >= tileSize * 8.2 &&
                        player.getY() + player.getHeight() <= tileSize * 8.4) &&
                !(player.getX() + player.getWidth() >= tileSize * 18.6 &&
                        player.getX() <= tileSize * 18.4 &&
                        player.getY() + player.getHeight() >= tileSize * 11.2 &&
                        player.getY() + player.getHeight() <= tileSize * 11.4) &&
                !(player.getX() + player.getWidth() >= tileSize * 18.6 &&
                        player.getX() <= tileSize * 18.4 &&
                        player.getY() + player.getHeight() >= tileSize * 14.2 &&
                        player.getY() + player.getHeight() <= tileSize * 14.4)) {
            return true;
        }
        return false;
    }

    public boolean moveDown(Rectangle player) {
        if (player.getY() + player.getHeight() <= tileSize * 14 &&
                !(player.getX() + player.getWidth() >= piano.getX() + tileSize * 1 &&
                        player.getX() <= piano.getX() + piano.getWidth() - tileSize * 1 &&
                        player.getY() + player.getHeight() >= piano.getY() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= piano.getY() + tileSize * 0.4) &&
                !(player.getX() + player.getWidth() >= dinningTable1.getX() + tileSize * 0.8 &&
                        player.getX() <= dinningTable1.getX() + dinningTable1.getWidth() - tileSize * 0.8 &&
                        player.getY() + player.getHeight() >= dinningTable1.getY() + tileSize * 0.4 &&
                        player.getY() + player.getHeight() <= dinningTable1.getY() + tileSize * 0.6) &&
                !(player.getX() + player.getWidth() >= dinningTable2.getX() + tileSize * 0.8 &&
                        player.getX() <= dinningTable2.getX() + dinningTable2.getWidth() - tileSize * 0.8 &&
                        player.getY() + player.getHeight() >= dinningTable2.getY() + tileSize * 0.4 &&
                        player.getY() + player.getHeight() <= dinningTable2.getY() + tileSize * 0.6) &&
                !(player.getX() + player.getWidth() >= leftChair1.getX() + tileSize * 0.6 &&
                        player.getX() <= leftChair1.getX() + leftChair1.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= leftChair1.getY() - tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= leftChair1.getY()) &&
                !(player.getX() + player.getWidth() >= leftChair2.getX() + tileSize * 0.6 &&
                        player.getX() <= leftChair2.getX() + leftChair2.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= leftChair2.getY() - tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= leftChair2.getY()) &&
                !(player.getX() + player.getWidth() >= rightChair1.getX() + tileSize * 0.6 &&
                        player.getX() <= rightChair1.getX() + rightChair1.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= rightChair1.getY() - tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= rightChair1.getY()) &&
                !(player.getX() + player.getWidth() >= rightChair2.getX() + tileSize * 0.6 &&
                        player.getX() <= rightChair2.getX() + rightChair2.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= rightChair2.getY() - tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= rightChair2.getY()) &&
                !(player.getX() + player.getWidth() >= tileSize * 1.6 &&
                        player.getX() <= tileSize * 1.4 &&
                        player.getY() + player.getHeight() >= tileSize * 7.2 &&
                        player.getY() + player.getHeight() <= tileSize * 7.4) &&
                !(player.getX() + player.getWidth() >= tileSize * 3.6 &&
                        player.getX() <= tileSize * 3.4 &&
                        player.getY() + player.getHeight() >= tileSize * 7.2 &&
                        player.getY() + player.getHeight() <= tileSize * 7.4) &&
                !(player.getX() + player.getWidth() >= tileSize * 5.6 &&
                        player.getX() <= tileSize * 5.4 &&
                        player.getY() + player.getHeight() >= tileSize * 7.2 &&
                        player.getY() + player.getHeight() <= tileSize * 7.4) &&
                !(player.getX() + player.getWidth() >= tileSize * 7.6 &&
                        player.getX() <= tileSize * 7.4 &&
                        player.getY() + player.getHeight() >= tileSize * 7.2 &&
                        player.getY() + player.getHeight() <= tileSize * 7.4) &&
                !(player.getX() + player.getWidth() >= tileSize * 18.6 &&
                        player.getX() <= tileSize * 18.4 &&
                        player.getY() + player.getHeight() >= tileSize * 3.2 &&
                        player.getY() + player.getHeight() <= tileSize * 3.4) &&
                !(player.getX() + player.getWidth() >= tileSize * 18.6 &&
                        player.getX() <= tileSize * 18.4 &&
                        player.getY() + player.getHeight() >= tileSize * 6.2 &&
                        player.getY() + player.getHeight() <= tileSize * 6.4) &&
                !(player.getX() + player.getWidth() >= tileSize * 18.6 &&
                        player.getX() <= tileSize * 18.4 &&
                        player.getY() + player.getHeight() >= tileSize * 9.2 &&
                        player.getY() + player.getHeight() <= tileSize * 9.4) &&
                !(player.getX() + player.getWidth() >= tileSize * 18.6 &&
                        player.getX() <= tileSize * 18.4 &&
                        player.getY() + player.getHeight() >= tileSize * 12.2 &&
                        player.getY() + player.getHeight() <= tileSize * 12.4)) {
            return true;
        }
        return false;
    }

    public boolean moveRight(Rectangle player) {
        if (player.getX() + player.getWidth() <= tileSize * 20.2 &&
                !(player.getX() + player.getWidth() >= piano.getX() + tileSize * 0.8 &&
                        player.getX() + player.getWidth() <= piano.getX() + tileSize * 1 &&
                        player.getY() + player.getHeight() >= piano.getY() + tileSize * 0.4 &&
                        player.getY() + player.getHeight() <= piano.getY() + piano.getHeight() - tileSize * 0.4) &&
                !(player.getX() + player.getWidth() >= missionBoard.getX() + tileSize * 0.4 &&
                        player.getX() + player.getWidth() <= missionBoard.getX() + tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= missionBoard.getY() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= missionBoard.getY() + missionBoard.getHeight() + tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= coffee.getX() + tileSize * 0.6 &&
                        player.getX() + player.getWidth() <= coffee.getX() + tileSize * 0.8 &&
                        player.getY() + player.getHeight() >= coffee.getY() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= coffee.getY() + coffee.getHeight() + tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= crack.getX() + tileSize * 0.6 &&
                        player.getX() + player.getWidth() <= crack.getX() + tileSize * 0.8 &&
                        player.getY() + player.getHeight() >= crack.getY() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= crack.getY() + crack.getHeight() + tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= cabinet.getX() + tileSize * 0.2 &&
                        player.getX() + player.getWidth() <= cabinet.getX() + tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= cabinet.getY() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= cabinet.getY() + cabinet.getHeight()) &&
                !(player.getX() + player.getWidth() >= dinningTable1.getX() + tileSize * 0.6 &&
                        player.getX() + player.getWidth() <= dinningTable1.getX() + tileSize * 0.8 &&
                        player.getY() + player.getHeight() >= dinningTable1.getY() + tileSize * 0.6 &&
                        player.getY() + player.getHeight() <= dinningTable1.getY() + dinningTable1.getHeight()) &&
                !(player.getX() + player.getWidth() >= dinningTable2.getX() + tileSize * 0.6 &&
                        player.getX() + player.getWidth() <= dinningTable2.getX() + tileSize * 0.8 &&
                        player.getY() + player.getHeight() >= dinningTable2.getY() + tileSize * 0.6 &&
                        player.getY() + player.getHeight() <= dinningTable2.getY() + dinningTable2.getHeight()) &&
                !(player.getX() + player.getWidth() >= leftChair1.getX() + tileSize * 0.4 &&
                        player.getX() + player.getWidth() <= leftChair1.getX() + tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= leftChair1.getY() &&
                        player.getY() + player.getHeight() <= leftChair1.getY() + leftChair1.getHeight()) &&
                !(player.getX() + player.getWidth() >= leftChair2.getX() + tileSize * 0.4 &&
                        player.getX() + player.getWidth() <= leftChair2.getX() + tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= leftChair2.getY() &&
                        player.getY() + player.getHeight() <= leftChair2.getY() + leftChair2.getHeight()) &&
                !(player.getX() + player.getWidth() >= rightChair1.getX() + tileSize * 0.4 &&
                        player.getX() + player.getWidth() <= rightChair1.getX() + tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= rightChair1.getY() &&
                        player.getY() + player.getHeight() <= rightChair1.getY() + rightChair1.getHeight()) &&
                !(player.getX() + player.getWidth() >= rightChair2.getX() + tileSize * 0.4 &&
                        player.getX() + player.getWidth() <= rightChair2.getX() + tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= rightChair2.getY() &&
                        player.getY() + player.getHeight() <= rightChair2.getY() + rightChair2.getHeight()) &&
                !(player.getX() + player.getWidth() >= tileSize * 1.4 &&
                        player.getX() + player.getWidth() <= tileSize * 1.6 &&
                        player.getY() + player.getHeight() >= tileSize * 7.4 &&
                        player.getY() + player.getHeight() <= tileSize * 9.2) &&
                !(player.getX() + player.getWidth() >= tileSize * 3.4 &&
                        player.getX() + player.getWidth() <= tileSize * 3.6 &&
                        player.getY() + player.getHeight() >= tileSize * 7.4 &&
                        player.getY() + player.getHeight() <= tileSize * 9.2) &&
                !(player.getX() + player.getWidth() >= tileSize * 5.4 &&
                        player.getX() + player.getWidth() <= tileSize * 5.6 &&
                        player.getY() + player.getHeight() >= tileSize * 7.4 &&
                        player.getY() + player.getHeight() <= tileSize * 9.2) &&
                !(player.getX() + player.getWidth() >= tileSize * 7.4 &&
                        player.getX() + player.getWidth() <= tileSize * 7.6 &&
                        player.getY() + player.getHeight() >= tileSize * 7.4 &&
                        player.getY() + player.getHeight() <= tileSize * 9.2) &&
                !(player.getX() + player.getWidth() >= tileSize * 18.4 &&
                        player.getX() + player.getWidth() <= tileSize * 18.6 &&
                        player.getY() + player.getHeight() >= tileSize * 3.4 &&
                        player.getY() + player.getHeight() <= tileSize * 5.2) &&
                !(player.getX() + player.getWidth() >= tileSize * 18.4 &&
                        player.getX() + player.getWidth() <= tileSize * 18.6 &&
                        player.getY() + player.getHeight() >= tileSize * 6.4 &&
                        player.getY() + player.getHeight() <= tileSize * 8.2) &&
                !(player.getX() + player.getWidth() >= tileSize * 18.4 &&
                        player.getX() + player.getWidth() <= tileSize * 18.6 &&
                        player.getY() + player.getHeight() >= tileSize * 9.4 &&
                        player.getY() + player.getHeight() <= tileSize * 11.2) &&
                !(player.getX() + player.getWidth() >= tileSize * 18.4 &&
                        player.getX() + player.getWidth() <= tileSize * 18.6 &&
                        player.getY() + player.getHeight() >= tileSize * 12.4 &&
                        player.getY() + player.getHeight() <= tileSize * 14.2)) {
            return true;
        }
        return false;
    }

    public boolean moveLeft(Rectangle player) {
        if (player.getX() >= -tileSize * 0.2 &&
                !(player.getX() >= piano.getX() + piano.getWidth() - tileSize * 1 &&
                        player.getX() <= piano.getX() + piano.getWidth() - tileSize * 0.8 &&
                        player.getY() + player.getHeight() >= piano.getY() + tileSize * 0.4 &&
                        player.getY() + player.getHeight() <= piano.getY() + piano.getHeight() - tileSize * 0.4) &&
                !(player.getX() >= missionBoard.getX() + missionBoard.getWidth() - tileSize * 0.6 &&
                        player.getX() <= missionBoard.getX() + missionBoard.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= missionBoard.getY() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= missionBoard.getY() + missionBoard.getHeight() + tileSize * 0.2) &&
                !(player.getX() >= coffee.getX() + coffee.getWidth() - tileSize * 0.8 &&
                        player.getX() <= coffee.getX() + coffee.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= coffee.getY() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= coffee.getY() + coffee.getHeight() + tileSize * 0.2) &&
                !(player.getX() >= crack.getX() + crack.getWidth() - tileSize * 0.8 &&
                        player.getX() <= crack.getX() + crack.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= crack.getY() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= crack.getY() + crack.getHeight() + tileSize * 0.2) &&
                !(player.getX() >= cabinet.getX() + cabinet.getWidth() - tileSize * 0.4 &&
                        player.getX() <= cabinet.getX() + cabinet.getWidth() - tileSize * 0.2 &&
                        player.getY() + player.getHeight() >= cabinet.getY() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= cabinet.getY() + cabinet.getHeight()) &&
                !(player.getX() >= dinningTable1.getX() + dinningTable1.getWidth() - tileSize * 0.8 &&
                        player.getX() <= dinningTable1.getX() + dinningTable1.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= dinningTable1.getY() + tileSize * 0.6 &&
                        player.getY() + player.getHeight() <= dinningTable1.getY() + dinningTable1.getHeight()) &&
                !(player.getX() >= dinningTable2.getX() + dinningTable2.getWidth() - tileSize * 0.8 &&
                        player.getX() <= dinningTable2.getX() + dinningTable2.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= dinningTable2.getY() + tileSize * 0.6 &&
                        player.getY() + player.getHeight() <= dinningTable2.getY() + dinningTable2.getHeight()) &&
                !(player.getX() >= leftChair1.getX() + leftChair1.getWidth() - tileSize * 0.6 &&
                        player.getX() <= leftChair1.getX() + leftChair1.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= leftChair1.getY() &&
                        player.getY() + player.getHeight() <= leftChair1.getY() + leftChair1.getHeight()) &&
                !(player.getX() >= leftChair2.getX() + leftChair2.getWidth() - tileSize * 0.6 &&
                        player.getX() <= leftChair2.getX() + leftChair2.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= leftChair2.getY() &&
                        player.getY() + player.getHeight() <= leftChair2.getY() + leftChair1.getHeight()) &&
                !(player.getX() >= rightChair1.getX() + rightChair1.getWidth() - tileSize * 0.6 &&
                        player.getX() <= rightChair1.getX() + rightChair1.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= rightChair1.getY() &&
                        player.getY() + player.getHeight() <= rightChair1.getY() + rightChair1.getHeight()) &&
                !(player.getX() >= rightChair2.getX() + rightChair2.getWidth() - tileSize * 0.6 &&
                        player.getX() <= rightChair2.getX() + rightChair2.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= rightChair2.getY() &&
                        player.getY() + player.getHeight() <= rightChair2.getY() + rightChair2.getHeight()) &&
                !(player.getX() >= tileSize * 1.4 &&
                        player.getX() <= tileSize * 1.6 &&
                        player.getY() + player.getHeight() >= tileSize * 7.4 &&
                        player.getY() + player.getHeight() <= tileSize * 9.2) &&
                !(player.getX() >= tileSize * 3.4 &&
                        player.getX() <= tileSize * 3.6 &&
                        player.getY() + player.getHeight() >= tileSize * 7.4 &&
                        player.getY() + player.getHeight() <= tileSize * 9.2) &&
                !(player.getX() >= tileSize * 5.4 &&
                        player.getX() <= tileSize * 5.6 &&
                        player.getY() + player.getHeight() >= tileSize * 7.4 &&
                        player.getY() + player.getHeight() <= tileSize * 9.2) &&
                !(player.getX() >= tileSize * 7.4 &&
                        player.getX() <= tileSize * 7.6 &&
                        player.getY() + player.getHeight() >= tileSize * 7.4 &&
                        player.getY() + player.getHeight() <= tileSize * 9.2) &&
                !(player.getX() >= tileSize * 18.4 &&
                        player.getX() <= tileSize * 18.6 &&
                        player.getY() + player.getHeight() >= tileSize * 3.4 &&
                        player.getY() + player.getHeight() <= tileSize * 5.2) &&
                !(player.getX() >= tileSize * 18.4 &&
                        player.getX() <= tileSize * 18.6 &&
                        player.getY() + player.getHeight() >= tileSize * 6.4 &&
                        player.getY() + player.getHeight() <= tileSize * 8.2) &&
                !(player.getX() >= tileSize * 18.4 &&
                        player.getX() <= tileSize * 18.6 &&
                        player.getY() + player.getHeight() >= tileSize * 9.4 &&
                        player.getY() + player.getHeight() <= tileSize * 11.2) &&
                !(player.getX() >= tileSize * 18.4 &&
                        player.getX() <= tileSize * 18.6 &&
                        player.getY() + player.getHeight() >= tileSize * 12.4 &&
                        player.getY() + player.getHeight() <= tileSize * 14.2)) {
            return true;
        }
        return false;
    }

    public boolean closeToDinningTable(Rectangle player) {
        if (player.getX() + player.getWidth() / 2 >= tileSize * 1 &&
                player.getX() + player.getWidth() / 2 <= tileSize * 6 &&
                player.getY() + player.getHeight() <= tileSize * 8 &&
                player.getY() + player.getHeight() >= tileSize * 3) {
            return true;
        }
        return false;
    }

    public boolean closeToBoard(Rectangle player) {
        if (player.getX() + player.getWidth() / 2 >= tileSize * 10 &&
                player.getX() + player.getWidth() / 2 <= tileSize * 14 &&
                player.getY() + player.getHeight() <= tileSize * 4 &&
                player.getY() + player.getHeight() >= tileSize * 1) {
            return true;
        }
        return false;
    }

    public void acceptMission(Mission choosedMission, Player player, Cafe cafe) {
        if(player.getMissionCapacity() > player.getMission().size()){
            UI.showPopup("Mission " + choosedMission.getName() + " Accepted!", root);
            player.getMission().add(choosedMission);
            cafe.getBoard().getAvailableMission().remove(choosedMission);
            cafe.getBoard().getActiveMission().add(choosedMission);
        }
        else {
            UI.showPopup("You don't have enough Mission capacity!", root);
        }
    }

    public void removeMission(Mission choosedMission, Player player, Cafe cafe) {
        UI.showPopup("Mission " + choosedMission.getName() + " Removed!", root);
        player.getMission().remove(choosedMission);
        cafe.getBoard().getAvailableMission().add(choosedMission);
        cafe.getBoard().getActiveMission().remove(choosedMission);
    }
}
