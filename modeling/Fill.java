import java.util.HashMap;
import java.util.Map;

public class Fill extends Task {

    public Fill() {
        super.name = "Fill";
        super.featureChangeRate = new HashMap<String, Double>();
        featureChangeRate.put("Energy", -7.0);
        super.executionTime = new Date();
    }

    public String run(WateringCan wateringCan, Player player) {
        wateringCan.setWaterLevel(wateringCan.getMaxWaterLevel());
        for (Map.Entry<String, Double> entry : featureChangeRate.entrySet())
            for (int i = 0; i < player.getFeature().size(); i++)
                if (entry.getKey().equals(player.getFeature().get(i).getName()))
                    player.getFeature().get(i).setCurrent(player.getFeature().get(i).getCurrent() - entry.getValue());
        UI.missionHandler(name, "Watering Can");
        return wateringCan.getName() + " was successfully Filled!";
    }
}
