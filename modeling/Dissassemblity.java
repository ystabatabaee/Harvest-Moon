import com.google.common.collect.HashMultiset;

public class Dissassemblity {
    private double price;
    private HashMultiset<Item> initialIngredients;
    private boolean dissassemblity;

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public HashMultiset<Item> getInitialIngredients() {
        return initialIngredients;
    }

    public void setInitialIngredients(HashMultiset<Item> initialIngredients) {
        this.initialIngredients = initialIngredients;
    }

    public boolean isDissassemblity() {
        return dissassemblity;
    }

    public void setDissassemblity(boolean dissassemblity) {
        this.dissassemblity = dissassemblity;
    }

    public Dissassemblity(double price, HashMultiset<Item> initialIngredients, boolean dissassemblity) {
        this.price = price;
        this.initialIngredients = initialIngredients;
        this.dissassemblity = dissassemblity;
    }
}
