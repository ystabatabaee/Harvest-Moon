import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.util.ArrayList;

public class BackpackUI {

    Group root = new Group();
    private Backpack backpack;
    private ArrayList<Item> items = new ArrayList<>();
    private ArrayList<Rectangle> itemsG = new ArrayList<>();
    private Text name;

    BackpackUI(Group root, Game game) {
        this.root = root;
        this.backpack = game.getPlayer().getBackpack();
    }


    private Rectangle rectangleBuilder(double width, double height, Paint fill, double setX, double setY) {
        Rectangle rectangle = new Rectangle(width, height, fill);
        rectangle.setX(setX);
        rectangle.setY(setY);
        return rectangle;
    }

    private Text buttonDesigner(String name, double x, double y, double fontSize, Color color) {
        Text text = new Text(x, y, name);
        text.setFont(Font.font("Comic Sans MS", fontSize));
        text.setFill(Color.BLACK);
        text.setStroke(color);
        text.setOnMouseEntered(event -> text.setFill(Color.DARKGREY));
        text.setOnMouseExited(event -> text.setFill(Color.BLACK));
        return text;
    }

    public void backpackSceneBuilder(Stage primaryStage, Group root, Scene scene) {
            root.getChildren().clear();
            itemsG.clear();
            items.clear();
            Image wood = new Image("pics/menus/menuWood.png");
            Image titleImage = new Image("pics/menus/backpackTitle.png");
            Rectangle title = rectangleBuilder(180, 60, new ImagePattern(titleImage), 430, 20);
            root.getChildren().addAll(title);

            for (int i = 0; i < 9; i++) {
                for (int j = 0; j < 6; j++) {
                    Rectangle objectButton = rectangleBuilder(90, 90, new ImagePattern(wood), 90 * (i + 1), 90 * (j + 1));
                    root.getChildren().addAll(objectButton);
                }
            }
            int countX = 0;
            int countY = 0;
            for (Item item : backpack.getItem().elementSet()) {
                if (countX == 9) {
                    countX = 0;
                    countY++;
                }
                countX++;
                Rectangle itemPic = rectangleBuilder(60, 60, new ImagePattern(item.getPicture()), 90 * (countX + 0.2), 90 * (countY + 1.1));
                items.add(item);
                itemsG.add(itemPic);
                Text itemName = buttonDesigner(item.getName(), 90 * (countX + 0.18), 90 * (countY + 1.8), 10, Color.GOLD);
                root.getChildren().addAll(itemPic, itemName);
            }
            Text titleText = buttonDesigner("Backpack", 450, 53, 30, Color.SADDLEBROWN);

            root.getChildren().addAll(titleText);


    }

    public int clickOnRightPlace(MouseEvent event) {
        System.out.println(itemsG.size());
        for (int i = 0; i < itemsG.size(); i++) {
            if (event.getX() >= itemsG.get(i).getX() &&
                    event.getX() <= itemsG.get(i).getX() + itemsG.get(i).getWidth() &&
                    event.getY() >= itemsG.get(i).getY() &&
                    event.getY() <= itemsG.get(i).getY() + itemsG.get(i).getHeight())
                return i;
        }
        return -1;
    }

    public void writeName(MouseEvent event) {
        if (clickOnRightPlace(event) != -1) {
            root.getChildren().remove(name);
            String valid = "";
            if (!items.get(clickOnRightPlace(event)).isValid())
                valid = " (Broken) ";
            name = buttonDesigner(items.get(clickOnRightPlace(event)).getName() + valid, 450, 93, 30, Color.SADDLEBROWN);
            root.getChildren().add(name);
        } else root.getChildren().remove(name);
    }

    public Item choose(String name, int num) {
        if(items.get(num).getName().contains(name))
                return items.get(num);
        return null;
    }

    public Item choose(int num) {
        return items.get(num);
    }


    public ArrayList<Item> getItems() {
        return items;
    }

    public ArrayList<Rectangle> getItemsG() {
        return itemsG;
    }
}
