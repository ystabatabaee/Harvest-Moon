import javafx.scene.image.Image;

import java.util.ArrayList;
import java.util.HashMap;

public class Drink extends Item {

    private Object source;

    public Drink(HashMap<String, Double> featureChangeRate, double capacity, String name, Probability invalid,
                 double price, ArrayList<Task> task, Dissassemblity dissassemblity, Object source, Image picture) {
        super("Drink", featureChangeRate, capacity, name, invalid, price, task, 1, 1,
                true, false, dissassemblity, picture);
        this.source = source;
        if (source instanceof Item)
            dissassemblity.getInitialIngredients().add((Item) source);
    }

}
