import java.lang.reflect.Method;
import java.util.ArrayList;

public class Person {

    private int level;
    private int experience;
    private ArrayList<Feature> feature;
    private ArrayList<Task> task;
    private String name;

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public ArrayList<Feature> getFeature() {
        return feature;
    }

    public void setFeature(ArrayList<Feature> feature) {
        this.feature = feature;
    }

    public ArrayList<Task> getTask() {
        return task;
    }

    public void setTask(ArrayList<Task> task) {
        this.task = task;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Person(String name) {
        this.name = name;
        feature = new ArrayList<>();
        Class featuresClass = AllOfFeatures.class;
        Object obj = null;
        try {
            obj = featuresClass.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        Method[] methods = featuresClass.getDeclaredMethods();
        for (Method feature : methods) {
            try {
                this.feature.add((Feature) feature.invoke(obj));
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

    public Person(int level, int experience, ArrayList<Feature> feature, String name) {
        this.level = level;
        this.experience = experience;
        this.feature = feature;
        this.name = name;
    }

    public Person() {
    }
}