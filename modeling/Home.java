import com.google.common.collect.HashMultiset;

import java.lang.reflect.InvocationTargetException;

public class Home extends Building {

    private Bed bed;
    private StorageBox storageBox;
    private Kitchen kitchen;


    public Home(boolean isRuin, Probability ruin, double repairPrice) throws IllegalAccessException, InvocationTargetException, InstantiationException {
        super(isRuin, ruin, HashMultiset.create(), repairPrice);
        bed = new Bed();
        storageBox = new StorageBox();
        kitchen = new Kitchen();

        HashMultiset repairObjects = HashMultiset.create();
        setRepairObjects(repairObjects);
    }

    public Home() throws IllegalAccessException, InvocationTargetException, InstantiationException {
        super();
        bed = new Bed();
        storageBox = new StorageBox();
        kitchen = new Kitchen();

    }

    public Bed getBed() {
        return bed;
    }

    public void setBed(Bed bed) {
        this.bed = bed;
    }

    public StorageBox getStorageBox() {
        return storageBox;
    }

    public void setStorageBox(StorageBox storageBox) {
        this.storageBox = storageBox;
    }

    public Kitchen getKitchen() {
        return kitchen;
    }

    public void setKitchen(Kitchen kitchen) {
        this.kitchen = kitchen;
    }
}
