import java.io.Serializable;

public class FirstData implements Serializable {
    private Integer numOfPlayers;
    private Integer clientIndex;


    public FirstData(Integer numOfPlayers) {
        this.numOfPlayers = numOfPlayers;
    }


    public Integer getNumOfPlayers() {
        return numOfPlayers;
    }

    public Integer getClientIndex() {
        return clientIndex;
    }


    public void setClientIndex(Integer clientIndex) {
        this.clientIndex = clientIndex;
    }

}
