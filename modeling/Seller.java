import java.util.ArrayList;

public class Seller extends Person {

	private Shop shop;
	private ArrayList interests;
	private ArrayList dislikes;

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public ArrayList getInterests() {
		return interests;
	}

	public void setInterests(ArrayList interests) {
		this.interests = interests;
	}

	public ArrayList getDislikes() {
		return dislikes;
	}

	public void setDislikes(ArrayList dislikes) {
		this.dislikes = dislikes;
	}

	public Seller(ArrayList interests, ArrayList dislikes, Shop shop) throws InstantiationException, IllegalAccessException {
		super("Seller");
		this.shop = shop;
		this.dislikes = dislikes;
		this.interests = interests;
	}
	public Seller() {
		super("Seller");
	}
}