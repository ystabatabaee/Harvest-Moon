import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;

import java.util.Map;

public class Plow extends Task {

    public Plow(Shovel shovel) {
        super.name = "Plow";
        super.featureChangeRate = shovel.getFeatureChangeRate();
        super.executionTime = new Date();
    }

    public String run(Shovel shovel, Cell[][] cell, Player player, int x, int y, Rectangle[][] cellG) {
        Image image = new Image("pics/plowedEarth.png");
        if (shovel.isValid()) {
            for (int i = 0; i < cell.length; i++)
                for (int j = 0; j < cell[i].length; j++)
                    if (cell[i][j] != null) {
                        cell[i][j].setPlowed(true);
                        cellG[j + x][i + y].setFill(new ImagePattern(image));
                        UI.missionHandler(name, "Shovel");
                    }
            for (Map.Entry<String, Double> entry : featureChangeRate.entrySet())
                for (int i = 0; i < player.getFeature().size(); i++)
                    if (entry.getKey().equals(player.getFeature().get(i).getName()))
                        player.getFeature().get(i).setCurrent(player.getFeature().get(i).getCurrent() + entry.getValue());
            Double random = Math.random();
            if (shovel.getInvalid().getProbability() > random) {
                shovel.setValid(false);
                String[] string = {" broke! & Chosen cells plowed", " cassé! & Les cellules choisies ont labouré", "rompió & Células escogidas aradas"};
                return shovel.getName() + string[UI.language];
            } else{
                String[] string = {"Chosen cells plowed", "Les cellules choisies ont labouré", "Células escogidas aradas"};
                return string[UI.language];
            }

        } else {
            String[] string = {" is broken!", " est cassé!", "شکسته است!", "está roto"};
            return shovel.getName() + string[UI.language];
        }
    }

}
