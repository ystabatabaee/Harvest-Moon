import com.google.common.collect.HashMultiset;

import java.lang.reflect.Method;
import java.util.ArrayList;

public class ToolShelf {

	private ArrayList<Tool> existingTools;
	private ArrayList<Task> task;
	private ArrayList<String> cookingTools;

	public ArrayList<Tool> getExistingTools() {
		return existingTools;
	}

	public void setExistingTools(ArrayList<Tool> existingTools) {
		this.existingTools = existingTools;
	}

	public ArrayList<Task> getTask() {
		return task;
	}

	public void setTask(ArrayList<Task> task) {
		this.task = task;
	}

	public ArrayList<String> getCookingTools() {
		return cookingTools;
	}

	public void setCookingTools(ArrayList<String> cookingTools) {
		this.cookingTools = cookingTools;
	}

	public ToolShelf() throws IllegalAccessException, InstantiationException {
		existingTools = new ArrayList<>();
		cookingTools = new ArrayList<>();
		Class toolsClass = AllOfTools.class;
		Object obj = toolsClass.newInstance();
		Method[] tools = toolsClass.getDeclaredMethods();
		for(Method tool : tools) {
			try {
				Tool utensil = (Tool)tool.invoke(obj);
				if(utensil.getType().contains("Cooking Utensil")) {
					cookingTools.add(utensil.getName());
					existingTools.add(utensil);
				}
			}
			catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
	}

}