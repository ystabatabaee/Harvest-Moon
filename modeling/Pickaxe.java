import com.google.common.collect.HashMultiset;
import javafx.scene.image.Image;

import java.util.ArrayList;
import java.util.HashMap;

public class Pickaxe extends Tool{

    private HashMap<String, Integer> extractedMaterial;

    public HashMap<String, Integer> getExtractedMaterial() {
        return extractedMaterial;
    }

    public void setExtractedMaterial(HashMap<String, Integer> extractedMaterial) {
        this.extractedMaterial = extractedMaterial;
    }

    public Pickaxe(String type, HashMap<String, Double> featureChangeRate, double capacity, String name,
                   double price, ArrayList<Task> task, int level, Dissassemblity dissassemblity, double buildingPrice,
                   double repairPrice, HashMultiset<Item> repairItems, HashMap<String, Integer> extractedMaterial, Image picture) {
        super(type, featureChangeRate, capacity, name, new Probability(){{setProbability((double)(5 - level) / 20);}}, price,
                task, 4, level, dissassemblity, buildingPrice, repairPrice, repairItems, picture);
        this.extractedMaterial = extractedMaterial;
    }

    public Pickaxe() {
    }
}
