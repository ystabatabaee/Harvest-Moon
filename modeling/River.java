import java.util.ArrayList;

public class River {

    private ArrayList<Task> task;
    private ArrayList<Animal> fish;
    private Probability fishExistence;

    public River(Probability fishExistence) {
        this.task = new ArrayList<>();
        this.fishExistence = fishExistence;
        fish = new ArrayList<>();

    }

    public ArrayList<Task> getTask() {
        return task;
    }

    public void setTask(ArrayList<Task> task) {
        this.task = task;
    }

    public ArrayList<Animal> getFish() {
        return fish;
    }

    public void setFish(ArrayList<Animal> fish) {
        this.fish = fish;
    }

    public Probability getFishExistence() {
        return fishExistence;
    }

    public void setFishExistence(Probability fishExistence) {
        this.fishExistence = fishExistence;
    }

}