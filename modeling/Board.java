import com.google.common.collect.HashMultiset;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

public class Board {

    private ArrayList<Mission> possibleMission;
    private ArrayList<Mission> availableMission;
    private ArrayList<Mission> activeMission;
    private ArrayList<Task> task;

    public Board(ArrayList<Mission> possibleMission, ArrayList<Mission> availableMission,
                 ArrayList<Mission> activeMission, ArrayList<Task> task) throws IllegalAccessException, InstantiationException {
        this.possibleMission = possibleMission;
        this.availableMission = availableMission;
        this.activeMission = activeMission;
        this.task = task;
    }

    public ArrayList<Mission> getPossibleMission() {
        return possibleMission;
    }

    public void setPossibleMission(ArrayList<Mission> possibleMission) {
        this.possibleMission = possibleMission;
    }

    public ArrayList<Mission> getAvailableMission() {
        return availableMission;
    }

    public void setAvailableMission(ArrayList<Mission> availableMission) {
        this.availableMission = availableMission;
    }

    public ArrayList<Mission> getActiveMission() {
        return activeMission;
    }

    public void setActiveMission(ArrayList<Mission> activeMission) {
        this.activeMission = activeMission;
    }

    public ArrayList<Task> getTask() {
        return task;
    }

    public void setTask(ArrayList<Task> task) {
        this.task = task;
    }
}