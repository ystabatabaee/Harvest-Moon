import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class BarnUI implements Mover {
    Group root = new Group();
    static double tileSize = 50;
    private static Barn barn;
    private Rectangle[][] cowG;
    private Rectangle[][] chickenG;
    private Rectangle[][] sheepG;
    private Rectangle[][] machineG;

    public Rectangle rectangleBuilder(double width, double height, Paint fill, double setX, double setY) {
        Rectangle rectangle = new Rectangle(width, height, fill);
        rectangle.setX(setX);
        rectangle.setY(setY);
        return rectangle;
    }

    public BarnUI(Group root, Barn barn) {
        this.root = root;
        this.barn = barn;
    }

    public Scene sceneBuilder(Stage primaryStage) {
        Scene barnScene = new Scene(root, 1000, 700, Color.BLACK);
        Image barnPic = new Image("pics/maps/barnMap.png");
        Image doorPic = new Image("pics/doors/barnDoor.png");
        Image hayPic = new Image("pics/hay.png");
        Image chickenHayPic = new Image("pics/chickenHay.png");

        Rectangle rectangle = new Rectangle(1000, 700, new ImagePattern(barnPic));
        Rectangle door = rectangleBuilder(tileSize, tileSize * 2, new ImagePattern(doorPic), 1000 - tileSize * 3, tileSize);
        root.getChildren().add(rectangle);
        root.getChildren().addAll(door);

        cowG = new Rectangle[3][2];
        sheepG = new Rectangle[3][2];
        chickenG = new Rectangle[4][2];
        machineG = new Rectangle[3][2];

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 2; j++) {
                machineG[i][j] = rectangleBuilder(tileSize * 1.5, tileSize * 2, null,
                        tileSize * (3 * i + 12), tileSize * (3 * j + 9));
                root.getChildren().addAll(machineG[i][j]);
            }
        }

        //cows part
        for (int i = 0; i < 3; i++) {
            cowG[i][0] = new Rectangle();
            cowG[i][1] = new Rectangle();
            Rectangle hay1 = rectangleBuilder(tileSize * 2, tileSize * 3, new ImagePattern(hayPic), tileSize * (3 * i + 1), tileSize * 3);
            Rectangle hay2 = rectangleBuilder(tileSize * 2, tileSize * 3, new ImagePattern(hayPic), tileSize * (3 * i + 1), tileSize * 6);
            root.getChildren().addAll(hay1, hay2);
            try {
                if (i < barn.getBarnPart().get(0).getAnimal().size()) {
                    cowG[i][0] = rectangleBuilder(tileSize * 2, tileSize * 3, new ImagePattern(barn.getBarnPart().get(0).getAnimal().get(i).getPicture()),
                            tileSize * (3 * i + 1), tileSize * 3);
                    root.getChildren().addAll(cowG[i][0]);
                }
                if (i + 3 < barn.getBarnPart().get(0).getAnimal().size()) {
                    cowG[i][1] = rectangleBuilder(tileSize * 2, tileSize * 3, new ImagePattern(barn.getBarnPart().get(0).getAnimal().get(i + 3).getPicture()),
                            tileSize * (3 * i + 1), tileSize * 6);
                    root.getChildren().addAll(cowG[i][1]);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        //sheep part
        for (int i = 0; i < 3; i++) {
            sheepG[i][0] = new Rectangle();
            sheepG[i][1] = new Rectangle();
            Rectangle hay1 = rectangleBuilder(tileSize * 2, tileSize * 3, new ImagePattern(hayPic), tileSize * (3 * i + 12), tileSize * 3);
            Rectangle hay2 = rectangleBuilder(tileSize * 2, tileSize * 3, new ImagePattern(hayPic), tileSize * (3 * i + 12), tileSize * 6);
            root.getChildren().addAll(hay1, hay2);
            try {
                if (i < barn.getBarnPart().get(1).getAnimal().size()) {
                    sheepG[i][0] = rectangleBuilder(tileSize * 2, tileSize * 2, new ImagePattern(barn.getBarnPart().get(1).getAnimal().get(i).getPicture()),
                            tileSize * (3 * i + 12), tileSize * 3);
                    root.getChildren().addAll(sheepG[i][0]);
                }
                if (i + 3 < barn.getBarnPart().get(1).getAnimal().size()) {
                    sheepG[i][1] = rectangleBuilder(tileSize * 2, tileSize * 2, new ImagePattern(barn.getBarnPart().get(1).getAnimal().get(i + 3).getPicture()),
                            tileSize * (3 * i + 12), tileSize * 6);
                    root.getChildren().addAll(sheepG[i][1]);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        //chickens part
        for (int i = 0; i < 4; i++) {
            chickenG[i][0] = new Rectangle();
            chickenG[i][1] = new Rectangle();
            Rectangle hay1 = rectangleBuilder(tileSize * 2, tileSize, new ImagePattern(chickenHayPic), tileSize * 2 * i, tileSize * 10);
            Rectangle hay2 = rectangleBuilder(tileSize * 2, tileSize, new ImagePattern(chickenHayPic), tileSize * 2 * i, tileSize * 12);
            root.getChildren().addAll(hay1, hay2);
            try {
                if (i < barn.getBarnPart().get(2).getAnimal().size()) {
                    chickenG[i][0] = rectangleBuilder(tileSize * 1, tileSize * 1, new ImagePattern(barn.getBarnPart().get(2).getAnimal().get(i).getPicture()),
                            tileSize * (2 * i + 1), tileSize * 10 - 10);
                    root.getChildren().addAll(chickenG[i][0]);
                }
                if (i + 4 < barn.getBarnPart().get(2).getAnimal().size()) {
                    chickenG[i][1] = rectangleBuilder(tileSize * 1, tileSize * 1, new ImagePattern(barn.getBarnPart().get(2).getAnimal().get(i + 4).getPicture()),
                            tileSize * (2 * i + 1), tileSize * 12 - 10);
                    root.getChildren().addAll(chickenG[i][1]);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return barnScene;
    }

    public boolean moveUp(Rectangle player) {
        if (player.getY() + player.getHeight() >= tileSize * 3.4 &&
                !(player.getX() + player.getWidth() >= tileSize * 0 &&
                        player.getX() <= tileSize * 8.8 &&
                        player.getY() + player.getHeight() >= tileSize * 10 &&
                        player.getY() + player.getHeight() <= tileSize * 10.2) &&
                !(player.getX() + player.getWidth() >= tileSize * 12.4 &&
                        player.getX() <= tileSize * 20 &&
                        player.getY() + player.getHeight() >= tileSize * 10 &&
                        player.getY() + player.getHeight() <= tileSize * 10.2) &&
                !(player.getX() + player.getWidth() >= tileSize * 10.8 &&
                        player.getX() <= tileSize * 10.4 &&
                        player.getY() + player.getHeight() >= tileSize * 9.2 &&
                        player.getY() + player.getHeight() <= tileSize * 9.4)) {
            return true;
        }
        return false;
    }

    public boolean moveDown(Rectangle player) {
        if (player.getY() + player.getHeight() <= tileSize * 14 &&
                !(player.getX() + player.getWidth() >= tileSize * 0 &&
                        player.getX() <= tileSize * 8.8 &&
                        player.getY() + player.getHeight() >= tileSize * 9 &&
                        player.getY() + player.getHeight() <= tileSize * 9.2) &&
                !(player.getX() + player.getWidth() >= tileSize * 12.4 &&
                        player.getX() <= tileSize * 20 &&
                        player.getY() + player.getHeight() >= tileSize * 9 &&
                        player.getY() + player.getHeight() <= tileSize * 9.2) &&
                !(player.getX() + player.getWidth() >= tileSize * 10.8 &&
                        player.getX() <= tileSize * 10.4 &&
                        player.getY() + player.getHeight() >= tileSize * 9.8 &&
                        player.getY() + player.getHeight() <= tileSize * 10)) {
            return true;
        }
        return false;
    }

    public boolean moveRight(Rectangle player) {
        if (player.getX() + player.getWidth() <= tileSize * 20.2 &&
                !(player.getX() + player.getWidth() >= tileSize * 12.2 &&
                        player.getX() + player.getWidth() <= tileSize * 12.4 &&
                        player.getY() + player.getHeight() >= tileSize * 9.2 &&
                        player.getY() + player.getHeight() <= tileSize * 10) &&
                !(player.getX() + player.getWidth() >= tileSize * 10.4 &&
                        player.getX() + player.getWidth() <= tileSize * 10.8 &&
                        player.getY() + player.getHeight() >= tileSize * 0 &&
                        player.getY() + player.getHeight() <= tileSize * 9.2) &&
                !(player.getX() + player.getWidth() >= tileSize * 10.4 &&
                        player.getX() + player.getWidth() <= tileSize * 10.8 &&
                        player.getY() + player.getHeight() >= tileSize * 10 &&
                        player.getY() + player.getHeight() <= tileSize * 14.5)) {
            return true;
        }
        return false;
    }

    public boolean moveLeft(Rectangle player) {
        if (player.getX() >= tileSize * 0 &&
                !(player.getX() >= tileSize * 8.6 &&
                        player.getX() <= tileSize * 8.8 &&
                        player.getY() + player.getHeight() >= tileSize * 9.2 &&
                        player.getY() + player.getHeight() <= tileSize * 10) &&
                !(player.getX() >= tileSize * 10.4 &&
                        player.getX() <= tileSize * 10.8 &&
                        player.getY() + player.getHeight() >= tileSize * 0 &&
                        player.getY() + player.getHeight() <= tileSize * 9.2) &&
                !(player.getX() >= tileSize * 10.4 &&
                        player.getX() <= tileSize * 10.8 &&
                        player.getY() + player.getHeight() >= tileSize * 10 &&
                        player.getY() + player.getHeight() <= tileSize * 14.5)) {
            return true;
        }
        return false;
    }

    public int chosenAnimal(Rectangle player) {
        for (int i = 0; i < cowG.length; i++) {
            for (int j = 0; j < cowG[0].length; j++) {
                if (player.getX() + player.getWidth() / 2 <= cowG[i][j].getX() + cowG[i][j].getWidth() + 30 &&
                        player.getX() + player.getWidth() / 2 >= cowG[i][j].getX() - 30 &&
                        player.getY() + player.getHeight() <= cowG[i][j].getY() + cowG[i][j].getHeight() + 30 &&
                        player.getY() + player.getHeight() >= cowG[i][j].getY() - 30) {
                    return 2 * j + i;
                }
            }
        }
        for (int i = 0; i < sheepG.length; i++) {
            for (int j = 0; j < sheepG[0].length; j++) {
                if (player.getX() + player.getWidth() / 2 <= sheepG[i][j].getX() + sheepG[i][j].getWidth() + 30 &&
                        player.getX() + player.getWidth() / 2 >= sheepG[i][j].getX() - 30 &&
                        player.getY() + player.getHeight() <= sheepG[i][j].getY() + sheepG[i][j].getHeight() + 30 &&
                        player.getY() + player.getHeight() >= sheepG[i][j].getY() - 30) {
                    return 2 * j + i + 6;
                }
            }
        }
        for (int i = 0; i < chickenG.length; i++) {
            for (int j = 0; j < chickenG[0].length; j++) {
                if (player.getX() + player.getWidth() / 2 <= chickenG[i][j].getX() + chickenG[i][j].getWidth() + 30 &&
                        player.getX() + player.getWidth() / 2 >= chickenG[i][j].getX() - 30 &&
                        player.getY() + player.getHeight() <= chickenG[i][j].getY() + chickenG[i][j].getHeight() + 30 &&
                        player.getY() + player.getHeight() >= chickenG[i][j].getY() - 30) {
                    return 2 * j + i + 12;
                }
            }
        }
        return -1;
    }

    public void feed(int animalNum, Item choosedItem, Player player, boolean[] flags) {
        System.out.println(animalNum);
        Feed feed = new Feed();
        if (animalNum < 6) {
            if (animalNum <= barn.getBarnPart().get(0).getAnimal().size()) {
                UI.showPopup(feed.run(barn.getBarnPart().get(0).getAnimal().get(animalNum), choosedItem, player, cowG,
                        animalNum % 3, animalNum / 3, root), root);
            }
        } else if (animalNum < 12) {
            if (animalNum - 6 <= barn.getBarnPart().get(1).getAnimal().size()) {
                UI.showPopup(feed.run(barn.getBarnPart().get(1).getAnimal().get(animalNum - 6), choosedItem, player, sheepG,
                        (animalNum - 6) % 3, (animalNum - 6) / 3, root), root);
            }
        } else if (animalNum < 20) {
            if (animalNum - 12 <= barn.getBarnPart().get(2).getAnimal().size()) {
                UI.showPopup(feed.run(barn.getBarnPart().get(2).getAnimal().get(animalNum - 12), choosedItem, player, chickenG,
                        (animalNum - 12) % 4, (animalNum - 12) / 4, root), root);
            }
        } else {
            UI.showPopup("There is no animal to feed in this hay!", root);
            UI.initializeFlags();
        }
        flags[2] = true;
    }

    public void heal(int animalNum, Item choosedItem, Player player, boolean[] flags) {
        System.out.println(animalNum);
        HealAnimal heal = new HealAnimal();
        if (animalNum < 6) {
            if (animalNum <= barn.getBarnPart().get(0).getAnimal().size()) {
                UI.showPopup(heal.run(barn.getBarnPart().get(0).getAnimal().get(animalNum), choosedItem, player, cowG,
                        animalNum % 3, animalNum / 3, root), root);
            }
        } else if (animalNum < 12) {
            if (animalNum - 6 <= barn.getBarnPart().get(1).getAnimal().size()) {
                UI.showPopup(heal.run(barn.getBarnPart().get(1).getAnimal().get(animalNum - 6), choosedItem, player, sheepG,
                        (animalNum - 6) % 3, (animalNum - 6) / 3, root), root);
            }
        } else if (animalNum < 20) {
            if (animalNum - 12 <= barn.getBarnPart().get(2).getAnimal().size()) {
                UI.showPopup(heal.run(barn.getBarnPart().get(2).getAnimal().get(animalNum - 12), choosedItem, player, chickenG,
                        (animalNum - 12) % 4, (animalNum - 12) / 4, root), root);
            }
        } else {
            UI.showPopup("There is no animal to heal in this hay!", root);
            UI.initializeFlags();
        }
        flags[2] = true;
    }

    public void getProduct(int animalNum, Item choosedItem, Player player, boolean[] flags) {
        System.out.println(animalNum);
        GetProduct getProduct = new GetProduct();
        if (animalNum < 6) {
            if (animalNum <= barn.getBarnPart().get(0).getAnimal().size()) {
                UI.showPopup(getProduct.run(barn.getBarnPart().get(0).getAnimal().get(animalNum), choosedItem, player, cowG,
                        animalNum % 3, animalNum / 3, root), root);
            }
        } else if (animalNum < 12) {
            if (animalNum - 6 <= barn.getBarnPart().get(1).getAnimal().size()) {
                UI.showPopup(getProduct.run(barn.getBarnPart().get(1).getAnimal().get(animalNum - 6), choosedItem, player, sheepG,
                        (animalNum - 6) % 3, (animalNum - 6) / 3, root), root);
            }
        } else if (animalNum < 20) {
            if (animalNum - 12 <= barn.getBarnPart().get(2).getAnimal().size()) {
                UI.showPopup(getProduct.run(barn.getBarnPart().get(2).getAnimal().get(animalNum - 12), new AllOfTools().hand(), player, chickenG,
                        (animalNum - 12) % 4, (animalNum - 12) / 4, root), root);
            }
        } else {
            UI.showPopup("There is no animal to heal in this hay!", root);
            UI.initializeFlags();
        }
        flags[3] = true;
    }

    public Animal chosedAnimal(int animalNum, boolean[] flags) {
        if (animalNum < 6) {
            if (animalNum <= barn.getBarnPart().get(0).getAnimal().size()) {
                flags[1] = true;
                return barn.getBarnPart().get(0).getAnimal().get(animalNum);
            }
        } else if (animalNum < 12) {
            if (animalNum - 6 <= barn.getBarnPart().get(1).getAnimal().size()) {
                flags[1] = true;
                return barn.getBarnPart().get(1).getAnimal().get(animalNum - 6);
            }
        } else if (animalNum < 20) {
            if (animalNum - 12 <= barn.getBarnPart().get(2).getAnimal().size()) {
                flags[1] = true;
                return barn.getBarnPart().get(2).getAnimal().get(animalNum - 12);
            }
        } else {
            UI.showPopup("There is no animal in this hay!", root);
            UI.initializeFlags();
        }
        return null;
    }

    public Group getRoot() {
        return root;
    }

    public void setRoot(Group root) {
        this.root = root;
    }

    public Rectangle[][] getCowG() {
        return cowG;
    }

    public void setCowG(Rectangle[][] cowG) {
        this.cowG = cowG;
    }

    public Rectangle[][] getChickenG() {
        return chickenG;
    }

    public void setChickenG(Rectangle[][] chickenG) {
        this.chickenG = chickenG;
    }

    public Rectangle[][] getSheepG() {
        return sheepG;
    }

    public void setSheepG(Rectangle[][] sheepG) {
        this.sheepG = sheepG;
    }

    public Rectangle[][] getMachineG() {
        return machineG;
    }

    public void setMachineG(Rectangle[][] machineG) {
        this.machineG = machineG;
    }

    public void useMachine(Item choosedItem, boolean[] flags, Player player) {
        player.getBackpack().getItem().remove(choosedItem);
        if(choosedItem.getName().contains("Tomato")){
            player.getBackpack().getItem().add(new AllOfDrinks().tomatoJuice());
            UI.showPopup("Tomato Juice was successfully made out of Tomato!", root);
        }else if(choosedItem.getName().contains("Milk")){
            player.getBackpack().getItem().add(new AllOfProducts().cheese());
            UI.showPopup("Cheese was successfully made out of Milk!", root);
        }else if(choosedItem.getName().contains("Wool")){
            player.getBackpack().getItem().add(new AllOfProducts().thread());
            UI.showPopup("Thread was successfully made out of Wool!", root);
        }
        flags[4] = true;
    }
}
// TODO: 6/20/2017 vaziatte extende in daghigh baressi she...
// FIXME: 6/20/2017 in barne ye eshtebahe strategic dare!! taraf az dar ke miad to nabayad safe bere to ye barnpart ke!!
