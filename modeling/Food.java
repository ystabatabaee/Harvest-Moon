import javafx.scene.image.Image;

import java.util.ArrayList;
import java.util.HashMap;

public class Food extends Item {

	private Recipe recipe;
	private ArrayList<Tool> cookingTools;

	public Food() {

	}


	public Recipe getRecipe() {
		return recipe;
	}

	public void setRecipe(Recipe recipe) {
		this.recipe = recipe;
	}

	public ArrayList<Tool> getCookingTools() {
		return cookingTools;
	}

	public void setCookingTools(ArrayList<Tool> cookingTools) {
		this.cookingTools = cookingTools;
	}

	public Food(HashMap<String, Double> featureChangeRate, double capacity, String name, Probability invalid,
				double price, ArrayList<Tool> tools, ArrayList<Task> task, Dissassemblity dissassemblity, Recipe recipe, Image picture) {
		super("Food", featureChangeRate, capacity, name, invalid, price, task, 1, 1,
				true, false, dissassemblity, picture);
		this.recipe = recipe;
		cookingTools = tools;

	}
}